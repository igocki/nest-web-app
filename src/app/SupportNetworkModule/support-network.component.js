var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { AppStatusActions } from '../../actions/app-status-actions';
import { LoadingActions } from '../../actions/loading-actions';
import { NestcareService } from '../../services/nestcareService';
import { SupportNetworkService } from '../../services/supportNetworkService';
import { NgRedux } from 'ng2-redux';
import { StatesConstant } from "../CommonComponentsModule/statesPopover/states.constant";
import { Router } from '@angular/router';
export var SupportNetworkComponent = (function () {
    function SupportNetworkComponent(ngRedux, _nc, _sp, _appStatusActions, router) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._sp = _sp;
        this._appStatusActions = _appStatusActions;
        this.router = router;
        this.display = false;
        this.statesArray = StatesConstant;
        this.assignCopy = function () {
            this.filteredSupportUsersList = Object.assign([], this.supportUsersList);
        };
    }
    SupportNetworkComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        var secure = this.ngRedux.select(function (state) {
            return state.secure;
        });
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var sup_net = this.ngRedux.select(function (state) { return state.supportNetwork; });
        sup_net.subscribe(function (data) {
            if (data) {
                _this.successName = data.get('first_name') + ' ' + data.get('last_name');
                _this.successfulChange = data.get('changed');
                _this.cardChanged = data.get('last4OfChange');
            }
        });
        this.unsubscribeApp = app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
                if (_this.loginStatus !== 'logged-in') {
                    _this.router.navigate(['/loginscreen']);
                }
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        secure.subscribe(function (data) {
            if (data) {
                _this.backupToken = data.get('backupToken');
                _this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        userViewSub.subscribe(function (data) {
            if (data) {
                _this.custID = data.get('nc_id');
            }
        });
        if (this.loginStatus === 'logged-in') {
            if (this.backupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
        this._sp.getPeopleYouSupport(this.custID).subscribe(function (resp) {
            console.log('ya');
            console.log(resp);
            _this.supportUsersList = resp;
            _this.filteredSupportUsersList = resp;
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('errored');
            console.log(error);
        });
    };
    SupportNetworkComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeApp.unsubscribe();
        if (!this.dontClearSupport) {
            this.ngRedux.dispatch({
                type: 'CLEAR_SUPPORT_NETWORK'
            });
        }
    };
    SupportNetworkComponent.prototype.changeBilling = function (user) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.checkIfAuthorized(this.custID).subscribe(function (resp) {
            console.log('ya');
            _this.ngRedux.dispatch({
                type: 'ADD_IG_TOKEN',
                payload: resp.json().token
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.ngRedux.dispatch({
                type: 'STORE_SUPPORT_NETWORK_USER',
                payload: {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    phone: user.mobile
                }
            });
            // call made to get new igocki token and permissions.
            _this.dontClearSupport = true;
            _this.router.navigate(['/payment']);
        }, function (error) {
            console.log('erroed');
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    SupportNetworkComponent.prototype.filterItem = function (value) {
        this.ngRedux.dispatch({
            type: 'CLEAR_SUPPORT_NETWORK'
        });
        if (!value)
            this.assignCopy(); //when nothing has typed
        this.filteredSupportUsersList = Object.assign([], this.supportUsersList).filter(function (item) { return item.email.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.mobile.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.first_name + ' ' + item.last_name).toLowerCase().indexOf(value.toLowerCase()) > -1; });
    };
    SupportNetworkComponent = __decorate([
        Component({
            templateUrl: 'support-network.component.html',
            providers: [AppStatusActions, NestcareService, SupportNetworkService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, SupportNetworkService, AppStatusActions, Router])
    ], SupportNetworkComponent);
    return SupportNetworkComponent;
}());
