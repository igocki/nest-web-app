import { Component, OnInit, EventEmitter } from '@angular/core';
import {Animations} from '../animations';
import { AppStatusActions } from '../../actions/app-status-actions';
import { LoadingActions } from '../../actions/loading-actions';
import { NestcareService } from '../../services/nestcareService';
import { SupportNetworkService } from '../../services/supportNetworkService'
import { RootState } from '../../store/index';
import { NgRedux } from 'ng2-redux';
import {Observable} from "rxjs";
import {StatesConstant} from "../CommonComponentsModule/statesPopover/states.constant";

import { Router } from '@angular/router';
@Component({
    templateUrl: 'support-network.component.html',
    providers: [AppStatusActions, NestcareService, SupportNetworkService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class SupportNetworkComponent implements OnInit {
    public currentState;
    display: boolean = false;
    private supportUsersList;
    public statesArray = StatesConstant;
    private custID;
    private filteredSupportUsersList;
    private loginStatus;
    private backupToken;
    private successfulChange;
    private successName;
    private cardChanged;
    private nc_backupToken;
    public unsubscribeApp;
    private dontClearSupport;
    private assignCopy = function(){
        this.filteredSupportUsersList = Object.assign([], this.supportUsersList);
    };
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _sp: SupportNetworkService,
        private _appStatusActions: AppStatusActions,
        private router: Router) {

    }

    ngOnInit() {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        let secure = this.ngRedux.select(state => {
            return state.secure;
        });
        let app_status = this.ngRedux.select(state => state.appStatus);
        let sup_net = this.ngRedux.select(state => state.supportNetwork);
        sup_net.subscribe(data =>{
           if(data){
               this.successName = data.get('first_name') + ' ' + data.get('last_name');
               this.successfulChange = data.get('changed');
               this.cardChanged = data.get('last4OfChange');
           }
        });
        this.unsubscribeApp = app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                if(this.loginStatus !== 'logged-in'){
                    this.router.navigate(['/loginscreen']);
                }
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });


        secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken')
            }
        });

        userViewSub.subscribe(data => {
            if(data){
                this.custID = data.get('nc_id');
            }
        });
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }

        this._sp.getPeopleYouSupport(this.custID).subscribe((resp) =>{
            console.log('ya');
            console.log(resp);
            this.supportUsersList = resp;
            this.filteredSupportUsersList = resp;
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, error => {
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('errored');
            console.log(error);
        });




    }

    ngOnDestroy(){
        this.unsubscribeApp.unsubscribe();
        if(!this.dontClearSupport){
            this.ngRedux.dispatch({
                type: 'CLEAR_SUPPORT_NETWORK'
            });
        }
    }

    changeBilling(user){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.checkIfAuthorized(this.custID).subscribe((resp) =>{
            console.log('ya');
            this.ngRedux.dispatch({
                type: 'ADD_IG_TOKEN',
                payload: resp.json().token
            });
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });

            this.ngRedux.dispatch({
                type: 'STORE_SUPPORT_NETWORK_USER',
                payload: {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    phone: user.mobile
                }
            });

            // call made to get new igocki token and permissions.
            this.dontClearSupport = true;
            this.router.navigate(['/payment']);
        }, error => {
            console.log('erroed');
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    }

    filterItem(value){
        this.ngRedux.dispatch({
            type: 'CLEAR_SUPPORT_NETWORK'
        });
        if(!value) this.assignCopy(); //when nothing has typed
        this.filteredSupportUsersList = Object.assign([], this.supportUsersList).filter(
            item => item.email.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.mobile.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.first_name + ' ' + item.last_name).toLowerCase().indexOf(value.toLowerCase()) > -1
        )
    }
}
