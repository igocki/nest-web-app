import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { SupportNetworkComponent }    from './support-network.component';
import { SupportNetworkRoutingModule } from './support-network-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module'
import {Animations} from '../animations';
import { SidebarModule } from 'ng-sidebar';
import {TabMenuModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SupportNetworkRoutingModule,
        TabMenuModule,
        DialogModule,
        InputTextModule,
        IonicModule,
        SidebarModule,
        CommonComponentsModule
    ],
    declarations: [
        SupportNetworkComponent
    ],
    providers: [
        Animations
    ]
})
export class SupportNetworkModule {}
