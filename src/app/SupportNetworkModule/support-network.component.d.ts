import { OnInit } from '@angular/core';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { SupportNetworkService } from '../../services/supportNetworkService';
import { RootState } from '../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
export declare class SupportNetworkComponent implements OnInit {
    private ngRedux;
    private _nc;
    private _sp;
    private _appStatusActions;
    private router;
    currentState: any;
    display: boolean;
    private supportUsersList;
    statesArray: {
        "name": string;
        "abbreviation": string;
    }[];
    private custID;
    private filteredSupportUsersList;
    private loginStatus;
    private backupToken;
    private successfulChange;
    private successName;
    private cardChanged;
    private nc_backupToken;
    unsubscribeApp: any;
    private dontClearSupport;
    private assignCopy;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _sp: SupportNetworkService, _appStatusActions: AppStatusActions, router: Router);
    ngOnInit(): void;
    ngOnDestroy(): void;
    changeBilling(user: any): void;
    filterItem(value: any): void;
}
