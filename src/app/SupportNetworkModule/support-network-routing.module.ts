import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { SupportNetworkComponent }  from './support-network.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'support-network',
                component: SupportNetworkComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SupportNetworkRoutingModule { }
