var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { ViewAllOrdersComponent } from './view-all-orders.component';
import { ViewAllOrdersRoutingModule } from './view-all-orders-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import { Animations } from '../animations';
import { InputTextModule } from 'primeng/primeng';
import { Ng2PaginationModule } from 'ng2-pagination';
import { DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
// import { PhonePipe } from '../CommonComponentsModule/phoneNumberPipe';
export var ViewAllOrdersModule = (function () {
    function ViewAllOrdersModule() {
    }
    ViewAllOrdersModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ViewAllOrdersRoutingModule,
                InputTextModule,
                CalendarModule,
                Ng2PaginationModule,
                IonicModule,
                DialogModule,
                CommonComponentsModule
            ],
            declarations: [
                ViewAllOrdersComponent
            ],
            providers: [
                Animations
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ViewAllOrdersModule);
    return ViewAllOrdersModule;
}());
