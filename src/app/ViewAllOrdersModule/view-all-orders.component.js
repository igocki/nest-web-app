var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { NestcareService } from '../../services/nestcareService';
import { PaywhirlService } from '../../services/paywhirlService';
import { TwilioService } from '../../services/twilioService';
import { LoadingActions } from '../../actions/loading-actions';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { NestCareConstants } from '../shared/constants/nestcare.constants';
export var ViewAllOrdersComponent = (function () {
    function ViewAllOrdersComponent(ngRedux, _nc, _pw, _appStatusActions, _twilio, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._pw = _pw;
        this._appStatusActions = _appStatusActions;
        this._twilio = _twilio;
        this.router = router;
        this.display = false;
        this.account_type_hash = NestCareConstants.account_type_hash;
        this.showListItems = false;
        this.customerSubscribedList = {};
        this.assignCopy = function () {
            this.filteredtotalOrders = Object.assign([], this.totalOrders);
        };
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var secure = this.ngRedux.select(function (state) {
            return state.secure;
        });
        this.unsubscribeApp = app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
            }
        });
        if (this.loginStatus !== 'logged-in') {
            console.log('this login ran test 6');
            this.router.navigate(['/loginscreen']);
        }
        secure.subscribe(function (data) {
            if (data) {
                _this.backupToken = data.get('backupToken');
                _this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        if (this.loginStatus === 'logged-in') {
            if (this.backupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
    }
    ViewAllOrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showListItems = false;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var ctrl = this;
        var getCustomerInfo = this._nc.adminGetAllCustomers().flatMap(function (resp1) {
            console.log('first response');
            console.log(resp1);
            _this.allCustomers = resp1.reduce(function (map, obj) {
                map[obj.id] = {
                    first_name: obj.first_name,
                    last_name: obj.last_name,
                    email: obj.email
                };
                return map;
            }, {});
            console.log(_this.allCustomers);
            console.log('ok');
            return ctrl._nc.getCompletedOrders().map(function (resp2) {
                _this.refresh(_this.allCustomers, resp2);
                return Observable.create(function (observer) {
                    observer.next('done');
                });
            }, function (error) {
                console.log('errored out');
                console.log(error);
                ctrl.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
        });
        getCustomerInfo.subscribe(function () {
            console.log('done');
            ctrl.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            if (_this.filteredtotalOrders.length >= 21) {
                _this.showListItems = true;
            }
        }, function (error) {
            console.log('recurly errored out');
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    ViewAllOrdersComponent.prototype.refresh = function (allCustomers, resp2) {
        this.completedFilter = true;
        resp2.forEach(function (itm) {
            if (allCustomers[itm.user_id]) {
                itm.first_name = allCustomers[itm.user_id].first_name,
                    itm.last_name = allCustomers[itm.user_id].last_name,
                    itm.email = allCustomers[itm.user_id].email,
                    itm.full_name = allCustomers[itm.user_id].first_name + ' ' + allCustomers[itm.user_id].last_name;
            }
        });
        this.totalOrders = resp2;
        this.assignCopy();
        this.numberOfItems = this.filteredtotalOrders.length;
        this.filterCompletedTransaction(this.completedFilter);
    };
    ViewAllOrdersComponent.prototype.dateFilter = function (fromDate, toDate) {
        var _this = this;
        console.log('here');
        console.log(fromDate);
        console.log(toDate);
        var fromDateString;
        var toDateString;
        if (fromDate) {
            fromDateString = fromDate.getFullYear() + '-' + ('0' + (fromDate.getMonth() + 1)).slice(-2) + '-' + ('0' + (fromDate.getDate())).slice(-2);
        }
        if (toDate) {
            toDateString = toDate.getFullYear() + '-' + ('0' + (toDate.getMonth() + 1)).slice(-2) + '-' + ('0' + (toDate.getDate())).slice(-2);
        }
        this.showListItems = false;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.getCompletedOrders(fromDateString, toDateString).subscribe(function (dateResp) {
            _this.refresh(_this.allCustomers, dateResp);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            if (_this.filteredtotalOrders.length >= 21) {
                _this.showListItems = true;
            }
        }, function (error) {
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    ViewAllOrdersComponent.prototype.filterItem = function (value) {
        if (!value)
            this.assignCopy(); //when nothing has typed
        this.filteredtotalOrders = Object.assign([], this.totalOrders).filter(function (item) { return (item.id + '').toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.invoiceNumber + '').toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.shippingAddress.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.tracking_number.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.full_name).toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.payment_status + '').toLowerCase().indexOf(value.toLowerCase()) > -1; });
        this.filterCompletedTransaction(this.completedFilter, true);
    };
    ViewAllOrdersComponent.prototype.filterCompletedTransaction = function (completedStatus, noCircle) {
        if (noCircle === void 0) { noCircle = false; }
        // this.search = '';
        if (completedStatus) {
            this.filteredtotalOrders = Object.assign([], this.filteredtotalOrders).filter(function (item) { return item.payment_status === completedStatus; });
        }
        else {
            if (!this.search || this.search === '') {
                this.assignCopy();
            }
            else {
                if (!noCircle) {
                    this.filterItem(this.search);
                }
            }
        }
    };
    ViewAllOrdersComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeApp.unsubscribe();
    };
    ViewAllOrdersComponent = __decorate([
        Component({
            templateUrl: 'view-all-orders.component.html',
            providers: [AppStatusActions, NestcareService, PaywhirlService, TwilioService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, PaywhirlService, AppStatusActions, TwilioService, Router])
    ], ViewAllOrdersComponent);
    return ViewAllOrdersComponent;
}());
