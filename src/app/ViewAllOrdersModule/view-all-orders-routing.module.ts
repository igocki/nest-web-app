import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { ViewAllOrdersComponent }  from './view-all-orders.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'view-orders',
                component: ViewAllOrdersComponent,
                resolve: {

                }
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ViewAllOrdersRoutingModule { }
