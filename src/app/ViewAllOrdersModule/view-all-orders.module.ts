import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { ViewAllOrdersComponent }    from './view-all-orders.component';
import { ViewAllOrdersRoutingModule } from './view-all-orders-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module'
import {Animations} from '../animations';
import {InputTextModule} from 'primeng/primeng';
import {Ng2PaginationModule } from 'ng2-pagination';
import {DialogModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';

// import { PhonePipe } from '../CommonComponentsModule/phoneNumberPipe';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ViewAllOrdersRoutingModule,
        InputTextModule,
        CalendarModule,
        Ng2PaginationModule,
        IonicModule,
        DialogModule,
        CommonComponentsModule
    ],
    declarations: [
        ViewAllOrdersComponent
        // PhonePipe
    ],
    providers: [
        Animations
    ]
})
export class ViewAllOrdersModule {}
