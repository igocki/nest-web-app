import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { NestcareService } from '../../services/nestcareService';
import { PaywhirlService } from '../../services/paywhirlService';
import { TwilioService } from '../../services/twilioService';
import { LoadingActions } from '../../actions/loading-actions';
import { RootState } from '../../store';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import {Observable} from "rxjs";
import { Router } from '@angular/router';
import { NestCareConstants } from '../shared/constants/nestcare.constants';

@Component({
    templateUrl: 'view-all-orders.component.html',
    providers: [AppStatusActions, NestcareService, PaywhirlService, TwilioService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class ViewAllOrdersComponent implements OnInit {
    display: boolean = false;
    randNumberDisplay: string;

    private account_type_hash = NestCareConstants.account_type_hash;
    private filteredtotalOrders;
    private numberOfItems;
    private search;
    private customerVerify;
    private completedFilter;
    private showListItems = false;
    private customerSubscribedList = {};
    private detailUserPhone;
    private allCustomers;
    private totalOrders;
    private loginStatus;
    private backupToken;
    private nc_backupToken;
    public unsubscribeApp;
    private assignCopy = function(){
        this.filteredtotalOrders = Object.assign([], this.totalOrders);
    };
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _pw: PaywhirlService,
        private _appStatusActions: AppStatusActions,
        private _twilio: TwilioService,
        private router: Router) {

        let app_status = this.ngRedux.select(state => state.appStatus);

        let secure = this.ngRedux.select(state => {
            return state.secure;
        });
        this.unsubscribeApp = app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
            }
        });
        if(this.loginStatus !== 'logged-in'){
            console.log('this login ran test 6');
            this.router.navigate(['/loginscreen']);
        }

        secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }

    }

    ngOnInit() {
        this.showListItems = false;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var ctrl = this;

        var getCustomerInfo = this._nc.adminGetAllCustomers().flatMap(resp1 => {
            console.log('first response');
            console.log(resp1);
            this.allCustomers = resp1.reduce(function(map, obj){
                map[obj.id] = {
                    first_name: obj.first_name,
                    last_name: obj.last_name,
                    email: obj.email
                }
                return map
            }, {});

            console.log(this.allCustomers);
            console.log('ok');
            return ctrl._nc.getCompletedOrders().map(resp2 => {
                this.refresh(this.allCustomers, resp2);
                return Observable.create(observer => {
                    observer.next('done');
                })
            }, error => {
                console.log('errored out');
                console.log(error);
                ctrl.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
        });
        getCustomerInfo.subscribe(() =>{
            console.log('done');

            ctrl.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            if(this.filteredtotalOrders.length >= 21){
                this.showListItems = true;
            }
        }, error => {
            console.log('recurly errored out');
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        })
    }


    refresh(allCustomers, resp2){
        this.completedFilter = true;
        resp2.forEach(function(itm){
            if(allCustomers[itm.user_id]){
                itm.first_name = allCustomers[itm.user_id].first_name,
                    itm.last_name = allCustomers[itm.user_id].last_name,
                    itm.email = allCustomers[itm.user_id].email,
                    itm.full_name = allCustomers[itm.user_id].first_name + ' ' + allCustomers[itm.user_id].last_name
            }
        });
        this.totalOrders = resp2;
        this.assignCopy();

        this.numberOfItems = this.filteredtotalOrders.length;
        this.filterCompletedTransaction(this.completedFilter);
    }

    dateFilter(fromDate, toDate){
        console.log('here');
        console.log(fromDate);
        console.log(toDate);
        var fromDateString;
        var toDateString;
        if(fromDate){
            fromDateString = fromDate.getFullYear() + '-' + ('0' + (fromDate.getMonth()+1)).slice(-2) + '-' + ('0' + (fromDate.getDate())).slice(-2);
        }

        if(toDate){
            toDateString = toDate.getFullYear() + '-' + ('0' + (toDate.getMonth()+1)).slice(-2) + '-' + ('0' + (toDate.getDate())).slice(-2)
        }

        this.showListItems = false;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.getCompletedOrders(fromDateString, toDateString).subscribe((dateResp) =>{
            this.refresh(this.allCustomers, dateResp);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            if(this.filteredtotalOrders.length >= 21){
                this.showListItems = true;
            }
        },
        error => {
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    }

    filterItem(value){
        if(!value) this.assignCopy(); //when nothing has typed
        this.filteredtotalOrders = Object.assign([], this.totalOrders).filter(
            item => (item.id + '').toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.invoiceNumber + '').toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.shippingAddress.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.tracking_number.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.full_name).toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.payment_status + '').toLowerCase().indexOf(value.toLowerCase()) > -1
        )
        this.filterCompletedTransaction(this.completedFilter, true);
    }

    filterCompletedTransaction(completedStatus, noCircle = false){

        // this.search = '';
        if(completedStatus){

            this.filteredtotalOrders = Object.assign([], this.filteredtotalOrders).filter(
                item => item.payment_status === completedStatus
            );
        } else {

            if(!this.search || this.search === ''){
                this.assignCopy();
            } else {
                if(!noCircle){
                    this.filterItem(this.search);
                }
            }

        }
    }


    ngOnDestroy(){
        this.unsubscribeApp.unsubscribe();
    }



}
