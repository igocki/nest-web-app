var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { NgRedux } from 'ng2-redux';
import { enhancers } from '../store/index';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NgReduxRouter } from 'ng2-redux-router';
import reducer from '../store/index';
import createLogger from 'redux-logger';
import { middleware } from '../store/index';
import { PaywhirlService } from '../services/paywhirlService';
import { ConfigEnvService } from '../services/env/configEnvService';
var logger = createLogger({
    level: 'info',
    collapsed: true
});
export var MyApp = (function () {
    function MyApp(platform, ngRedux, ngReduxRouter, activatedRoute, router) {
        var _this = this;
        this.platform = platform;
        this.ngRedux = ngRedux;
        this.ngReduxRouter = ngReduxRouter;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this._opened = false;
        this.logoAppear = true;
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var thisRoute = this.ngRedux.select(function (state) { return state.router; });
        thisRoute.subscribe(function (data) {
            if (data) {
                _this.currentState = data;
            }
        });
        app_status.subscribe(function (data) {
            if (data) {
                _this.status = data.get('appStatus');
                _this.loginStatus = data.get('loginStatus');
            }
        });
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
            // this.ngRedux.dispatch({
            //     type: LoadingActions.HIDE_LOADING
            // });
        });
        this.ngRedux.configureStore(reducer, {}, middleware, enhancers);
        ngReduxRouter.initialize();
        this.router.events.subscribe(function (event) {
            if (event instanceof NavigationStart) {
                document.getElementById('main-container').scrollIntoView();
            }
        });
        var ctrl = this;
        this.router.events
            .filter(function (event) { return event instanceof NavigationEnd; })
            .map(function () { return _this.activatedRoute; })
            .subscribe(function (event) {
            var workflowStates = ['/login', '/loginscreen', '/sso-login', 'sso-success', '/signup', '/select-devices-household', '/signupInfo', '/select-plan',
                '/select-devices', '/shipping', '/shipping-details', '/payment', '/order-summary'];
            var loggedInOnlyStates = ['/admin', '/account', '/support-network', '/dashboard'];
            var ctrl = _this;
            if (ctrl.loginStatus === 'logged-in') {
                ctrl.menuAppear = true;
                if (ctrl.currentState === 'login') {
                }
            }
            else {
                ctrl.menuAppear = false;
            }
            if (workflowStates.indexOf(ctrl.currentState) > -1) {
                ctrl.logoAppear = true;
            }
            else {
                ctrl.logoAppear = false;
            }
        });
    }
    MyApp = __decorate([
        Component({
            selector: 'root',
            providers: [PaywhirlService, ConfigEnvService],
            templateUrl: 'app.component.html'
        }), 
        __metadata('design:paramtypes', [Platform, NgRedux, NgReduxRouter, ActivatedRoute, Router])
    ], MyApp);
    return MyApp;
}());
export var InitialComponent = (function () {
    function InitialComponent() {
    }
    InitialComponent = __decorate([
        Component({
            selector: 'initial',
            template: '<div><p>TESTING 343434</p></div>'
        }), 
        __metadata('design:paramtypes', [])
    ], InitialComponent);
    return InitialComponent;
}());
export var NotFoundComponent = (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent = __decorate([
        Component({
            selector: 'not-found',
            template: '<div><p style="font-size: 24px">Oops.... It seems you have requested a page that cannot be found.</p></div>'
        }), 
        __metadata('design:paramtypes', [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());
