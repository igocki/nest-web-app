import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { OrderSummaryComponent }    from './order-summary.component';
import { OrderConfirmComponent } from  './orderConfirm/orderConfirm.component'
import { OrderSummaryRoutingModule } from './order-summary-routing.module';
import {Animations} from '../animations';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import {InputTextModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        InputTextModule,
        OrderSummaryRoutingModule,
        IonicModule,
        CommonComponentsModule
    ],
    declarations: [
        OrderSummaryComponent,
        OrderConfirmComponent
    ],
    providers: [
        Animations
    ]
})
export class OrderSummaryModule {}
