var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../actions/loading-actions';
import { ShippoService } from '../../services/shippoService';
import { PaywhirlService } from '../../services/paywhirlService';
import { FulfillmentService } from '../../services/fulfillmentService';
import { NestcareService } from '../../services/nestcareService';
import { Router } from '@angular/router';
import { DestroySubscribers } from "../destroySubscribers";
export var OrderSummaryComponent = (function () {
    function OrderSummaryComponent(ngRedux, _paywhirl, _fulfillmentService, router) {
        this.ngRedux = ngRedux;
        this._paywhirl = _paywhirl;
        this._fulfillmentService = _fulfillmentService;
        this.router = router;
        this.subscribers = {};
        this.orderSummary = this.ngRedux.select(function (state) { return state.orderSummary; });
        this.selectedPlan = this.ngRedux.select(function (state) { return state.appStatus.getIn(['signupInfo', 'selectPlan']); });
        this.shippingAddress = this.ngRedux.select(function (state) { return state.appStatus.getIn(['signupInfo', 'shippingAddress']); });
        this.shippingRate = this.ngRedux.select(function (state) { return state.shippingRates.get('selectedRate'); });
        this.billingAddress = this.ngRedux.select(function (state) { return state.billing.get('billingAddress'); });
        this.selectedCard = this.ngRedux.select(function (state) { return state.billing.get('selectedCard'); });
    }
    OrderSummaryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscribers.app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        this.subscribers.shippingRates = this.ngRedux.select(function (state) { return state.shippingRates; });
        //let app_status = this.ngRedux.select(state => state.appStatus);
        this.subscribers.secure = this.ngRedux.select(function (state) { return state.secure; });
        //let secure = this.ngRedux.select(state => state.secure);
        this.subscribers.user_view = this.ngRedux.select(function (state) { return state.userView; });
        //let user_view = this.ngRedux.select(state => state.userView);
        this.subscribers.app_status.subscribe(function (data) {
            if (data) {
                _this.deviceLineItems = data.getIn(['signupInfo', 'deviceLineItems']);
                _this.loginStatus = data.get('loginStatus');
                _this.workflow = data.get('workflow');
                _this.isAdmin = data.get('admin');
                console.log('DID THIS');
                console.log('uh huh');
                if (_this.workflow === 'other') {
                    console.log('ya');
                    _this.cusID = data.get('paidForCustomerID');
                    console.log(_this.cusID);
                    console.log(data);
                    console.log(data.get('paidForCustomerID'));
                }
                else {
                    _this.cusID = data.get('payingCustomerID');
                }
            }
        });
        this.subscribers.shippingRates.subscribe(function (data) {
            if (data) {
                _this.shippingObject = {
                    amount: data.getIn(['selectedRate', 'amount']),
                    service: data.getIn(['selectedRate', 'servicelevel_name'])
                };
            }
        });
        this.subscribers.user_view.subscribe(function (data) {
            if (data) {
                if (_this.loginStatus === 'logged-in') {
                    if (!_this.cusID) {
                        _this.cusID = data.get('nc_id');
                    }
                }
            }
        });
        this.subscribers.secure.subscribe(function (data) {
            if (data) {
                _this.backupToken = data.get('backupToken');
                _this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
    };
    OrderSummaryComponent.prototype.redeemPromo = function (promo) {
        console.log('clicked');
        // this._paywhirl.getPromoCodeAmount(this.customerID).subscribe(resp => {
        //     console.log(resp);
        // },
        // error => {
        //     console.log('error');
        // })
        this._paywhirl.applyPromoCode(promo, this.cusID).subscribe(function (resp) {
            console.log(resp);
        }, function (error) {
            console.log('error');
        });
    };
    OrderSummaryComponent.prototype.submitFulfillmentOrder = function (params) {
        var _this = this;
        console.log('device line items');
        console.log(this.deviceLineItems);
        console.log(this.deviceLineItems.size);
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        if (this.deviceLineItems.size > 0) {
            console.log(this.cusID);
            console.log('AHHHHH');
            console.log(this.shippingRate);
            this._fulfillmentService.fulfillOrder(params, true, this.cusID, this.shippingObject).subscribe(function (resp) {
                _this.completeOrderAndNavigate();
            }, function (error) {
                console.log('errored');
                console.log(error);
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
        }
        else {
            this._fulfillmentService.fulfillOrder(params, false, this.cusID, this.shippingObject).subscribe(function (resp) {
                _this.completeOrderAndNavigate();
            }, function (error) {
                console.log('errored');
                console.log(error);
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
        }
    };
    OrderSummaryComponent.prototype.completeOrderAndNavigate = function () {
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        console.log('NEVERE');
        console.log(this.loginStatus);
        if (this.loginStatus === 'logged-in') {
            if (this.backupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
        if (this.loginStatus !== 'logged-in' && this.workflow === 'household') {
            this.ngRedux.dispatch({
                type: 'CLEAR_NC_TOKEN'
            });
            this.ngRedux.dispatch({
                type: 'CLEAR_NC_BACKUP_TOKEN'
            });
        }
        if (!this.isAdmin) {
            this.ngRedux.dispatch({
                type: 'CLEAR_SIGNUP'
            });
        }
        this.ngRedux.dispatch({
            type: 'CLEAR_USER_VIEW'
        });
        this.router.navigate(['/order-confirm']);
    };
    OrderSummaryComponent = __decorate([
        Component({
            templateUrl: 'order-summary.component.html',
            providers: [ShippoService, PaywhirlService, FulfillmentService, NestcareService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }),
        DestroySubscribers(), 
        __metadata('design:paramtypes', [NgRedux, PaywhirlService, FulfillmentService, Router])
    ], OrderSummaryComponent);
    return OrderSummaryComponent;
}());
