import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { Observable } from 'rxjs/Observable';
import { select, NgRedux } from 'ng2-redux';
import { IAppStatus, RootState } from '../../store';
import { Map, List } from 'immutable';
import { LoadingActions } from '../../actions/loading-actions';
import { ShippoService } from '../../services/shippoService';
import { PaywhirlService } from '../../services/paywhirlService';
import { FulfillmentService } from '../../services/fulfillmentService';
import { NestcareService } from '../../services/nestcareService';
import { Router } from '@angular/router';
import {DestroySubscribers} from "../destroySubscribers";
@Component({
    templateUrl: 'order-summary.component.html',
    providers: [ ShippoService, PaywhirlService, FulfillmentService, NestcareService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
@DestroySubscribers()
export class OrderSummaryComponent implements OnInit {
    orderSummary: Observable<Map<string, any>>;
    shippingAddress: Observable<Map<string, any>>;
    shippingRate: Observable<Map<string, any>>;
    billingAddress: Observable<Map<string, any>>;
    selectedCard: Observable<Map<string, any>>;
    selectedPlan: Observable<Map<string, any>>;
    deviceLineItems: any;
    shippingObject: any;
    private cusID;
    private isAdmin: any;
    public subscribers: any = {};
    private loginStatus;
    private workflow;
    private backupToken;
    private nc_backupToken;
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _paywhirl: PaywhirlService,
        private _fulfillmentService: FulfillmentService,
        private router: Router
    ) {
        this.orderSummary = this.ngRedux.select(state => state.orderSummary);
        this.selectedPlan = this.ngRedux.select(state => state.appStatus.getIn(['signupInfo', 'selectPlan']));
        this.shippingAddress = this.ngRedux.select(state => state.appStatus.getIn(['signupInfo', 'shippingAddress']));
        this.shippingRate = this.ngRedux.select(state => state.shippingRates.get('selectedRate'));
        this.billingAddress = this.ngRedux.select(state => state.billing.get('billingAddress'));
        this.selectedCard = this.ngRedux.select(state => state.billing.get('selectedCard'));

    }


    ngOnInit() {
        this.subscribers.app_status = this.ngRedux.select(state => state.appStatus);
        this.subscribers.shippingRates = this.ngRedux.select(state => state.shippingRates);
        //let app_status = this.ngRedux.select(state => state.appStatus);
        this.subscribers.secure = this.ngRedux.select(state => state.secure);
        //let secure = this.ngRedux.select(state => state.secure);
        this.subscribers.user_view = this.ngRedux.select(state => state.userView);
        //let user_view = this.ngRedux.select(state => state.userView);

        this.subscribers.app_status.subscribe(data => {
            if(data){
                this.deviceLineItems = data.getIn(['signupInfo', 'deviceLineItems']);
                this.loginStatus = data.get('loginStatus');
                this.workflow = data.get('workflow');
                this.isAdmin = data.get('admin');
                console.log('DID THIS')
                console.log('uh huh')
                if(this.workflow === 'other'){
                    console.log('ya');
                    this.cusID = data.get('paidForCustomerID');
                    console.log(this.cusID);
                    console.log(data);
                    console.log(data.get('paidForCustomerID'));
                } else {
                    this.cusID = data.get('payingCustomerID');
                }

            }
        });
        this.subscribers.shippingRates.subscribe(data => {
            if(data){
                this.shippingObject = {
                    amount: data.getIn(['selectedRate', 'amount']),
                    service: data.getIn(['selectedRate', 'servicelevel_name'])
                }
            }
        });
        this.subscribers.user_view.subscribe(data => {
            if(data){
                if(this.loginStatus === 'logged-in'){
                    if(!this.cusID){
                        this.cusID = data.get('nc_id');
                    }
                }
            }
        })
        this.subscribers.secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken');
            }
        })
    }

    redeemPromo(promo){
        console.log('clicked');
        // this._paywhirl.getPromoCodeAmount(this.customerID).subscribe(resp => {
        //     console.log(resp);
        // },
        // error => {
        //     console.log('error');
        // })
        this._paywhirl.applyPromoCode(promo, this.cusID).subscribe(resp =>{
                console.log(resp);
            },
            error =>{
                console.log('error');
            });
    }

    submitFulfillmentOrder(params){
        console.log('device line items');
        console.log(this.deviceLineItems);
        console.log(this.deviceLineItems.size);
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        if(this.deviceLineItems.size > 0){
            console.log(this.cusID);
            console.log('AHHHHH');
            console.log(this.shippingRate);

            this._fulfillmentService.fulfillOrder(params, true, this.cusID, this.shippingObject).subscribe((resp) =>{

                this.completeOrderAndNavigate();
            },
            error =>{
                console.log('errored');
                console.log(error);
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            })
        } else {
            this._fulfillmentService.fulfillOrder(params, false, this.cusID, this.shippingObject).subscribe((resp) =>{
                this.completeOrderAndNavigate();
            },
            error =>{
                console.log('errored');
                console.log(error);
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            })
        }

    }

    completeOrderAndNavigate(){
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        console.log('NEVERE');
        console.log(this.loginStatus);
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }

        if(this.loginStatus !== 'logged-in' && this.workflow === 'household'){
            this.ngRedux.dispatch({
                type: 'CLEAR_NC_TOKEN'
            });
            this.ngRedux.dispatch({
                type: 'CLEAR_NC_BACKUP_TOKEN'
            });
        }

        if(!this.isAdmin){
            this.ngRedux.dispatch({
                type: 'CLEAR_SIGNUP'
            });
        }
        this.ngRedux.dispatch({
            type: 'CLEAR_USER_VIEW'
        });
        this.router.navigate(['/order-confirm']);

    }



}
