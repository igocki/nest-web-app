import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { OrderSummaryComponent }  from './order-summary.component';
import { OrderConfirmComponent }  from './orderConfirm/orderConfirm.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'order-summary',
                component: OrderSummaryComponent
            },
            {
                path: 'order-confirm',
                component: OrderConfirmComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class OrderSummaryRoutingModule { }
