import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RootState } from '../../../store';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../../../services/nestcareService';
export declare class OrderConfirmComponent implements OnInit {
    private route;
    private router;
    private ngRedux;
    private _nc;
    private account_type_hash;
    private enrollPlan;
    private isAdmin;
    private adminUserId;
    private workflow;
    private loginStatus;
    private signedUpNewPerson;
    private paidForCustomer;
    constructor(route: ActivatedRoute, router: Router, ngRedux: NgRedux<RootState>, _nc: NestcareService);
    ngOnInit(): void;
    goToAccountAdmin(): void;
}
