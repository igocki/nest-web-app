var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Animations } from '../../animations';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BackButtonComponent } from "../../CommonComponentsModule/backButton/backbutton.component";
import { HttpClient } from '../../../services/http-header-service';
import { NgRedux } from 'ng2-redux';
import { NestCareConstants } from '../../shared/constants/nestcare.constants';
import { LoadingActions } from '../../../actions/loading-actions';
import { ShippoService } from '../../../services/shippoService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { FulfillmentService } from '../../../services/fulfillmentService';
import { NestcareService } from '../../../services/nestcareService';
export var OrderConfirmComponent = (function () {
    function OrderConfirmComponent(route, router, ngRedux, _nc) {
        this.route = route;
        this.router = router;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.account_type_hash = NestCareConstants.account_type_hash;
    }
    OrderConfirmComponent.prototype.ngOnInit = function () {
        var _this = this;
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.enrollPlan = data.get('enrollNewPlan');
                _this.isAdmin = data.get('admin');
                _this.loginStatus = data.get('loginStatus');
                _this.workflow = data.get('workflow');
                _this.signedUpNewPerson = data.get('signupInfo');
                _this.paidForCustomer = data.get('paidForCustomerID');
            }
        });
        var user_view = this.ngRedux.select(function (state) { return state.userView; });
        user_view.subscribe(function (data) {
            if (data) {
                if (data) {
                    _this.adminUserId = data.get('nc_id');
                }
            }
        });
    };
    OrderConfirmComponent.prototype.goToAccountAdmin = function () {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        if (this.isAdmin) {
            var userIdToUse = (this.adminUserId) ? this.adminUserId : this.paidForCustomer;
            console.log('id to use');
            console.log(userIdToUse);
            console.log(this.adminUserId);
            this._nc.adminGetCustomer(userIdToUse).subscribe(function (resp) {
                console.log('asdasdasdLAUGH');
                console.log(resp);
                var userObj = {
                    email: resp.email,
                    first_name: resp.first_name,
                    last_name: resp.last_name,
                    fullname: resp.first_name + ' ' + resp.last_name,
                    userPhone: resp.mobile,
                    userBirth: resp.dob,
                    userGender: resp.gender,
                    nc_id: resp.id,
                    accountType: _this.account_type_hash[resp.account_type_id]
                };
                _this.ngRedux.dispatch({
                    type: 'CLEAR_SIGNUP'
                });
                _this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: userObj
                });
                _this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.address
                });
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                _this.router.navigate(['/account/plan']);
            }, function (error) {
                console.log(error);
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
        }
        else {
            this.router.navigate(['/account/plan']);
        }
    };
    OrderConfirmComponent = __decorate([
        Component({
            entryComponents: [BackButtonComponent],
            templateUrl: 'orderConfirm.component.html',
            providers: [HttpClient, ShippoService, PaywhirlService, FulfillmentService, NestcareService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [ActivatedRoute, Router, NgRedux, NestcareService])
    ], OrderConfirmComponent);
    return OrderConfirmComponent;
}());
