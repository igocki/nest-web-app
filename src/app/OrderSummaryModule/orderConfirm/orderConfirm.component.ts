import {Animations} from '../../animations';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {BackButtonComponent} from "../../CommonComponentsModule/backButton/backbutton.component";
import { HttpClient } from '../../../services/http-header-service';
import { RootState } from '../../../store'
import { NgRedux } from 'ng2-redux';
import { NestCareConstants} from '../../shared/constants/nestcare.constants'
import { LoadingActions } from '../../../actions/loading-actions';
import { ShippoService } from '../../../services/shippoService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { FulfillmentService } from '../../../services/fulfillmentService';
import { NestcareService } from '../../../services/nestcareService';
@Component({
    entryComponents: [BackButtonComponent],
    templateUrl: 'orderConfirm.component.html',
    providers: [ HttpClient, ShippoService, PaywhirlService, FulfillmentService, NestcareService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class OrderConfirmComponent implements OnInit {
    private account_type_hash = NestCareConstants.account_type_hash
    private enrollPlan: any;
    private isAdmin: any;
    private adminUserId;
    private workflow;
    private loginStatus;
    private signedUpNewPerson;
    private paidForCustomer;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService
    ) {

    }


    ngOnInit() {
        let app_status = this.ngRedux.select(state => state.appStatus);
        app_status.subscribe(data => {
            if(data){
                this.enrollPlan = data.get('enrollNewPlan');
                this.isAdmin = data.get('admin');
                this.loginStatus = data.get('loginStatus');
                this.workflow = data.get('workflow');
                this.signedUpNewPerson = data.get('signupInfo');
                this.paidForCustomer = data.get('paidForCustomerID');
            }
        });

        let user_view = this.ngRedux.select(state => state.userView);

        user_view.subscribe(data => {
            if(data){
                if(data){
                    this.adminUserId = data.get('nc_id');
                }
            }
        });
    }

    goToAccountAdmin(){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        if(this.isAdmin){
            var userIdToUse = (this.adminUserId) ? this.adminUserId : this.paidForCustomer;
            console.log('id to use');
            console.log(userIdToUse);
            console.log(this.adminUserId);
            this._nc.adminGetCustomer(userIdToUse).subscribe(resp =>{
                console.log('asdasdasdLAUGH');
                console.log(resp);
                let userObj = {
                    email: resp.email,
                    first_name: resp.first_name,
                    last_name: resp.last_name,
                    fullname: resp.first_name + ' ' + resp.last_name,
                    userPhone: resp.mobile,
                    userBirth: resp.dob,
                    userGender: resp.gender,
                    nc_id: resp.id,
                    accountType: this.account_type_hash[resp.account_type_id]
                };
                this.ngRedux.dispatch({
                    type: 'CLEAR_SIGNUP'
                });
                this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: userObj
                });

                this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.address
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                this.router.navigate(['/account/plan']);
            }, error =>{
                console.log(error);
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });

        } else {
            this.router.navigate(['/account/plan']);
        }

    }


}
