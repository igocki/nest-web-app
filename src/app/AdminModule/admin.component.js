var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { NestcareService } from '../../services/nestcareService';
import { PaywhirlService } from '../../services/paywhirlService';
import { TwilioService } from '../../services/twilioService';
import { LoadingActions } from '../../actions/loading-actions';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { NestCareConstants } from '../shared/constants/nestcare.constants';
export var AdminComponent = (function () {
    function AdminComponent(ngRedux, _nc, _pw, _appStatusActions, _twilio, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._pw = _pw;
        this._appStatusActions = _appStatusActions;
        this._twilio = _twilio;
        this.router = router;
        this.display = false;
        this.account_type_hash = NestCareConstants.account_type_hash;
        this.showListItems = false;
        this.customerSubscribedList = {};
        this.todaysDate = new Date();
        this.assignCopy = function () {
            this.filteredCustomersList = Object.assign([], this.customersList);
        };
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var secure = this.ngRedux.select(function (state) {
            return state.secure;
        });
        this.unsubscribeApp = app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
            }
        });
        if (this.loginStatus !== 'logged-in') {
            this.router.navigate(['/loginscreen']);
        }
        secure.subscribe(function (data) {
            if (data) {
                _this.backupToken = data.get('backupToken');
                _this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        if (this.loginStatus === 'logged-in') {
            if (this.backupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
    }
    AdminComponent.prototype.ngOnInit = function () {
        this.refreshUsers();
    };
    AdminComponent.prototype.refreshUsers = function () {
        var _this = this;
        this.showListItems = false;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var ctrl = this;
        var getCustomerInfo = this._pw.GetAllStatusCustomers().flatMap(function (resp) {
            resp.forEach(function (itm) {
                ctrl.customerSubscribedList[itm.account_code] = true;
            });
            return ctrl._nc.adminGetAllCustomers().map(function (resp) {
                console.log('received');
                console.log(resp);
                if (resp && resp.length > 0) {
                    resp.forEach(function (idx) {
                        if (ctrl.customerSubscribedList[idx.id]) {
                            idx.status = "Subscribed";
                        }
                        else {
                            idx.status = "Non paying";
                        }
                    });
                }
                ctrl.customersList = resp;
                ctrl.customersList.forEach(function (item) {
                    if (item.status_id === 1) {
                        item.verified = true;
                    }
                    else {
                        item.verified = false;
                    }
                });
                ctrl.assignCopy();
                _this.currentViewList = _this.filteredCustomersList.slice(0, 9);
                _this.numberOfItems = _this.filteredCustomersList.length;
                return Observable.create(function (observer) {
                    observer.next('done');
                });
            }, function (error) {
                console.log('errored out');
                console.log(error);
                ctrl.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
        });
        getCustomerInfo.subscribe(function () {
            console.log('done');
            ctrl.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.showListItems = true;
        }, function (error) {
            console.log('recurly errored out: possible auth error');
            console.log(error);
            _this._appStatusActions.logoff();
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    AdminComponent.prototype.sendSMS = function () {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._twilio.sendRandomNumberSMS(this.customerVerify.mobile).subscribe(function (res) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.randNumberDisplay = res.randomNumber;
        }, function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log(error);
        });
    };
    AdminComponent.prototype.saveUserDetails = function () {
        var _this = this;
        this.userDetailsError = undefined;
        this.verifiedFail = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.adminUpdateUserDetails(this.userDetails).subscribe(function (res) {
            _this.updateUsersSuccess = 'User Information has successfully updated';
            _this.display = false;
            _this.refreshUsers();
        }, function (error) {
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.userDetailsError = 'There was an error updating the profile.';
        });
    };
    AdminComponent.prototype.changeVerifiedStatus = function (cust) {
        var _this = this;
        this.userDetailsError = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        cust.verified = !cust.verified;
        this._nc.adminUpdateAccountVerified({
            id: cust.id,
            status_id: (cust.verified) ? '2' : '1'
        }).subscribe(function (res) {
            _this.updateUsersSuccess = 'User Verification Status has successfully updated';
            _this.display = false;
            _this.refreshUsers();
        }, function (error) {
            console.log(error);
            _this.verifiedFail = 'User Verification Status change has failed.';
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.userDetailsError = 'There was an error updating the profile.';
        });
    };
    AdminComponent.prototype.showUserDetailsDialog = function (cust) {
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.userDetails = {
            email: cust.email,
            first_name: cust.first_name,
            last_name: cust.last_name,
            mobile: cust.mobile,
            gender: cust.gender,
            dob: new Date(cust.dob),
            id: cust.id,
            status_id: cust.status_id
        };
        this.verificationModal = false;
        this.userDetailsModal = true;
        this.display = true;
    };
    AdminComponent.prototype.showVerificationDialog = function (cust) {
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.verificationModal = true;
        this.userDetailsModal = false;
        this.detailUserPhone = cust.mobile;
        this.randNumberDisplay = '';
        this.customerVerify = cust;
        this.display = true;
    };
    AdminComponent.prototype.viewUserProfile = function (customer) {
        var _this = this;
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.adminGetCustomer(customer.id).subscribe(function (resp) {
            console.log('asdasdasdLAUGH');
            console.log(resp);
            _this.ngRedux.dispatch({
                type: 'CLEAR_PLAN'
            });
            var userObj = {
                email: customer.email,
                first_name: customer.first_name,
                last_name: customer.last_name,
                fullname: customer.first_name + ' ' + customer.last_name,
                userPhone: customer.mobile,
                userBirth: customer.dob,
                userGender: customer.gender,
                nc_id: customer.id,
                accountType: _this.account_type_hash[resp.account_type_id]
            };
            _this.ngRedux.dispatch({
                type: 'STORE_USER_INFO_VIEW1',
                payload: userObj
            });
            _this.ngRedux.dispatch({
                type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                payload: resp.address
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.router.navigate(['/account']);
        }, function (error) {
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    AdminComponent.prototype.filterItem = function (value) {
        if (!value)
            this.assignCopy(); //when nothing has typed
        this.filteredCustomersList = Object.assign([], this.customersList).filter(function (item) { return item.email.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.mobile.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.status.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.first_name + ' ' + item.last_name).toLowerCase().indexOf(value.toLowerCase()) > -1; });
    };
    AdminComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeApp.unsubscribe();
    };
    AdminComponent = __decorate([
        Component({
            templateUrl: 'admin.component.html',
            providers: [AppStatusActions, NestcareService, PaywhirlService, TwilioService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, PaywhirlService, AppStatusActions, TwilioService, Router])
    ], AdminComponent);
    return AdminComponent;
}());
