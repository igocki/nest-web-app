import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { AdminComponent }    from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module'
import {Animations} from '../animations';
import {InputTextModule} from 'primeng/primeng';
import {Ng2PaginationModule } from 'ng2-pagination';
import {DialogModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
// import { PhonePipe } from '../CommonComponentsModule/phoneNumberPipe';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AdminRoutingModule,
        InputTextModule,
        Ng2PaginationModule,
        IonicModule,
        DialogModule,
        CommonComponentsModule,
        CalendarModule
    ],
    declarations: [
        AdminComponent
        // PhonePipe
    ],
    providers: [
        Animations
    ]
})
export class AdminModule {}
