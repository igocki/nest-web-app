import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { NestcareService } from '../../services/nestcareService';
import { PaywhirlService } from '../../services/paywhirlService';
import { TwilioService } from '../../services/twilioService';
import { LoadingActions } from '../../actions/loading-actions';
import { RootState } from '../../store';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import {Observable} from "rxjs";
import { Router } from '@angular/router';
import { NestCareConstants } from '../shared/constants/nestcare.constants';

@Component({
    templateUrl: 'admin.component.html',
    providers: [AppStatusActions, NestcareService, PaywhirlService, TwilioService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class AdminComponent implements OnInit {
    display: boolean = false;
    randNumberDisplay: string;

    private account_type_hash = NestCareConstants.account_type_hash;
    private customersList;
    private filteredCustomersList;
    private numberOfItems;
    private customerVerify;
    private showListItems = false;
    private customerSubscribedList = {};
    private currentViewList;
    private detailUserPhone;
    private loginStatus;
    private updateUsersSuccess;
    private verificationModal;
    public unsubscribeApp;
    private userDetailsModal;
    private userDetails;
    private backupToken;
    private userDetailsError;
    private verifiedFail;
    private maleRadio;
    private femaleRadio;
    private todaysDate = new Date();
    private nc_backupToken;
    private assignCopy = function(){
        this.filteredCustomersList = Object.assign([], this.customersList);
    };
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _pw: PaywhirlService,
        private _appStatusActions: AppStatusActions,
        private _twilio: TwilioService,
        private router: Router) {

        let app_status = this.ngRedux.select(state => state.appStatus);

        let secure = this.ngRedux.select(state => {
            return state.secure;
        });
        this.unsubscribeApp = app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
            }
        });
        if(this.loginStatus !== 'logged-in'){
            this.router.navigate(['/loginscreen']);
        }

        secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }

    }

    ngOnInit() {
        this.refreshUsers();
    }

    refreshUsers(){
        this.showListItems = false;

        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var ctrl = this;

        var getCustomerInfo = this._pw.GetAllStatusCustomers().flatMap(resp => {
            resp.forEach(function(itm){
                ctrl.customerSubscribedList[itm.account_code] = true;
            });
            return ctrl._nc.adminGetAllCustomers().map(resp => {
                console.log('received');
                console.log(resp);
                if(resp && resp.length > 0){
                    resp.forEach(function(idx){
                        if(ctrl.customerSubscribedList[idx.id]){
                            idx.status = "Subscribed";
                        } else {
                            idx.status = "Non paying";
                        }
                    })
                }
                ctrl.customersList = resp;
                ctrl.customersList.forEach(function(item){
                    if(item.status_id === 1){
                        item.verified = true;
                    } else {
                        item.verified = false;
                    }
                });
                ctrl.assignCopy();
                this.currentViewList = this.filteredCustomersList.slice(0, 9);
                this.numberOfItems = this.filteredCustomersList.length;
                return Observable.create(observer => {
                    observer.next('done');
                })
            }, error => {
                console.log('errored out');
                console.log(error);
                ctrl.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
        });
        getCustomerInfo.subscribe(() =>{
            console.log('done');

            ctrl.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.showListItems = true;
        }, error => {
            console.log('recurly errored out: possible auth error');
            console.log(error);
            this._appStatusActions.logoff();
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        })
    }

    sendSMS(){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this._twilio.sendRandomNumberSMS(this.customerVerify.mobile).subscribe((res) =>{
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.randNumberDisplay = res.randomNumber;
        }, error => {
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log(error);
        });
    }

    saveUserDetails(){
        this.userDetailsError = undefined;
        this.verifiedFail = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.adminUpdateUserDetails(this.userDetails).subscribe((res) =>{
            this.updateUsersSuccess = 'User Information has successfully updated';
            this.display = false;
            this.refreshUsers();
        }, error => {
           console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.userDetailsError = 'There was an error updating the profile.';
        });
    }

    changeVerifiedStatus(cust){
        this.userDetailsError = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        cust.verified = !cust.verified;
        this._nc.adminUpdateAccountVerified(
            {
                id: cust.id,
                status_id: (cust.verified) ? '2' : '1'
            }).subscribe((res) => {
            this.updateUsersSuccess = 'User Verification Status has successfully updated';
            this.display = false;
            this.refreshUsers();
        }, error => {
            console.log(error);
            this.verifiedFail = 'User Verification Status change has failed.'
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.userDetailsError = 'There was an error updating the profile.';
        })
    }

    showUserDetailsDialog(cust) {
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.userDetails = {
            email: cust.email,
            first_name: cust.first_name,
            last_name: cust.last_name,
            mobile: cust.mobile,
            gender: cust.gender,
            dob: new Date(cust.dob),
            id: cust.id,
            status_id: cust.status_id
        };
        this.verificationModal = false;
        this.userDetailsModal = true;
        this.display = true;
    }

    showVerificationDialog(cust) {
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.verificationModal = true;
        this.userDetailsModal = false;
        this.detailUserPhone = cust.mobile;
        this.randNumberDisplay = '';
        this.customerVerify = cust;
        this.display = true;
    }

    viewUserProfile(customer){
        this.updateUsersSuccess = undefined;
        this.verifiedFail = undefined;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.adminGetCustomer(customer.id).subscribe(resp =>{
            console.log('asdasdasdLAUGH');
            console.log(resp);
            this.ngRedux.dispatch({
                type: 'CLEAR_PLAN'
            });
            let userObj = {
                email: customer.email,
                first_name: customer.first_name,
                last_name: customer.last_name,
                fullname: customer.first_name + ' ' + customer.last_name,
                userPhone: customer.mobile,
                userBirth: customer.dob,
                userGender: customer.gender,
                nc_id: customer.id,
                accountType: this.account_type_hash[resp.account_type_id]
            };
            this.ngRedux.dispatch({
                type: 'STORE_USER_INFO_VIEW1',
                payload: userObj
            });

            this.ngRedux.dispatch({
                type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                payload: resp.address
            });
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.router.navigate(['/account']);
        }, error =>{
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });


    }


    filterItem(value){
        if(!value) this.assignCopy(); //when nothing has typed
        this.filteredCustomersList = Object.assign([], this.customersList).filter(
            item => item.email.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.first_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.mobile.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            item.status.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
            (item.first_name + ' ' + item.last_name).toLowerCase().indexOf(value.toLowerCase()) > -1
        )
    }

    ngOnDestroy(){
        this.unsubscribeApp.unsubscribe();
    }


}
