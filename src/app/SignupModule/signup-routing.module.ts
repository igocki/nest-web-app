import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { SignupComponent }  from './signup.component';
import { SignupInfoComponent }  from './SignupInfo/signupInfo.component';
import { PurchaseForOtherComponent } from  './PurchaseForOther/purchaseForOther.component'
import { PurchaseHouseholdComponent } from  './PurchaseHousehold/purchaseHousehold.component'
import { HouseholdFormSignupComponent } from  './householdForm/householdForm.component'
import { ExploreNestcareComponent } from  './ExploreNestcare/exploreNestcare.component'
import { OtherFormSignupComponent } from  './OtherForm/otherForm.component'
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'signup',
                component: SignupComponent
            },
            {
                path: 'signupInfo',
                component: SignupInfoComponent
            },
            {
                path: 'signup-other',
                component: PurchaseForOtherComponent
            },
            {
                path: 'signup-household',
                component: PurchaseHouseholdComponent
            },
            {
                path: 'explore',
                component: ExploreNestcareComponent
            },
            {
                path: 'other-form-signup',
                component: OtherFormSignupComponent
            },
            {
                path: 'household-form-signup',
                component: HouseholdFormSignupComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SignupRoutingModule { }
