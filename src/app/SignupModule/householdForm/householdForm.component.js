var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Animations } from '../../animations';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BackButtonComponent } from "../../CommonComponentsModule/backButton/backbutton.component";
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgRedux } from 'ng2-redux';
import { select } from 'ng2-redux';
export var HouseholdFormSignupComponent = (function () {
    function HouseholdFormSignupComponent(route, ngRedux, router, builder) {
        this.route = route;
        this.ngRedux = ngRedux;
        this.router = router;
        this.builder = builder;
    }
    HouseholdFormSignupComponent.prototype.ngOnInit = function () {
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'household'
        });
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], HouseholdFormSignupComponent.prototype, "signup$", void 0);
    HouseholdFormSignupComponent = __decorate([
        Component({
            entryComponents: [BackButtonComponent],
            templateUrl: 'householdForm.component.html',
            providers: [HttpClient],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [ActivatedRoute, NgRedux, Router, FormBuilder])
    ], HouseholdFormSignupComponent);
    return HouseholdFormSignupComponent;
}());
