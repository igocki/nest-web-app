var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SignupComponent } from './signup.component';
import { SignupInfoComponent } from './SignupInfo/signupInfo.component';
import { PurchaseForOtherComponent } from './PurchaseForOther/purchaseForOther.component';
import { PurchaseHouseholdComponent } from './PurchaseHousehold/purchaseHousehold.component';
import { HouseholdFormSignupComponent } from './householdForm/householdForm.component';
import { ExploreNestcareComponent } from './ExploreNestcare/exploreNestcare.component';
import { OtherFormSignupComponent } from './OtherForm/otherForm.component';
export var SignupRoutingModule = (function () {
    function SignupRoutingModule() {
    }
    SignupRoutingModule = __decorate([
        NgModule({
            imports: [
                RouterModule.forChild([
                    {
                        path: 'signup',
                        component: SignupComponent
                    },
                    {
                        path: 'signupInfo',
                        component: SignupInfoComponent
                    },
                    {
                        path: 'signup-other',
                        component: PurchaseForOtherComponent
                    },
                    {
                        path: 'signup-household',
                        component: PurchaseHouseholdComponent
                    },
                    {
                        path: 'explore',
                        component: ExploreNestcareComponent
                    },
                    {
                        path: 'other-form-signup',
                        component: OtherFormSignupComponent
                    },
                    {
                        path: 'household-form-signup',
                        component: HouseholdFormSignupComponent
                    }
                ])
            ],
            exports: [
                RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], SignupRoutingModule);
    return SignupRoutingModule;
}());
