import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ISignUp } from '../../../store';
import { Observable } from 'rxjs/Observable';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store';
export declare class OtherFormSignupComponent implements OnInit {
    private route;
    private ngRedux;
    private router;
    private builder;
    signup$: Observable<ISignUp>;
    constructor(route: ActivatedRoute, ngRedux: NgRedux<RootState>, router: Router, builder: FormBuilder);
    ngOnInit(): void;
}
