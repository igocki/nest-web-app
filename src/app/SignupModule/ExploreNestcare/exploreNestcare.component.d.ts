import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ISignUp } from '../../../store';
import { Observable } from 'rxjs/Observable';
import { RootState } from '../../../store';
import { NgRedux } from 'ng2-redux';
export declare class ExploreNestcareComponent implements OnInit {
    private route;
    private ngRedux;
    private router;
    private builder;
    signup$: Observable<ISignUp>;
    constructor(route: ActivatedRoute, ngRedux: NgRedux<RootState>, router: Router, builder: FormBuilder);
    ngOnInit(): void;
}
