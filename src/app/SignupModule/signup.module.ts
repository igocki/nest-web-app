import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { SignupComponent }    from './signup.component';
import { SignupInfoComponent } from  './SignupInfo/signupInfo.component'
import { OtherFormSignupComponent } from  './OtherForm/otherForm.component'
import { PurchaseForOtherComponent } from  './PurchaseForOther/purchaseForOther.component'
import { ExploreNestcareComponent } from  './ExploreNestcare/exploreNestcare.component'
import { SignupRoutingModule } from './signup-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import { PurchaseHouseholdComponent } from './PurchaseHousehold/purchaseHousehold.component'
import { HouseholdFormSignupComponent } from './householdForm/householdForm.component'
import {Animations} from '../animations';
import {HttpClient} from '../../services/http-header-service'
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SignupRoutingModule,
        CommonComponentsModule,
        IonicModule
    ],
    declarations: [
        SignupComponent,
        SignupInfoComponent,
        PurchaseForOtherComponent,
        ExploreNestcareComponent,
        OtherFormSignupComponent,
        PurchaseHouseholdComponent,
        HouseholdFormSignupComponent
    ],
    providers: [
        Animations,
        HttpClient
    ]
})
export class SignupModule {}
