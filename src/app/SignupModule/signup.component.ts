import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'signup.component.html',
    providers: [ ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class SignupComponent implements OnInit {
    private loginStatus;
    constructor(private ngRedux: NgRedux<RootState>,
                private router: Router) {

    }


    ngOnInit() {
        let app_status = this.ngRedux.select(state => state.appStatus);
        app_status.subscribe(data => {
            if(data) {
                this.loginStatus = data.get('loginStatus');
            }
        });

        if(this.loginStatus === 'logged-in'){
            this.router.navigate(['/account/plan']);
        }
    }

    signupYourself(){
        this.router.navigate(['/signupInfo']);
    }

    houseHoldPlan(){
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'household'
        });
        this.router.navigate(['/signup-household']);
    }

    signupOther(){
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'other'
        });
        this.router.navigate(['/signup-other']);
    }

    exploreNestcare(){
        this.router.navigate(['/explore']);
    }


}
