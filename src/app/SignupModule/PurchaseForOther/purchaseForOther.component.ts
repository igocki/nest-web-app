import {Animations} from '../../animations';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {BackButtonComponent} from "../../CommonComponentsModule/backButton/backbutton.component";
import { HttpClient } from '../../../services/http-header-service';
import {FormBuilder,  FormGroup, Validators} from '@angular/forms';
import { ISignUp } from '../../../store';
import { Observable } from 'rxjs/Observable';
import { RootState } from '../../../store';
import { select, NgRedux } from 'ng2-redux';



@Component({
    entryComponents: [BackButtonComponent],
    templateUrl: 'purchaseForOther.component.html',
    providers: [HttpClient],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class PurchaseForOtherComponent implements OnInit {
    @select() signup$: Observable<ISignUp>;
    private loginStatus;
    constructor(
        private route: ActivatedRoute,
        private ngRedux: NgRedux<RootState>,
        private router: Router,
        private builder: FormBuilder
    ) {

    }


    ngOnInit() {
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'other'
        });
        let app_status = this.ngRedux.select(state => state.appStatus);
        app_status.subscribe(data => {
            if(data) {
                this.loginStatus = data.get('loginStatus');
            }
        });

        if(this.loginStatus === 'logged-in'){
            this.router.navigate(['/account/plan']);
        }
    }



}
