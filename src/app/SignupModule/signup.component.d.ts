import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store';
import { Router } from '@angular/router';
export declare class SignupComponent implements OnInit {
    private ngRedux;
    private router;
    private loginStatus;
    constructor(ngRedux: NgRedux<RootState>, router: Router);
    ngOnInit(): void;
    signupYourself(): void;
    houseHoldPlan(): void;
    signupOther(): void;
    exploreNestcare(): void;
}
