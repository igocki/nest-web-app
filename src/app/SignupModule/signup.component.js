var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
export var SignupComponent = (function () {
    function SignupComponent(ngRedux, router) {
        this.ngRedux = ngRedux;
        this.router = router;
    }
    SignupComponent.prototype.ngOnInit = function () {
        var _this = this;
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
            }
        });
        if (this.loginStatus === 'logged-in') {
            this.router.navigate(['/account/plan']);
        }
    };
    SignupComponent.prototype.signupYourself = function () {
        this.router.navigate(['/signupInfo']);
    };
    SignupComponent.prototype.houseHoldPlan = function () {
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'household'
        });
        this.router.navigate(['/signup-household']);
    };
    SignupComponent.prototype.signupOther = function () {
        this.ngRedux.dispatch({
            type: 'CHANGE_WORKFLOW',
            payload: 'other'
        });
        this.router.navigate(['/signup-other']);
    };
    SignupComponent.prototype.exploreNestcare = function () {
        this.router.navigate(['/explore']);
    };
    SignupComponent = __decorate([
        Component({
            templateUrl: 'signup.component.html',
            providers: [],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [NgRedux, Router])
    ], SignupComponent);
    return SignupComponent;
}());
