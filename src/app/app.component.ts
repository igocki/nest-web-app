import {Component} from '@angular/core';
import {Platform, Nav} from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import { DevToolsExtension, NgRedux, select } from 'ng2-redux';
import { RootState, enhancers } from '../store/index';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute  } from '@angular/router'
import { NgReduxRouter } from 'ng2-redux-router';
import reducer from '../store/index';
import createLogger from 'redux-logger';

import { reimmutify, middleware } from '../store/index';
import { PaywhirlService } from '../services/paywhirlService';
import { ConfigEnvService } from '../services/env/configEnvService';
import { LoadingActions } from '../actions/loading-actions';
import { RECURLY } from '../app/shared/recurly/recurly';
const logger = createLogger({
    level: 'info',
    collapsed: true
});

@Component({
    selector: 'root',
    providers: [PaywhirlService, ConfigEnvService],
    templateUrl: 'app.component.html'
})
export class MyApp {
    private _opened: boolean = false;
    private currentState: any;
    public menuAppear: boolean;
    public logoAppear = true;
    private status: any;
    private loginStatus: any;

    constructor(private platform:Platform,
                private ngRedux: NgRedux<RootState>,
                private ngReduxRouter: NgReduxRouter,
                private activatedRoute: ActivatedRoute,
                private router: Router) {


        let app_status = this.ngRedux.select(state => state.appStatus);

        let thisRoute = this.ngRedux.select(state => state.router);
        thisRoute.subscribe(data => {
            if(data){
                this.currentState = data;
            }
        });
        app_status.subscribe(data => {
            if(data){
                this.status = data.get('appStatus');
                this.loginStatus = data.get('loginStatus');
            }
        });
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
            // this.ngRedux.dispatch({
            //     type: LoadingActions.HIDE_LOADING
            // });
        });
        this.ngRedux.configureStore(
            reducer,
            {},
            middleware,
            enhancers);
        ngReduxRouter.initialize();
        this.router.events.subscribe(event => {
            if(event instanceof NavigationStart){
                document.getElementById('main-container').scrollIntoView();
            }
        });
        let ctrl = this;
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .subscribe((event) => {
                let workflowStates = ['/login', '/loginscreen', '/sso-login', 'sso-success','/signup','/select-devices-household', '/signupInfo', '/select-plan',
                    '/select-devices','/shipping', '/shipping-details', '/payment', '/order-summary'];
                let loggedInOnlyStates = ['/admin', '/account', '/support-network', '/dashboard'];
                let ctrl = this;
                if(ctrl.loginStatus === 'logged-in'){
                    ctrl.menuAppear = true;
                    if(ctrl.currentState === 'login'){
                        // ctrl.router.navigate(['/account']);
                    }
                } else {
                    ctrl.menuAppear = false;
                    // if(loggedInOnlyStates.indexOf(ctrl.currentState) > -1 ){
                    //     ctrl.router.navigate(['/loginscreen']);
                    // }

                }
                if(workflowStates.indexOf(ctrl.currentState) > -1){
                    ctrl.logoAppear = true;
                } else {
                    ctrl.logoAppear = false;
                }
            });
    }
}


@Component({
    selector: 'initial',
    template: '<div><p>TESTING 343434</p></div>'
})

export class InitialComponent{

}

@Component({
    selector: 'not-found',
    template: '<div><p style="font-size: 24px">Oops.... It seems you have requested a page that cannot be found.</p></div>'
})

export class NotFoundComponent{

}
