import { Component, OnInit, EventEmitter } from '@angular/core';
import {Animations} from '../animations';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { LoadingActions } from '../../actions/loading-actions';
import { RootState } from '../../store';
import { NgRedux } from 'ng2-redux';
import { NestCareConstants } from '../shared/constants/nestcare.constants';
import {NCMeasurementFilter} from "../CommonComponentsModule/NCMeasurementFilter";
import {Observable} from "rxjs";
import { Router } from '@angular/router';
declare var $;
import moment from "moment";

@Component({
    templateUrl: 'dashboard.component.html',
    providers: [AppStatusActions, NestcareService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class DashboardComponent implements OnInit {
    public timeEmitter = function(interval?: number) {
        interval = interval || 1000
        //noinspection TypeScriptUnresolvedFunction
        return Observable.interval(interval)
    };

    public timeEmitterSub: any;

    private finalobject:any = [];
    private allTimeSchedule:any = [];

    public getWellnessNameIconObject() {
        return [
            {
                id: 1,
                name: 'Drink Water',
                icon: 'icon-drink-water',
                bgClass: 'drinkWater',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 10,
                    end: 40
                },
                BgTitle: [
                    'Drink Water',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Drink Water',
                    Note: 'Drink 2 cups of water',
                    subTitleStep1: 'Regularly drinking water does wonders for your body: balancing body fluids, controlling caloric intake, energizing muscles, and even helping your kidney function, overall skin appearance, and digestion.',
                    subTitleStep2: 'How many times should I measure my DRINK WATER activity?',
                    subTitleStep3: 'I want to Drink Water 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Drink Water activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Drink Water activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 2,
                name: 'Track Mood',
                icon: 'icon-track-mood',
                bgClass: 'trackMood',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 41,
                    end: 70
                },
                BgTitle: [
                    'Track Mood',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Track Mood',
                    Note: 'Record your mood',
                    stepTitle1: 'Track Mood',
                    subTitleStep1: 'Your mood is an important indicator of overall wellness management, and can serve as a powerful early indicator of what is working and what is not. For anyone dealing with a diagnosed mood disorder, tracking your mood is critically important.',
                    subTitleStep2: 'How many times should I measure my Track Mood activity?',
                    subTitleStep3: 'I want to Track Mood 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Track Mood activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Track Mood activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 3,
                name: 'Eat Healthy',
                icon: 'icon-eat-healthy',
                bgClass: 'eatFood',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 71,
                    end: 100
                },
                BgTitle: [
                    'Eat Healthy',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Eat Healthy',
                    Note: 'Eat 1 serving of something healthy',
                    stepTitle1: 'Eat Healthy',
                    subTitleStep1: 'Eating healthy food can positively impact your wellness in so many ways: weight, energy level, mood, combat disease, and even improve your longevity!',
                    subTitleStep2: 'How many times should I measure my Eat Healthy activity?',
                    subTitleStep3: 'I want to Eat Healthy 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Eat Healthy activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Eat Healthy activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 4,
                name: 'Sleep Regularly',
                icon: 'icon-sleep-regularly',
                bgClass: 'sleepRegularly',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 101,
                    end: 130
                },
                BgTitle: [
                    'Sleep Regularly',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Sleep Regularly',
                    Note: 'Go to sleep within an hour',
                    stepTitle1: 'Sleep Regularly',
                    subTitleStep1: 'Sleeping is when your body recuperates, and getting enough quality sleep ensures a much healthier life. However, quality sleep is a habit that must be developed - get it going by following a sleep schedule.',
                    subTitleStep2: 'How many times should I measure my Sleep Regularly activity?',
                    subTitleStep3: 'I want to Sleep Regularly 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Sleep Regularly activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Sleep Regularly activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 5,
                name: 'Move More',
                icon: 'icon-move-more',
                bgClass: 'moveMore',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 131,
                    end: 160
                },
                BgTitle: [
                    'Move More',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Move More',
                    Note: 'Get up and move around for a while',
                    stepTitle1: 'Move More',
                    subTitleStep1: 'Our bodies were designed to move, so help your body out while at the same time burning some calories, preventing fat accumulations in your heart, liver, and brain. Regular movement can even help prevent diabetes and heart disease.',
                    subTitleStep2: 'How many times should I measure my Move More activity?',
                    subTitleStep3: 'I want to Move More 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Move More activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Move More activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 6,
                name: 'Track Pain',
                icon: 'icon-track-pain',
                bgClass: 'trackPain',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                BgTitle: [
                    'Track Pain',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                localNotification: {
                    start: 161,
                    end: 190
                },
                targetAlert: {
                    Title: 'Setup Track Pain',
                    Note: 'Record your overall pain (1-10 scale)',
                    stepTitle1: 'Track Pain',
                    subTitleStep1: 'For anyone living with frequent pain, tracking it will be immensely beneficial in designing / revising the best possible treatment regimen (including medications).',
                    subTitleStep2: 'How many times should I measure my Track Pain activity?',
                    subTitleStep3: 'I want to Track Pain 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Track Pain activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Track Pain activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 7,
                name: 'Socialize',
                icon: 'icon-chat-bbl2',
                bgClass: 'socialize',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 191,
                    end: 220
                },
                BgTitle: [
                    'Socialize',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Socialize',
                    Note: 'Talk to a loved one',
                    stepTitle1: 'Socialize',
                    subTitleStep1: 'As much as this app will help you stay connected with your support network, there is no substitue for some old-fashioned socializing, either in person or over the phone. Your overall wellness will thank you for it!',
                    subTitleStep2: 'How many times should I measure my Socialize activity?',
                    subTitleStep3: 'I want to Socialize 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Socialize activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Socialize activity setup. View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            },
            {
                id: 8,
                name: 'Track Digestion',
                icon: 'icon-track-digestion',
                bgClass: 'digestion',
                navigate: {
                    schedule: {},
                    graphPage: {}
                },
                localNotification: {
                    start: 221,
                    end: 250
                },
                BgTitle: [
                    'Track Digestion',
                    'Activity Frequency',
                    'Activity Schedule',
                    'Activity Schedule',
                    'Missed Activity Alert',
                    'Activity Setup Complete'
                ],
                targetAlert: {
                    Title: 'Setup Digestion',
                    Note: 'Record your digestive pattern',
                    stepTitle1: 'Digestion',
                    subTitleStep1: "Digestion (a.k.a bowel movements) is a highly individualized part of any person's life, but there are several conditions that are very indicative of overall wellness. Tracking digestion can provide additional insights into your wellness and medication regimen.",
                    subTitleStep2: 'How many times should I measure my Digestion activity?',
                    subTitleStep3: 'I want to Digestion 3 times a day. I would like my FIRST activity at :',
                    subTitleStep4: 'Here are my 3 DAILY Digestion activity times:',
                    subTitleStep5: 'How many missed measurements before nestCARE alerts me and my support networks?',
                    subTitleStep6: 'Hoory! I have my Digestion activity setup. <br> View or manage it at any time on the Wellness Management Page',
                },
                button_text: "Setup"
            }
        ];
    };

    public getVitalsNameIconObject = function() {
        return [
            {
                id: 1,
                title : 'Pulse',
                headerTitle: 'Setup Pulse',
                icon: 'icon-puls',
                localNotification: {
                    start: 71,
                    end: 100
                },
                numOfSlides: 13,
                className: 'setup-pulse-step',
                stepTitle: [
                    'Pulse',
                    'Active Pulse - TARGET',
                    'Active Pulse - ALERT',
                    'Normal Pulse - TARGET',
                    'Normal Pulse - ALERT',
                    'Normal Pulse - TARGET',
                    'Normal Pulse - ALERT',
                    'Pulse Thresholds',
                    'Pulse Measurements',
                    'Pulse Measurements',
                    'Pulse Measurements',
                    'Missed Pulse Measurements',
                    'Pulse Setup Complete',
                ],
                button_text: "Begin Pulse Setup"
            },
            {
                id: 2,
                title : 'Blood Oxygen',
                headerTitle: 'Setup Blood Oxygen',
                icon: 'icon-oxigen-saturation',
                localNotification: {
                    start: 41,
                    end: 70
                },
                numOfSlides: 9,
                className: 'setup-blood-oxygen-step',
                stepTitle: [
                    'Blood Oxygen',
                    'Blood Oxygen: TARGET',
                    'Blood Oxygen: ALERT',
                    'Blood Oxygen Thresholds',
                    'Blood Oxygen Measurements',
                    'Blood Oxygen Measurements',
                    'Blood Oxygen Measurements',
                    'Missed Blood Oxygen Measurements',
                    'Blood Oxygen Setup Complete',
                ],
                button_text: "Begin Blood Oxygen Setup"
            },
            {
                id: 3,
                title : 'Weight',
                headerTitle: 'Setup Weight',
                icon: 'icon-weight',
                localNotification: {
                    start: 101,
                    end: 130
                },
                numOfSlides: 11,
                className: 'setup-weight-step',
                stepTitle: [
                    'Weight',
                    'Weight: Upper',
                    'Weight: Upper',
                    'Weight: Lower',
                    'Weight: Lower',
                    'Weight Thresholds',
                    'Weight Measurements',
                    'Weight Measurements',
                    'Weight Measurements',
                    'Missed Weight Measurements',
                    'Weight Setup Complete'
                ],
                button_text: "Begin Weight Setup"
            },
            {
                id: 5,
                title: 'Blood Pressure',
                headerTitle: 'Setup Blood Pressure',
                icon: 'icon-blood-pressure',
                localNotification: {
                    start: 10,
                    end: 40
                },
                numOfSlides: 11,
                className: 'setup-blood-pressure-step',
                stepTitle: [
                    'Blood Pressure',
                    'BP:High - TARGET',
                    'BP:High - ALERT',
                    'BP:Low - TARGET',
                    'BP:Low - ALERT',
                    'BP: Vital Thresholds',
                    'BP: Measurements',
                    'BP: Measurements',
                    'BP: Measurements',
                    'Missed BP Measurements',
                    'BP Setup Complete'
                ],
                button_text: "Begin Blood Pressure Setup"
            }
        ];
    };


    public getMedicationShape = function(){
        return [
            {id: 1, name: 'Circle', icon: 'icon-pill',spanObject: [1,2,3]},
            {id: 2, name: 'Half Circle', icon: 'icon-pill2',spanObject: [1,2,3]},
            {id: 3, name: 'Square', icon: 'icon-pill3',spanObject: [1,2,3]},
            {id: 4, name: 'Rectangle', icon: 'icon-pill4',spanObject: [1,2,3]},
            {id: 5, name: 'Oval', icon: 'icon-pill5',spanObject: [1,2,3]},
            {id: 6, name: 'Oblong', icon: 'icon-pill6',spanObject: [1,2,3]},
            {id: 7, name: 'Capsule', icon: 'icon-pill7',spanObject: [1,2,3,4]},
            {id: 8, name: 'Trapezoid', icon: 'icon-pill8',spanObject: [1,2,3]},
            {id: 9, name: 'Figure Eight', icon: 'icon-pill9',spanObject: [1,2,3]},
            {id: 10, name: 'Diamond', icon: 'icon-pill10',spanObject: [1,2,3,4,5]},
            {id: 11, name: 'Triangle', icon: 'icon-pill11',spanObject: [1,2,3]},
            {id: 12, name: 'Pentagon', icon: 'icon-pill12',spanObject: [1,2,3]},
            {id: 13, name: 'Hexagon', icon: 'icon-pill13',spanObject: [1,2,3]},
            {id: 14, name: 'Heptagon', icon: 'icon-pill14',spanObject: [1,2,3]},
            {id: 15, name: 'Octagon', icon: 'icon-pill15',spanObject: [1,2,3]},
        ]
    };

    public getMedicationType = function(){
        return [
            {id: 1, name: 'Pill', icon: 'icon-pill',spanObject: [1,2,3]},
            {id: 2, name: 'Inhaler', icon: 'icon-inhaler',spanObject: [1,2,3]},
            {id: 3, name: 'Syrup/Liquid', icon: 'icon-syrup-liquid',spanObject: [1,2,3,4]},
            {id: 4, name: 'Powder', icon: 'icon-powder',spanObject: [1,2]},
            {id: 5, name: 'Swab', icon: 'icon-swab',spanObject: [1,2,3,4,5]},
            {id: 6, name: 'Spray', icon: 'icon-spray',spanObject: [1,2,3,4,5,6,7,8,9,10,11,12,13,14]},
            {id: 7, name: 'Enema', icon: 'icon-enema',spanObject: [1,2,3,4]},
            {id: 8, name: 'Injection', icon: 'icon-injection',spanObject: [1,2,3,4,5]},
            {id: 9, name: 'Cream/Gel', icon: 'icon-cream-gel',spanObject: [1,2,3,4]},
            {id: 10, name: 'Cream/Gel', icon: 'icon-cream-gel-paste',spanObject: [1,2,3,4]},
            {id: 11, name: 'Foam', icon: 'icon-foam',spanObject: [1,2,3]},
            {id: 12, name: 'Patch', icon: 'icon-patch',spanObject: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]},
            {id: 13, name: 'Stick', icon: 'icon-stick',spanObject: [1,2,3,4,5]},
            {id: 14, name: 'Granules', icon: 'icon-granules',spanObject: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]},
        ]
    };

    public getMedicationColor = function(){
        return [
            {id: 1, name: 'Green', color: '#A9BE87',spanObject: [1,2]},
            {id: 2, name: 'White', color: '#FFFFFF',spanObject: [1,2]},
            {id: 3, name: 'Yellow', color: '#FFDD15',spanObject: [1,2]},
            {id: 4, name: 'Pink', color: '#DF8EBB',spanObject: [1,2]},
            {id: 5, name: 'Peach', color: '#F8A87B',spanObject: [1,2]},
            {id: 6, name: 'Tan', color: '#C3996B',spanObject: [1,2]},
            {id: 7, name: 'Red', color: '#DB5958',spanObject: [1,2]},
            {id: 8, name: 'Orange', color: '#FAAF40',spanObject: [1,2]},
            {id: 9, name: 'Blue', color: '#87C2D7',spanObject: [1,2]},
            {id: 10, name: 'Purple', color: '#7E6493',spanObject: [1,2]},
        ]
    };

    public getDateFromTime(time: string): Date {

        let currentDate = NestCareConstants.convertDate(moment().toDate())
        let timeArray = time.split(":")
        return moment(`${currentDate.Y}-${currentDate.m}-${currentDate.d} ${parseInt(timeArray[0])}:${parseInt(timeArray[1])}:${parseInt(timeArray[2])}`, "YYYY-MM-DD HH:mm:ss").toDate()

    }

    public getMedicationByType = function(typeId ){
        return this.getMedicationType().filter(( o )=> {
            return o.id == typeId;
        })[ 0 ]
    };

    public getMedicationByShape = function(shapeId  ){
        return this.getMedicationShape().filter(( o )=> {
            return o.id == shapeId;
        })[ 0 ]
    };

    public getMedicationByColor = function(colorID   ){
        return this.getMedicationColor().filter(( o )=> {
            return o.id == colorID;
        })[ 0 ]
    };

    public getVitalsById = function(id    ){
        return this.getVitalsNameIconObject().filter(( o )=> {
            return o.id == id
        })[ 0 ]
    };

    public getWellnessById(id) {
        return this.getWellnessNameIconObject().filter((o)=> {
            return o.id == id
        })[0]
    };

    public checkIfDone = function(objectMeasurement: any): boolean {
        return objectMeasurement.is_done == '1'
    };

    public checkIfSkipped = function(objectMeasurement: any): boolean {
        return objectMeasurement.is_done == '1'
    };

    public checkIfMissed = function(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {

        if (this.checkIfSkipped(objectMeasurement)) {
            return true;
        }
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement)
        } else {
            return currentDate['seconds'] > measurementDate['seconds'] && !this.checkIfDone(objectMeasurement)
        }
    };

    public checkIfAllow = function(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] - 1 &&
                currentDate['H'] < measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement)
        } else {
            return currentDate['seconds'] >= measurementDate['seconds'] && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement)
        }
    };

    public checkIfBlock = function(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {
        return !this.checkIfAllow(objectMeasurement, currentDate, measurementDate) && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement)
    };

    public checkMeasurement = function(objectMeasurement: any, checkFor: string, time: Date, type: string): boolean {
        var timeNow = NestCareConstants.convertDate(time)
        var currentDate = NestCareConstants.convertDate(moment().toDate())
        var splitTime = ["0", "0", "0"]
        if(type == 'V') {
            //splitTime = objectMeasurement.measurement_time.split(':')
            splitTime = objectMeasurement.scheduleTime.split(':')
        } else if(type == 'M') {
            splitTime = objectMeasurement.medication_at.split(':')
        } else if(type == 'W') {
            splitTime = objectMeasurement.wellness_at.split(':')
        }
        var newDate = moment(`${timeNow.Y}-${timeNow.m}-${timeNow.d} ${parseInt(splitTime[0])}:${parseInt(splitTime[1])}:${parseInt(splitTime[2])}`, "YYYY-MM-DD HH:mm:ss").toDate()
        var measurementDate = NestCareConstants.convertDate(newDate)

        let returnValue = false
        switch (checkFor) {
            case 'done':
                returnValue = this.checkIfDone(objectMeasurement)
                break

            case 'miss':
                returnValue = this.checkIfMissed(objectMeasurement, currentDate, measurementDate)
                break

            case 'create':
                returnValue = this.checkIfAllow(objectMeasurement, currentDate, measurementDate)
                break

            case 'nocreate':
                returnValue = this.checkIfBlock(objectMeasurement, currentDate, measurementDate)
                break

            case 'skip':
                returnValue = this.checkIfSkipped(objectMeasurement)
                break

            default:
                returnValue = false

        }
        return returnValue
    };
    private loginStatus;
    private backupToken;
    private nc_backupToken;
    public unsubscribeApp;
    private dateTitle:String = 'Today';
    private currentTime:any = moment().format('hh:mm:ss A');
    private currentDate:any = moment();
    private medicationScheduleObjectData:any = [];
    private wellnessScheduleObjectData:any = [];
    private vitalScheduleObjectDataToday:any = [];
    private medicationScheduleObjectData1:any = [];
    private vitalObject:Array<any> = ["", {
        icon: 'icon-puls',
        title: "Pulse"
    }, {
        icon: 'icon-oxigen-saturation',
        title: "Blood Oxygen"
    }, {
        icon: 'icon-weight',
        title: "Weight"
    }, "", {
        icon: 'icon-blood-pressure',
        title: "Blood Pressure"
    }];

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _appStatusActions: AppStatusActions,
        private router: Router) {
        let secure = this.ngRedux.select(state => {
            return state.secure;
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        let app_status = this.ngRedux.select(state => state.appStatus);

        this.unsubscribeApp = app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                if(this.loginStatus !== 'logged-in'){
                    console.log('this login ran test 3');
                    this.router.navigate(['/loginscreen']);
                }
            }
        });

        secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
    }

    ngOnInit() {
        let that = this
        let dayChange = true
        this.timeEmitterSub = this.timeEmitter(10000).subscribe(() => {
            if(this.dateTitle == 'Today') {
                this.currentDate = moment();
                console.log(this.currentDate);
            }
            if (moment().dayOfYear() == moment(this.currentDate).dayOfYear()) {
                this.dateTitle = 'Today';
            } else if (moment().add(1, 'day').dayOfYear() == moment(this.currentDate).dayOfYear()) {
                this.dateTitle = 'Tomorrow';
            } else if (moment().subtract(1, 'day').dayOfYear() == moment(this.currentDate).dayOfYear()) {
                this.dateTitle = 'Yesterday';
            } else {
                this.dateTitle = moment(this.currentDate).format('MM/DD/YYYY');
            }

            this.currentTime = moment().format('hh:mm:ss A');
            this.allTimeSchedule.map((o) => {
                o.items.map((oo) => {
                    let time = moment(moment(this.currentDate).format("YYYY-MM-DD ") + oo.scheduleTime)
                    let now = moment()
                    if ((time.format("YYYYMMDD") == now.format("YYYYMMDD")) && time.isAfter(now) && (oo.comman_type == "create" || oo.comman_type == "nocreate")) {
                        o.items[0]['dueTime'] = moment.utc(time.diff(now)).format('H') == '0' ? moment.utc(time.diff(now)).add(1, 'second').format("[Due in] m [mins] ") : moment.utc(time.diff(now)).format("[Due in] H [hrs] m [mins]");
                        let mid = moment(oo.scheduleTime + ":00:00", "HH:mm:ss");
                        let hours = mid.diff(now, 'hours'); //Get hours 'till end of day
                        oo['reamingHour'] = "another " + hours + " hrs"
                    }
                    else {
                        oo['reamingHour'] = "Today"
                        oo['dueTime'] = ""
                    }
                })

                if (moment().format('mmss') == "0000" && dayChange && this.dateTitle == 'Today') {
                    this.loadTime(moment())
                    this.currentDate = moment();
                    this.dateTitle = 'Today';
                    dayChange = false
                }
            })
            console.log(that.allTimeSchedule);
        });
        this.loadTime(this.currentDate);
    }

    ngOnDestroy(){
        this.unsubscribeApp.unsubscribe();
        this.timeEmitterSub.unsubscribe();
    }

    loadTime(changeDate) {

        let that = this;
        that.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        let currentDate = new Date()
        let dt1 = moment(currentDate).format("YYYY-MM-DD")
        let dt2 = moment(changeDate).format("YYYY-MM-DD")
        let fullDate = moment(changeDate).format('YYYY-MM-DD');
        let dayName = moment(fullDate).format("dddd");
        let dateNumber = moment(fullDate).format("DD");
        let vitaDateNumber = moment(fullDate).format("D");
        let vitaDateNumber1 = moment(currentDate).format("D");
        let wellnessScheduleObjectDataToday = [];
        let medicationScheduleObjectDataToday = [];
        let AllScheduleObjectDataToday = [];
        let setMedicationsActivityToday = [];
        let medicationObjectToday = [];
        let weekObject = {
            "1": "Sunday",
            "2": "Monday",
            "3": "Tuesday",
            "4": "Wednesday",
            "5": "Thursday",
            "6": "Friday",
            "7": "Saturday"
        }
        let setWellnessActivityToday:any = []
        let dateYear = moment(changeDate).format("MMDD")

        let setWellnessActivity:any = [];
        let setMedicationsActivity:any = []
        let promiseA;
        that.wellnessScheduleObjectData = [];
        if (moment(dt1).isSameOrBefore(dt2)) {
            promiseA = that._nc.getWellnessActivities(dt1).map(data =>{
                setWellnessActivity  = data
                return {};
            }).map(nullRes => {
                let object = {user_id: ""};
            }).flatMap(() =>{
                return that._nc.getWellnessSchedules().map(data => {
                    data.filter((dataFilter) => {
                        return dataFilter.actively_tracking == 1;
                    }).forEach(function (data) {
                        let object = data.wellness_schedule;
                        object.forEach(function (o:any) {
                            let scheduleTime = moment(o.wellness_at, "HH:mm:ss").toDate().getTime();
                            let createdAt = moment(moment(data.created_at).subtract(1, 'h')).toDate().getTime();
                            let scheduleTimeNew = moment(o.wellness_at, "HH:mm:ss").toDate().getTime();
                            if (scheduleTimeNew > createdAt) {
                                o['wellness_type_id'] = data.wellness_type_id;
                                o['wellness_id'] = data.id;
                                o['wellness_schedule_id'] = o.id;
                                o['main_type'] = 'wellness';
                                o['scheduleTime'] = o.wellness_at;
                                o['time'] = moment(o.wellness_at, "HH:mm:ss").format("hh:mm A");
                                let wellnessHelper = that.getWellnessById(data.wellness_type_id)
                                o['name'] = wellnessHelper.name
                                o['icon'] = wellnessHelper.icon
                                o['navigate'] = wellnessHelper.navigate
                                o['is_done'] = 0;
                                o['skip'] = 0;
                                if (data.activities_per == 'day') {
                                    that.wellnessScheduleObjectData.push(o);
                                } else if (data.activities_per == 'week' && dayName == weekObject[o.wellness_on]) {
                                    that.wellnessScheduleObjectData.push(o);
                                } else if (data.activities_per == 'month' && dateNumber == o.wellness_on) {
                                    that.wellnessScheduleObjectData.push(o);
                                } else if (data.activities_per == 'year' && dateYear == o.wellness_on) {
                                    that.wellnessScheduleObjectData.push(o);
                                }
                            }
                        })
                    })

                    that.wellnessScheduleObjectData.map(function (data) {
                        var keepGoing = true;
                        setWellnessActivity.forEach(function (o) {
                            if (keepGoing == true && o.wellness_schedule_id) {
                                let splitTime = data.wellness_at.split(":")
                                let scheduleTime = moment().set({
                                    'hours': splitTime[0],
                                    'minutes': splitTime[1],
                                    'seconds': splitTime[2]
                                })
                                let scheduleTimeAddHour = scheduleTime.clone().add(1, 'h')
                                let scheduleTimeSubtractHour = scheduleTime.clone().subtract(1, 'h')
                                let measurementTime = moment(o.wellness_time);
                                if (dt1 == dt2) {
                                    if (measurementTime.isBetween(scheduleTimeSubtractHour, scheduleTimeAddHour) && data.id == o.wellness_schedule_id) {
                                        data['is_done'] = 1;
                                        data['skip'] = o.skip;
                                        data['wellnessActivity'] = o;
                                        o['donotcheck'] = true
                                        keepGoing = false;
                                    } else {
                                        data['is_done'] = 0;
                                    }
                                } else {
                                    o['is_done'] = 0;
                                }

                            }
                        })

                    })
                    if (dt1 == dt2) {
                        setWellnessActivity.forEach((o:any) => {
                            var newObject = $.extend({}, o);
                            if (!o.hasOwnProperty('donotcheck') && o.wellness_schedule_id) {
                                o['is_done'] = 1;
                                o['wellness_at'] = moment(o.wellness_time).format("HH:00:00");
                                o['time'] = moment(o.wellness_time).format("hh:mm A");
                                o['scheduleTime'] = o.wellness_at;
                                o['main_type'] = 'wellness';
                                o['wellnessActivity'] = newObject;
                                let wellnessHelper = that.getWellnessById(o.wellness_type_id);
                                o.id = wellnessHelper.id
                                o.name = wellnessHelper.name
                                o.icon = wellnessHelper.icon
                                o.navigate = wellnessHelper.navigate
                                let dateNew = moment(o.wellness_time).format("YYYY-MM-DD");
                                if (moment(dt1).isSame(dateNew)) {
                                    that.wellnessScheduleObjectData.push(o)
                                }
                            }
                        })
                    }

                    that.wellnessScheduleObjectData.map(function (wellnessSchedule:any) {
                        let types = ["done", "miss", "create", "nocreate"]
                        types.forEach(function (type) {
                            if (NCMeasurementFilter.checkMeasurement(wellnessSchedule, type, moment(fullDate).toDate(), 'W')) {
                                wellnessSchedule.comman_type = type
                            }
                        })
                    })

                    console.log('weellness');
                    console.log(that.wellnessScheduleObjectData);
                });
            })
        }


        that.medicationScheduleObjectData = [];
        let getAllMedicationObject: any = [];

        let promiseB;

        if (moment(dt1).isSameOrBefore(dt2)) {
            promiseB = that._nc.getMedicationActivities(dt1).map(data =>{
                setMedicationsActivity = data
                return {};
            }).map(nullRes => {
                let object = {user_id: ""};
            }).flatMap(() =>{
                return that._nc.medicationSchedules().map(data => {
                    getAllMedicationObject = $.extend([], data);
                    data.forEach(function (data) {

                        let object = data.medication_schedule;
                        object.forEach(function (o) {
                            let scheduleTime = moment(fullDate + " " + o.medication_at).toDate().getTime();
                            let createdAt = moment(moment(data.created_at).subtract(1, 'h')).toDate().getTime();
                            let endAt = moment(data.end_date).format('YYYY-MM-DD');
                            let checkFrequencyDate;
                            if (data.frequency == 'amount_of_time') {
                                checkFrequencyDate = moment(fullDate).isSameOrBefore(endAt);
                            } else {
                                checkFrequencyDate = true
                            }
                            if (scheduleTime > createdAt && checkFrequencyDate) {
                                o['med_shape_id'] = data.med_shape_id;
                                o['med_color_id'] = data.med_color_id;
                                o['med_type_id'] = data.med_type_id;
                                o['mainID'] = data.id;
                                o['taken_for'] = data.taken_for
                                o['name'] = data.name;
                                o['medication_per'] = data.medication_per;
                                o['dosage'] = data.dosage;
                                o['frequency'] = data.frequency
                                o['totalMeasurements'] = object.length
                                o['is_done'] = 0;
                                o['main_type'] = 'meds';
                                o['scheduleTime'] = o.medication_at;
                                o['actively_tracking'] = data.actively_tracking;
                                o['time'] = moment(o.medication_at, "HH:mm:ss").format("hh:mm A");
                                if (o.med_type_id == 1) {
                                    o['medicationIconObject'] = that.getMedicationByShape(o.med_shape_id);
                                    o['medicationColorObject'] = that.getMedicationByColor(o.med_color_id);
                                } else {
                                    o['medicationIconObject'] = that.getMedicationByType(o.med_type_id);
                                    o['medicationColorObject'] = that.getMedicationByColor(2);
                                }
                                if (data.medication_per == 'day') {
                                    that.medicationScheduleObjectData.push(o);
                                } else if (data.medication_per == 'week' && dayName == weekObject[o.medication_on]) {
                                    that.medicationScheduleObjectData.push(o);
                                } else if (data.medication_per == 'month' && dateNumber == o.medication_on) {
                                    that.medicationScheduleObjectData.push(o);
                                } else if (data.medication_per == 'year' && dateYear == o.medication_on) {
                                    that.medicationScheduleObjectData.push(o);
                                }
                            }
                        })
                    });
                    that.medicationScheduleObjectData.map(function (o) {
                        var keepGoing = true;
                        setMedicationsActivity.forEach(function (data) {
                            if (keepGoing && data.medication_schedule_id) {
                                let splitTime = o.medication_at.split(":")
                                let scheduleTime = moment().set({
                                    'hours': splitTime[0],
                                    'minutes': splitTime[1],
                                    'seconds': splitTime[2]
                                })
                                let scheduleTimeAddHour = scheduleTime.clone().add(1, 'h')
                                let scheduleTimeSubtractHour = scheduleTime.clone().subtract(1, 'h')
                                let measurementTime = moment(data.measurement_time);
                                if (dt1 == dt2) {
                                    if (measurementTime.isBetween(scheduleTimeSubtractHour, scheduleTimeAddHour) && data.medication_schedule_id == o.id) {
                                        o['is_done'] = 1;
                                        o['medicationsActivity'] = data;
                                        o['skip'] = data.skip;
                                        data['donotcheck'] = true
                                        keepGoing = false;
                                    } else {
                                        o['is_done'] = 0;
                                    }
                                } else {
                                    o['is_done'] = 0;
                                }

                            }
                        })
                    })
                    if (dt1 == dt2) {
                        setMedicationsActivity.forEach((o:any) => {
                            var newObject = $.extend({}, o);
                            if (!o.hasOwnProperty('donotcheck') && o.medication_schedule_id) {
                                let filterWithId =  getAllMedicationObject.filter( (oo) =>{
                                    return oo.id == o.medication_id
                                } )
                                if(filterWithId.length > 0) {
                                    o['name'] = filterWithId[0].name;
                                    o['med_shape_id'] = filterWithId[0].med_shape_id;
                                    o['med_color_id'] = filterWithId[0].med_color_id;
                                    o['med_type_id'] = filterWithId[0].med_type_id;
                                    o['is_done'] = 1;
                                    o['medication_at'] = moment(o.measurement_time).format("HH:mm:ss");
                                    o['scheduleTime'] = o.medication_at;
                                    o['main_type'] = 'meds';
                                    o['medicationsActivity'] = newObject;
                                    o['time'] = moment(o.medication_at, "HH:mm:ss").format("hh:mm A");
                                    if (o.med_type_id == 1) {
                                        o['medicationIconObject'] = that.getMedicationByShape(o.med_shape_id);
                                        o['medicationColorObject'] = that.getMedicationByColor(o.med_color_id);
                                    } else {
                                        o['medicationIconObject'] = that.getMedicationByType(o.med_type_id);
                                        o['medicationColorObject'] = that.getMedicationByColor(2);
                                    }

                                    let dateNew = moment(o.measurement_time).format("YYYY-MM-DD");
                                    if (moment(dt1).isSame(dateNew)) {
                                        if (o.med_type_id) {
                                            that.medicationScheduleObjectData.push(o)
                                        }
                                    }
                                }
                            }
                        })
                    }

                    that.medicationScheduleObjectData.map(function (medicationSchedule:any) {
                        let types = ["done", "miss", "create", "nocreate"]
                        types.forEach(function (type) {
                            if (that.checkMeasurement(medicationSchedule, type, moment(fullDate).toDate(), 'M')) {
                                medicationSchedule.comman_type = type
                            }
                        })
                        console.log('success 6');
                        return medicationSchedule
                    })

                    that.medicationScheduleObjectData1 = [];
                    that.medicationScheduleObjectData1 = that.medicationScheduleObjectData.filter((data) => {
                        return (data.actively_tracking == 1 || data.is_done == 1);
                    })
                    console.log('success 7');
                    console.log(that.medicationScheduleObjectData1);
                    console.log(that.medicationScheduleObjectData);
                })
            })
        }

        let setVitalActivityToday:any = [];
        that.vitalScheduleObjectDataToday = []
        let promiseC;
        if (moment(dt1).isSameOrBefore(dt2)) {
            promiseC = that._nc.getVitalMeasurements(dt1).map(data => {
                setVitalActivityToday  = data;
                let vitalObjectToday = [];
                console.log('success 1');
                return {};
            }).map(nullRes => {
                let object = {user_id: ""};
                console.log('success 2');
            }).flatMap(() =>{
                return that._nc.getVitalSchedules().map(data => {
                    data.filter((dataFilter) => {
                        return dataFilter.actively_tracking == 1;
                    }).forEach(function (data:any) {
                        let object = data.measurement_schedule;
                        object.forEach(function (o:any) {
                            let scheduleTime = moment(fullDate + " " + o.measurement_time).toDate().getTime();
                            let createdAt = moment(moment(data.created_at).subtract(1, 'h')).toDate().getTime();
                            let splitTime = o.measurement_time.split(":");
                            if (scheduleTime > createdAt) {
                                o['vital_id'] = data.vital_id;
                                o['main_type'] = 'vital';
                                o['schedule'] = that.getVitalsById(data.vital_id)
                                o['time'] = moment(o.measurement_time, "HH:mm:ss").format("hh:mm A");
                                o['scheduleTime'] = o.measurement_time;
                                o['is_done'] = 0;
                                o['name'] = that.vitalObject[data.vital_id].title
                                o['icon'] = that.vitalObject[data.vital_id].icon
                                if (data.measurements_per == 'day') {
                                    that.vitalScheduleObjectDataToday.push(o);
                                } else if (data.measurements_per == 'week' && dayName == weekObject[o.measurement_date]) {
                                    that.vitalScheduleObjectDataToday.push(o);
                                } else if (data.measurements_per == 'month' && vitaDateNumber == o.measurement_date) {
                                    that.vitalScheduleObjectDataToday.push(o);
                                } else if (data.measurements_per == 'year' && dateYear == o.measurement_date) {
                                    that.vitalScheduleObjectDataToday.push(o);
                                }
                            }
                        });

                        that.vitalScheduleObjectDataToday.map(function (o) {
                            var keepGoing = true;
                            setVitalActivityToday.forEach(function (data) {
                                if (keepGoing && data.is_scheduled) {
                                    let splitTime = o.measurement_time.split(":")
                                    let scheduleTime = moment(fullDate).set({
                                        'hours': splitTime[0],
                                        'minutes': splitTime[1],
                                        'seconds': splitTime[2]
                                    })
                                    let scheduleTimeAddHour = scheduleTime.clone().add(1, 'h')
                                    let scheduleTimeSubtractHour = scheduleTime.clone().subtract(1, 'h')
                                    let measurementTime = moment(data.measurement_time);
                                    if (dt1 == dt2) {
                                        if (measurementTime.isBetween(scheduleTimeSubtractHour, scheduleTimeAddHour) && data.is_scheduled == o.id) {
                                            o['is_done'] = 1;
                                            o['skip'] = data.skip;
                                            o['measurement_reading'] = data.measurement_reading;
                                            data['donotcheck'] = true
                                            keepGoing = false;
                                        } else {
                                            o['is_done'] = 0;
                                        }
                                    } else {
                                        o['is_done'] = 0;
                                    }


                                }
                            })
                        })
                    })

                    if (dt1 == dt2) {
                        setVitalActivityToday.forEach((o:any) => {
                            if (!o.hasOwnProperty('donotcheck') && o.is_scheduled) {
                                o['is_done'] = 1;
                                o['scheduleTime'] = moment(o.measurement_time).format("HH:mm:ss");
                                o['time'] = moment(o.measurement_time).format("hh:mm A");
                                o['main_type'] = 'vital';
                                o['name'] = that.vitalObject[o.vital_id].title
                                o['icon'] = that.vitalObject[o.vital_id].icon
                                let dateNew = moment(o.measurement_time).format("YYYY-MM-DD");
                                if (moment(dt1).isSame(dateNew)) {
                                    that.vitalScheduleObjectDataToday.push(o)
                                }
                            }
                        })
                    }


                    that.vitalScheduleObjectDataToday = that.vitalScheduleObjectDataToday.map(function (medicationSchedule:any) {
                        let types = ["done", "miss", "create", "nocreate"]
                        types.forEach(function (type) {
                            if (that.checkMeasurement(medicationSchedule, type, moment(fullDate).toDate(), 'V')) {
                                medicationSchedule.comman_type = type
                            }
                        })
                        return medicationSchedule
                    })

                    console.log(that.vitalScheduleObjectDataToday)
                })
            })
        }


        Observable.forkJoin([
            promiseA,
            promiseB,
            promiseC
        ]).subscribe(responses => {
            that.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            let AllScheduleObjectDataToday = [];
            that.vitalScheduleObjectDataToday.forEach((data) => {
                AllScheduleObjectDataToday.push(data);
            })

            that.wellnessScheduleObjectData.forEach((data) => {
                AllScheduleObjectDataToday.push(data);
            })

            that.medicationScheduleObjectData1.forEach((data) => {
                AllScheduleObjectDataToday.push(data);
            })

            this.finalobject = AllScheduleObjectDataToday.sort((a, b):number => {
                let one = this.getDateFromTime(a.scheduleTime).getTime()
                let two = this.getDateFromTime(b.scheduleTime).getTime()
                return one > two ? 1 : one == two ? 0 : -1
            })

            that.allTimeSchedule = [];
            let fullDate1 = moment(fullDate).format('YYYY-MM-DD')
            let j = -1;
            let checkfornextevent = true
            for (let i = 0; i < 24; i++) {
                //that.allTimeSchedule[i] = []
                let createObject = true
                this.finalobject.map((o:any) => {
                    let splitTime = o.scheduleTime.split(":")
                    if (splitTime[0] == i) {
                        if (createObject) {
                            j++;
                            that.allTimeSchedule[j] = {
                                items: [],
                                parrotGreenBG: false,
                                scoreToTodayReminder: false
                            }
                            createObject = false;
                        }
                        let comapreDate = moment().format("YYYY-MM-DD");
                        let hh = parseInt(moment().format("HH")) - 1;
                        /*if (moment(comapreDate).isSame(fullDate1) && i > hh && checkfornextevent && o.comman_type != 'miss' && o.comman_type != 'done') {
                         o['checkfornextevent'] = "yes"
                         checkfornextevent = false
                         }*/

                        let scheduleTime = moment(fullDate).set({
                            'hours': splitTime[0],
                            'minutes': splitTime[1],
                            'seconds': splitTime[2]
                        })
                        let scheduleTimeAddHour = scheduleTime.clone().add(1, 'h')
                        let scheduleTimeSubtractHour = scheduleTime.clone().subtract(1, 'h').subtract(1,'seconds')
                        let measurementTime = moment(moment().format("HH:00:00"), "HH:mm:ss");
                        if (measurementTime.isBetween(scheduleTimeSubtractHour, scheduleTimeAddHour) && (o.comman_type == "create" || o.comman_type == "nocreate")) {
                            that.allTimeSchedule[j].parrotGreenBG = that.allTimeSchedule[j].parrotGreenBG || true
                        }

                        if (o.comman_type == "nocreate" || o.comman_type == "create") {
                            that.allTimeSchedule[j].scoreToTodayReminder = that.allTimeSchedule[j].scoreToTodayReminder || true
                        }

                        that.allTimeSchedule[j].items.push(o);
                    }
                })
            }
            console.log('ran it');
            console.log(that.finalobject);
            console.log(that.allTimeSchedule);
        });

    }


}
