import { OnInit } from '@angular/core';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { RootState } from '../../store';
import { NgRedux } from 'ng2-redux';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
export declare class DashboardComponent implements OnInit {
    private ngRedux;
    private _nc;
    private _appStatusActions;
    private router;
    timeEmitter: (interval?: number) => Observable<number>;
    timeEmitterSub: any;
    private finalobject;
    private allTimeSchedule;
    getWellnessNameIconObject(): ({
        id: number;
        name: string;
        icon: string;
        bgClass: string;
        navigate: {
            schedule: {};
            graphPage: {};
        };
        localNotification: {
            start: number;
            end: number;
        };
        BgTitle: string[];
        targetAlert: {
            Title: string;
            Note: string;
            subTitleStep1: string;
            subTitleStep2: string;
            subTitleStep3: string;
            subTitleStep4: string;
            subTitleStep5: string;
            subTitleStep6: string;
        };
        button_text: string;
    } | {
        id: number;
        name: string;
        icon: string;
        bgClass: string;
        navigate: {
            schedule: {};
            graphPage: {};
        };
        localNotification: {
            start: number;
            end: number;
        };
        BgTitle: string[];
        targetAlert: {
            Title: string;
            Note: string;
            stepTitle1: string;
            subTitleStep1: string;
            subTitleStep2: string;
            subTitleStep3: string;
            subTitleStep4: string;
            subTitleStep5: string;
            subTitleStep6: string;
        };
        button_text: string;
    })[];
    getVitalsNameIconObject: () => {
        id: number;
        title: string;
        headerTitle: string;
        icon: string;
        localNotification: {
            start: number;
            end: number;
        };
        numOfSlides: number;
        className: string;
        stepTitle: string[];
        button_text: string;
    }[];
    getMedicationShape: () => {
        id: number;
        name: string;
        icon: string;
        spanObject: number[];
    }[];
    getMedicationType: () => {
        id: number;
        name: string;
        icon: string;
        spanObject: number[];
    }[];
    getMedicationColor: () => {
        id: number;
        name: string;
        color: string;
        spanObject: number[];
    }[];
    getDateFromTime(time: string): Date;
    getMedicationByType: (typeId: any) => any;
    getMedicationByShape: (shapeId: any) => any;
    getMedicationByColor: (colorID: any) => any;
    getVitalsById: (id: any) => any;
    getWellnessById(id: any): {
        id: number;
        name: string;
        icon: string;
        bgClass: string;
        navigate: {
            schedule: {};
            graphPage: {};
        };
        localNotification: {
            start: number;
            end: number;
        };
        BgTitle: string[];
        targetAlert: {
            Title: string;
            Note: string;
            subTitleStep1: string;
            subTitleStep2: string;
            subTitleStep3: string;
            subTitleStep4: string;
            subTitleStep5: string;
            subTitleStep6: string;
        };
        button_text: string;
    } | {
        id: number;
        name: string;
        icon: string;
        bgClass: string;
        navigate: {
            schedule: {};
            graphPage: {};
        };
        localNotification: {
            start: number;
            end: number;
        };
        BgTitle: string[];
        targetAlert: {
            Title: string;
            Note: string;
            stepTitle1: string;
            subTitleStep1: string;
            subTitleStep2: string;
            subTitleStep3: string;
            subTitleStep4: string;
            subTitleStep5: string;
            subTitleStep6: string;
        };
        button_text: string;
    };
    checkIfDone: (objectMeasurement: any) => boolean;
    checkIfSkipped: (objectMeasurement: any) => boolean;
    checkIfMissed: (objectMeasurement: any, currentDate: any, measurementDate: any) => boolean;
    checkIfAllow: (objectMeasurement: any, currentDate: any, measurementDate: any) => boolean;
    checkIfBlock: (objectMeasurement: any, currentDate: any, measurementDate: any) => boolean;
    checkMeasurement: (objectMeasurement: any, checkFor: string, time: Date, type: string) => boolean;
    private loginStatus;
    private backupToken;
    private nc_backupToken;
    unsubscribeApp: any;
    private dateTitle;
    private currentTime;
    private currentDate;
    private medicationScheduleObjectData;
    private wellnessScheduleObjectData;
    private vitalScheduleObjectDataToday;
    private medicationScheduleObjectData1;
    private vitalObject;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _appStatusActions: AppStatusActions, router: Router);
    ngOnInit(): void;
    ngOnDestroy(): void;
    loadTime(changeDate: any): void;
}
