import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { DashboardComponent }    from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import {Animations} from '../animations';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module'
import { SidebarModule } from 'ng-sidebar';
import {MomentModule} from 'angular2-moment';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DashboardRoutingModule,
        IonicModule,
        SidebarModule,
        CommonComponentsModule,
        MomentModule
    ],
    declarations: [
        DashboardComponent
    ],
    providers: [
        Animations
    ]
})
export class DashboardModule {}
