var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Animations } from "../animations";
import { Router } from "@angular/router";
import { select, NgRedux } from 'ng2-redux';
import { Map } from 'immutable';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, Validators } from "@angular/forms";
import { NestCareConstants } from "../shared/constants/nestcare.constants";
import { PopoverController } from "ionic-angular";
;
import { PaywhirlService } from '../../services/paywhirlService';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { HttpClient } from '../../services/http-header-service';
import { AppStatusActions } from '../../actions/app-status-actions';
import { LoadingActions } from '../../actions/loading-actions';
import { RECURLY, getPaymentJquery } from '../shared/recurly/recurly';
import { DestroySubscribers } from "../destroySubscribers";
export var PaymentComponent = (function () {
    function PaymentComponent(router, formBuilder, popoverCtrl, _paywhirlService, _nestcare, ngRedux, _registerService) {
        var _this = this;
        this.router = router;
        this.formBuilder = formBuilder;
        this.popoverCtrl = popoverCtrl;
        this._paywhirlService = _paywhirlService;
        this._nestcare = _nestcare;
        this.ngRedux = ngRedux;
        this._registerService = _registerService;
        this.stateSelect = '';
        this.subscribers = {};
        this.subscribers.userInfo = this.ngRedux.select(function (state) { return (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null; });
        // let userInfo = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        this.subscribers.userShipping = this.ngRedux.select(function (state) { return (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null; });
        //let userShipping = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        this.subscribers.supportNetworkUser = this.ngRedux.select(function (state) { return (state.supportNetwork) ? state.supportNetwork : null; });
        //let supportNetworkUser = this.ngRedux.select(state => (state.supportNetwork) ? state.supportNetwork : null);
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        this.subscribers.supportNetworkUser.subscribe(function (data) {
            if (data) {
                _this.supportNetworkCustId = data.get('id');
                _this.supportNetworkCustomer = data;
            }
        });
        app_status.subscribe(function (data) {
            if (data) {
                _this.enrollPlan = data.get('enrollNewPlan');
                console.log('hello');
                _this.loginStatus = data.getIn(['loginStatus']);
                console.log('hello2');
                _this.isAdmin = data.get('admin');
                _this.workflow = data.get('workflow');
                _this.paidForCust = data.get('paidForCustomerID');
                if (data.get('otherSignUpInfo') && data.getIn(['otherSignUpInfo', 'userInfo'])) {
                    _this.otherSignUpInfo = data.getIn(['otherSignUpInfo', 'userInfo']);
                }
                if (_this.loginStatus === 'logged-in') {
                    _this.phone = {};
                }
                else {
                    console.log('hello4');
                    _this.phone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
                }
                console.log('hello3');
            }
            _this.signup = data.get('signupInfo');
            if (!_this.signup) {
                _this.loggedInChangeBilling = true;
            }
        });
        userViewSub.subscribe(function (data) {
            console.log('UserView');
            console.log(data);
            _this.loggedinUser = data;
        });
        console.log('THIS RANRRRR23');
        if (this.loginStatus === 'logged-in' && (!this.isAdmin && this.enrollPlan)) {
            if (this.supportNetworkCustId) {
                console.log('hit this');
                this.userDetails = this.supportNetworkCustomer;
            }
            else {
                this.userDetails = this.loggedinUser;
                this.phone = this.userDetails.get('userPhone');
                console.log('THIS RASD444444');
            }
        }
        else {
            this.subscribers.userInfo.subscribe(function (data) {
                if (data) {
                    if (_this.workflow === 'other') {
                        console.log(_this.otherSignUpInfo);
                        _this.userDetails = _this.otherSignUpInfo;
                    }
                    else if (_this.supportNetworkCustId) {
                        console.log('hit this2');
                        _this.userDetails = _this.supportNetworkCustomer;
                    }
                    else {
                        _this.userDetails = data.get('userInfo');
                        if (!data.get('userInfo')) {
                            console.log('changed to loggedinUser');
                            _this.userDetails = _this.loggedinUser;
                        }
                        console.log(_this.userDetails);
                        console.log('THIS 344444');
                    }
                }
                else {
                    _this.userDetails = _this.supportNetworkCustomer;
                }
            });
        }
        if (this.loginStatus === 'logged-in' && !this.userDetails.get('userInfo') && !this.userDetails.get('nc_id')) {
            this.userDetails = this.loggedinUser;
        }
        console.log('THIS RANRRRR55555');
        if (this.loginStatus === 'logged-in' && this.userDetails && this.userDetails.get('shippingAddress')) {
            this.userShipDetails = Map({
                addressLine1: this.userDetails.getIn(['shippingAddress', 'line1']),
                addressLine2: this.userDetails.getIn(['shippingAddress', 'line2']),
                city: this.userDetails.getIn(['shippingAddress', 'city_id']),
                state: this.userDetails.getIn(['shippingAddress', 'state_id']),
                zipCode: this.userDetails.getIn(['shippingAddress', 'zip_code']),
                name: this.userDetails.get('fullname')
            });
        }
        else {
            this.subscribers.userShipping.subscribe(function (data) {
                if (data) {
                    if (_this.supportNetworkCustId) {
                        _this.noSameAsShippingAddress = true;
                    }
                    else {
                        _this.userShipDetails = data.get('shippingAddress');
                    }
                }
            });
        }
        this.sameAsShip = false;
        this.cards = this._paywhirlService.paymentCards;
        console.log('THIS RANRRRR666666');
        this.selectedCard = this.ngRedux.select(function (state) { return (state.billing) ? state.billing.get('selectedCard') : null; });
        this.errorForm = this.ngRedux.select(function (state) { return (state.billing) ? state.billing.get('error') : null; });
        this.addCreditCardForm = formBuilder.group({
            'name': ['', Validators.required],
            'cardNumber': ['', Validators.required],
            'expirationMonth': ['', Validators.required],
            'expirationYear': ['', Validators.required],
            'cvc': ['', Validators.required],
            'addressLine1': ['', Validators.required],
            'addressLine2': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', Validators.required]
        });
        console.log('THIS RANRRRR277777777');
        var subscription = this.appStatus$.subscribe(function (app) {
            var appMap = Map(app);
            console.log('THIS RANRRRR88888');
            console.log(_this.userDetails);
            if (_this.loginStatus === 'logged-in' && !_this.isAdmin) {
                if (_this.workflow === 'other') {
                    _this.userCustId = appMap.get('paidForCustomerID');
                }
                else if (_this.workflow === 'household') {
                    _this.userCustId = appMap.get('payingCustomerID');
                }
                else if (_this.supportNetworkCustId) {
                    _this.userCustId = _this.supportNetworkCustId;
                }
                else {
                    _this.userCustId = _this.userDetails.get('nc_id');
                }
            }
            else if (_this.loginStatus === 'logged-in' && _this.isAdmin) {
                console.log('ADMIN YAYA');
                console.log(_this.paidForCust);
                _this.userCustId = _this.paidForCust;
                if (!_this.userCustId) {
                    _this.userCustId = _this.userDetails.get('nc_id');
                    console.log(_this.userCustId);
                }
            }
            else {
                if (_this.workflow === 'other') {
                    _this.userCustId = appMap.get('paidForCustomerID');
                }
                else {
                    _this.userCustId = appMap.get('payingCustomerID');
                }
            }
            // this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
        });
        subscription.unsubscribe();
        console.log('THIS RANRRRR89999988');
        this.cards.subscribe(function (listCards) {
            console.log('HHHHHHHHHHHEEEEEE');
            console.log(listCards);
            if (listCards && listCards[0] && listCards.length > 0) {
                _this.firstCC = listCards[0];
            }
            else {
                _this.firstCC = {};
            }
        });
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        console.log(this.userCustId);
        console.log('this ran');
        console.log(this.userDetails);
        this._registerService.checkRecurlyAndCreateAndNoToken(this.userCustId, {
            first_name: this.userDetails.get('first_name'),
            last_name: this.userDetails.get('last_name'),
            email: this.userDetails.get('email'),
            nc_id: this.userCustId
        }).subscribe(function (resp) {
            console.log('and this');
            console.log(resp);
            _this.ngRedux.dispatch({
                type: 'SIGNUP_SUCCESS',
                payload: resp
            });
            _this._registerService.getCardsAfterRecurlyRegister(_this.userCustId).subscribe(function () {
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            }, function (error) {
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log(error);
            });
        });
    }
    PaymentComponent.prototype.ngOnInit = function () {
    };
    PaymentComponent.prototype.raiseError = function (error) {
        console.log('raised error');
        console.log(error);
        if (error.fields) {
            this.errors = "The following fields are invalid: " + error.fields.join(', ');
        }
    };
    PaymentComponent.prototype.sameAsShipping = function () {
        this.sameAsShip = !this.sameAsShip;
        if (this.userDetails.get('first_name') && this.userShipDetails.get('address1')) {
            this.customerShippingValues = {
                first_name: this.userDetails.get('first_name'),
                last_name: this.userDetails.get('last_name'),
                address1: this.userShipDetails.get('addressLine1'),
                address2: this.userShipDetails.get('addressLine2'),
                city: this.userShipDetails.get('city'),
                state: this.userShipDetails.get('state'),
                postal_code: this.userShipDetails.get('zipCode')
            };
        }
        console.log('SAME AS SHIPPING');
        console.log(this.customerShippingValues);
    };
    PaymentComponent.prototype.clearErrors = function () {
        this.errors = undefined;
        this.ngRedux.dispatch({
            type: 'CLEAR_BILLING_ERRORS'
        });
    };
    PaymentComponent.prototype.changeSt = function (stateAbbr) {
        console.log('clicked state');
        console.log(stateAbbr);
        this.stateSelect = stateAbbr;
    };
    PaymentComponent.prototype.saveBilling = function () {
        var _this = this;
        console.log(this.firstCC);
        console.log('WOWOWOWW');
        console.log('update Billing is successful');
        if (this.firstCC) {
            var postData = {
                "addressLine1": this.firstCC.address1,
                "addressLine2": (this.firstCC.address2) ? this.firstCC.address2 : '',
                "city": this.firstCC.city,
                "state": this.firstCC.state,
                "zipCode": this.firstCC.zip,
                "phone": this.phone
            };
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            console.log('USER ID');
            console.log(this.userCustId);
            if (this.isAdmin) {
                //This is actually updating billing even though the service is named updateShipping
                this._nestcare.updateShippingAdmin(postData, this.userCustId, true).subscribe(function (resp) {
                    _this.saveBillingAndNavigate();
                }, function (error) {
                    _this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });
            }
            else if (this.supportNetworkCustId) {
                this._nestcare.updateBillingAdminAsAuthorizedUser(postData, this.userCustId, true, this.userDetails.get('phone')).subscribe(function (resp) {
                    _this.saveBillingAndNavigate();
                }, function (error) {
                    _this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });
            }
            else {
                this._nestcare.updateBilling(postData).subscribe(function (resp) {
                    _this.saveBillingAndNavigate();
                }, function (error) {
                    _this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });
            }
        }
    };
    PaymentComponent.prototype.submitNewPayment = function () {
        this.recurlyGetToken(false);
    };
    PaymentComponent.prototype.loggedInSubmitNewPayment = function () {
        this.recurlyGetToken(true);
    };
    PaymentComponent.prototype.saveBillingAndNavigate = function () {
        var _this = this;
        console.log('update billing is successful');
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        this.ngRedux.dispatch({
            type: "SAVING_BILLING_SUCCESS"
        });
        console.log('THIS IS IT');
        console.log(this.supportNetworkCustId);
        if (this.supportNetworkCustId) {
            this.ngRedux.dispatch({
                type: "SUPPORT_NETWORK_BILLING_SUCCESS",
                payload: {
                    last4: this.firstCC.last_four,
                    changed: true
                }
            });
            this.router.navigate(['/support-network']);
        }
        else {
            this.ngRedux.dispatch({
                type: "ADD_BILLING_ADDRESS",
                payload: {
                    address: this.firstCC.address1 + '_' + this.firstCC.address2,
                    city: this.firstCC.city,
                    state: this.firstCC.state,
                    zip: this.firstCC.zip
                }
            });
            if (!this.signup && !this.enrollPlan) {
                this.router.navigate(['/account/bill']);
            }
            else if (!this.signup && this.enrollPlan) {
                var userViewSub = this.ngRedux.select(function (state) {
                    return state.userView;
                });
                userViewSub.subscribe(function (data) {
                    _this.ngRedux.dispatch({
                        type: 'ADD_CUSTOMER_ID',
                        payload: data.get('nc_id')
                    });
                });
                this.router.navigate(['/order-summary']);
            }
            else {
                this.router.navigate(['/order-summary']);
            }
        }
    };
    PaymentComponent.prototype.recurlyGetToken = function (loggedin, ctrl) {
        if (ctrl === void 0) { ctrl = undefined; }
        var that = this;
        var secForm = getPaymentJquery();
        if (ctrl) {
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            var params = {
                id: that.userCustId
            };
            console.log('this ran');
            that = ctrl;
        }
        console.log(secForm);
        console.log("AAAAAAA");
        RECURLY.token(secForm, function (err, token) {
            if (err) {
                console.log(err);
                console.log('ERRORRED ANDREW');
                if (err && err.name === 'api-error' && err.message === 'There was a problem parsing the API response.') {
                    setTimeout(function () {
                        that.recurlyGetToken(loggedin, that);
                    }, 1500);
                }
                else {
                    that.raiseError(err);
                }
                that.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log('sadasda1s');
            }
            else {
                console.log('sadasdas');
                that.clearErrors();
                that.addPayment(token, that.userShipDetails, that.userCustId);
                if (loggedin) {
                    that._paywhirlService.loadPaymentCardsWithoutSub(that.userCustId).subscribe(function (resp) {
                        console.log('GOT PAYMENT CARDS');
                        console.log(resp);
                        var pmtCards = resp;
                        that.ngRedux.dispatch({
                            type: 'GET_CARDS',
                            payload: pmtCards
                        });
                        that.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                        that.saveBilling();
                    }, function (error) {
                        that.ngRedux.dispatch({
                            type: 'FETCH_CARDS_FAILURE',
                            payload: { err: error }
                        });
                        that.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    });
                }
            }
        });
    };
    PaymentComponent.prototype.ngAfterViewInit = function () {
        this.configureRecurly();
    };
    PaymentComponent.prototype.configureRecurly = function () {
        setTimeout(function () {
            RECURLY.configure({
                publicKey: NestCareConstants.recurlyPublicKey,
                style: {
                    all: {
                        fontFamily: 'Open Sans',
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        fontColor: '#2c0730'
                    },
                    number: {
                        placeholder: 'card number'
                    },
                    month: {
                        placeholder: 'exp month (mm)'
                    },
                    year: {
                        placeholder: 'exp year (yy or yyyy)'
                    },
                    cvv: {
                        placeholder: 'cvv'
                    },
                    first_name: {
                        placeholder: 'First Name'
                    },
                    last_name: {
                        placeholder: 'Last Name'
                    }
                }
            });
            console.log('configured');
        }, 10);
        setTimeout(function () {
            RECURLY.configure({
                publicKey: NestCareConstants.recurlyPublicKey,
                style: {
                    all: {
                        fontFamily: 'Open Sans',
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        fontColor: '#2c0730'
                    },
                    number: {
                        placeholder: 'card number'
                    },
                    month: {
                        placeholder: 'exp month (mm)'
                    },
                    year: {
                        placeholder: 'exp year (yy or yyyy)'
                    },
                    cvv: {
                        placeholder: 'cvv'
                    },
                    first_name: {
                        placeholder: 'First Name'
                    },
                    last_name: {
                        placeholder: 'Last Name'
                    }
                }
            });
            console.log('reconfigured');
        }, 3000);
    };
    PaymentComponent.prototype.addPayment = function (token, userDetails, custId) {
        console.log('THIS HAPPENED');
        console.log(token);
        console.log(userDetails);
        var params = {
            token_id: token.id
        };
        this._paywhirlService.replaceCard(params, custId);
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], PaymentComponent.prototype, "appStatus$", void 0);
    PaymentComponent = __decorate([
        Component({
            templateUrl: 'payment.component.html',
            providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions, LoadingActions, RegisterService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }),
        DestroySubscribers(), 
        __metadata('design:paramtypes', [Router, FormBuilder, PopoverController, PaywhirlService, NestcareService, NgRedux, RegisterService])
    ], PaymentComponent);
    return PaymentComponent;
}());
