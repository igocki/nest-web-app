import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {IonicModule} from "ionic-angular";
import {PaymentComponent} from "./payment.component";
import {PaymentRoutingModule} from "./payment-routing.module";
import {Animations} from "../animations";
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PaymentRoutingModule,
        IonicModule,
        CommonComponentsModule
    ],
    declarations: [
        PaymentComponent
    ],
    providers: [
        Animations
    ]
})
export class PaymentModule {}
