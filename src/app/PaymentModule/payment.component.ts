import {Component, OnInit} from "@angular/core";
import {Animations} from "../animations";
import {Router} from "@angular/router";
import { select, NgRedux } from 'ng2-redux';
import { IAppStatus, RootState } from '../../store'
import { Map, List } from 'immutable';
import { Observable } from 'rxjs/Observable';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {NestCareConstants } from "../shared/constants/nestcare.constants"
import {PopoverController} from "ionic-angular";;
import { PaywhirlService } from '../../services/paywhirlService';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { HttpClient } from '../../services/http-header-service'
import { AppStatusActions } from '../../actions/app-status-actions';
import { LoadingActions } from '../../actions/loading-actions';
import { RECURLY, getPaymentJquery } from '../shared/recurly/recurly';
import {DestroySubscribers} from "../destroySubscribers";
@Component({
    templateUrl: 'payment.component.html',
    providers: [ NestcareService, PaywhirlService,  HttpClient, AppStatusActions, LoadingActions, RegisterService],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
@DestroySubscribers()
export class PaymentComponent implements OnInit {
    @select() appStatus$: Observable<IAppStatus>;
    addCreditCardForm: FormGroup;
    userCustId: any;
    public isAdmin;
    public signup;
    enrollPlan: any;
    loggedinUser: any;
    changePlan: any;
    loginStatus: any;
    cards: Observable<any>;
    firstCC: any;
    public loggedInChangeBilling;
    selectedCard: Observable<Map<string, any>>;
    errorForm: Observable<any>;
    sameAsShip: boolean;
    userDetails: any;
    userShipDetails: any;
    public workflow;
    stateSelect = '';
    errors: any;
    customerShippingValues: any;
    phone: any
    private supportNetworkCustId;
    private supportNetworkCustomer;
    private paidForCust;
    private otherSignUpInfo;
    private noSameAsShippingAddress;
    public subscribers: any = {};

    constructor(
        public router: Router,
        private formBuilder: FormBuilder,
        private popoverCtrl: PopoverController,
        private _paywhirlService: PaywhirlService,
        private _nestcare: NestcareService,
        private ngRedux: NgRedux<RootState>,
        private _registerService: RegisterService
    ) {
        this.subscribers.userInfo = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        // let userInfo = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        this.subscribers.userShipping = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        //let userShipping = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo']) : null);
        this.subscribers.supportNetworkUser = this.ngRedux.select(state => (state.supportNetwork) ? state.supportNetwork : null);
        //let supportNetworkUser = this.ngRedux.select(state => (state.supportNetwork) ? state.supportNetwork : null);

        let app_status = this.ngRedux.select(state => state.appStatus);
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        this.subscribers.supportNetworkUser.subscribe(data => {
            if(data){
                this.supportNetworkCustId = data.get('id');
                this.supportNetworkCustomer = data;
            }
        });

        app_status.subscribe(data => {
            if(data){
                this.enrollPlan = data.get('enrollNewPlan');
                console.log('hello');
                this.loginStatus = data.getIn(['loginStatus']);
                console.log('hello2');
                this.isAdmin = data.get('admin');
                this.workflow = data.get('workflow');
                this.paidForCust = data.get('paidForCustomerID');
                if(data.get('otherSignUpInfo') && data.getIn(['otherSignUpInfo', 'userInfo'])){
                    this.otherSignUpInfo = data.getIn(['otherSignUpInfo', 'userInfo']);
                }
                if(this.loginStatus === 'logged-in'){
                    this.phone = {};
                } else {
                    console.log('hello4');
                    this.phone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
                }
                console.log('hello3');
            }
            this.signup = data.get('signupInfo');
            if(!this.signup){
                this.loggedInChangeBilling = true
            }
        });
        userViewSub.subscribe(data => {
            console.log('UserView');
            console.log(data);
            this.loggedinUser = data;
        });
        console.log('THIS RANRRRR23');

        if(this.loginStatus === 'logged-in' && (!this.isAdmin && this.enrollPlan)){
            if(this.supportNetworkCustId){
                console.log('hit this');
                this.userDetails = this.supportNetworkCustomer;
            } else {
                this.userDetails = this.loggedinUser;
                this.phone = this.userDetails.get('userPhone');
                console.log('THIS RASD444444')
            }

        } else {
            this.subscribers.userInfo.subscribe(data => {
                if(data){
                    if(this.workflow === 'other'){
                        console.log(this.otherSignUpInfo);
                        this.userDetails = this.otherSignUpInfo
                    } else if(this.supportNetworkCustId){
                        console.log('hit this2');
                        this.userDetails = this.supportNetworkCustomer;
                    } else {
                        this.userDetails = data.get('userInfo');
                        if(!data.get('userInfo')){
                            console.log('changed to loggedinUser');
                            this.userDetails = this.loggedinUser;
                        }
                        console.log(this.userDetails);
                        console.log('THIS 344444')
                    }
                } else {
                    this.userDetails = this.supportNetworkCustomer;
                }
            });
        }

        if(this.loginStatus === 'logged-in' && !this.userDetails.get('userInfo') && !this.userDetails.get('nc_id')){
            this.userDetails = this.loggedinUser;
        }

        console.log('THIS RANRRRR55555');
        if(this.loginStatus === 'logged-in' && this.userDetails && this.userDetails.get('shippingAddress')) {
            this.userShipDetails = Map<string, any>({
                addressLine1: this.userDetails.getIn(['shippingAddress', 'line1']),
                addressLine2: this.userDetails.getIn(['shippingAddress', 'line2']),
                city: this.userDetails.getIn(['shippingAddress', 'city_id']),
                state: this.userDetails.getIn(['shippingAddress', 'state_id']),
                zipCode: this.userDetails.getIn(['shippingAddress', 'zip_code']),
                name: this.userDetails.get('fullname')
            });
        } else {
            this.subscribers.userShipping.subscribe(data => {
                if(data) {
                    if(this.supportNetworkCustId){
                        this.noSameAsShippingAddress = true;
                    } else {
                        this.userShipDetails = data.get('shippingAddress');
                    }
                }
            });
        }

        this.sameAsShip = false;
        this.cards = this._paywhirlService.paymentCards;
        console.log('THIS RANRRRR666666');
        this.selectedCard = this.ngRedux.select(state => (state.billing) ? state.billing.get('selectedCard') : null);
        this.errorForm = this.ngRedux.select(state => (state.billing) ? state.billing.get('error') : null);
        this.addCreditCardForm = formBuilder.group({
            'name': ['', Validators.required],
            'cardNumber': ['', Validators.required],
            'expirationMonth': ['', Validators.required],
            'expirationYear': ['', Validators.required],
            'cvc': ['', Validators.required],
            'addressLine1': ['', Validators.required],
            'addressLine2': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', Validators.required]
        });
        console.log('THIS RANRRRR277777777');
        let subscription = this.appStatus$.subscribe((app) => {
            let appMap = Map<string, IAppStatus>(app);
            console.log('THIS RANRRRR88888');
            console.log(this.userDetails);
            if(this.loginStatus === 'logged-in' && !this.isAdmin){
                if(this.workflow === 'other'){
                    this.userCustId = appMap.get('paidForCustomerID');
                } else if(this.workflow === 'household'){
                    this.userCustId = appMap.get('payingCustomerID');
                } else if(this.supportNetworkCustId){
                    this.userCustId = this.supportNetworkCustId;
                } else {
                    this.userCustId = this.userDetails.get('nc_id');
                }
            } else if(this.loginStatus === 'logged-in' && this.isAdmin){
                console.log('ADMIN YAYA');
                console.log(this.paidForCust);
                this.userCustId = this.paidForCust;
                if(!this.userCustId){
                    this.userCustId = this.userDetails.get('nc_id');
                    console.log(this.userCustId);
                }
            } else {
                if(this.workflow === 'other'){
                    this.userCustId = appMap.get('paidForCustomerID');
                } else {
                    this.userCustId = appMap.get('payingCustomerID');
                }
            }

            // this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
        });
        subscription.unsubscribe();
        console.log('THIS RANRRRR89999988');
        this.cards.subscribe((listCards) =>{
            console.log('HHHHHHHHHHHEEEEEE');
            console.log(listCards);
            if(listCards && listCards[0] && listCards.length > 0){
                this.firstCC = listCards[0];
            } else {
                this.firstCC = {};
            }
        });

        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        console.log(this.userCustId);
        console.log('this ran');
        console.log(this.userDetails);
        this._registerService.checkRecurlyAndCreateAndNoToken(this.userCustId, {
            first_name: this.userDetails.get('first_name'),
            last_name: this.userDetails.get('last_name'),
            email: this.userDetails.get('email'),
            nc_id: this.userCustId
        }).subscribe(resp =>  {
            console.log('and this');
            console.log(resp);
            this.ngRedux.dispatch({
                type: 'SIGNUP_SUCCESS',
                payload: resp
            });
            this._registerService.getCardsAfterRecurlyRegister(this.userCustId).subscribe(()=>{
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            }, error => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log(error);
            });
        });

    }

    ngOnInit() {

    }

    raiseError(error){
        console.log('raised error');
        console.log(error);
        if(error.fields){
            this.errors = "The following fields are invalid: " + error.fields.join(', ');
        }
    }

    sameAsShipping() {
        this.sameAsShip = !this.sameAsShip;
        if(this.userDetails.get('first_name') && this.userShipDetails.get('address1')){
            this.customerShippingValues = {
                first_name: this.userDetails.get('first_name'),
                last_name: this.userDetails.get('last_name'),
                address1: this.userShipDetails.get('addressLine1'),
                address2: this.userShipDetails.get('addressLine2'),
                city: this.userShipDetails.get('city'),
                state: this.userShipDetails.get('state'),
                postal_code: this.userShipDetails.get('zipCode')
            };
        }
        console.log('SAME AS SHIPPING');
        console.log(this.customerShippingValues);
    }

    clearErrors(){
        this.errors = undefined;
        this.ngRedux.dispatch({
            type: 'CLEAR_BILLING_ERRORS'
        });
    }

    changeSt(stateAbbr){
        console.log('clicked state');
        console.log(stateAbbr);
        this.stateSelect = stateAbbr;
    }


    saveBilling() {
        console.log(this.firstCC);
        console.log('WOWOWOWW');
        console.log('update Billing is successful');
        if(this.firstCC) {
            var postData = {
                "addressLine1": this.firstCC.address1,
                "addressLine2": (this.firstCC.address2) ? this.firstCC.address2 : '',
                "city": this.firstCC.city,
                "state": this.firstCC.state,
                "zipCode": this.firstCC.zip,
                "phone": this.phone
            }
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            console.log('USER ID');
            console.log(this.userCustId)
            if(this.isAdmin){
            //This is actually updating billing even though the service is named updateShipping
                this._nestcare.updateShippingAdmin(postData, this.userCustId, true).subscribe((resp) => {
                    this.saveBillingAndNavigate();
                }, error => {
                    this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });

            } else if(this.supportNetworkCustId) {
                this._nestcare.updateBillingAdminAsAuthorizedUser(postData, this.userCustId, true, this.userDetails.get('phone')).subscribe((resp) => {
                    this.saveBillingAndNavigate();
                }, error => {
                    this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });
            } else {
                this._nestcare.updateBilling(postData).subscribe((resp) => {
                    this.saveBillingAndNavigate();
                }, error => {
                    this.ngRedux.dispatch({
                        type: 'SAVING_BILLING_FAILURE',
                        payload: {
                            error: error,
                            defaultMessage: 'Something went wrong saving your billing information. Contact Customer Support or try again.'
                        }
                    });
                });
            }
        }
    }


    submitNewPayment(){
        this.recurlyGetToken(false);
    }

    loggedInSubmitNewPayment(){
        this.recurlyGetToken(true)
    }

    saveBillingAndNavigate(){
        console.log('update billing is successful');
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        this.ngRedux.dispatch({
            type: "SAVING_BILLING_SUCCESS"
        });
        console.log('THIS IS IT');
        console.log(this.supportNetworkCustId);
        if(this.supportNetworkCustId){
            this.ngRedux.dispatch({
                type: "SUPPORT_NETWORK_BILLING_SUCCESS",
                payload: {
                    last4: this.firstCC.last_four,
                    changed: true
                }
            });

            this.router.navigate(['/support-network']);
        } else {
            this.ngRedux.dispatch({
                type: "ADD_BILLING_ADDRESS",
                payload: {
                    address: this.firstCC.address1 + '_' + this.firstCC.address2,
                    city: this.firstCC.city,
                    state: this.firstCC.state,
                    zip: this.firstCC.zip
                }
            });
            if (!this.signup && !this.enrollPlan) {
                this.router.navigate(['/account/bill']);
            } else if (!this.signup && this.enrollPlan) {
                let userViewSub = this.ngRedux.select(state => {
                    return state.userView;
                });
                userViewSub.subscribe(data => {
                    this.ngRedux.dispatch({
                        type: 'ADD_CUSTOMER_ID',
                        payload: data.get('nc_id')
                    })
                });

                this.router.navigate(['/order-summary']);
            }
            else {
                this.router.navigate(['/order-summary']);
            }
        }




    }

    recurlyGetToken(loggedin, ctrl = undefined){
        var that = this;
        var secForm = getPaymentJquery();
        if(ctrl){
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            var params = {
                id: that.userCustId
            };
            console.log('this ran');

            that = ctrl;
        }

        console.log(secForm);
        console.log("AAAAAAA");
        RECURLY.token(secForm, function (err, token){
            if(err){
                console.log(err);
                console.log('ERRORRED ANDREW');
                if(err && err.name === 'api-error' && err.message === 'There was a problem parsing the API response.'){
                    setTimeout(function(){
                        that.recurlyGetToken(loggedin, that);
                    }, 1500);
                } else {
                    that.raiseError(err);
                }
                that.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log('sadasda1s');
            }  else {
                console.log('sadasdas');
                that.clearErrors();
                that.addPayment(token, that.userShipDetails, that.userCustId);
                if(loggedin){
                    that._paywhirlService.loadPaymentCardsWithoutSub(that.userCustId).subscribe((resp)=>{
                        console.log('GOT PAYMENT CARDS');
                        console.log(resp);
                        let pmtCards = resp;
                            that.ngRedux.dispatch({
                            type: 'GET_CARDS',
                            payload: pmtCards
                        });

                        that.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                        that.saveBilling();
                    },
                    error => {
                        that.ngRedux.dispatch({
                            type: 'FETCH_CARDS_FAILURE',
                            payload: {err: error}
                        });

                        that.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    });
                }
            }
        });
    }

    ngAfterViewInit(){
        this.configureRecurly();
    }

    configureRecurly(){
        setTimeout(function() {
            RECURLY.configure(
                {
                    publicKey: NestCareConstants.recurlyPublicKey,
                    style: {
                        all: {
                            fontFamily: 'Open Sans',
                            fontSize: '1rem',
                            fontWeight: 'bold',
                            fontColor: '#2c0730'
                        },
                        number: {
                            placeholder: 'card number'
                        },
                        month: {
                            placeholder: 'exp month (mm)'
                        },
                        year: {
                            placeholder: 'exp year (yy or yyyy)'
                        },
                        cvv: {
                            placeholder: 'cvv'
                        },
                        first_name: {
                            placeholder: 'First Name'
                        },
                        last_name: {
                            placeholder: 'Last Name'
                        }
                    }
                });
            console.log('configured');
        }, 10);
        setTimeout(function() {
            RECURLY.configure(
                {
                    publicKey: NestCareConstants.recurlyPublicKey,
                    style: {
                        all: {
                            fontFamily: 'Open Sans',
                            fontSize: '1rem',
                            fontWeight: 'bold',
                            fontColor: '#2c0730'
                        },
                        number: {
                            placeholder: 'card number'
                        },
                        month: {
                            placeholder: 'exp month (mm)'
                        },
                        year: {
                            placeholder: 'exp year (yy or yyyy)'
                        },
                        cvv: {
                            placeholder: 'cvv'
                        },
                        first_name: {
                            placeholder: 'First Name'
                        },
                        last_name: {
                            placeholder: 'Last Name'
                        }
                    }
                });
            console.log('reconfigured');
        }, 3000);
    }

    addPayment(token, userDetails, custId){
        console.log('THIS HAPPENED');
        console.log(token);
        console.log(userDetails);
        let params = {
            token_id: token.id
        };
        this._paywhirlService.replaceCard(params, custId);
    }

}
