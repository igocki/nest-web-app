import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { SelectDevicesComponent }    from './selectdevices.component';
import { SelectHouseholdDevicesComponent } from './selectHouseholdDevices/selectHouseholdDevices.component'
import { SelectDevicesRoutingModule } from './selectdevices-routing.module';
import {Animations} from '../animations';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SelectDevicesRoutingModule,
        IonicModule
    ],
    declarations: [
        SelectDevicesComponent,
        SelectHouseholdDevicesComponent
    ],
    providers: [
        Animations
    ]
})
export class SelectDevicesModule {}
