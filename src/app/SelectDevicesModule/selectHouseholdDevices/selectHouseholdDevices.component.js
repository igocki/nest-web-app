var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../../animations';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { NgRedux } from 'ng2-redux';
export var SelectHouseholdDevicesComponent = (function () {
    function SelectHouseholdDevicesComponent(_appStatusActions, ngRedux) {
        this._appStatusActions = _appStatusActions;
        this.ngRedux = ngRedux;
        this.options = [
            { selected: true },
            { selected: false },
            { selected: false }
        ];
    }
    SelectHouseholdDevicesComponent.prototype.optionSelected = function (index) {
        this.options[index].selected = !this.options[index].selected;
    };
    SelectHouseholdDevicesComponent.prototype.ngOnInit = function () {
        this.ngRedux.dispatch({
            type: 'CLEAR_LISTED_DEVICES',
        });
    };
    SelectHouseholdDevicesComponent.prototype.selectDevices = function () {
        if (this.options[0].selected) {
            this.ngRedux.dispatch({
                type: 'ADD_ORDER_LINE_ITEM',
                payload: {
                    lineItemTitle: 'Bundled Devices',
                    amt: 0,
                    type: 'product'
                }
            });
            this.ngRedux.dispatch({
                type: 'ADD_ORDER_LINE_ITEM',
                payload: {
                    lineItemTitle: 'Activity Tracker x 2',
                    amt: 0,
                    type: 'product'
                }
            });
            this._appStatusActions.storeSelectDevices([{
                    amt: 0,
                    name: 'default bundle',
                    qty: 1,
                }]);
        }
        else {
            this._appStatusActions.storeSelectDevices([]);
        }
    };
    SelectHouseholdDevicesComponent = __decorate([
        Component({
            templateUrl: 'selectHouseholdDevices.component.html',
            providers: [AppStatusActions, NestcareService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [AppStatusActions, NgRedux])
    ], SelectHouseholdDevicesComponent);
    return SelectHouseholdDevicesComponent;
}());
