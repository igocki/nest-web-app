import { OnInit } from '@angular/core';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store';
export declare class SelectDevicesComponent implements OnInit {
    private _appStatusActions;
    private ngRedux;
    planSelected: any;
    options: {
        selected: boolean;
    }[];
    constructor(_appStatusActions: AppStatusActions, ngRedux: NgRedux<RootState>);
    optionSelected(index: number): void;
    ngOnInit(): void;
    selectDevices(): void;
}
