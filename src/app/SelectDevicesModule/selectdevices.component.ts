import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store';
@Component({
    templateUrl: 'selectdevices.component.html',
    providers: [ AppStatusActions, NestcareService],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class SelectDevicesComponent implements OnInit {
    public planSelected;
    public options = [
        { selected: true },
        { selected: false },
        { selected: false }
    ];

    constructor(private _appStatusActions: AppStatusActions,
                private ngRedux: NgRedux<RootState>) {
        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data){
                if(data.get('signupInfo') && data.getIn(['signupInfo', 'selectPlan'])){
                    this.planSelected = data.getIn(['signupInfo', 'selectPlan']);
                }
            }
        });
    }

    optionSelected(index: number) {
        this.options[index].selected = !this.options[index].selected;
    }


    ngOnInit() {
        this.ngRedux.dispatch({
            type: 'CLEAR_LISTED_DEVICES',
        });
    }

    selectDevices(){
        if(this.options[0].selected){
            this.ngRedux.dispatch({
                type: 'ADD_ORDER_LINE_ITEM',
                payload: {
                    lineItemTitle: 'Bundled Devices',
                    amt: 0,
                    type: 'product'
                }
            });
            this._appStatusActions.storeSelectDevices([{
                amt: 0,
                name: 'default bundle',
                qty: 1,
            }]);

        } else {
            this._appStatusActions.storeSelectDevices([])
        }
    }


}
