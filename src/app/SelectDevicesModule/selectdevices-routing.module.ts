import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelectDevicesComponent }  from './selectdevices.component';
import { SelectHouseholdDevicesComponent }  from './selectHouseholdDevices/selectHouseholdDevices.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'select-devices',
                component: SelectDevicesComponent
            },
            {
                path: 'select-devices-household',
                component: SelectHouseholdDevicesComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SelectDevicesRoutingModule { }
