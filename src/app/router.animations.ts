import {trigger, state, animate, style, transition} from '@angular/core';

export function routerTransition() {
    return slideToLeft();
}

function slideToLeft() {
    return trigger('routerTransition', [
        // state('void', style({position:'fixed', width:'100%', left: 0, right:0}) ),
        // state('*', style({position:'fixed', width:'100%', left: 0, right:0}) ),
        // transition(':enter', [  // before 2.1: transition('void => *', [
        //     style({transform: 'translateX(100%)', opacity: 0}),
        //     animate('0.5s cubic-bezier(0.215, 0.610, 0.355, 1.000)')
        // ]),
        // transition(':leave', [  // before 2.1: transition('* => void', [
        //     style({transform: 'translateX(0%)'}),
        //     animate('0.5s cubic-bezier(0.215, 0.610, 0.355, 1.000)', style({
        //         transform: 'translateX(-100%)',
        //         opacity: 0
        //     }))
        // ])
    ]);
}
