import { style, state, trigger } from '@angular/core';
export var Animations = (function () {
    function Animations() {
    }
    Animations.page = [
        trigger('routeAnimation', [
            state('*', style({ transform: 'translateX(0)', opacity: 1 }))
        ])
    ];
    return Animations;
}());
