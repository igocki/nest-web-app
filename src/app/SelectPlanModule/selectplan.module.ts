import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { SelectPlanComponent }    from './selectplan.component';
import { SelectHouseholdPlanComponent }    from './selectHouseholdPlan/selectHouseholdPlan.component';
import { SelectPlanRoutingModule } from './selectplan-routing.module';
import {Animations} from '../animations';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SelectPlanRoutingModule,
        IonicModule
    ],
    declarations: [
        SelectHouseholdPlanComponent,
        SelectPlanComponent
    ],
    providers: [
        Animations
    ]
})
export class SelectPlanModule {}
