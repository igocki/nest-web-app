import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Animations} from '../animations';
import { Map, List } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../actions/loading-actions';
import { IAppStatus, RootState } from '../../store';
import { AppStatusActions } from '../../actions/app-status-actions';
import { PaywhirlService } from '../../services/paywhirlService';
import { NestcareService } from '../../services/nestcareService';
import { NestCareConstants } from '../shared/constants/nestcare.constants';
import { Router, ActivatedRoute} from '@angular/router';
@Component({
    templateUrl: 'selectplan.component.html',
    providers: [AppStatusActions, NestcareService, PaywhirlService ],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class SelectPlanComponent implements OnInit {

    private paymentPlan: string;
    private loginStatus: any;
    private plans: Observable<List<any>>;
    private tokenString: string;
    private paymentPlans = NestCareConstants.paymentPlans;
    private workflow;
    private paymentPlanInfo: { string: string };

    constructor(
        private _appStatusActions: AppStatusActions,
        private ngRedux: NgRedux<RootState>,
        private route: ActivatedRoute,
        private _paywhirlService: PaywhirlService) {
        this.paymentPlan = 'everyYear';
        this.paymentPlanInfo = this.paymentPlans[this.paymentPlan];
    }

    ngOnInit() {
        let app_status = this.ngRedux.select(state => state.appStatus);
        app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                this.workflow = data.get('workflow');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_ORDER_SUMMARY'
        });
        this.plans = this._paywhirlService.userPlans;
        //for getting plans from recurly -- TODO implement later
        // this._paywhirlService.GetPlans();
        this.route.queryParams.subscribe(params =>{
            this.tokenString = params['token'];
            if(this.tokenString){
                this.ngRedux.dispatch({
                    type: LoadingActions.SHOW_LOADING
                });
                this._appStatusActions.tokenLogin(this.tokenString);
            }
        });
        console.log(this.plans);
    }

    selectedPaymentPlan(paymentPlan: string) {
        this.paymentPlan = paymentPlan;
        this.paymentPlanInfo = this.paymentPlans[paymentPlan];
    }

    selectPlan(selectedPlan: any){
        console.log('seleceted plan');
        console.log(selectedPlan);
        selectedPlan.type = 'plan';
        this._appStatusActions.storeSelectPlan(selectedPlan);
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: selectedPlan
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'One Time Setup Fee',
                amt: selectedPlan.setupFee,
                type: 'fee'
            }
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'Security Deposit',
                amt: selectedPlan.securityDeposit,
                type: 'fee'
            }
        });
    }

}
