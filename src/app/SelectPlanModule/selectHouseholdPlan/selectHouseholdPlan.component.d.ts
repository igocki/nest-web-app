import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { PaywhirlService } from '../../../services/paywhirlService';
import { ActivatedRoute } from '@angular/router';
export declare class SelectHouseholdPlanComponent implements OnInit {
    private _appStatusActions;
    private ngRedux;
    private route;
    private _paywhirlService;
    private paymentPlan;
    private loginStatus;
    private plans;
    private tokenString;
    private paymentPlans;
    private paymentPlanInfo;
    constructor(_appStatusActions: AppStatusActions, ngRedux: NgRedux<RootState>, route: ActivatedRoute, _paywhirlService: PaywhirlService);
    ngOnInit(): void;
    selectedPaymentPlan(paymentPlan: string): void;
    selectPlan(selectedPlan: any): void;
}
