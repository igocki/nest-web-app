var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../../animations';
import { NgRedux } from 'ng2-redux';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { PaywhirlService } from '../../../services/paywhirlService';
import { NestcareService } from '../../../services/nestcareService';
import { NestCareConstants } from '../../shared/constants/nestcare.constants';
import { ActivatedRoute } from '@angular/router';
export var SelectHouseholdPlanComponent = (function () {
    function SelectHouseholdPlanComponent(_appStatusActions, ngRedux, route, _paywhirlService) {
        this._appStatusActions = _appStatusActions;
        this.ngRedux = ngRedux;
        this.route = route;
        this._paywhirlService = _paywhirlService;
        this.paymentPlans = NestCareConstants.householdPaymentPlans;
        this.paymentPlan = 'everyYear';
        this.paymentPlanInfo = this.paymentPlans[this.paymentPlan];
    }
    SelectHouseholdPlanComponent.prototype.ngOnInit = function () {
        var _this = this;
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_ORDER_SUMMARY'
        });
        this.plans = this._paywhirlService.userPlans;
        this._paywhirlService.GetPlans();
        this.route.queryParams.subscribe(function (params) {
            _this.tokenString = params['token'];
        });
        console.log(this.plans);
    };
    SelectHouseholdPlanComponent.prototype.selectedPaymentPlan = function (paymentPlan) {
        this.paymentPlan = paymentPlan;
        this.paymentPlanInfo = this.paymentPlans[paymentPlan];
    };
    SelectHouseholdPlanComponent.prototype.selectPlan = function (selectedPlan) {
        console.log('seleceted plan');
        console.log(selectedPlan);
        selectedPlan.type = 'plan';
        this._appStatusActions.storeSelectHouseholdPlan(selectedPlan);
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: selectedPlan
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'One Time Setup Fee x 2',
                amt: selectedPlan.setupFee,
                type: 'fee'
            }
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'Security Deposit x 2',
                amt: selectedPlan.securityDeposit,
                type: 'fee'
            }
        });
    };
    SelectHouseholdPlanComponent = __decorate([
        Component({
            templateUrl: 'selectHouseholdPlan.component.html',
            providers: [AppStatusActions, NestcareService, PaywhirlService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [AppStatusActions, NgRedux, ActivatedRoute, PaywhirlService])
    ], SelectHouseholdPlanComponent);
    return SelectHouseholdPlanComponent;
}());
