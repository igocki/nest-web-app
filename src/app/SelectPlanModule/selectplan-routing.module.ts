import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelectPlanComponent }  from './selectplan.component';
import { SelectHouseholdPlanComponent }  from './selectHouseholdPlan/selectHouseholdPlan.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'select-plan',
                component: SelectPlanComponent
            },
            {
                path: 'select-plan-household',
                component: SelectHouseholdPlanComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SelectPlanRoutingModule { }
