var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../actions/loading-actions';
import { AppStatusActions } from '../../actions/app-status-actions';
import { PaywhirlService } from '../../services/paywhirlService';
import { NestcareService } from '../../services/nestcareService';
import { NestCareConstants } from '../shared/constants/nestcare.constants';
import { ActivatedRoute } from '@angular/router';
export var SelectPlanComponent = (function () {
    function SelectPlanComponent(_appStatusActions, ngRedux, route, _paywhirlService) {
        this._appStatusActions = _appStatusActions;
        this.ngRedux = ngRedux;
        this.route = route;
        this._paywhirlService = _paywhirlService;
        this.paymentPlans = NestCareConstants.paymentPlans;
        this.paymentPlan = 'everyYear';
        this.paymentPlanInfo = this.paymentPlans[this.paymentPlan];
    }
    SelectPlanComponent.prototype.ngOnInit = function () {
        var _this = this;
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
                _this.workflow = data.get('workflow');
            }
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_ORDER_SUMMARY'
        });
        this.plans = this._paywhirlService.userPlans;
        //for getting plans from recurly -- TODO implement later
        // this._paywhirlService.GetPlans();
        this.route.queryParams.subscribe(function (params) {
            _this.tokenString = params['token'];
            if (_this.tokenString) {
                _this.ngRedux.dispatch({
                    type: LoadingActions.SHOW_LOADING
                });
                _this._appStatusActions.tokenLogin(_this.tokenString);
            }
        });
        console.log(this.plans);
    };
    SelectPlanComponent.prototype.selectedPaymentPlan = function (paymentPlan) {
        this.paymentPlan = paymentPlan;
        this.paymentPlanInfo = this.paymentPlans[paymentPlan];
    };
    SelectPlanComponent.prototype.selectPlan = function (selectedPlan) {
        console.log('seleceted plan');
        console.log(selectedPlan);
        selectedPlan.type = 'plan';
        this._appStatusActions.storeSelectPlan(selectedPlan);
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: selectedPlan
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'One Time Setup Fee',
                amt: selectedPlan.setupFee,
                type: 'fee'
            }
        });
        this.ngRedux.dispatch({
            type: 'ADD_ORDER_LINE_ITEM',
            payload: {
                lineItemTitle: 'Security Deposit',
                amt: selectedPlan.securityDeposit,
                type: 'fee'
            }
        });
    };
    SelectPlanComponent = __decorate([
        Component({
            templateUrl: 'selectplan.component.html',
            providers: [AppStatusActions, NestcareService, PaywhirlService],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [AppStatusActions, NgRedux, ActivatedRoute, PaywhirlService])
    ], SelectPlanComponent);
    return SelectPlanComponent;
}());
