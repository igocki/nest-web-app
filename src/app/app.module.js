var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { DevToolsExtension, NgReduxModule } from 'ng2-redux';
import { NgReduxRouter } from 'ng2-redux-router';
import { NgRedux } from 'ng2-redux';
import { LoginModule } from './LoginModule/login.module';
import { SignupModule } from './SignupModule/signup.module';
import { SelectPlanModule } from './SelectPlanModule/selectplan.module';
import { SelectDevicesModule } from './SelectDevicesModule/selectdevices.module';
import { ShippingModule } from './ShippingModule/shipping.module';
import { AdminModule } from './AdminModule/admin.module';
import { DashboardModule } from './DashboardModule/dashboard.module';
import { AccountModule } from './AccountModule/account.module';
import { SupportNetworkModule } from './SupportNetworkModule/support-network.module';
import { PaymentModule } from './PaymentModule/payment.module';
import { OrderSummaryModule } from './OrderSummaryModule/order-summary.module';
import { CommonComponentsModule } from './CommonComponentsModule/commoncomponents.module';
import { LoadingModule } from './LoadingModule/loading.module';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from '@angular/material';
import { TopMenuLogoComponent } from './topmenulogo/topmenulogo.component';
import { SidebarModule } from 'ng-sidebar';
import { ViewAllOrdersModule } from './ViewAllOrdersModule/view-all-orders.module';
import 'hammerjs';
import { DialogModule } from 'primeng/primeng';
import { InitialComponent } from './app.component';
import { BackButtonComponent } from "./CommonComponentsModule/backButton/backbutton.component";
import { StatesPopoverComponent } from "./CommonComponentsModule/statesPopover/statespopover.component";
export var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                InitialComponent,
                TopMenuLogoComponent
            ],
            imports: [
                BrowserModule,
                NgReduxModule,
                LoginModule,
                LoadingModule,
                SignupModule,
                SelectPlanModule,
                SelectDevicesModule,
                DialogModule,
                ShippingModule,
                PaymentModule,
                OrderSummaryModule,
                AdminModule,
                ViewAllOrdersModule,
                DashboardModule,
                AccountModule,
                SupportNetworkModule,
                AppRoutingModule,
                MaterialModule,
                CommonComponentsModule,
                SidebarModule,
                IonicModule.forRoot(MyApp, {
                    tabsPlacement: 'bottom',
                    platforms: {
                        ios: {
                            mode: 'md',
                        }
                    }
                }),
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                StatesPopoverComponent,
                BackButtonComponent
            ],
            providers: [NgReduxRouter, NgRedux, DevToolsExtension]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
