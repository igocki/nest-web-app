import { Platform } from 'ionic-angular';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store/index';
import { Router, ActivatedRoute } from '@angular/router';
import { NgReduxRouter } from 'ng2-redux-router';
export declare class MyApp {
    private platform;
    private ngRedux;
    private ngReduxRouter;
    private activatedRoute;
    private router;
    private _opened;
    private currentState;
    menuAppear: boolean;
    logoAppear: boolean;
    private status;
    private loginStatus;
    constructor(platform: Platform, ngRedux: NgRedux<RootState>, ngReduxRouter: NgReduxRouter, activatedRoute: ActivatedRoute, router: Router);
}
export declare class InitialComponent {
}
export declare class NotFoundComponent {
}
