import { trigger } from '@angular/core';
export function routerTransition() {
    return slideToLeft();
}
function slideToLeft() {
    return trigger('routerTransition', []);
}
