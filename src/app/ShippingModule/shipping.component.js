var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Animations } from '../animations';
import { Observable } from 'rxjs/Observable';
import { select, NgRedux } from 'ng2-redux';
import { ShippingActions } from '../../actions/shipping-actions';
import { ShippingAddressService } from '../../services/shippingAddressService';
import { NestcareService } from '../../services/nestcareService';
import { ShippoService } from '../../services/shippoService';
import { HttpClient } from '../../services/http-header-service';
import { AppStatusActions } from '../../actions/app-status-actions';
export var ShippingComponent = (function () {
    function ShippingComponent(ngRedux, _shippingAddress) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._shippingAddress = _shippingAddress;
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.workflow = data.get('workflow');
                _this.enrollPlan = data.get('enrollNewPlan');
                _this.signup = data.getIn(['signupInfo']);
                _this.userInfo = data.getIn(['signupInfo', 'userInfo']);
                _this.loginStatus = data.get('loginStatus');
                _this.isAdmin = data.get('admin');
                if (data.get('shippingAddress')) {
                    _this.billID = data.getIn(['shippingAddress', 'id']);
                }
                _this.paidForCustID = data.get('paidForCustomerID');
            }
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data) {
                if (data.get('shippingAddress') && data.getIn(['shippingAddress', 'line1'])) {
                    console.log('ditto');
                    _this.loggedInUser = {
                        name: data.getIn(['fullname']),
                        addressLine1: data.getIn(['shippingAddress', 'line1']),
                        addressLine2: data.getIn(['shippingAddress', 'line2']),
                        city: data.getIn(['shippingAddress', 'city_id']),
                        state: data.getIn(['shippingAddress', 'state_id']),
                        zipCode: data.getIn(['shippingAddress', 'zip_code'])
                    };
                }
                if (!_this.userInfo) {
                    console.log('this rannnn');
                    console.log(_this.loggedInUser);
                    if (_this.loggedInUser) {
                        console.log('did this');
                        _this.ngRedux.dispatch({
                            type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                            payload: {
                                name: _this.loggedInUser.name,
                                addressLine1: _this.loggedInUser.addressLine1,
                                addressLine2: _this.loggedInUser.addressLine2,
                                city: _this.loggedInUser.city,
                                state: _this.loggedInUser.state,
                                zipCode: _this.loggedInUser.zipCode
                            }
                        });
                    }
                    if (_this.workflow === 'other') {
                        _this.loggedInChangeAddress = false;
                    }
                    else {
                        _this.loggedInChangeAddress = true;
                    }
                }
                _this.viewUserId = data.get('nc_id');
            }
        });
    }
    ShippingComponent.prototype.ngOnInit = function () {
    };
    ShippingComponent.prototype.clearErrors = function () {
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_ERRORS
        });
    };
    ShippingComponent.prototype.addNewShipping = function (formData) {
        console.log('yahhsss');
        console.log(this.isAdmin);
        if (this.isAdmin) {
            if (!this.paidForCustID) {
                this._shippingAddress.addNewShippingAddress(formData, this.isAdmin, this.viewUserId, this.billID);
            }
            else {
                this._shippingAddress.addNewShippingAddress(formData, this.isAdmin, this.paidForCustID, this.billID);
            }
        }
        else {
            this._shippingAddress.addNewShippingAddress(formData);
        }
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], ShippingComponent.prototype, "shipping$", void 0);
    ShippingComponent = __decorate([
        Component({
            templateUrl: 'shipping.component.html',
            providers: [HttpClient, ShippingAddressService, NestcareService, ShippoService, AppStatusActions],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page
        }), 
        __metadata('design:paramtypes', [NgRedux, ShippingAddressService])
    ], ShippingComponent);
    return ShippingComponent;
}());
