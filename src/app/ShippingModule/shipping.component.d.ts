import { OnInit } from '@angular/core';
import { IShipping } from '../../store';
import { Observable } from 'rxjs/Observable';
import { RootState } from '../../store';
import { NgRedux } from 'ng2-redux';
import { ShippingAddressService } from '../../services/shippingAddressService';
export declare class ShippingComponent implements OnInit {
    private ngRedux;
    private _shippingAddress;
    shipping$: Observable<IShipping>;
    signup: any;
    private enrollPlan;
    private loginStatus;
    loggedInChangeAddress: any;
    isAdmin: any;
    viewUserId: any;
    billID: any;
    loggedInUser: any;
    paidForCustID: any;
    userInfo: any;
    workflow: any;
    constructor(ngRedux: NgRedux<RootState>, _shippingAddress: ShippingAddressService);
    ngOnInit(): void;
    clearErrors(): void;
    addNewShipping(formData: any): void;
}
