import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShippingComponent }  from './shipping.component';
import { ShippingDetailsComponent } from './details/details.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'shipping',
                component: ShippingComponent
            },
            {
                path: 'shipping-details',
                component: ShippingDetailsComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ShippingRoutingModule { }
