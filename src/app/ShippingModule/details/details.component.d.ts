import { OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgRedux } from 'ng2-redux';
import { IShipping, IAppStatus, RootState } from '../../../store';
import { Map, List } from 'immutable';
import { ShippoService } from '../../../services/shippoService';
import { Router } from '@angular/router';
export declare class ShippingDetailsComponent implements OnInit {
    private _shippoService;
    private ngRedux;
    private router;
    appStatus$: Observable<IAppStatus>;
    shipping$: Observable<IShipping>;
    rates: Observable<List<any>>;
    selectedRate: Observable<Map<string, any>>;
    user: any;
    subscriptionAuth: any;
    loggedinUser: any;
    shippingAddressChosen: any;
    constructor(_shippoService: ShippoService, ngRedux: NgRedux<RootState>, router: Router);
    ngOnInit(): void;
    selectRate(rate: any): void;
}
