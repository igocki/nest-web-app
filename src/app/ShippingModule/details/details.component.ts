import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {Animations} from '../../animations';
import { Observable } from 'rxjs/Observable';
import { select, NgRedux } from 'ng2-redux';
import { IShipping, IAppStatus,RootState } from '../../../store'
import { Map, List } from 'immutable';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'details.component.html',
    providers: [ShippingActions, NestcareService, ShippoService,  HttpClient, AppStatusActions],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShippingDetailsComponent implements OnInit {
    @select() appStatus$: Observable<IAppStatus>;
    @select() shipping$: Observable<IShipping>;
    rates: Observable<List<any>>;
    selectedRate: Observable<Map<string, any>>;
    user: any;
    subscriptionAuth: any;
    loggedinUser: any;
    shippingAddressChosen: any;

    constructor(private _shippoService: ShippoService,
                private ngRedux: NgRedux<RootState>,
                private router: Router) {
        this.rates = _shippoService.shippingRates;
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        this.subscriptionAuth = userViewSub.subscribe(data => {
            this.loggedinUser = {
                email: data.get('email'),
                mobile: data.get('userPhone')
            }
        });
        this.subscriptionAuth.unsubscribe();
        this.selectedRate = this.ngRedux.select(state => (state.shippingRates) ? state.shippingRates.get('selectedRate') : null);
        let subscription = this.appStatus$.subscribe((app) => {
                let appMap = Map<string, IAppStatus>(app);
                if(appMap.getIn(['loginStatus']) !== 'logged-in'){
                    this.user = appMap.getIn(['signupInfo', 'userInfo']).toJS();
                    this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
                } else {
                    this.user = this.loggedinUser;
                    console.log('YOOOO');
                    console.log(appMap);
                    console.log(appMap.getIn(['signupInfo', 'shippingAddress']));
                    this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
                }

        });
        subscription.unsubscribe();



    }

    ngOnInit() {
        if(this.user && this.shippingAddressChosen){
            this._shippoService.loadShippingRates(this.shippingAddressChosen, this.user);
        } else {
            this.router.navigate(['/select-devices']);
        }

    }


    selectRate(rate){
        this.ngRedux.dispatch({
            type: 'SELECT_RATE',
            payload: rate
        });
    }
}
