var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Animations } from '../../animations';
import { Observable } from 'rxjs/Observable';
import { select, NgRedux } from 'ng2-redux';
import { Map } from 'immutable';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Router } from '@angular/router';
export var ShippingDetailsComponent = (function () {
    function ShippingDetailsComponent(_shippoService, ngRedux, router) {
        var _this = this;
        this._shippoService = _shippoService;
        this.ngRedux = ngRedux;
        this.router = router;
        this.rates = _shippoService.shippingRates;
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        this.subscriptionAuth = userViewSub.subscribe(function (data) {
            _this.loggedinUser = {
                email: data.get('email'),
                mobile: data.get('userPhone')
            };
        });
        this.subscriptionAuth.unsubscribe();
        this.selectedRate = this.ngRedux.select(function (state) { return (state.shippingRates) ? state.shippingRates.get('selectedRate') : null; });
        var subscription = this.appStatus$.subscribe(function (app) {
            var appMap = Map(app);
            if (appMap.getIn(['loginStatus']) !== 'logged-in') {
                _this.user = appMap.getIn(['signupInfo', 'userInfo']).toJS();
                _this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
            }
            else {
                _this.user = _this.loggedinUser;
                console.log('YOOOO');
                console.log(appMap);
                console.log(appMap.getIn(['signupInfo', 'shippingAddress']));
                _this.shippingAddressChosen = appMap.getIn(['signupInfo', 'shippingAddress']).toJS();
            }
        });
        subscription.unsubscribe();
    }
    ShippingDetailsComponent.prototype.ngOnInit = function () {
        if (this.user && this.shippingAddressChosen) {
            this._shippoService.loadShippingRates(this.shippingAddressChosen, this.user);
        }
        else {
            this.router.navigate(['/select-devices']);
        }
    };
    ShippingDetailsComponent.prototype.selectRate = function (rate) {
        this.ngRedux.dispatch({
            type: 'SELECT_RATE',
            payload: rate
        });
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], ShippingDetailsComponent.prototype, "appStatus$", void 0);
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], ShippingDetailsComponent.prototype, "shipping$", void 0);
    ShippingDetailsComponent = __decorate([
        Component({
            templateUrl: 'details.component.html',
            providers: [ShippingActions, NestcareService, ShippoService, HttpClient, AppStatusActions],
            host: { '[@routeAnimation]': 'true' },
            animations: Animations.page,
            changeDetection: ChangeDetectionStrategy.OnPush
        }), 
        __metadata('design:paramtypes', [ShippoService, NgRedux, Router])
    ], ShippingDetailsComponent);
    return ShippingDetailsComponent;
}());
