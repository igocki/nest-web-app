import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations';
import { IShipping } from '../../store';
import { Observable } from 'rxjs/Observable';
import { IAppStatus, RootState } from '../../store'
import { select, NgRedux } from 'ng2-redux';
import { ShippingActions } from '../../actions/shipping-actions'
import { ShippingAddressService } from '../../services/shippingAddressService';
import { NestcareService } from '../../services/nestcareService';
import { ShippoService } from '../../services/shippoService';
import { HttpClient } from '../../services/http-header-service';
import { AppStatusActions } from '../../actions/app-status-actions'

@Component({
    templateUrl: 'shipping.component.html',
    providers: [ HttpClient, ShippingAddressService, NestcareService, ShippoService, AppStatusActions],
    host: { '[@routeAnimation]': 'true' },
    animations: Animations.page
})
export class ShippingComponent implements OnInit {
    @select() shipping$: Observable<IShipping>;
    public signup;
    private enrollPlan;
    private loginStatus: any;
    public loggedInChangeAddress;
    public isAdmin;
    public viewUserId;
    public billID;
    public loggedInUser;
    public paidForCustID;
    public userInfo;
    public workflow;

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _shippingAddress: ShippingAddressService
    ) {
        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data){
                this.workflow = data.get('workflow');
                this.enrollPlan = data.get('enrollNewPlan');
                this.signup = data.getIn(['signupInfo']);
                this.userInfo = data.getIn(['signupInfo', 'userInfo']);
                this.loginStatus = data.get('loginStatus');
                this.isAdmin = data.get('admin');
                if(data.get('shippingAddress')){
                    this.billID = data.getIn(['shippingAddress','id']);
                }
                this.paidForCustID = data.get('paidForCustomerID');
            }
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        userViewSub.subscribe((data) =>{
            if(data){
                if(data.get('shippingAddress') && data.getIn(['shippingAddress', 'line1'])){
                    console.log('ditto');
                    this.loggedInUser = {
                        name: data.getIn(['fullname']),
                        addressLine1: data.getIn(['shippingAddress','line1']),
                        addressLine2: data.getIn(['shippingAddress','line2']),
                        city: data.getIn(['shippingAddress','city_id']),
                        state: data.getIn(['shippingAddress', 'state_id']),
                        zipCode: data.getIn(['shippingAddress','zip_code'])
                    }
                }

                if(!this.userInfo){
                    console.log('this rannnn');
                    console.log(this.loggedInUser);
                    if(this.loggedInUser){
                        console.log('did this')
                        this.ngRedux.dispatch({
                            type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                            payload: {
                                name: this.loggedInUser.name,
                                addressLine1: this.loggedInUser.addressLine1,
                                addressLine2: this.loggedInUser.addressLine2,
                                city: this.loggedInUser.city,
                                state: this.loggedInUser.state,
                                zipCode: this.loggedInUser.zipCode
                            }
                        });
                    }
                    if(this.workflow === 'other'){
                        this.loggedInChangeAddress = false;
                    } else {
                        this.loggedInChangeAddress = true
                    }
                }
                this.viewUserId = data.get('nc_id') ;
            }
        });
    }

    ngOnInit() {

    }

    clearErrors(){
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_ERRORS
        });
    }

    addNewShipping(formData){
        console.log('yahhsss');
        console.log(this.isAdmin);
        if(this.isAdmin){
            if(!this.paidForCustID) {
                this._shippingAddress.addNewShippingAddress(formData, this.isAdmin, this.viewUserId, this.billID);
            } else {
                this._shippingAddress.addNewShippingAddress(formData, this.isAdmin, this.paidForCustID, this.billID);
            }

        } else {
            this._shippingAddress.addNewShippingAddress(formData);
        }

    }

}
