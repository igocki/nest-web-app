import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { ShippingComponent }    from './shipping.component';
import { ShippingRoutingModule } from './shipping-routing.module';
import { ShippingDetailsComponent } from './details/details.component';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import {Animations} from '../animations';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ShippingRoutingModule,
        IonicModule,
        CommonComponentsModule
    ],
    declarations: [
        ShippingComponent,
        ShippingDetailsComponent
    ],
    providers: [
        Animations
    ]
})
export class ShippingModule {}
