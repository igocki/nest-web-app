var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Pipe } from "@angular/core";
import { NestCareConstants } from '../shared/constants/nestcare.constants';
import moment from "moment";
export var NCMeasurementFilter = (function () {
    function NCMeasurementFilter(type) {
        /*if(type != 'V' || type != 'W' || type != 'M') {
         throw new TypeError("Invalid type passed")
         }*/
        this.type = type;
    }
    NCMeasurementFilter.checkIfDone = function (objectMeasurement) {
        return objectMeasurement.is_done == '1';
    };
    NCMeasurementFilter.checkIfSkipped = function (objectMeasurement) {
        return objectMeasurement.skip == '1';
    };
    NCMeasurementFilter.checkIfMissed = function (objectMeasurement, currentDate, measurementDate) {
        if (this.checkIfSkipped(objectMeasurement)) {
            return true;
        }
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement);
        }
        else {
            return currentDate['seconds'] > measurementDate['seconds'] && !this.checkIfDone(objectMeasurement);
        }
    };
    NCMeasurementFilter.checkIfAllow = function (objectMeasurement, currentDate, measurementDate) {
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] - 1 &&
                currentDate['H'] < measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement);
        }
        else {
            return currentDate['seconds'] >= measurementDate['seconds'] && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement);
        }
    };
    NCMeasurementFilter.checkIfBlock = function (objectMeasurement, currentDate, measurementDate) {
        return !this.checkIfAllow(objectMeasurement, currentDate, measurementDate) && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement);
    };
    NCMeasurementFilter.checkMeasurement = function (objectMeasurement, checkFor, time, type) {
        var timeNow = NestCareConstants.convertDate(time);
        var currentDate = NestCareConstants.convertDate(moment().toDate());
        var splitTime = ["0", "0", "0"];
        if (type == 'V') {
            //splitTime = objectMeasurement.measurement_time.split(':')
            splitTime = objectMeasurement.scheduleTime.split(':');
        }
        else if (type == 'M') {
            splitTime = objectMeasurement.medication_at.split(':');
        }
        else if (type == 'W') {
            splitTime = objectMeasurement.wellness_at.split(':');
        }
        var newDate = moment(timeNow.Y + "-" + timeNow.m + "-" + timeNow.d + " " + parseInt(splitTime[0]) + ":" + parseInt(splitTime[1]) + ":" + parseInt(splitTime[2]), "YYYY-MM-DD HH:mm:ss").toDate();
        var measurementDate = NestCareConstants.convertDate(newDate);
        var returnValue = false;
        switch (checkFor) {
            case 'done':
                returnValue = NCMeasurementFilter.checkIfDone(objectMeasurement);
                break;
            case 'miss':
                returnValue = NCMeasurementFilter.checkIfMissed(objectMeasurement, currentDate, measurementDate);
                break;
            case 'create':
                returnValue = NCMeasurementFilter.checkIfAllow(objectMeasurement, currentDate, measurementDate);
                break;
            case 'nocreate':
                returnValue = NCMeasurementFilter.checkIfBlock(objectMeasurement, currentDate, measurementDate);
                break;
            case 'skip':
                returnValue = NCMeasurementFilter.checkIfSkipped(objectMeasurement);
                break;
            default:
                returnValue = false;
        }
        return returnValue;
    };
    NCMeasurementFilter.prototype.transform = function (value, args) {
        var _this = this;
        return value.filter(function (objectMeasurement) {
            var a = false;
            switch (_this.type) {
                case 'V':
                    a = objectMeasurement.vital_id != args[2] && args[2] != "";
                    break;
                case 'W':
                    a = objectMeasurement.wellness_type_id != args[2] && args[2] != "";
                    break;
                case 'M':
                    a = objectMeasurement.medication_id != args[2] && args[2] != "";
                    break;
            }
            if (a) {
                return false;
            }
            return NCMeasurementFilter.checkMeasurement(objectMeasurement, args[0], args[1], _this.type);
        });
    };
    NCMeasurementFilter = __decorate([
        Pipe({
            name: 'NCMeasurementFilter'
        }), 
        __metadata('design:paramtypes', [Object])
    ], NCMeasurementFilter);
    return NCMeasurementFilter;
}());
