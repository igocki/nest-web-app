import {Pipe} from "@angular/core";
import { NestCareConstants } from '../shared/constants/nestcare.constants'
import moment from "moment";

@Pipe({
    name: 'NCMeasurementFilter'
})
export class NCMeasurementFilter {

    private type: string;

    constructor(type) {
        /*if(type != 'V' || type != 'W' || type != 'M') {
         throw new TypeError("Invalid type passed")
         }*/
        this.type = type
    }

    public static checkIfDone(objectMeasurement: any): boolean {
        return objectMeasurement.is_done == '1'
    }

    public static checkIfSkipped(objectMeasurement: any): boolean {
        return objectMeasurement.skip == '1'
    }

    public static checkIfMissed(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {
        if (this.checkIfSkipped(objectMeasurement)) {
            return true;
        }
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement)
        } else {
            return currentDate['seconds'] > measurementDate['seconds'] && !this.checkIfDone(objectMeasurement)
        }
    }

    public static checkIfAllow(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {
        if (currentDate['date'] == measurementDate['date']) {
            return currentDate['H'] >= measurementDate['H'] - 1 &&
                currentDate['H'] < measurementDate['H'] + 1 && !this.checkIfDone(objectMeasurement)
        } else {
            return currentDate['seconds'] >= measurementDate['seconds'] && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement)
        }
    }

    private static checkIfBlock(objectMeasurement: any, currentDate: any, measurementDate: any): boolean {
        return !this.checkIfAllow(objectMeasurement, currentDate, measurementDate) && !this.checkIfMissed(objectMeasurement, currentDate, measurementDate) && !this.checkIfDone(objectMeasurement)
    }

    public static checkMeasurement(objectMeasurement: any, checkFor: string, time: Date, type: string): boolean {
        var timeNow = NestCareConstants.convertDate(time)
        var currentDate = NestCareConstants.convertDate(moment().toDate())
        var splitTime = ["0", "0", "0"]
        if(type == 'V') {
            //splitTime = objectMeasurement.measurement_time.split(':')
            splitTime = objectMeasurement.scheduleTime.split(':')
        } else if(type == 'M') {
            splitTime = objectMeasurement.medication_at.split(':')
        } else if(type == 'W') {
            splitTime = objectMeasurement.wellness_at.split(':')
        }
        var newDate = moment(`${timeNow.Y}-${timeNow.m}-${timeNow.d} ${parseInt(splitTime[0])}:${parseInt(splitTime[1])}:${parseInt(splitTime[2])}`, "YYYY-MM-DD HH:mm:ss").toDate()
        var measurementDate = NestCareConstants.convertDate(newDate)

        let returnValue = false
        switch (checkFor) {
            case 'done':
                returnValue = NCMeasurementFilter.checkIfDone(objectMeasurement)
                break

            case 'miss':
                returnValue = NCMeasurementFilter.checkIfMissed(objectMeasurement, currentDate, measurementDate)
                break

            case 'create':
                returnValue = NCMeasurementFilter.checkIfAllow(objectMeasurement, currentDate, measurementDate)
                break

            case 'nocreate':
                returnValue = NCMeasurementFilter.checkIfBlock(objectMeasurement, currentDate, measurementDate)
                break

            case 'skip':
                returnValue = NCMeasurementFilter.checkIfSkipped(objectMeasurement)
                break

            default:
                returnValue = false

        }
        return returnValue
    }


    transform(value, args) {
        return value.filter(objectMeasurement => {
            let a = false
            switch (this.type) {
                case 'V':
                    a = objectMeasurement.vital_id != args[2] && args[2] != ""
                    break

                case 'W':
                    a = objectMeasurement.wellness_type_id != args[2] && args[2] != ""
                    break

                case 'M':
                    a = objectMeasurement.medication_id != args[2] && args[2] != ""
                    break
            }
            if(a) {
                return false
            }
            return NCMeasurementFilter.checkMeasurement(objectMeasurement, args[0], args[1], this.type)
        })
    }
}
