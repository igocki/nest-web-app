var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { SignUpActions } from '../../../actions/signup-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { NestCareConstants } from '../../shared/constants/nestcare.constants';
export var NCSignUpFormComponent = (function () {
    function NCSignUpFormComponent(ngRedux, _signUpActions, _nc, builder, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._signUpActions = _signUpActions;
        this._nc = _nc;
        this.builder = builder;
        this.router = router;
        this.cc = 1;
        this.mask = [/[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.errorIsVisible = false;
        this.signupInfoForm = builder.group({
            'first_name': ['', Validators.required],
            'last_name': ['', Validators.required],
            'countrycode': ['', [Validators.required, Validators.maxLength(3), Validators.pattern('[0-9]{1,3}')]],
            'mobile': ['', Validators.required],
            'date_of_birth': ['', Validators.required],
            'gender': ['', Validators.required],
            'email': ['', Validators.required],
            'password': ['', [Validators.required, Validators.minLength(8)]],
            'confirmpassword': ['', [Validators.required, Validators.minLength(8)]]
        });
        this.signupInfoForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(); });
    }
    NCSignUpFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.status.subscribe(function (status) {
            if (status === 'submitted') {
                _this.errorIsVisible = true;
            }
        });
    };
    NCSignUpFormComponent.prototype.onValueChanged = function () {
        if (this.errorIsVisible) {
            this.errorIsVisible = false;
            this._signUpActions.clearErrors();
        }
    };
    NCSignUpFormComponent.prototype.confirmPass = function () {
        var signupForm = this.signupInfoForm.value;
        return signupForm.password && signupForm.password !== '' &&
            signupForm.confirmpassword && signupForm.confirmpassword !== '' &&
            signupForm.password !== signupForm.confirmpassword;
    };
    NCSignUpFormComponent.prototype.submitThis = function () {
        var formData = this.signupInfoForm.value;
        formData.mobile = '+' + formData.countrycode + formData.mobile.replace(/\D+/g, '').substring(0, 10);
        console.log(formData);
        if (this.otherForm) {
            this._signUpActions.registerOther(formData);
        }
        else {
            if (this.householdForm) {
                this._signUpActions.registerHousehold(formData);
            }
            else {
                this._signUpActions.register(formData);
            }
        }
    };
    NCSignUpFormComponent.prototype.fakeRegister = function () {
        var formData = {
            first_name: "Andrew",
            last_name: "Test",
            email: NestCareConstants.defaultUserEmail,
            date_of_birth: "1985-03-20",
            gender: "female",
            countrycode: "1",
            mobile: "2105555555",
            password: "password1234"
        };
        formData.mobile = '+' + formData.countrycode + formData.mobile.replace(/\D+/g, '').substring(0, 10);
        this._signUpActions.fakeRegister(formData);
    };
    __decorate([
        select(['signup', 'status']), 
        __metadata('design:type', Observable)
    ], NCSignUpFormComponent.prototype, "status", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], NCSignUpFormComponent.prototype, "signUpResponse", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NCSignUpFormComponent.prototype, "otherForm", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NCSignUpFormComponent.prototype, "householdForm", void 0);
    NCSignUpFormComponent = __decorate([
        Component({
            selector: 'ncsignup-form',
            providers: [SignUpActions, NestcareService, HttpClient, AppStatusActions, RegisterService],
            changeDetection: ChangeDetectionStrategy.OnPush,
            templateUrl: 'ncsignupform.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, SignUpActions, NestcareService, FormBuilder, Router])
    ], NCSignUpFormComponent);
    return NCSignUpFormComponent;
}());
;
