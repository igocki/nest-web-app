import { Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import { RootState } from '../../../store/index';
import { SignUpActions } from '../../../actions/signup-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { ISignUp } from '../../../store'
import { NestCareConstants } from '../../shared/constants/nestcare.constants'

@Component({
    selector: 'ncsignup-form',
    providers: [SignUpActions, NestcareService, HttpClient, AppStatusActions, RegisterService],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'ncsignupform.component.html'
})
export class NCSignUpFormComponent {
    model: any;
    signupInfoForm: FormGroup;
    errorIsVisible: boolean;
    gender: string;
    public cc = 1;
    public phone: string;
    public mask = [ /[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    @select(['signup', 'status']) status: Observable<string>;

    @Input() signUpResponse: ISignUp;
    @Input() otherForm: boolean;
    @Input() householdForm: boolean;
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _signUpActions: SignUpActions,
        private _nc: NestcareService,
        private builder: FormBuilder,
        private router: Router
    ) {
        this.errorIsVisible = false;
        this.signupInfoForm = builder.group({
            'first_name': ['', Validators.required],
            'last_name': ['', Validators.required],
            'countrycode': ['', [Validators.required, Validators.maxLength(3), Validators.pattern('[0-9]{1,3}')]],
            'mobile': ['', Validators.required],
            'date_of_birth': ['', Validators.required],
            'gender': ['', Validators.required],
            'email': ['', Validators.required],
            'password': ['',[Validators.required, Validators.minLength(8)]],
            'confirmpassword': ['', [Validators.required, Validators.minLength(8)]]
        });


        this.signupInfoForm.valueChanges
            .subscribe(data => this.onValueChanged());
    }


    ngOnInit() {

        this.status.subscribe(status =>
        {
            if(status === 'submitted'){
                this.errorIsVisible = true;
            }
        });
    }

    onValueChanged(){
        if(this.errorIsVisible){
            this.errorIsVisible = false;
            this._signUpActions.clearErrors();
        }
    }

    confirmPass(){
        let signupForm = this.signupInfoForm.value;
        return signupForm.password && signupForm.password !== '' &&
            signupForm.confirmpassword && signupForm.confirmpassword !== '' &&
            signupForm.password !== signupForm.confirmpassword;
    }

    submitThis(){
        let formData = this.signupInfoForm.value;
        formData.mobile = '+' + formData.countrycode + formData.mobile.replace(/\D+/g, '').substring(0,10);
        console.log(formData);
        if(this.otherForm){
            this._signUpActions.registerOther(formData);
        } else {
            if(this.householdForm){
                this._signUpActions.registerHousehold(formData);
            } else {
                this._signUpActions.register(formData);
            }
        }
    }

    fakeRegister(){
        let formData = {
            first_name: "Andrew",
            last_name: "Test",
            email: NestCareConstants.defaultUserEmail,
            date_of_birth: "1985-03-20",
            gender: "female",
            countrycode: "1",
            mobile: "2105555555",
            password: "password1234"
        };
        formData.mobile = '+' + formData.countrycode + formData.mobile.replace(/\D+/g, '').substring(0,10);
        this._signUpActions.fakeRegister(formData);
    }

};
