var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { ShippingActions } from '../../../actions/shipping-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { StatesConstant } from "../statesPopover/states.constant";
export var NewShippingFormComponent = (function () {
    function NewShippingFormComponent(ngRedux, _shippingActions, _nc, formBuilder, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._shippingActions = _shippingActions;
        this._nc = _nc;
        this.formBuilder = formBuilder;
        this.router = router;
        this.statesArray = StatesConstant;
        this.addNewShipping = new EventEmitter();
        this.clearErrors = new EventEmitter();
        this.errorIsVisible = false;
        this.addAddressForm = formBuilder.group({
            'name': ['', Validators.required],
            'addressLine1': ['', Validators.required],
            'addressLine2': [''],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', [Validators.required, Validators.minLength(5)]]
        });
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.phone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });
        this.addAddressForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(); });
    }
    NewShippingFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.status.subscribe(function (status) {
            if (status === 'submitted') {
                _this.errorIsVisible = true;
            }
        });
    };
    NewShippingFormComponent.prototype.onValueChanged = function () {
        if (this.errorIsVisible) {
            this.errorIsVisible = false;
            this.clearErrors.emit();
        }
    };
    NewShippingFormComponent.prototype.addShippingAddress = function () {
        var formData = this.addAddressForm.value;
        console.log('this');
        console.log(this.phone);
        formData.phone = this.phone;
        formData.state = this.stateSelected;
        this.addNewShipping.emit(formData);
    };
    NewShippingFormComponent.prototype.fakeAddShippingAddress = function () {
        var formData = {
            name: "Tara Mctesterson",
            addressLine1: "11459 Huebner Rd",
            city: "San Antonio",
            state: "TX",
            zipCode: "78230"
        };
        this._shippingActions.fakeUpdateShippingAddressSuccess(formData);
    };
    __decorate([
        select(['shipping', 'status']), 
        __metadata('design:type', Observable)
    ], NewShippingFormComponent.prototype, "status", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], NewShippingFormComponent.prototype, "newShippingResponse", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewShippingFormComponent.prototype, "addNewShipping", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewShippingFormComponent.prototype, "clearErrors", void 0);
    NewShippingFormComponent = __decorate([
        Component({
            selector: 'new-shipping-form',
            providers: [ShippingActions, NestcareService, ShippoService, HttpClient, AppStatusActions],
            changeDetection: ChangeDetectionStrategy.OnPush,
            templateUrl: 'newshippingform.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, ShippingActions, NestcareService, FormBuilder, Router])
    ], NewShippingFormComponent);
    return NewShippingFormComponent;
}());
;
