import { Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { IShipping } from '../../../store'
import {StatesConstant} from "../statesPopover/states.constant";
import { Map } from 'immutable';

@Component({
    selector: 'new-shipping-form',
    providers: [ShippingActions, NestcareService, ShippoService, HttpClient, AppStatusActions],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'newshippingform.component.html'
})
export class NewShippingFormComponent {
    errorIsVisible: boolean;
    addAddressForm: FormGroup;
    public statesArray = StatesConstant;
    @select(['shipping', 'status']) status: Observable<string>;
    private phone: string;
    public stateSelected: any;

    @Input() newShippingResponse: IShipping;
    @Output() addNewShipping = new EventEmitter();
    @Output() clearErrors = new EventEmitter();

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _shippingActions: ShippingActions,
        private _nc: NestcareService,
        private formBuilder: FormBuilder,
        private router: Router
    ) {
        this.errorIsVisible = false;
        this.addAddressForm = formBuilder.group({
            'name': ['', Validators.required],
            'addressLine1': ['', Validators.required],
            'addressLine2': [''],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', [Validators.required, Validators.minLength(5)]]
        });

        let app_status = this.ngRedux.select(state => state.appStatus);

        app_status.subscribe(data => {
            if(data){
                this.phone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });

        this.addAddressForm.valueChanges
            .subscribe(data => this.onValueChanged());
    }


    ngOnInit() {
        this.status.subscribe(status =>
        {
            if(status === 'submitted'){
                this.errorIsVisible = true;
            }
        });
    }

    onValueChanged(){
        if(this.errorIsVisible){
            this.errorIsVisible = false;
            this.clearErrors.emit();
        }
    }

    addShippingAddress(){
        let formData = this.addAddressForm.value;
        console.log('this');
        console.log(this.phone);
        formData.phone = this.phone;
        formData.state = this.stateSelected;
        this.addNewShipping.emit(formData);
    }

    fakeAddShippingAddress(){
        let formData = {
            name: "Tara Mctesterson",
            addressLine1: "11459 Huebner Rd",
            city: "San Antonio",
            state: "TX",
            zipCode: "78230"
        };
        this._shippingActions.fakeUpdateShippingAddressSuccess(formData);
    }

};
