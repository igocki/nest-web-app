import { EventEmitter } from '@angular/core';
import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Observable } from 'rxjs/Observable';
import { IShipping } from '../../../store';
export declare class NewShippingFormComponent {
    private ngRedux;
    private _shippingActions;
    private _nc;
    private formBuilder;
    private router;
    errorIsVisible: boolean;
    addAddressForm: FormGroup;
    statesArray: {
        "name": string;
        "abbreviation": string;
    }[];
    status: Observable<string>;
    private phone;
    stateSelected: any;
    newShippingResponse: IShipping;
    addNewShipping: EventEmitter<{}>;
    clearErrors: EventEmitter<{}>;
    constructor(ngRedux: NgRedux<RootState>, _shippingActions: ShippingActions, _nc: NestcareService, formBuilder: FormBuilder, router: Router);
    ngOnInit(): void;
    onValueChanged(): void;
    addShippingAddress(): void;
    fakeAddShippingAddress(): void;
}
