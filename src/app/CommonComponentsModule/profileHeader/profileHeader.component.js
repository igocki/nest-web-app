var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { HttpClient } from '../../../services/http-header-service';
import { AppStatusActions } from '../../../actions/app-status-actions';
export var ProfileHeaderComponent = (function () {
    function ProfileHeaderComponent(ngRedux, _nc, _appStatusActions, router) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._appStatusActions = _appStatusActions;
        this.router = router;
    }
    ProfileHeaderComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "email", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "fullname", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "userStatus", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "userBirth", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "userGender", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "userPhone", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ProfileHeaderComponent.prototype, "accountType", void 0);
    ProfileHeaderComponent = __decorate([
        Component({
            selector: 'profile-header',
            providers: [NestcareService, HttpClient, AppStatusActions],
            changeDetection: ChangeDetectionStrategy.OnPush,
            templateUrl: 'profileHeader.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, AppStatusActions, Router])
    ], ProfileHeaderComponent);
    return ProfileHeaderComponent;
}());
