import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { AppStatusActions } from '../../../actions/app-status-actions';
export declare class ProfileHeaderComponent {
    private ngRedux;
    private _nc;
    private _appStatusActions;
    private router;
    email: any;
    fullname: any;
    userStatus: any;
    userBirth: any;
    userGender: any;
    userPhone: any;
    accountType: any;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _appStatusActions: AppStatusActions, router: Router);
    ngOnInit(): void;
}
