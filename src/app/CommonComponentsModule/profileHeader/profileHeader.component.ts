import { Component,
    Input,
    ChangeDetectionStrategy,
    Pipe,
    PipeTransform
} from '@angular/core';
import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { Map, List } from 'immutable';
import { AppStatusActions } from '../../../actions/app-status-actions';

@Component({
    selector: 'profile-header',
    providers: [NestcareService, HttpClient, AppStatusActions],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'profileHeader.component.html'
})



export class ProfileHeaderComponent {
    @Input() email: any;
    @Input() fullname: any;
    @Input() userStatus: any;
    @Input() userBirth: any;
    @Input() userGender: any;
    @Input() userPhone: any;
    @Input() accountType: any;

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _appStatusActions: AppStatusActions,
        private router: Router
    ) {

    }


    ngOnInit() {
    }

}


