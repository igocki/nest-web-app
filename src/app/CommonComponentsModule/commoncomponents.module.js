var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { LoginFormComponent } from './loginForm/loginform.component';
import { NCSignUpFormComponent } from './ncSignUpForm/ncsignupform.component';
import { NewShippingFormComponent } from './newShippingForm/newshippingform.component';
import { ShippingSelectorComponent } from './shippingSelector/shippingSelector.component';
import { ShippingRateSelectorComponent } from './shippingRateSelector/shippingRateSelector.component';
import { BackButtonComponent } from './backButton/backbutton.component';
import { ShipAddressComponent } from './ShipAddress/shipAddress.component';
import { TwoFactorFormComponent } from './twofactorForm/twofactorForm.component';
import { ProfileHeaderComponent } from './profileHeader/profileHeader.component';
import { PaymentSelectorComponent } from './paymentSelector/paymentSelector.component';
import { NewPaymentFormComponent } from './newPaymentForm/newPaymentForm.component';
import { OrderSummaryComponent } from './orderSummary/orderSummary.component';
import { ForgotFormComponent } from './forgotForm/forgotForm.component';
import { RouterModule } from "@angular/router";
import { StatesPopoverComponent } from "./statesPopover/statespopover.component";
import { TextMaskModule } from 'angular2-text-mask';
import { PhonePipe } from 'phoneNumberPipe';
import { HttpClient } from '../../services/http-header-service';
export var CommonComponentsModule = (function () {
    function CommonComponentsModule() {
    }
    CommonComponentsModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule,
                TextMaskModule
            ],
            exports: [
                LoginFormComponent,
                BackButtonComponent,
                ProfileHeaderComponent,
                StatesPopoverComponent,
                NCSignUpFormComponent,
                TwoFactorFormComponent,
                ShipAddressComponent,
                NewShippingFormComponent,
                ShippingSelectorComponent,
                ShippingRateSelectorComponent,
                PaymentSelectorComponent,
                PhonePipe,
                NewPaymentFormComponent,
                OrderSummaryComponent,
                ForgotFormComponent
            ],
            declarations: [
                LoginFormComponent,
                BackButtonComponent,
                ProfileHeaderComponent,
                StatesPopoverComponent,
                NCSignUpFormComponent,
                TwoFactorFormComponent,
                ShipAddressComponent,
                PhonePipe,
                NewShippingFormComponent,
                ShippingSelectorComponent,
                ShippingRateSelectorComponent,
                PaymentSelectorComponent,
                NewPaymentFormComponent,
                OrderSummaryComponent,
                ForgotFormComponent
            ],
            providers: [
                HttpClient
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CommonComponentsModule);
    return CommonComponentsModule;
}());
