import { Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { RootState } from '../../../store/index';
import { AuthActions } from '../../../actions/auth-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { IAuth } from '../../../store'
import { Map } from 'immutable';
@Component({
    selector: 'two-factor-form',
    providers: [AuthActions, NestcareService, HttpClient, UserService, RegisterService],
    templateUrl: 'twofactorForm.component.html'
})
export class TwoFactorFormComponent {
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;

    @Output() submitAuthCode = new EventEmitter();

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _authActions: AuthActions,
        private _nc: NestcareService,
        private builder: FormBuilder,
        private router: Router
    ) {
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'authCode': ['', Validators.required]
        });

    }


    ngOnInit() {
    }

    onValueChanged(){

    }

    submitThis(){
        let formData = this.loginForm.value;
        this.submitAuthCode.emit(formData);
        // let formData = this.loginForm.value;
        // console.log(this.loginForm.value);
        // if(formData.email === 'admin.user@mynest.care'){
        //     this.router.navigate(['/admin']);
        //     this.ngRedux.dispatch({
        //         type: 'CHANGE_TO_LOGGED_IN'
        //     });
        // } else {
        //     this._authActions.login(formData.email, formData.password, this.isSso);
        // }
    }

};
