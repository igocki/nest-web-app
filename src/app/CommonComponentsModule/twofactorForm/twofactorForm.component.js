var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter } from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
export var TwoFactorFormComponent = (function () {
    function TwoFactorFormComponent(ngRedux, _authActions, _nc, builder, router) {
        this.ngRedux = ngRedux;
        this._authActions = _authActions;
        this._nc = _nc;
        this.builder = builder;
        this.router = router;
        this.submitAuthCode = new EventEmitter();
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'authCode': ['', Validators.required]
        });
    }
    TwoFactorFormComponent.prototype.ngOnInit = function () {
    };
    TwoFactorFormComponent.prototype.onValueChanged = function () {
    };
    TwoFactorFormComponent.prototype.submitThis = function () {
        var formData = this.loginForm.value;
        this.submitAuthCode.emit(formData);
        // let formData = this.loginForm.value;
        // console.log(this.loginForm.value);
        // if(formData.email === 'admin.user@mynest.care'){
        //     this.router.navigate(['/admin']);
        //     this.ngRedux.dispatch({
        //         type: 'CHANGE_TO_LOGGED_IN'
        //     });
        // } else {
        //     this._authActions.login(formData.email, formData.password, this.isSso);
        // }
    };
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], TwoFactorFormComponent.prototype, "submitAuthCode", void 0);
    TwoFactorFormComponent = __decorate([
        Component({
            selector: 'two-factor-form',
            providers: [AuthActions, NestcareService, HttpClient, UserService, RegisterService],
            templateUrl: 'twofactorForm.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, AuthActions, NestcareService, FormBuilder, Router])
    ], TwoFactorFormComponent);
    return TwoFactorFormComponent;
}());
;
