import { EventEmitter } from '@angular/core';
import { RootState } from '../../../store/index';
import { AuthActions } from '../../../actions/auth-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { FormGroup, FormBuilder } from "@angular/forms";
export declare class TwoFactorFormComponent {
    private ngRedux;
    private _authActions;
    private _nc;
    private builder;
    private router;
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;
    submitAuthCode: EventEmitter<{}>;
    constructor(ngRedux: NgRedux<RootState>, _authActions: AuthActions, _nc: NestcareService, builder: FormBuilder, router: Router);
    ngOnInit(): void;
    onValueChanged(): void;
    submitThis(): void;
}
