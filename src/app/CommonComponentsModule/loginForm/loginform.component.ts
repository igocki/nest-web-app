import { Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import { RootState } from '../../../store/index';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { IAuth } from '../../../store'
import { Map } from 'immutable';
@Component({
    selector: 'login-form',
    providers: [AuthActions, NestcareService, HttpClient, AppStatusActions],
    templateUrl: 'loginform.component.html'
})
export class LoginFormComponent {
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;

    @select(['auth', 'status']) status: Observable<string>;
    @select(['auth', 'token']) token: Observable<string>;
    @select() auth$: Observable<IAuth>;
    @Input() authObject: IAuth;
    @Input() isSso: boolean;

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _authActions: AuthActions,
        private _nc: NestcareService,
        private builder: FormBuilder,
        private _appStatusActions: AppStatusActions,
        private router: Router
    ) {
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'email': ['', Validators.required],
            'password': ['', [Validators.required, Validators.minLength(8)]],
        });


        this.loginForm.valueChanges
            .subscribe(data => this.onValueChanged());
    }


    ngOnInit() {

        this.status.subscribe(status =>
        {
            if(status === 'submitted'){
                this.errorIsVisible = true;
            }

        });

        // this.auth$.subscribe(auth => {
        //     let auth2 = Map<string, IAuth>(auth);
        //     if(auth2 && auth2.toJS().status === 'authenticated' &&
        //         auth2.toJS().token){
        //         this.router.navigate(['/dashboard']);
        //     }
        // });

        this.token.subscribe(token =>
        {
            if(token){

            }
        });
    }

    onValueChanged(){
        if(this.errorIsVisible){
            this.errorIsVisible = false;
            this._authActions.clearErrors();
        }

    }

    submitThis(){
        let formData = this.loginForm.value;
        console.log(this.loginForm.value);
        if(formData.email === 'admin.user@mynest.care'){
            this._appStatusActions.clearTokens();
            this._authActions.adminLogin(formData.email, formData.password);
        } else {
            this._authActions.login(formData.email, formData.password, this.isSso);
        }
    }

};
