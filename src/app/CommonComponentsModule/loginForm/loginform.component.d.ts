import { RootState } from '../../../store/index';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Observable } from 'rxjs/Observable';
import { IAuth } from '../../../store';
export declare class LoginFormComponent {
    private ngRedux;
    private _authActions;
    private _nc;
    private builder;
    private _appStatusActions;
    private router;
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;
    status: Observable<string>;
    token: Observable<string>;
    auth$: Observable<IAuth>;
    authObject: IAuth;
    isSso: boolean;
    constructor(ngRedux: NgRedux<RootState>, _authActions: AuthActions, _nc: NestcareService, builder: FormBuilder, _appStatusActions: AppStatusActions, router: Router);
    ngOnInit(): void;
    onValueChanged(): void;
    submitThis(): void;
}
