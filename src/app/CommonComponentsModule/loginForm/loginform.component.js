var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
export var LoginFormComponent = (function () {
    function LoginFormComponent(ngRedux, _authActions, _nc, builder, _appStatusActions, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._authActions = _authActions;
        this._nc = _nc;
        this.builder = builder;
        this._appStatusActions = _appStatusActions;
        this.router = router;
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'email': ['', Validators.required],
            'password': ['', [Validators.required, Validators.minLength(8)]],
        });
        this.loginForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(); });
    }
    LoginFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.status.subscribe(function (status) {
            if (status === 'submitted') {
                _this.errorIsVisible = true;
            }
        });
        // this.auth$.subscribe(auth => {
        //     let auth2 = Map<string, IAuth>(auth);
        //     if(auth2 && auth2.toJS().status === 'authenticated' &&
        //         auth2.toJS().token){
        //         this.router.navigate(['/dashboard']);
        //     }
        // });
        this.token.subscribe(function (token) {
            if (token) {
            }
        });
    };
    LoginFormComponent.prototype.onValueChanged = function () {
        if (this.errorIsVisible) {
            this.errorIsVisible = false;
            this._authActions.clearErrors();
        }
    };
    LoginFormComponent.prototype.submitThis = function () {
        var formData = this.loginForm.value;
        console.log(this.loginForm.value);
        if (formData.email === 'admin.user@mynest.care') {
            this._appStatusActions.clearTokens();
            this._authActions.adminLogin(formData.email, formData.password);
        }
        else {
            this._authActions.login(formData.email, formData.password, this.isSso);
        }
    };
    __decorate([
        select(['auth', 'status']), 
        __metadata('design:type', Observable)
    ], LoginFormComponent.prototype, "status", void 0);
    __decorate([
        select(['auth', 'token']), 
        __metadata('design:type', Observable)
    ], LoginFormComponent.prototype, "token", void 0);
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], LoginFormComponent.prototype, "auth$", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], LoginFormComponent.prototype, "authObject", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], LoginFormComponent.prototype, "isSso", void 0);
    LoginFormComponent = __decorate([
        Component({
            selector: 'login-form',
            providers: [AuthActions, NestcareService, HttpClient, AppStatusActions],
            templateUrl: 'loginform.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, AuthActions, NestcareService, FormBuilder, AppStatusActions, Router])
    ], LoginFormComponent);
    return LoginFormComponent;
}());
;
