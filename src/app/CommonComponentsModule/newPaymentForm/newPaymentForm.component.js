var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { StatesConstant } from "../statesPopover/states.constant";
export var NewPaymentFormComponent = (function () {
    function NewPaymentFormComponent(ngRedux, _nc, formBuilder, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.formBuilder = formBuilder;
        this.router = router;
        this.statesArray = StatesConstant;
        this.cardMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
        this.twoMask = [/\d/, /\d/];
        this.fourMask = [/\d/, /\d/, /\d/, /\d/];
        this.fiveMask = [/\d/, /\d/, /\d/, /\d/, /\d/];
        this.something = 'somethingsds';
        this.loggedInSubmitNewPayment = new EventEmitter();
        this.submitNewPayment = new EventEmitter();
        this.clearErrors = new EventEmitter();
        this.sameAsShipping = new EventEmitter();
        this.changeState = new EventEmitter();
        this.raiseError = new EventEmitter();
        this.removeCurrentCard = new EventEmitter();
        this.errorIsVisible = false;
        this.addPaymentForm = formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'cardNumber': ['', [Validators.required, Validators.minLength(14)]],
            'expirationMonth': ['', [Validators.required, Validators.minLength(2)]],
            'expirationYear': ['', [Validators.required, Validators.minLength(4)]],
            // 'cvc': ['', [Validators.required, Validators.minLength(3)]],
            'addressLine1': ['', Validators.required],
            'addressLine2': [''],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', [Validators.required, Validators.minLength(5)]]
        });
        this.addPaymentForm.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(); });
    }
    NewPaymentFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.status.subscribe(function (status) {
            if (status === 'submitted') {
                _this.errorIsVisible = true;
            }
        });
    };
    NewPaymentFormComponent.prototype.onValueChanged = function () {
        if (this.errorIsVisible) {
            this.errorIsVisible = false;
            this.clearErrors.emit();
        }
    };
    NewPaymentFormComponent.prototype.onSubmit = function () {
        console.log('SUBMITTED');
    };
    NewPaymentFormComponent.prototype.completeNewCard = function () {
        if (this.loggedInChangeBilling) {
            this.loggedInSubmitNewPayment.emit();
        }
        else {
            this.submitNewPayment.emit();
        }
    };
    NewPaymentFormComponent.prototype.addShippingAddress = function () {
        var formData = this.addPaymentForm.value;
        //this._shippingActions.fakeUpdateShippingAddressSuccess(formData);
    };
    __decorate([
        select(['billing', 'status']), 
        __metadata('design:type', Observable)
    ], NewPaymentFormComponent.prototype, "status", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "loggedInSubmitNewPayment", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "submitNewPayment", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "clearErrors", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "sameAsShipping", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "changeState", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "raiseError", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "removeCurrentCard", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', String)
    ], NewPaymentFormComponent.prototype, "errorsText", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NewPaymentFormComponent.prototype, "defaultShipping", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NewPaymentFormComponent.prototype, "shippingChecked", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "errorForm", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', String)
    ], NewPaymentFormComponent.prototype, "stateSelect", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], NewPaymentFormComponent.prototype, "customerShippingValues", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NewPaymentFormComponent.prototype, "loggedInChangeBilling", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NewPaymentFormComponent.prototype, "enrollNewPlan", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], NewPaymentFormComponent.prototype, "noSameAsShipping", void 0);
    NewPaymentFormComponent = __decorate([
        Component({
            selector: 'new-payment-form',
            providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
            templateUrl: 'newPaymentForm.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, FormBuilder, Router])
    ], NewPaymentFormComponent);
    return NewPaymentFormComponent;
}());
;
