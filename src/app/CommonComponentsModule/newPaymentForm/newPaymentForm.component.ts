import { Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';
import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { IShipping } from '../../../store'
import {StatesConstant} from "../statesPopover/states.constant";
import { Map } from 'immutable';

@Component({
    selector: 'new-payment-form',
    providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
    templateUrl: 'newPaymentForm.component.html'
})
export class NewPaymentFormComponent {
    errorIsVisible: boolean;
    addPaymentForm: FormGroup;
    public statesArray = StatesConstant;
    public cardMask = [ /\d/, /\d/, /\d/, /\d/,/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
    public twoMask = [ /\d/, /\d/];
    public fourMask = [ /\d/, /\d/, /\d/, /\d/];
    public fiveMask = [ /\d/, /\d/, /\d/, /\d/, /\d/];
    public something = 'somethingsds';


    @select(['billing', 'status']) status: Observable<string>;
    @Output() loggedInSubmitNewPayment = new EventEmitter();
    @Output() submitNewPayment = new EventEmitter();
    @Output() clearErrors = new EventEmitter();
    @Output() sameAsShipping = new EventEmitter();
    @Output() changeState = new EventEmitter();
    @Output() raiseError = new EventEmitter();
    @Output() removeCurrentCard = new EventEmitter();
    @Input() errorsText: string;
    @Input() defaultShipping: boolean;
    @Input() shippingChecked: boolean;
    @Input() errorForm: any;
    @Input() stateSelect: string;
    @Input() customerShippingValues: any;
    @Input() loggedInChangeBilling: boolean;
    @Input() enrollNewPlan: boolean;
    @Input() noSameAsShipping: boolean;
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private formBuilder: FormBuilder,
        private router: Router
    ) {
        this.errorIsVisible = false;
        this.addPaymentForm = formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'cardNumber': ['', [Validators.required, Validators.minLength(14)]],
            'expirationMonth': ['', [Validators.required, Validators.minLength(2)]],
            'expirationYear': ['', [Validators.required, Validators.minLength(4)]],
            // 'cvc': ['', [Validators.required, Validators.minLength(3)]],
            'addressLine1': ['', Validators.required],
            'addressLine2': [''],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipCode': ['', [Validators.required, Validators.minLength(5)]]
        });

        this.addPaymentForm.valueChanges
            .subscribe(data => this.onValueChanged());

    }


    ngOnInit() {
        this.status.subscribe(status =>
        {
            if(status === 'submitted'){
                this.errorIsVisible = true;
            }
        });
    }

    onValueChanged(){
        if(this.errorIsVisible){
            this.errorIsVisible = false;
            this.clearErrors.emit();
        }
    }

    onSubmit(){
        console.log('SUBMITTED');
    }

    completeNewCard(){

        if(this.loggedInChangeBilling){
            this.loggedInSubmitNewPayment.emit();
        } else {
            this.submitNewPayment.emit();
        }



    }

    addShippingAddress(){
        let formData = this.addPaymentForm.value;
        //this._shippingActions.fakeUpdateShippingAddressSuccess(formData);
    }

    // fakeAddNewPayment(){
    //     let formData = {
    //         firstName: "Tara",
    //         lastName: "Mctesterson",
    //         cardNumber: "4242555542425555",
    //         expirationMonth: "09",
    //         expirationYear: "18",
    //         addressLine1: "1234 Maintest Avenue",
    //         addressLine2: "APT 103",
    //         city: "Nowhereville",
    //         state: "TX",
    //         zipCode: "78214"
    //     };
    //     return formData;
    //     // this._shippingActions.fakeUpdateShippingAddressSuccess(formData);
    // }

};
