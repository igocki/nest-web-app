import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { IonicApp, IonicModule } from 'ionic-angular';
import { LoginFormComponent } from './loginForm/loginform.component';
import { NCSignUpFormComponent } from './ncSignUpForm/ncsignupform.component';
import { NewShippingFormComponent } from './newShippingForm/newshippingform.component';
import { ShippingSelectorComponent } from './shippingSelector/shippingSelector.component'
import { ShippingRateSelectorComponent } from './shippingRateSelector/shippingRateSelector.component'
import { BackButtonComponent } from './backButton/backbutton.component';
import { ShipAddressComponent } from './ShipAddress/shipAddress.component';
import { TwoFactorFormComponent } from './twofactorForm/twofactorForm.component';
import { ProfileHeaderComponent } from './profileHeader/profileHeader.component';
import { PaymentSelectorComponent} from './paymentSelector/paymentSelector.component';
import { NewPaymentFormComponent} from './newPaymentForm/newPaymentForm.component';
import { OrderSummaryComponent } from './orderSummary/orderSummary.component';
import { ForgotFormComponent } from './forgotForm/forgotForm.component';
import {RouterModule} from "@angular/router";
import {StatesPopoverComponent} from "./statesPopover/statespopover.component";
import { TextMaskModule } from 'angular2-text-mask';
import { PhonePipe } from 'phoneNumberPipe';
import {HttpClient} from '../../services/http-header-service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule,
        TextMaskModule
    ],
    exports: [
        LoginFormComponent,
        BackButtonComponent,
        ProfileHeaderComponent,
        StatesPopoverComponent,
        NCSignUpFormComponent,
        TwoFactorFormComponent,
        ShipAddressComponent,
        NewShippingFormComponent,
        ShippingSelectorComponent,
        ShippingRateSelectorComponent,
        PaymentSelectorComponent,
        PhonePipe,
        NewPaymentFormComponent,
        OrderSummaryComponent,
        ForgotFormComponent
    ],
    declarations: [
        LoginFormComponent,
        BackButtonComponent,
        ProfileHeaderComponent,
        StatesPopoverComponent,
        NCSignUpFormComponent,
        TwoFactorFormComponent,
        ShipAddressComponent,
        PhonePipe,
        NewShippingFormComponent,
        ShippingSelectorComponent,
        ShippingRateSelectorComponent,
        PaymentSelectorComponent,
        NewPaymentFormComponent,
        OrderSummaryComponent,
        ForgotFormComponent
    ],
    providers: [
        HttpClient
    ]
})

export class CommonComponentsModule {}
