import { Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { AppStatusActions } from '../../../actions/app-status-actions';
@Component({
    selector: 'shipping-rate-selector',
    providers: [NestcareService, ShippoService, HttpClient, AppStatusActions],
    templateUrl: 'shippingRateSelector.component.html'
})
export class ShippingRateSelectorComponent {
    @select(['shipping', 'status']) status: Observable<string>;
    @select(['shipping']) shippingData: Observable<any>;
    ratesForm: FormGroup;
    @Input() rateItems: List<any>;
    @Input() selectedRate: Map<string, any>;
    @Output() rateSelect = new EventEmitter();

    public options = [
        { selected: true },
        { selected: false },
        { selected: false }
    ];

    public shipOptions = [{
        description: 'UPS Next-day Air',
        selected: false
    },
    {
        description: 'Fedex',
        selected: false
    },
    {
        description: 'Fedex EXPRESS',
        selected: false
    }];

    constructor(
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _appStatusActions: AppStatusActions,
        private router: Router,
        private formBuilder: FormBuilder
    ) {
        this.ratesForm = formBuilder.group({
            'rateSelected': ['', Validators.required]
        });

    }


    ngOnInit() {

    }

    ngOnChanges(){
        if(this.rateItems && this.selectedRate){
            let matchedRate = this.rateItems.filter(x => x.servicelevel_token === this.selectedRate.get('servicelevel_token')).first();
            if(matchedRate){
                this.ratesForm.controls['rateSelected'].setValue(matchedRate.object_id);
            }
        }
    }

    useThisAddress(address){
        let shippingAddress = address;
        this._appStatusActions.storeShippingAddress(shippingAddress);
    }

    filterSelectedRate(){
        let selected = this.rateItems.filter(x => x.object_id === this.ratesForm.value.rateSelected).first();
        return selected;
    }

};
