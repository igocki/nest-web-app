var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Map, List } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, Validators } from "@angular/forms";
import { AppStatusActions } from '../../../actions/app-status-actions';
export var ShippingRateSelectorComponent = (function () {
    function ShippingRateSelectorComponent(ngRedux, _nc, _appStatusActions, router, formBuilder) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._appStatusActions = _appStatusActions;
        this.router = router;
        this.formBuilder = formBuilder;
        this.rateSelect = new EventEmitter();
        this.options = [
            { selected: true },
            { selected: false },
            { selected: false }
        ];
        this.shipOptions = [{
                description: 'UPS Next-day Air',
                selected: false
            },
            {
                description: 'Fedex',
                selected: false
            },
            {
                description: 'Fedex EXPRESS',
                selected: false
            }];
        this.ratesForm = formBuilder.group({
            'rateSelected': ['', Validators.required]
        });
    }
    ShippingRateSelectorComponent.prototype.ngOnInit = function () {
    };
    ShippingRateSelectorComponent.prototype.ngOnChanges = function () {
        var _this = this;
        if (this.rateItems && this.selectedRate) {
            var matchedRate = this.rateItems.filter(function (x) { return x.servicelevel_token === _this.selectedRate.get('servicelevel_token'); }).first();
            if (matchedRate) {
                this.ratesForm.controls['rateSelected'].setValue(matchedRate.object_id);
            }
        }
    };
    ShippingRateSelectorComponent.prototype.useThisAddress = function (address) {
        var shippingAddress = address;
        this._appStatusActions.storeShippingAddress(shippingAddress);
    };
    ShippingRateSelectorComponent.prototype.filterSelectedRate = function () {
        var _this = this;
        var selected = this.rateItems.filter(function (x) { return x.object_id === _this.ratesForm.value.rateSelected; }).first();
        return selected;
    };
    __decorate([
        select(['shipping', 'status']), 
        __metadata('design:type', Observable)
    ], ShippingRateSelectorComponent.prototype, "status", void 0);
    __decorate([
        select(['shipping']), 
        __metadata('design:type', Observable)
    ], ShippingRateSelectorComponent.prototype, "shippingData", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ShippingRateSelectorComponent.prototype, "rateItems", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ShippingRateSelectorComponent.prototype, "selectedRate", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], ShippingRateSelectorComponent.prototype, "rateSelect", void 0);
    ShippingRateSelectorComponent = __decorate([
        Component({
            selector: 'shipping-rate-selector',
            providers: [NestcareService, ShippoService, HttpClient, AppStatusActions],
            templateUrl: 'shippingRateSelector.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, AppStatusActions, Router, FormBuilder])
    ], ShippingRateSelectorComponent);
    return ShippingRateSelectorComponent;
}());
;
