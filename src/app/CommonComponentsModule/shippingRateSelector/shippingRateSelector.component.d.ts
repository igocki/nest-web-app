import { EventEmitter } from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder } from "@angular/forms";
import { AppStatusActions } from '../../../actions/app-status-actions';
export declare class ShippingRateSelectorComponent {
    private ngRedux;
    private _nc;
    private _appStatusActions;
    private router;
    private formBuilder;
    status: Observable<string>;
    shippingData: Observable<any>;
    ratesForm: FormGroup;
    rateItems: List<any>;
    selectedRate: Map<string, any>;
    rateSelect: EventEmitter<{}>;
    options: {
        selected: boolean;
    }[];
    shipOptions: {
        description: string;
        selected: boolean;
    }[];
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _appStatusActions: AppStatusActions, router: Router, formBuilder: FormBuilder);
    ngOnInit(): void;
    ngOnChanges(): void;
    useThisAddress(address: any): void;
    filterSelectedRate(): any;
}
