import { Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Router } from '@angular/router';
@Component({
    selector: 'ship-address',
    providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
    templateUrl: 'shipAddress.component.html'
})
export class ShipAddressComponent {
    @Input() shipAddress: Map<string, any>;

    constructor(
        private ngRedux: NgRedux<RootState>
    ) {


    }


    ngOnInit() {
    }

    ngOnChanges(){

    }



};

