import { Map } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
export declare class ShipAddressComponent {
    private ngRedux;
    shipAddress: Map<string, any>;
    constructor(ngRedux: NgRedux<RootState>);
    ngOnInit(): void;
    ngOnChanges(): void;
}
