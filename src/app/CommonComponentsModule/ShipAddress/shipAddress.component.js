var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Map } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import { AppStatusActions } from '../../../actions/app-status-actions';
export var ShipAddressComponent = (function () {
    function ShipAddressComponent(ngRedux) {
        this.ngRedux = ngRedux;
    }
    ShipAddressComponent.prototype.ngOnInit = function () {
    };
    ShipAddressComponent.prototype.ngOnChanges = function () {
    };
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ShipAddressComponent.prototype, "shipAddress", void 0);
    ShipAddressComponent = __decorate([
        Component({
            selector: 'ship-address',
            providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
            templateUrl: 'shipAddress.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux])
    ], ShipAddressComponent);
    return ShipAddressComponent;
}());
;
