import { Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { IShipping } from '../../../store'
import { AppStatusActions } from '../../../actions/app-status-actions';
@Component({
    selector: 'shipping-selector',
    providers: [ShippingActions, NestcareService, ShippoService, HttpClient, AppStatusActions],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'shippingSelector.component.html'
})
export class ShippingSelectorComponent {
    @select(['shipping', 'status']) status: Observable<string>;
    @select(['shipping']) shippingData: Observable<any>;

    @Input() newShippingResponse: IShipping;
    @Input() loggedInChangeShipping: boolean;
    @Input() enrollNewPlan: boolean;
    constructor(
        private ngRedux: NgRedux<RootState>,
        private _shippingActions: ShippingActions,
        private _nc: NestcareService,
        private _appStatusActions: AppStatusActions,
        private router: Router
    ) {

    }


    ngOnInit() {
    }

    changeShipping(){
        this._shippingActions.changeShipping();
    }

    submitThisAddressReturntoAccount(address){
        console.log(address);
        this.ngRedux.dispatch({
            type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
            payload: {
                city_id: address.get('city'),
                zip_code: address.get('zipCode'),
                state_id: address.get('state'),
                line1: address.get('addressLine1'),
                line2: address.get('addressLine2'),
                name: address.get('name')
            }
        });
        this.router.navigate(['/account/ship']);
    }

    useThisAddress(address){
        let shippingAddress = address;
        this._appStatusActions.storeShippingAddress(shippingAddress);
    }

};
