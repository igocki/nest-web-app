import { RootState } from '../../../store/index';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { Observable } from 'rxjs/Observable';
import { IShipping } from '../../../store';
import { AppStatusActions } from '../../../actions/app-status-actions';
export declare class ShippingSelectorComponent {
    private ngRedux;
    private _shippingActions;
    private _nc;
    private _appStatusActions;
    private router;
    status: Observable<string>;
    shippingData: Observable<any>;
    newShippingResponse: IShipping;
    loggedInChangeShipping: boolean;
    enrollNewPlan: boolean;
    constructor(ngRedux: NgRedux<RootState>, _shippingActions: ShippingActions, _nc: NestcareService, _appStatusActions: AppStatusActions, router: Router);
    ngOnInit(): void;
    changeShipping(): void;
    submitThisAddressReturntoAccount(address: any): void;
    useThisAddress(address: any): void;
}
