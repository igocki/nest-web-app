var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { ShippingActions } from '../../../actions/shipping-actions';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { ShippoService } from '../../../services/shippoService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { AppStatusActions } from '../../../actions/app-status-actions';
export var ShippingSelectorComponent = (function () {
    function ShippingSelectorComponent(ngRedux, _shippingActions, _nc, _appStatusActions, router) {
        this.ngRedux = ngRedux;
        this._shippingActions = _shippingActions;
        this._nc = _nc;
        this._appStatusActions = _appStatusActions;
        this.router = router;
    }
    ShippingSelectorComponent.prototype.ngOnInit = function () {
    };
    ShippingSelectorComponent.prototype.changeShipping = function () {
        this._shippingActions.changeShipping();
    };
    ShippingSelectorComponent.prototype.submitThisAddressReturntoAccount = function (address) {
        console.log(address);
        this.ngRedux.dispatch({
            type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
            payload: {
                city_id: address.get('city'),
                zip_code: address.get('zipCode'),
                state_id: address.get('state'),
                line1: address.get('addressLine1'),
                line2: address.get('addressLine2'),
                name: address.get('name')
            }
        });
        this.router.navigate(['/account/ship']);
    };
    ShippingSelectorComponent.prototype.useThisAddress = function (address) {
        var shippingAddress = address;
        this._appStatusActions.storeShippingAddress(shippingAddress);
    };
    __decorate([
        select(['shipping', 'status']), 
        __metadata('design:type', Observable)
    ], ShippingSelectorComponent.prototype, "status", void 0);
    __decorate([
        select(['shipping']), 
        __metadata('design:type', Observable)
    ], ShippingSelectorComponent.prototype, "shippingData", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], ShippingSelectorComponent.prototype, "newShippingResponse", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], ShippingSelectorComponent.prototype, "loggedInChangeShipping", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], ShippingSelectorComponent.prototype, "enrollNewPlan", void 0);
    ShippingSelectorComponent = __decorate([
        Component({
            selector: 'shipping-selector',
            providers: [ShippingActions, NestcareService, ShippoService, HttpClient, AppStatusActions],
            changeDetection: ChangeDetectionStrategy.OnPush,
            templateUrl: 'shippingSelector.component.html'
        }), 
        __metadata('design:paramtypes', [NgRedux, ShippingActions, NestcareService, AppStatusActions, Router])
    ], ShippingSelectorComponent);
    return ShippingSelectorComponent;
}());
;
