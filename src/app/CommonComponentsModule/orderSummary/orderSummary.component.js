var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Map } from 'immutable';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
export var OrderSummaryComponent = (function () {
    function OrderSummaryComponent() {
        // @Input() selectedCard: Map<string, any>;
        this.fulfillOrder = new EventEmitter();
        this.redeemPromo = new EventEmitter();
    }
    OrderSummaryComponent.prototype.ngOnInit = function () {
        if (this.orderSummary && this.orderSummary.get('orderCart')) {
            var cartList = this.orderSummary.get('orderCart');
            this.totalSummary = cartList.reduce(function (total, value) { return total + value.get('amt'); }, 0);
            if (this.shippingRate) {
                this.shippingAmount = (parseFloat(this.shippingRate.get('amount'))) ? parseFloat(this.shippingRate.get('amount')) : 0;
            }
        }
        else {
            this.totalSummary = 0;
        }
    };
    OrderSummaryComponent.prototype.ngOnChanges = function () {
    };
    OrderSummaryComponent.prototype.submitFinalOrder = function () {
        console.log('final order clicked');
        var fulfillObject = {
            plan_id: this.selectedPlan.get('id'),
            shipAmount: this.shippingRate.get('amount'),
            shippingType: this.shippingRate.get('servicelevel_name'),
            securityDeposit: this.selectedPlan.get('securityDeposit'),
            setupFee: this.selectedPlan.get('setupFee'),
            shippingRateId: this.shippingRate.get('object_id'),
            amt: this.selectedPlan.get('amt')
        };
        this.fulfillOrder.emit(fulfillObject);
    };
    __decorate([
        select(['billing', 'status']), 
        __metadata('design:type', Observable)
    ], OrderSummaryComponent.prototype, "status", void 0);
    __decorate([
        select(['billing']), 
        __metadata('design:type', Observable)
    ], OrderSummaryComponent.prototype, "shippingData", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "orderSummary", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "shippingAddress", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "shippingRate", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "billingAddress", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "selectedCard", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "selectedPlan", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Number)
    ], OrderSummaryComponent.prototype, "shippedItems", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "fulfillOrder", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], OrderSummaryComponent.prototype, "redeemPromo", void 0);
    OrderSummaryComponent = __decorate([
        Component({
            selector: 'order-summary',
            providers: [HttpClient],
            templateUrl: 'orderSummary.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], OrderSummaryComponent);
    return OrderSummaryComponent;
}());
;
