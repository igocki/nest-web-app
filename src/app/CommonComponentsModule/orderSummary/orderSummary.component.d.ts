import { EventEmitter } from '@angular/core';
import { Map } from 'immutable';
import { Observable } from 'rxjs/Observable';
export declare class OrderSummaryComponent {
    totalSummary: number;
    shippingAmount: number;
    cartList: any;
    status: Observable<string>;
    shippingData: Observable<any>;
    orderSummary: Map<string, any>;
    shippingAddress: Map<string, any>;
    shippingRate: Map<string, any>;
    billingAddress: Map<string, any>;
    selectedCard: Map<string, any>;
    selectedPlan: Map<string, any>;
    shippedItems: number;
    fulfillOrder: EventEmitter<{}>;
    redeemPromo: EventEmitter<{}>;
    constructor();
    ngOnInit(): void;
    ngOnChanges(): void;
    submitFinalOrder(): void;
}
