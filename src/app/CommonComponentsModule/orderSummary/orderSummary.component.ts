import { Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'order-summary',
    providers: [HttpClient],
    templateUrl: 'orderSummary.component.html'
})
export class OrderSummaryComponent {
    totalSummary: number;
    shippingAmount: number;
    cartList: any;

    @select(['billing', 'status']) status: Observable<string>;
    @select(['billing']) shippingData: Observable<any>;
    @Input() orderSummary: Map<string, any>;
    @Input() shippingAddress: Map<string, any>;
    @Input() shippingRate: Map<string, any>;
    @Input() billingAddress: Map<string, any>;
    @Input() selectedCard: Map<string, any>;
    @Input() selectedPlan: Map<string, any>;
    @Input() shippedItems: number;
    // @Input() selectedCard: Map<string, any>;
    @Output() fulfillOrder = new EventEmitter();
    @Output() redeemPromo = new EventEmitter();

    constructor(
    ) {


    }


    ngOnInit() {

        if(this.orderSummary && this.orderSummary.get('orderCart')){
            let cartList = this.orderSummary.get('orderCart');
            this.totalSummary = cartList.reduce((total, value) => total + value.get('amt'), 0);
            if(this.shippingRate){
                this.shippingAmount = (parseFloat(this.shippingRate.get('amount'))) ? parseFloat(this.shippingRate.get('amount')) : 0;
            }
       } else {
            this.totalSummary = 0;
        }
    }

    ngOnChanges(){

    }

    submitFinalOrder(){
        console.log('final order clicked');
        let fulfillObject = {
            plan_id: this.selectedPlan.get('id'),
            shipAmount: this.shippingRate.get('amount'),
            shippingType: this.shippingRate.get('servicelevel_name'),
            securityDeposit: this.selectedPlan.get('securityDeposit'),
            setupFee: this.selectedPlan.get('setupFee'),
            shippingRateId: this.shippingRate.get('object_id'),
            amt: this.selectedPlan.get('amt')
        };
        this.fulfillOrder.emit(fulfillObject);
    }


};
