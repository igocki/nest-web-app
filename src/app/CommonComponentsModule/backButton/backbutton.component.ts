import {Component, Input} from "@angular/core";

@Component({
    selector: 'back-button',
    template: `<button ion-button clear color="secondary" routerLink="{{ routerLink }}" class="button-length spacer-top-2"><ion-icon name="arrow-back"></ion-icon>Go Back</button>`
})
export class BackButtonComponent {
    @Input() routerLink: any;
}
