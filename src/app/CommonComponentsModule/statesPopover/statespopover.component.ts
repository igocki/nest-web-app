
import {ViewController, NavParams} from "ionic-angular";
import {Component} from "@angular/core";
import {StatesConstant} from "./states.constant";

@Component({
    template: `
    <ion-list no-lines>
      <button ion-item *ngFor="let state of states" (click)="selectState(state)">{{ state.name }}</button>
    </ion-list>
  `
})

export class StatesPopoverComponent {

    private states = StatesConstant;
    private callback: (state: any) => void;

    constructor(
        public viewCtrl: ViewController,
        private params: NavParams
    ) {
        this.callback = this.params.get('callback');
    }

    selectState(state: any) {
        this.callback(state);
        this.viewCtrl.dismiss();
    }
}
