var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ViewController, NavParams } from "ionic-angular";
import { Component } from "@angular/core";
import { StatesConstant } from "./states.constant";
export var StatesPopoverComponent = (function () {
    function StatesPopoverComponent(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.states = StatesConstant;
        this.callback = this.params.get('callback');
    }
    StatesPopoverComponent.prototype.selectState = function (state) {
        this.callback(state);
        this.viewCtrl.dismiss();
    };
    StatesPopoverComponent = __decorate([
        Component({
            template: "\n    <ion-list no-lines>\n      <button ion-item *ngFor=\"let state of states\" (click)=\"selectState(state)\">{{ state.name }}</button>\n    </ion-list>\n  "
        }), 
        __metadata('design:paramtypes', [ViewController, NavParams])
    ], StatesPopoverComponent);
    return StatesPopoverComponent;
}());
