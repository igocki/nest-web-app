import { ViewController, NavParams } from "ionic-angular";
export declare class StatesPopoverComponent {
    viewCtrl: ViewController;
    private params;
    private states;
    private callback;
    constructor(viewCtrl: ViewController, params: NavParams);
    selectState(state: any): void;
}
