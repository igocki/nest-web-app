import { EventEmitter } from '@angular/core';
import { NestcareService } from '../../../services/nestcareService';
import { FormGroup, FormBuilder } from "@angular/forms";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store/index';
export declare class ForgotFormComponent {
    private builder;
    private ngRedux;
    private _nc;
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;
    submitted: boolean;
    error: string;
    successVisible: boolean;
    submitAuthCode: EventEmitter<{}>;
    constructor(builder: FormBuilder, ngRedux: NgRedux<RootState>, _nc: NestcareService);
    ngOnInit(): void;
    onValueChanged(): void;
    submitThis(): void;
}
