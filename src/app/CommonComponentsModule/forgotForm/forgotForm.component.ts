import { Component,
    Output,
    EventEmitter
} from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {LoadingActions} from "../../../actions/loading-actions";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store/index';

@Component({
    selector: 'forgot-form',
    providers: [AuthActions, NestcareService, HttpClient, UserService],
    templateUrl: 'forgotForm.component.html'
})
export class ForgotFormComponent {
    model: any;
    loginForm: FormGroup;
    errorIsVisible: boolean;
    submitted: boolean;
    error: string;
    successVisible: boolean;

    @Output() submitAuthCode = new EventEmitter();

    constructor(private builder: FormBuilder, private ngRedux: NgRedux<RootState>, private _nc: NestcareService) {
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'email': ['', Validators.required]
        });
        this.error = '';
        this.submitted = false;
        this.successVisible = false;
    }


    ngOnInit() {
    }

    onValueChanged(){

    }

    submitThis(){
        let formData = this.loginForm.value;
        this.submitted = true;
        this.errorIsVisible = false;
        this.error = '';

        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this._nc.resetPassword(formData.email).subscribe(resp => {
            console.log(resp);
            console.log('PIECE OF IRANIAN GARBAGE.');
            let comback = resp.json();

            if (comback.message) {
                this.error = comback.message;
                this.errorIsVisible = true;
                this.submitted = false;
                this.successVisible = false;
            } else {
                this.successVisible = true;
            }

            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    }

};
