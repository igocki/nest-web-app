var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter } from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { FormBuilder, Validators } from "@angular/forms";
import { LoadingActions } from "../../../actions/loading-actions";
import { NgRedux } from 'ng2-redux';
export var ForgotFormComponent = (function () {
    function ForgotFormComponent(builder, ngRedux, _nc) {
        this.builder = builder;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.submitAuthCode = new EventEmitter();
        this.errorIsVisible = false;
        this.loginForm = builder.group({
            'email': ['', Validators.required]
        });
        this.error = '';
        this.submitted = false;
        this.successVisible = false;
    }
    ForgotFormComponent.prototype.ngOnInit = function () {
    };
    ForgotFormComponent.prototype.onValueChanged = function () {
    };
    ForgotFormComponent.prototype.submitThis = function () {
        var _this = this;
        var formData = this.loginForm.value;
        this.submitted = true;
        this.errorIsVisible = false;
        this.error = '';
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.resetPassword(formData.email).subscribe(function (resp) {
            console.log(resp);
            console.log('PIECE OF IRANIAN GARBAGE.');
            var comback = resp.json();
            if (comback.message) {
                _this.error = comback.message;
                _this.errorIsVisible = true;
                _this.submitted = false;
                _this.successVisible = false;
            }
            else {
                _this.successVisible = true;
            }
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], ForgotFormComponent.prototype, "submitAuthCode", void 0);
    ForgotFormComponent = __decorate([
        Component({
            selector: 'forgot-form',
            providers: [AuthActions, NestcareService, HttpClient, UserService],
            templateUrl: 'forgotForm.component.html'
        }), 
        __metadata('design:paramtypes', [FormBuilder, NgRedux, NestcareService])
    ], ForgotFormComponent);
    return ForgotFormComponent;
}());
;
