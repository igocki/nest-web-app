export declare class NCMeasurementFilter {
    private type;
    constructor(type: any);
    static checkIfDone(objectMeasurement: any): boolean;
    static checkIfSkipped(objectMeasurement: any): boolean;
    static checkIfMissed(objectMeasurement: any, currentDate: any, measurementDate: any): boolean;
    static checkIfAllow(objectMeasurement: any, currentDate: any, measurementDate: any): boolean;
    private static checkIfBlock(objectMeasurement, currentDate, measurementDate);
    static checkMeasurement(objectMeasurement: any, checkFor: string, time: Date, type: string): boolean;
    transform(value: any, args: any): any;
}
