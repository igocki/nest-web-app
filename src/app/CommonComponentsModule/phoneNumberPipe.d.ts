import { PipeTransform } from '@angular/core';
export declare class PhonePipe implements PipeTransform {
    transform(tel: any, args: any): any;
}
