import { Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Router } from '@angular/router';
@Component({
    selector: 'payment-selector',
    providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
    templateUrl: 'paymentSelector.component.html'
})
export class PaymentSelectorComponent {
    @select(['billing', 'status']) status: Observable<string>;
    @select(['billing']) shippingData: Observable<any>;
    cardsForm: FormGroup;
    @Input() cardItems: List<any>;
    @Input() selectedCard: Map<string, any>;
    @Input() errorForm: any;
    @Input() loggedInChangeBilling: boolean;
    @Input() enrollNewPlan: boolean;
    @Output() cardSelect = new EventEmitter();
    @Output() saveBilling = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder,
        private ngRedux: NgRedux<RootState>
    ) {
        this.cardsForm = formBuilder.group({
            'cardSelected': ['', Validators.required]
        });

    }


    ngOnInit() {
    }

    ngOnChanges(){
        if(this.cardItems && this.cardItems[0]){
            let firstCard = this.cardItems[0];
            console.log('THIS RAN AND');
            console.log(firstCard);
            this.cardsForm.controls['cardSelected'].setValue(firstCard.first_six + firstCard.last_four);
            this.ngRedux.dispatch({
                type: 'SELECT_CARD',
                payload: firstCard
            });
        }
    }



};
