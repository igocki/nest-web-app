import { EventEmitter } from '@angular/core';
import { Map, List } from 'immutable';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder } from "@angular/forms";
export declare class PaymentSelectorComponent {
    private formBuilder;
    private ngRedux;
    status: Observable<string>;
    shippingData: Observable<any>;
    cardsForm: FormGroup;
    cardItems: List<any>;
    selectedCard: Map<string, any>;
    errorForm: any;
    loggedInChangeBilling: boolean;
    enrollNewPlan: boolean;
    cardSelect: EventEmitter<{}>;
    saveBilling: EventEmitter<{}>;
    constructor(formBuilder: FormBuilder, ngRedux: NgRedux<RootState>);
    ngOnInit(): void;
    ngOnChanges(): void;
}
