var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Map, List } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../../../services/nestcareService';
import { PaywhirlService } from '../../../services/paywhirlService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, Validators } from "@angular/forms";
import { AppStatusActions } from '../../../actions/app-status-actions';
export var PaymentSelectorComponent = (function () {
    function PaymentSelectorComponent(formBuilder, ngRedux) {
        this.formBuilder = formBuilder;
        this.ngRedux = ngRedux;
        this.cardSelect = new EventEmitter();
        this.saveBilling = new EventEmitter();
        this.cardsForm = formBuilder.group({
            'cardSelected': ['', Validators.required]
        });
    }
    PaymentSelectorComponent.prototype.ngOnInit = function () {
    };
    PaymentSelectorComponent.prototype.ngOnChanges = function () {
        if (this.cardItems && this.cardItems[0]) {
            var firstCard = this.cardItems[0];
            console.log('THIS RAN AND');
            console.log(firstCard);
            this.cardsForm.controls['cardSelected'].setValue(firstCard.first_six + firstCard.last_four);
            this.ngRedux.dispatch({
                type: 'SELECT_CARD',
                payload: firstCard
            });
        }
    };
    __decorate([
        select(['billing', 'status']), 
        __metadata('design:type', Observable)
    ], PaymentSelectorComponent.prototype, "status", void 0);
    __decorate([
        select(['billing']), 
        __metadata('design:type', Observable)
    ], PaymentSelectorComponent.prototype, "shippingData", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], PaymentSelectorComponent.prototype, "cardItems", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], PaymentSelectorComponent.prototype, "selectedCard", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Object)
    ], PaymentSelectorComponent.prototype, "errorForm", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], PaymentSelectorComponent.prototype, "loggedInChangeBilling", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], PaymentSelectorComponent.prototype, "enrollNewPlan", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], PaymentSelectorComponent.prototype, "cardSelect", void 0);
    __decorate([
        Output(), 
        __metadata('design:type', Object)
    ], PaymentSelectorComponent.prototype, "saveBilling", void 0);
    PaymentSelectorComponent = __decorate([
        Component({
            selector: 'payment-selector',
            providers: [NestcareService, PaywhirlService, HttpClient, AppStatusActions],
            templateUrl: 'paymentSelector.component.html'
        }), 
        __metadata('design:paramtypes', [FormBuilder, NgRedux])
    ], PaymentSelectorComponent);
    return PaymentSelectorComponent;
}());
;
