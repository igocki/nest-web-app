import { Subscription } from 'rxjs/Subscription';
export function DestroySubscribers() {
    return function (target) {
        // decorating the function ngOnDestroy
        target.prototype.ngOnDestroy = ngOnDestroyDecorator(target.prototype.ngOnDestroy);
        // decorator function
        function ngOnDestroyDecorator(f) {
            console.log('in on destroy');
            return function () {
                // saving the result of ngOnDestroy performance to the variable superData
                var superData = f ? f.apply(this, arguments) : null;
                // unsubscribing
                for (var subscriberKey in this.subscribers) {
                    var subscriber = this.subscribers[subscriberKey];
                    if (subscriber instanceof Subscription) {
                        console.log('unsusbscribed');
                        subscriber.unsubscribe();
                    }
                }
                // returning the result of ngOnDestroy performance
                return superData;
            };
        }
        // returning the decorated class
        return target;
    };
}
