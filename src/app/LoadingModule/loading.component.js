var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { NgRedux } from 'ng2-redux';
export var LoadingComponent = (function () {
    function LoadingComponent(ngRedux, loadingCtrl) {
        this.ngRedux = ngRedux;
        this.loadingCtrl = loadingCtrl;
        // this.nav = nav;
    }
    LoadingComponent.prototype.ngOnInit = function () {
        // let loader = this.loadingCtrl.create({
        //     content: "Please wait...",
        //     dismissOnPageChange: true
        // });
        var _this = this;
        this.ngRedux
            .select(function (state) { return state.loading.get('visible'); })
            .subscribe(function (visible) {
            if (visible) {
                _this.currentLoading = _this.createLoader();
                //this.nav.present(loader);
                _this.currentLoading.present();
            }
            else {
                if (_this.currentLoading) {
                    _this.currentLoading.dismiss();
                }
            }
        });
    };
    LoadingComponent.prototype.createLoader = function () {
        return this.loadingCtrl.create({
            content: "Please wait..."
        });
    };
    LoadingComponent = __decorate([
        Component({
            selector: 'loading-component',
            templateUrl: 'loading.component.html',
            providers: []
        }), 
        __metadata('design:paramtypes', [NgRedux, LoadingController])
    ], LoadingComponent);
    return LoadingComponent;
}());
