import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import {IonicApp, IonicModule} from 'ionic-angular';
import { LoadingComponent }    from './loading.component';
import { MockLoginService } from '../../services/mock-login-service';

@NgModule({
    imports: [
        CommonModule,
        IonicModule
    ],
    exports: [
        LoadingComponent
    ],
    declarations: [
        LoadingComponent
    ],
    providers: [
        MockLoginService
    ]
})
export class LoadingModule {}
