import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { RootState } from '../../store/index';
import { NgRedux } from 'ng2-redux';

@Component({
    selector: 'loading-component',
    templateUrl: 'loading.component.html',
    providers: [ ]
})
export class LoadingComponent implements OnInit {

    currentLoading: any

    constructor(
        private ngRedux: NgRedux<RootState>,
        public loadingCtrl: LoadingController
        // private nav: NavController
    ) {
        // this.nav = nav;
    }



    ngOnInit() {
        // let loader = this.loadingCtrl.create({
        //     content: "Please wait...",
        //     dismissOnPageChange: true
        // });

        this.ngRedux
            .select(state => state.loading.get('visible'))
            .subscribe(visible => {

                if (visible) {
                    this.currentLoading = this.createLoader();
                    //this.nav.present(loader);

                    this.currentLoading.present();

                } else {
                    if(this.currentLoading){
                        this.currentLoading.dismiss();
                    }
                }
            });
    }

    createLoader(){
        return this.loadingCtrl.create({
            content: "Please wait..."
        });
    }

    // showLoader(){
    //     let loader = this.loadingCtrl.create({
    //         content: "Please wait..."
    //     });
    //     loader.present();
    // }

}
