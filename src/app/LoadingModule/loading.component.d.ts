import { OnInit } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { RootState } from '../../store/index';
import { NgRedux } from 'ng2-redux';
export declare class LoadingComponent implements OnInit {
    private ngRedux;
    loadingCtrl: LoadingController;
    constructor(ngRedux: NgRedux<RootState>, loadingCtrl: LoadingController);
    ngOnInit(): void;
    createLoader(): any;
}
