var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { PaywhirlService } from "../../../services/paywhirlService";
export var AccountOrdersComponent = (function () {
    function AccountOrdersComponent(_userService, _recurlyService, ngRedux) {
        this._userService = _userService;
        this._recurlyService = _recurlyService;
        this.ngRedux = ngRedux;
    }
    AccountOrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (resp) {
            if (resp) {
                _this.custID = resp.get('nc_id');
            }
        });
        this._userService.refresh();
        this.orders = this._userService.orders;
    };
    AccountOrdersComponent.prototype.downloadInvoice = function (id) {
        // -- invoice_number..
        this._recurlyService.GetPdfInvoiceUrl(id, this.custID).subscribe(function (dt) {
            console.log('hit this');
            console.log(dt);
            location.href = (dt + '');
        }, function (error) {
            console.log('hit this error');
        });
        // var pdfWindow = window.open();
    };
    AccountOrdersComponent = __decorate([
        Component({
            templateUrl: 'orders.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService]
        }), 
        __metadata('design:paramtypes', [UserService, PaywhirlService, NgRedux])
    ], AccountOrdersComponent);
    return AccountOrdersComponent;
}());
