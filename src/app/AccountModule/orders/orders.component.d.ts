import { UserService } from '../../../services/userService';
import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store';
import { PaywhirlService } from "../../../services/paywhirlService";
export declare class AccountOrdersComponent implements OnInit {
    private _userService;
    private _recurlyService;
    private ngRedux;
    orders: any;
    custID: any;
    constructor(_userService: UserService, _recurlyService: PaywhirlService, ngRedux: NgRedux<RootState>);
    ngOnInit(): void;
    downloadInvoice(id: any): void;
}
