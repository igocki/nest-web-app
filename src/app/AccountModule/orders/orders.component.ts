import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component, OnInit } from '@angular/core';
import { Map, List } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store'
import {PaywhirlService} from "../../../services/paywhirlService";

@Component({
    templateUrl: 'orders.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ]
})
export class AccountOrdersComponent implements OnInit {

    public orders;
    public custID;
    constructor(private _userService: UserService,
                private _recurlyService: PaywhirlService,
                private ngRedux: NgRedux<RootState>) {

    }


    ngOnInit() {
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        userViewSub.subscribe(resp =>{
           if(resp){
               this.custID = resp.get('nc_id');
           }
        });
        this._userService.refresh();
        this.orders = this._userService.orders;
    }

    downloadInvoice(id) {
        // -- invoice_number..
        this._recurlyService.GetPdfInvoiceUrl(id, this.custID).subscribe((dt) =>{
            console.log('hit this');
            console.log(dt);
            location.href = (dt + '');
        }, error => {
            console.log('hit this error');
        })
        // var pdfWindow = window.open();
    }

}
