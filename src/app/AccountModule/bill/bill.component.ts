import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component, OnInit } from '@angular/core';
import { select, NgRedux } from 'ng2-redux';
import { RootState } from '../../../store'
import { LoadingActions } from '../../../actions/loading-actions';

@Component({
    templateUrl: 'bill.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ],
})
export class AccountBillComponent implements OnInit {

    public billAddress: any;
    public billingInfoLoading;
    public nc_id;
    public takeoverBilling;
    constructor(private _userService: UserService,
                private _nc: NestcareService,
                private ngRedux: NgRedux<RootState>) {

    }


    ngOnInit() {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.billingInfoLoading = false;
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        userViewSub.subscribe((data) =>{
            if(data){
                this.nc_id = data.get('nc_id');
                this.billAddress = data.get('billingAddress');
            }
        })
        this._nc.getPayingUser(this.nc_id).subscribe((res) =>{
            this.billingInfoLoading = true;
            console.log('ran');
            if(res && res.user_id){
                console.log('found it');
                this.takeoverBilling = true;
            }
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, error => {
            this.billingInfoLoading = true;
            this.takeoverBilling = false;
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('error')
        })
        console.log('hello');
        // this.billAddress = this._userService.billingInfo;

    }



}
