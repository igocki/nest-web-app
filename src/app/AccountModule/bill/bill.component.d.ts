import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store';
export declare class AccountBillComponent implements OnInit {
    private _userService;
    private _nc;
    private ngRedux;
    billAddress: any;
    billingInfoLoading: any;
    nc_id: any;
    takeoverBilling: any;
    constructor(_userService: UserService, _nc: NestcareService, ngRedux: NgRedux<RootState>);
    ngOnInit(): void;
}
