var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../../actions/loading-actions';
export var AccountBillComponent = (function () {
    function AccountBillComponent(_userService, _nc, ngRedux) {
        this._userService = _userService;
        this._nc = _nc;
        this.ngRedux = ngRedux;
    }
    AccountBillComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.billingInfoLoading = false;
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data) {
                _this.nc_id = data.get('nc_id');
                _this.billAddress = data.get('billingAddress');
            }
        });
        this._nc.getPayingUser(this.nc_id).subscribe(function (res) {
            _this.billingInfoLoading = true;
            console.log('ran');
            if (res && res.user_id) {
                console.log('found it');
                _this.takeoverBilling = true;
            }
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            _this.billingInfoLoading = true;
            _this.takeoverBilling = false;
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('error');
        });
        console.log('hello');
        // this.billAddress = this._userService.billingInfo;
    };
    AccountBillComponent = __decorate([
        Component({
            templateUrl: 'bill.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService],
        }), 
        __metadata('design:paramtypes', [UserService, NestcareService, NgRedux])
    ], AccountBillComponent);
    return AccountBillComponent;
}());
