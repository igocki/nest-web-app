import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";

@Component({
    templateUrl: 'ship.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ],
})
export class AccountShipComponent implements OnInit {

    public shipAddress: Observable<Map<string, any>>;
    public fullname: string;

    constructor(private _userService: UserService) {
        this.shipAddress = this._userService.shipAddress;
    }


    ngOnInit() {
        this._userService.refresh();
    }



}
