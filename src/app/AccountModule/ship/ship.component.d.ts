import { UserService } from '../../../services/userService';
import { OnInit } from '@angular/core';
import { Observable } from "rxjs";
export declare class AccountShipComponent implements OnInit {
    private _userService;
    shipAddress: Observable<Map<string, any>>;
    fullname: string;
    constructor(_userService: UserService);
    ngOnInit(): void;
}
