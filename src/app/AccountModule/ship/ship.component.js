var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component } from '@angular/core';
export var AccountShipComponent = (function () {
    function AccountShipComponent(_userService) {
        this._userService = _userService;
        this.shipAddress = this._userService.shipAddress;
    }
    AccountShipComponent.prototype.ngOnInit = function () {
        this._userService.refresh();
    };
    AccountShipComponent = __decorate([
        Component({
            templateUrl: 'ship.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService],
        }), 
        __metadata('design:paramtypes', [UserService])
    ], AccountShipComponent);
    return AccountShipComponent;
}());
