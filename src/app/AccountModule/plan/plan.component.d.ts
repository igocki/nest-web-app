import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store';
import { Router } from "@angular/router";
export declare class AccountPlanComponent implements OnInit {
    private _userService;
    private _registerService;
    private ngRedux;
    router: Router;
    plan: any;
    planName: any;
    planAmt: any;
    lastPmt: any;
    nextPmt: any;
    userStatus: any;
    constructor(_userService: UserService, _registerService: RegisterService, ngRedux: NgRedux<RootState>, router: Router);
    ngOnInit(): void;
    enrollPlan(): void;
}
