import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component, OnInit } from '@angular/core';
import { select, NgRedux } from 'ng2-redux';
import { RootState } from '../../../store'
import {Router} from "@angular/router";

@Component({
    templateUrl: 'plan.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ],
})
export class AccountPlanComponent implements OnInit {
    public plan;
    planName: any;
    planAmt : any;
    lastPmt : any;
    nextPmt : any;
    public userStatus;
    constructor(
        private _userService: UserService,
        private _registerService: RegisterService,
        private ngRedux: NgRedux<RootState>,
        public router: Router) {
    }


    ngOnInit() {
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        userViewSub.subscribe((data) =>{
            if(data){
                this.userStatus = data.get('userStatus');
            }

            if(data && data.get('plan')){
                this.plan = data.get('plan');
                this.planName = data.getIn(['plan', 'planName']);
                this.planAmt = data.getIn(['plan', 'planCost']);
                this.lastPmt = data.getIn(['plan', 'last_payment']);
                this.nextPmt = data.getIn(['plan', 'next_payment']);
            }
        })
    }

    enrollPlan(){
        this.ngRedux.dispatch({
            type: "CHANGE_TO_ENROLL_PLAN"
        });
        this.router.navigate(['/select-plan']);
    }



}
