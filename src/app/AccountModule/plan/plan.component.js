var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { Component } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { Router } from "@angular/router";
export var AccountPlanComponent = (function () {
    function AccountPlanComponent(_userService, _registerService, ngRedux, router) {
        this._userService = _userService;
        this._registerService = _registerService;
        this.ngRedux = ngRedux;
        this.router = router;
    }
    AccountPlanComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data) {
                _this.userStatus = data.get('userStatus');
            }
            if (data && data.get('plan')) {
                _this.plan = data.get('plan');
                _this.planName = data.getIn(['plan', 'planName']);
                _this.planAmt = data.getIn(['plan', 'planCost']);
                _this.lastPmt = data.getIn(['plan', 'last_payment']);
                _this.nextPmt = data.getIn(['plan', 'next_payment']);
            }
        });
    };
    AccountPlanComponent.prototype.enrollPlan = function () {
        this.ngRedux.dispatch({
            type: "CHANGE_TO_ENROLL_PLAN"
        });
        this.router.navigate(['/select-plan']);
    };
    AccountPlanComponent = __decorate([
        Component({
            templateUrl: 'plan.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService],
        }), 
        __metadata('design:paramtypes', [UserService, RegisterService, NgRedux, Router])
    ], AccountPlanComponent);
    return AccountPlanComponent;
}());
