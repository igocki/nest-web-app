import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { AccountComponent }    from './account.component';
import { AccountPlanComponent }    from './plan/plan.component';
import { AccountBillComponent }    from './bill/bill.component';
import { AccountShipComponent }    from './ship/ship.component';
import { AccountOrdersComponent }    from './orders/orders.component';
import { AccountRoutingModule } from './account-routing.module';
import {loginToAccountComponent} from './loginToAccount/loginToAccount.component'
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module'
import {Animations} from '../animations';
import { SidebarModule } from 'ng-sidebar';
import {InputTextModule, TabMenuModule, DialogModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AccountRoutingModule,
        TabMenuModule,
        CommonComponentsModule,
        IonicModule,
        SidebarModule,
        DialogModule
    ],
    declarations: [
        AccountComponent,
        AccountPlanComponent,
        AccountBillComponent,
        AccountShipComponent,
        AccountOrdersComponent,
        loginToAccountComponent
    ],
    providers: [
        Animations
    ]
})
export class AccountModule {}
