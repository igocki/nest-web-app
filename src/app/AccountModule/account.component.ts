import { Component, OnInit, EventEmitter } from '@angular/core';
import {routerTransition } from '../router.animations';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { UserService } from '../../services/userService';
import { RootState } from '../../store/index';
import {Observable} from "rxjs";
import { LoadingActions } from '../../actions/loading-actions';
import { NgRedux, select } from 'ng2-redux';
import {MenuItem} from 'primeng/primeng';
import {Router, ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
@Component({
    templateUrl: 'account.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ]
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})

export class AccountComponent implements OnInit {
    public currentState;
    public toggleSidebar = new EventEmitter();
    public selectedTab: MenuItem;
    public email: string;
    public fullname: string;
    public userStatus: string;
    public userBirth: any;
    public accountType: string;
    public userGender: string;
    public userPhone: string;
    public ncID: string;
    private userProf: any;
    private loginStatus;
    private backupToken;
    private nc_backupToken;
    public subscribers: any = {};
    public unsubscribeApp;
    public tokenString;
    public isAdmin;

    constructor(
        private _appStatusActions: AppStatusActions,
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private route: ActivatedRoute,
        private _registerService: RegisterService,
        private _userService: UserService,
        private router: Router) {
        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        let secure = this.ngRedux.select(state => {
            return state.secure;
        });
        userViewSub.subscribe(data => {
            this.email = data.getIn(['email']);
            this.fullname = data.getIn(['fullname']);
            this.userBirth = (new Date(data.getIn(['userBirth'])).setHours((new Date(data.getIn(['userBirth'])).getHours() + 24)));
            this.userPhone = data.getIn(['userPhone']);
            this.userGender = data.getIn(['userGender']);
            this.userStatus = data.get('userStatus');
            console.log("HEEEEEy");
            console.log(this.userStatus);
            this.accountType = data.getIn(['accountType']);
            this.ncID = data.getIn(['nc_id']);
        });


        this.subscribers.app_status = this.ngRedux.select(state => state.appStatus);
        //let app_status = this.ngRedux.select(state => state.appStatus);

        // this.route.queryParams.subscribe(params =>{
        //     if(params) {
        //         this.tokenString = params['token'];
        //         if (this.tokenString) {
        //             this.ngRedux.dispatch({
        //                 type: LoadingActions.SHOW_LOADING
        //             });
        //             this._appStatusActions.tokenLogin(this.tokenString, true);
        //         }
        //
        //     }
        //
        // });
        this.unsubscribeApp = this.subscribers.app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                this.isAdmin = data.get('admin');
                if(!this.tokenString){
                    if(this.loginStatus !== 'logged-in'){
                        console.log('this login ran test 1');
                        this.router.navigate(['/loginscreen']);
                    }
                }
            }
        });

        secure.subscribe(data => {
            if(data){
                this.backupToken = data.get('backupToken');
                this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });

     //    this.email = _userService.email;
     //    this.fullname = _userService.fullname;
     //    this.userBirth = _userService.birthdate;
     //
     //    this.userPhone = _userService.mobile;
     // //   this.userPhone = this.userPhone.replace(/(\d{2})(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
     //    this.userGender = _userService.gender;
    }
    private items: MenuItem[];

    ngOnInit() {
     /*   let app_auth = this.ngRedux.select(state => state.auth);

        app_auth.subscribe(data => {
            if (data) {
                this.userProf = data.get('profile');
                this.userBirth = this.userProf.get('date_of_birth');
                this.userGender = this.userProf.get('gender');
                this.userPhone = this.userProf.get('mobile');
            }
        });*/

        this.ngRedux.dispatch({
            type: "CLEAR_ENROLL_PLAN"
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });


        this.items = [
            {label: 'Plan', routerLink: ['/account/plan']},
            {label: 'Billing', routerLink: ['/account/bill']},
            {label: 'Shipping', routerLink: ['/account/ship']},
            {label: 'Orders', routerLink: ['/account/orders']}
        ];
        //
        // this.userStatus = this._userService.userStatus;
        console.log('YASDALSDLSADDAS');
        console.log(this.loginStatus);
        if(this.loginStatus === 'logged-in'){
            if(this.backupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }

        console.log(this._userService.paywhirlAccount);
        console.log('THIS RAN GENSSIS')
        this._userService.refresh();
        if((this._userService.nestcareID && this._userService.paywhirlAccount) || (this.ncID)){
            console.log('end here 5');
            let custIDUsed = (this.ncID) ?this.ncID : this._userService.nestcareID;
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            if(!this.isAdmin){
                this._registerService.getUserInformation(custIDUsed).subscribe(res => {
                    console.log(res);
                    this.ngRedux.dispatch({
                        type: 'STORE_PLAN_LOGIN',
                        payload: res[0]
                    });
                    console.log('WORROOZZERSS');
                    console.log(res[0]);
                    if(res[0].planName) {
                        this.ngRedux.dispatch({
                            type: 'STORE_USER_STATUS',
                            payload: 'Paid'
                        });
                    }
                    // } else {
                    //     this.ngRedux.dispatch({
                    //         type: 'STORE_USER_STATUS',
                    //         payload: 'Free'
                    //     });
                    // }
                    console.log("ASDSADDSA");
                    console.log(res[1]);
                    if(res[1] instanceof Array || !res[1] || !res[1].billing_info || (res[1].billing_info && !res[1].billing_info.last_four)){
                        this.ngRedux.dispatch({
                            type: 'CLEAR_BILLING_INFO'
                        });
                        console.log('no billing to store');
                    } else {
                        this.ngRedux.dispatch({
                            type: 'STORING_BILLING_INFO',
                            payload: res[1]
                        });
                    }

                    this.ngRedux.dispatch({
                        type: 'STORING_ORDERS',
                        payload: res[2]
                    });
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                }, error => {

                    if(error.recurlyNotFound){
                        console.log('this');
                        console.log(this.accountType);
                        this.ngRedux.dispatch({
                            type: 'CLEAR_PLAN'
                        });
                        if(this.accountType === 'Explorer' || this.accountType === 'Advocate'){
                            this.ngRedux.dispatch({
                                type: 'STORE_USER_STATUS',
                                payload: 'Trial'
                            });
                            this.items = [
                                {label: 'Plan', routerLink: ['/account/plan']},
                                {label: 'Orders', routerLink: ['/account/orders']}
                            ];
                            this.ngRedux.dispatch({
                                type: LoadingActions.HIDE_LOADING
                            });
                        } else {
                            this._nc.getPayingUser(this.ncID).subscribe((res) =>{
                                if(this.accountType === 'Subscriber' && res && res.user_id){
                                    this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Household'
                                    });
                                    this.items = [
                                        {label: 'Plan', routerLink: ['/account/plan']}
                                    ];
                                } else {
                                    this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Staff'
                                    });
                                    this.items = [
                                        {label: 'Plan', routerLink: ['/account/plan']}
                                    ];
                                }
                                console.log('successful');
                                console.log(res);

                                console.log('user does not have recurly id');

                                this.ngRedux.dispatch({
                                    type: 'CLEAR_BILLING_INFO'
                                });
                                console.log('no billing to store');
                                this.ngRedux.dispatch({
                                    type: 'CLEAR_SHIPPING_INFO'
                                });
                                console.log('no shipping to store');

                                this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            }, error => {
                                console.log('errored geting paying user');
                                console.log(error);
                                this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            });
                        }

                    } else {
                        console.log(error);
                        this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }
                });
            } else {
                this._registerService.adminGetUserInformation(custIDUsed).subscribe(res => {
                    console.log(res);
                    this.ngRedux.dispatch({
                        type: 'STORE_PLAN_LOGIN',
                        payload: res[0]
                    });

                    if (res[0].planName) {
                        this.ngRedux.dispatch({
                            type: 'STORE_USER_STATUS',
                            payload: 'Paid'
                        });
                    }

                    if (res[1] instanceof Array || !res[1] || !res[1].billing_info || (res[1].billing_info && !res[1].billing_info.last_four)) {
                        this.ngRedux.dispatch({
                            type: 'CLEAR_BILLING_INFO'
                        });
                        console.log('no billing to store');
                    } else {
                        this.ngRedux.dispatch({
                            type: 'STORING_BILLING_INFO',
                            payload: res[1]
                        });
                    }

                    this.ngRedux.dispatch({
                        type: 'STORING_ORDERS',
                        payload: res[2]
                    });
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                }, error => {

                    if (error.recurlyNotFound) {
                        console.log('this');
                        console.log(this.accountType);
                        this.ngRedux.dispatch({
                            type: 'CLEAR_PLAN'
                        });
                        if (this.accountType === 'Explorer' || this.accountType === 'Advocate') {
                            this.ngRedux.dispatch({
                                type: 'STORE_USER_STATUS',
                                payload: 'Trial'
                            });
                            this.items = [
                                {label: 'Plan', routerLink: ['/account/plan']},
                                {label: 'Orders', routerLink: ['/account/orders']}
                            ];
                            this.ngRedux.dispatch({
                                type: LoadingActions.HIDE_LOADING
                            });
                        } else {
                            this._nc.getPayingUser(this.ncID).subscribe((res) => {
                                if (this.accountType === 'Subscriber' && res && res.user_id) {
                                    this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Household'
                                    });
                                    this.items = [
                                        {label: 'Plan', routerLink: ['/account/plan']}
                                    ];
                                } else {
                                    this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Staff'
                                    });
                                    this.items = [
                                        {label: 'Plan', routerLink: ['/account/plan']}
                                    ];
                                }
                                console.log('successful');
                                console.log(res);

                                console.log('user does not have recurly id');

                                this.ngRedux.dispatch({
                                    type: 'CLEAR_BILLING_INFO'
                                });
                                console.log('no billing to store');
                                this.ngRedux.dispatch({
                                    type: 'CLEAR_SHIPPING_INFO'
                                });
                                console.log('no shipping to store');

                                this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            }, error => {
                                console.log('errored geting paying user');
                                console.log(error);
                                this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            });
                        }

                    } else {
                        console.log(error);
                        this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }

                });
            }

        } else {
            this.ngRedux.dispatch({
                type: 'USE_EXPLORER_VIEW'
            });
        }
        this.ngRedux.dispatch({
            type: 'CLEAR_ONLY_SIGNUP_INFO'
        });
    }

    ngAfterViewInit() {
        let thisRoute = this.ngRedux.select(state => state.router);
        let subjectSub = thisRoute.subscribe(data => {
            if(data){
                setTimeout(() => {
                    this.currentState = data;
                    if(this.accountType === 'Explorer' || this.accountType === 'Advocate'){
                        if(data === '/account/plan'){
                            this.selectedTab = this.items[0];
                        }
                        if(data === '/account/orders'){
                            this.selectedTab = this.items[1];
                        }
                    } else if(this.userStatus === 'Household'){
                        if(data === '/account/plan'){
                            this.selectedTab = this.items[0];
                        }
                    } else {
                        if(data === '/account/plan'){
                            this.selectedTab = this.items[0];
                        }
                        if(data === '/account/bill'){
                            this.selectedTab = this.items[1];
                        }
                        if(data === '/account/ship'){
                            this.selectedTab = this.items[2];
                        }
                        if(data === '/account/orders'){
                            this.selectedTab = this.items[3];
                        }
                    }


                }, 200);
            }
        });
    }

    ngOnDestroy(){
        this.unsubscribeApp.unsubscribe();
    }

}
