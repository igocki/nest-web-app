import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountComponent }  from './account.component';
import { AccountBillComponent } from './bill/bill.component';
import { AccountPlanComponent } from './plan/plan.component';
import { AccountShipComponent }    from './ship/ship.component';
import { AccountOrdersComponent }    from './orders/orders.component';
import {loginToAccountComponent} from './loginToAccount/loginToAccount.component'

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'account',
                component: AccountComponent,
                children: [
                    {
                        path: '', redirectTo: 'plan', pathMatch: 'full'
                    },
                    { path: 'plan', component: AccountPlanComponent},
                    { path: 'bill', component: AccountBillComponent},
                    { path: 'ship', component: AccountShipComponent},
                    { path: 'orders', component: AccountOrdersComponent},
                ]
            },
            {
                path: 'accountLogin',
                component: loginToAccountComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
