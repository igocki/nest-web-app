var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';
import { AccountBillComponent } from './bill/bill.component';
import { AccountPlanComponent } from './plan/plan.component';
import { AccountShipComponent } from './ship/ship.component';
import { AccountOrdersComponent } from './orders/orders.component';
import { loginToAccountComponent } from './loginToAccount/loginToAccount.component';
export var AccountRoutingModule = (function () {
    function AccountRoutingModule() {
    }
    AccountRoutingModule = __decorate([
        NgModule({
            imports: [
                RouterModule.forChild([
                    {
                        path: 'account',
                        component: AccountComponent,
                        children: [
                            {
                                path: '', redirectTo: 'plan', pathMatch: 'full'
                            },
                            { path: 'plan', component: AccountPlanComponent },
                            { path: 'bill', component: AccountBillComponent },
                            { path: 'ship', component: AccountShipComponent },
                            { path: 'orders', component: AccountOrdersComponent },
                        ]
                    },
                    {
                        path: 'accountLogin',
                        component: loginToAccountComponent
                    }
                ])
            ],
            exports: [
                RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AccountRoutingModule);
    return AccountRoutingModule;
}());
