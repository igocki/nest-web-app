var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { AccountComponent } from './account.component';
import { AccountPlanComponent } from './plan/plan.component';
import { AccountBillComponent } from './bill/bill.component';
import { AccountShipComponent } from './ship/ship.component';
import { AccountOrdersComponent } from './orders/orders.component';
import { AccountRoutingModule } from './account-routing.module';
import { loginToAccountComponent } from './loginToAccount/loginToAccount.component';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import { Animations } from '../animations';
import { SidebarModule } from 'ng-sidebar';
import { TabMenuModule, DialogModule } from 'primeng/primeng';
export var AccountModule = (function () {
    function AccountModule() {
    }
    AccountModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                AccountRoutingModule,
                TabMenuModule,
                CommonComponentsModule,
                IonicModule,
                SidebarModule,
                DialogModule
            ],
            declarations: [
                AccountComponent,
                AccountPlanComponent,
                AccountBillComponent,
                AccountShipComponent,
                AccountOrdersComponent,
                loginToAccountComponent
            ],
            providers: [
                Animations
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AccountModule);
    return AccountModule;
}());
