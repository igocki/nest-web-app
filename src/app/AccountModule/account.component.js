var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, EventEmitter } from '@angular/core';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { UserService } from '../../services/userService';
import { LoadingActions } from '../../actions/loading-actions';
import { NgRedux } from 'ng2-redux';
import { Router, ActivatedRoute } from "@angular/router";
export var AccountComponent = (function () {
    function AccountComponent(_appStatusActions, ngRedux, _nc, route, _registerService, _userService, router) {
        var _this = this;
        this._appStatusActions = _appStatusActions;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.route = route;
        this._registerService = _registerService;
        this._userService = _userService;
        this.router = router;
        this.toggleSidebar = new EventEmitter();
        this.subscribers = {};
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        var secure = this.ngRedux.select(function (state) {
            return state.secure;
        });
        userViewSub.subscribe(function (data) {
            _this.email = data.getIn(['email']);
            _this.fullname = data.getIn(['fullname']);
            _this.userBirth = (new Date(data.getIn(['userBirth'])).setHours((new Date(data.getIn(['userBirth'])).getHours() + 24)));
            _this.userPhone = data.getIn(['userPhone']);
            _this.userGender = data.getIn(['userGender']);
            _this.userStatus = data.get('userStatus');
            console.log("HEEEEEy");
            console.log(_this.userStatus);
            _this.accountType = data.getIn(['accountType']);
            _this.ncID = data.getIn(['nc_id']);
        });
        this.subscribers.app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        //let app_status = this.ngRedux.select(state => state.appStatus);
        // this.route.queryParams.subscribe(params =>{
        //     if(params) {
        //         this.tokenString = params['token'];
        //         if (this.tokenString) {
        //             this.ngRedux.dispatch({
        //                 type: LoadingActions.SHOW_LOADING
        //             });
        //             this._appStatusActions.tokenLogin(this.tokenString, true);
        //         }
        //
        //     }
        //
        // });
        this.unsubscribeApp = this.subscribers.app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
                _this.isAdmin = data.get('admin');
                if (!_this.tokenString) {
                    if (_this.loginStatus !== 'logged-in') {
                        console.log('this login ran test 1');
                        _this.router.navigate(['/loginscreen']);
                    }
                }
            }
        });
        secure.subscribe(function (data) {
            if (data) {
                _this.backupToken = data.get('backupToken');
                _this.nc_backupToken = data.get('nestcareBackupToken');
            }
        });
        //    this.email = _userService.email;
        //    this.fullname = _userService.fullname;
        //    this.userBirth = _userService.birthdate;
        //
        //    this.userPhone = _userService.mobile;
        // //   this.userPhone = this.userPhone.replace(/(\d{2})(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
        //    this.userGender = _userService.gender;
    }
    AccountComponent.prototype.ngOnInit = function () {
        /*   let app_auth = this.ngRedux.select(state => state.auth);
   
           app_auth.subscribe(data => {
               if (data) {
                   this.userProf = data.get('profile');
                   this.userBirth = this.userProf.get('date_of_birth');
                   this.userGender = this.userProf.get('gender');
                   this.userPhone = this.userProf.get('mobile');
               }
           });*/
        var _this = this;
        this.ngRedux.dispatch({
            type: "CLEAR_ENROLL_PLAN"
        });
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
        this.items = [
            { label: 'Plan', routerLink: ['/account/plan'] },
            { label: 'Billing', routerLink: ['/account/bill'] },
            { label: 'Shipping', routerLink: ['/account/ship'] },
            { label: 'Orders', routerLink: ['/account/orders'] }
        ];
        //
        // this.userStatus = this._userService.userStatus;
        console.log('YASDALSDLSADDAS');
        console.log(this.loginStatus);
        if (this.loginStatus === 'logged-in') {
            if (this.backupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: this.backupToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.nc_backupToken
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_BACKUP_TOKEN'
                });
                this.ngRedux.dispatch({
                    type: 'CLEAR_NC_BACKUP_TOKEN'
                });
            }
        }
        console.log(this._userService.paywhirlAccount);
        console.log('THIS RAN GENSSIS');
        this._userService.refresh();
        if ((this._userService.nestcareID && this._userService.paywhirlAccount) || (this.ncID)) {
            console.log('end here 5');
            var custIDUsed = (this.ncID) ? this.ncID : this._userService.nestcareID;
            this.ngRedux.dispatch({
                type: LoadingActions.SHOW_LOADING
            });
            if (!this.isAdmin) {
                this._registerService.getUserInformation(custIDUsed).subscribe(function (res) {
                    console.log(res);
                    _this.ngRedux.dispatch({
                        type: 'STORE_PLAN_LOGIN',
                        payload: res[0]
                    });
                    console.log('WORROOZZERSS');
                    console.log(res[0]);
                    if (res[0].planName) {
                        _this.ngRedux.dispatch({
                            type: 'STORE_USER_STATUS',
                            payload: 'Paid'
                        });
                    }
                    // } else {
                    //     this.ngRedux.dispatch({
                    //         type: 'STORE_USER_STATUS',
                    //         payload: 'Free'
                    //     });
                    // }
                    console.log("ASDSADDSA");
                    console.log(res[1]);
                    if (res[1] instanceof Array || !res[1] || !res[1].billing_info || (res[1].billing_info && !res[1].billing_info.last_four)) {
                        _this.ngRedux.dispatch({
                            type: 'CLEAR_BILLING_INFO'
                        });
                        console.log('no billing to store');
                    }
                    else {
                        _this.ngRedux.dispatch({
                            type: 'STORING_BILLING_INFO',
                            payload: res[1]
                        });
                    }
                    _this.ngRedux.dispatch({
                        type: 'STORING_ORDERS',
                        payload: res[2]
                    });
                    _this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                }, function (error) {
                    if (error.recurlyNotFound) {
                        console.log('this');
                        console.log(_this.accountType);
                        _this.ngRedux.dispatch({
                            type: 'CLEAR_PLAN'
                        });
                        if (_this.accountType === 'Explorer' || _this.accountType === 'Advocate') {
                            _this.ngRedux.dispatch({
                                type: 'STORE_USER_STATUS',
                                payload: 'Trial'
                            });
                            _this.items = [
                                { label: 'Plan', routerLink: ['/account/plan'] },
                                { label: 'Orders', routerLink: ['/account/orders'] }
                            ];
                            _this.ngRedux.dispatch({
                                type: LoadingActions.HIDE_LOADING
                            });
                        }
                        else {
                            _this._nc.getPayingUser(_this.ncID).subscribe(function (res) {
                                if (_this.accountType === 'Subscriber' && res && res.user_id) {
                                    _this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Household'
                                    });
                                    _this.items = [
                                        { label: 'Plan', routerLink: ['/account/plan'] }
                                    ];
                                }
                                else {
                                    _this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Staff'
                                    });
                                    _this.items = [
                                        { label: 'Plan', routerLink: ['/account/plan'] }
                                    ];
                                }
                                console.log('successful');
                                console.log(res);
                                console.log('user does not have recurly id');
                                _this.ngRedux.dispatch({
                                    type: 'CLEAR_BILLING_INFO'
                                });
                                console.log('no billing to store');
                                _this.ngRedux.dispatch({
                                    type: 'CLEAR_SHIPPING_INFO'
                                });
                                console.log('no shipping to store');
                                _this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            }, function (error) {
                                console.log('errored geting paying user');
                                console.log(error);
                                _this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            });
                        }
                    }
                    else {
                        console.log(error);
                        _this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }
                });
            }
            else {
                this._registerService.adminGetUserInformation(custIDUsed).subscribe(function (res) {
                    console.log(res);
                    _this.ngRedux.dispatch({
                        type: 'STORE_PLAN_LOGIN',
                        payload: res[0]
                    });
                    if (res[0].planName) {
                        _this.ngRedux.dispatch({
                            type: 'STORE_USER_STATUS',
                            payload: 'Paid'
                        });
                    }
                    if (res[1] instanceof Array || !res[1] || !res[1].billing_info || (res[1].billing_info && !res[1].billing_info.last_four)) {
                        _this.ngRedux.dispatch({
                            type: 'CLEAR_BILLING_INFO'
                        });
                        console.log('no billing to store');
                    }
                    else {
                        _this.ngRedux.dispatch({
                            type: 'STORING_BILLING_INFO',
                            payload: res[1]
                        });
                    }
                    _this.ngRedux.dispatch({
                        type: 'STORING_ORDERS',
                        payload: res[2]
                    });
                    _this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                }, function (error) {
                    if (error.recurlyNotFound) {
                        console.log('this');
                        console.log(_this.accountType);
                        _this.ngRedux.dispatch({
                            type: 'CLEAR_PLAN'
                        });
                        if (_this.accountType === 'Explorer' || _this.accountType === 'Advocate') {
                            _this.ngRedux.dispatch({
                                type: 'STORE_USER_STATUS',
                                payload: 'Trial'
                            });
                            _this.items = [
                                { label: 'Plan', routerLink: ['/account/plan'] },
                                { label: 'Orders', routerLink: ['/account/orders'] }
                            ];
                            _this.ngRedux.dispatch({
                                type: LoadingActions.HIDE_LOADING
                            });
                        }
                        else {
                            _this._nc.getPayingUser(_this.ncID).subscribe(function (res) {
                                if (_this.accountType === 'Subscriber' && res && res.user_id) {
                                    _this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Household'
                                    });
                                    _this.items = [
                                        { label: 'Plan', routerLink: ['/account/plan'] }
                                    ];
                                }
                                else {
                                    _this.ngRedux.dispatch({
                                        type: 'STORE_USER_STATUS',
                                        payload: 'Staff'
                                    });
                                    _this.items = [
                                        { label: 'Plan', routerLink: ['/account/plan'] }
                                    ];
                                }
                                console.log('successful');
                                console.log(res);
                                console.log('user does not have recurly id');
                                _this.ngRedux.dispatch({
                                    type: 'CLEAR_BILLING_INFO'
                                });
                                console.log('no billing to store');
                                _this.ngRedux.dispatch({
                                    type: 'CLEAR_SHIPPING_INFO'
                                });
                                console.log('no shipping to store');
                                _this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            }, function (error) {
                                console.log('errored geting paying user');
                                console.log(error);
                                _this.ngRedux.dispatch({
                                    type: LoadingActions.HIDE_LOADING
                                });
                            });
                        }
                    }
                    else {
                        console.log(error);
                        _this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }
                });
            }
        }
        else {
            this.ngRedux.dispatch({
                type: 'USE_EXPLORER_VIEW'
            });
        }
        this.ngRedux.dispatch({
            type: 'CLEAR_ONLY_SIGNUP_INFO'
        });
    };
    AccountComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var thisRoute = this.ngRedux.select(function (state) { return state.router; });
        var subjectSub = thisRoute.subscribe(function (data) {
            if (data) {
                setTimeout(function () {
                    _this.currentState = data;
                    if (_this.accountType === 'Explorer' || _this.accountType === 'Advocate') {
                        if (data === '/account/plan') {
                            _this.selectedTab = _this.items[0];
                        }
                        if (data === '/account/orders') {
                            _this.selectedTab = _this.items[1];
                        }
                    }
                    else if (_this.userStatus === 'Household') {
                        if (data === '/account/plan') {
                            _this.selectedTab = _this.items[0];
                        }
                    }
                    else {
                        if (data === '/account/plan') {
                            _this.selectedTab = _this.items[0];
                        }
                        if (data === '/account/bill') {
                            _this.selectedTab = _this.items[1];
                        }
                        if (data === '/account/ship') {
                            _this.selectedTab = _this.items[2];
                        }
                        if (data === '/account/orders') {
                            _this.selectedTab = _this.items[3];
                        }
                    }
                }, 200);
            }
        });
    };
    AccountComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeApp.unsubscribe();
    };
    AccountComponent = __decorate([
        Component({
            templateUrl: 'account.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService]
        }), 
        __metadata('design:paramtypes', [AppStatusActions, NgRedux, NestcareService, ActivatedRoute, RegisterService, UserService, Router])
    ], AccountComponent);
    return AccountComponent;
}());
