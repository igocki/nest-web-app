var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { LoadingActions } from '../../../actions/loading-actions';
import { NgRedux } from 'ng2-redux';
import { Router, ActivatedRoute } from "@angular/router";
export var loginToAccountComponent = (function () {
    function loginToAccountComponent(_appStatusActions, ngRedux, _nc, route, _registerService, _userService, router) {
        var _this = this;
        this._appStatusActions = _appStatusActions;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.route = route;
        this._registerService = _registerService;
        this._userService = _userService;
        this.router = router;
        this.display = false;
        this.route.queryParams.subscribe(function (params) {
            if (params) {
                _this.tokenString = params['token'];
                if (_this.tokenString) {
                    _this.ngRedux.dispatch({
                        type: LoadingActions.SHOW_LOADING
                    });
                    _this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: _this.tokenString
                    });
                    _this._appStatusActions.tokenLoginWithSubscription(_this.tokenString, true).subscribe(function () {
                        _this.router.navigate(['/account']);
                        _this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }, function (error) {
                        console.log('here ya go');
                        console.log(error);
                        if (error && error.message && error.message.message === 'Token has expired') {
                            _this.display = true;
                        }
                        _this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    });
                }
            }
            else {
                _this.router.navigate(['/login']);
            }
        });
    }
    loginToAccountComponent.prototype.ngOnInit = function () {
    };
    loginToAccountComponent.prototype.ngAfterViewInit = function () {
    };
    loginToAccountComponent.prototype.ngOnDestroy = function () {
    };
    loginToAccountComponent = __decorate([
        Component({
            templateUrl: 'loginToAccount.component.html',
            providers: [AppStatusActions, NestcareService, RegisterService, UserService],
            animations: [routerTransition()],
            host: { '[@routerTransition]': '' }
        }), 
        __metadata('design:paramtypes', [AppStatusActions, NgRedux, NestcareService, ActivatedRoute, RegisterService, UserService, Router])
    ], loginToAccountComponent);
    return loginToAccountComponent;
}());
