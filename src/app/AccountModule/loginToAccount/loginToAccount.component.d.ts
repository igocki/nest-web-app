import { OnInit } from '@angular/core';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { RootState } from '../../../store/index';
import { NgRedux } from 'ng2-redux';
import { Router, ActivatedRoute } from "@angular/router";
export declare class loginToAccountComponent implements OnInit {
    private _appStatusActions;
    private ngRedux;
    private _nc;
    private route;
    private _registerService;
    private _userService;
    private router;
    display: boolean;
    tokenString: any;
    isAdmin: any;
    constructor(_appStatusActions: AppStatusActions, ngRedux: NgRedux<RootState>, _nc: NestcareService, route: ActivatedRoute, _registerService: RegisterService, _userService: UserService, router: Router);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
}
