import { Component, OnInit, EventEmitter } from '@angular/core';
import {routerTransition } from '../../router.animations';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { RootState } from '../../../store/index';
import {Observable} from "rxjs";
import { LoadingActions } from '../../../actions/loading-actions';
import { NgRedux, select } from 'ng2-redux';
import {MenuItem} from 'primeng/primeng';
import {Router, ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
@Component({
    templateUrl: 'loginToAccount.component.html',
    providers: [AppStatusActions, NestcareService, RegisterService, UserService ],
    animations: [routerTransition()],
    host: {'[@routerTransition]': ''}
})

export class loginToAccountComponent implements OnInit {
    display: boolean = false;
    public tokenString;
    public isAdmin;

    constructor(
        private _appStatusActions: AppStatusActions,
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private route: ActivatedRoute,
        private _registerService: RegisterService,
        private _userService: UserService,
        private router: Router) {

        this.route.queryParams.subscribe(params =>{
            if(params) {
                this.tokenString = params['token'];
                if (this.tokenString) {
                    this.ngRedux.dispatch({
                        type: LoadingActions.SHOW_LOADING
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: this.tokenString
                    });
                    this._appStatusActions.tokenLoginWithSubscription(this.tokenString, true).subscribe(() =>{

                        this.router.navigate(['/account']);
                        this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    }, error => {
                        console.log('here ya go');
                        console.log(error);
                        if(error && error.message && error.message.message === 'Token has expired'){
                            this.display = true;
                        }

                        this.ngRedux.dispatch({
                            type: LoadingActions.HIDE_LOADING
                        });
                    });
                }
            } else {
                this.router.navigate(['/login']);
            }

        });

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    ngOnDestroy(){
    }

}
