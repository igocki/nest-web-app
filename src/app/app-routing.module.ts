import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { InitialComponent } from './app.component';
@NgModule({
    imports: [
        RouterModule.forRoot([
            {path: '', redirectTo: 'login', pathMatch: 'full'},
            {
                path: 'home',
                component: InitialComponent
            },
            {path: '', component: InitialComponent},
            {path: '**', component: InitialComponent}
        ])],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}
