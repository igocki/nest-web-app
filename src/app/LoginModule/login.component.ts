import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthActions } from '../../actions/auth-actions';
import { AppStatusActions } from '../../actions/app-status-actions';
import {routerTransition } from '../router.animations';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { UserService } from '../../services/userService';
import { HttpClient } from '../../services/http-header-service';
import { ConfigEnvService } from '../../services/env/configEnvService';
import { LoadingController } from 'ionic-angular';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store/index';
@Component({
    templateUrl: 'login.component.html',
    providers: [AuthActions, NestcareService, HttpClient, AppStatusActions, UserService, RegisterService, ConfigEnvService]
    // styles: [':host { position: absolute; margin-left: auto;margin-right: auto;left: 0;right: 0;}'],
    //host: { '[@routeAnimation]': 'true' },
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class LoginComponent implements OnInit {
    private status: any;
    private currentRoute: any;
    constructor(
        private ngRedux: NgRedux<RootState>,
        private router: Router,
        private _appStatusActions: AppStatusActions
    ) {


    }

    ngOnInit() {
        this._appStatusActions.clearSignupProcess();
        let app_status = this.ngRedux.select(state => state.appStatus);
        let app_route = this.ngRedux.select(state => state.router);
        let ctrl = this;
        app_route.subscribe(data => {
            this.currentRoute = data;
        });
        let appSubscribe = app_status.subscribe(data => {
            if(data){
                this.status = data.get('loginStatus');
                if(this.status === 'logged-in'){
                    console.log('logged in');
                    console.log(data);
                    setTimeout(function(){
                        if (ctrl.currentRoute === '/sso-login') {
                            ctrl.router.navigate(['/sso-login']);
                        }
                         else if(ctrl.currentRoute == '/login' || '/loginscreen'){
                            if(data.getIn(['admin'])){
                                ctrl.router.navigate(['/admin']);
                            } else {
                                ctrl.router.navigate(['/account']);
                            }
                        }
                    }, 100)
                }
            }
        });

        appSubscribe.unsubscribe();

    }



}

