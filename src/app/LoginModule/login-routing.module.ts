import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent }    from './login.component';
import { LoginScreenComponent }    from './login/loginscreen.component';
import { ForgetPasswordComponent }  from './forgetPassword/forgetPassword.component';
import { SsoLoginScreenComponent } from './sso-login/sso-login.component';
import { TwoFAComponent }    from './2fa/2fa.component';
import { SsoLoginSuccessComponent } from './sso-success/sso-success.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'loginscreen',
                component: LoginScreenComponent
            },
            {
                path: 'forgetPassword',
                component: ForgetPasswordComponent
            },
            {
                path: 'twofactor',
                component: TwoFAComponent
            },
            {
                path: 'sso-login',
                component: SsoLoginScreenComponent
            },
            {
                path: 'sso-success',
                component: SsoLoginSuccessComponent
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class LoginRoutingModule { }
