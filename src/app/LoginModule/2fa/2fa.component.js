var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../../actions/loading-actions';
export var TwoFAComponent = (function () {
    function TwoFAComponent(route, router, _nc, ngRedux, _userService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this._nc = _nc;
        this.ngRedux = ngRedux;
        this._userService = _userService;
        var custID = this.ngRedux.select(function (state) { return state.appStatus; });
        custID.subscribe(function (data) {
            if (data) {
                _this.customerID = data.get('payingCustomerID');
            }
        });
    }
    TwoFAComponent.prototype.ngOnInit = function () {
    };
    TwoFAComponent.prototype.submitAuthCode = function (form) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.sendAuthTwoFactor(form.authCode, this.customerID).subscribe(function (resp) {
            console.log(resp);
            _this.ngRedux.dispatch({
                type: 'LOGIN_SUCCESS',
                payload: resp
            });
            if (resp && resp.data && resp.data.address && resp.data.address.home) {
                _this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.data.address.home
                });
            }
            _this.ngRedux.dispatch({
                type: 'CHANGE_TO_LOGGED_IN'
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this._userService.login();
            _this.router.navigate(['/account/plan']);
        }, function (error) {
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    TwoFAComponent = __decorate([
        Component({
            templateUrl: '2fa.component.html',
            providers: [NestcareService, HttpClient, UserService, RegisterService],
        }), 
        __metadata('design:paramtypes', [ActivatedRoute, Router, NestcareService, NgRedux, UserService])
    ], TwoFAComponent);
    return TwoFAComponent;
}());
