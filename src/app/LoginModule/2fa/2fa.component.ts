import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { RootState } from '../../../store';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../../../actions/loading-actions';
import {routerTransition } from '../../router.animations';
@Component({
    templateUrl: '2fa.component.html',
    providers: [NestcareService, HttpClient, UserService, RegisterService],
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class TwoFAComponent implements OnInit {
    public customerID;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _nc: NestcareService,
        private ngRedux: NgRedux<RootState>,
        private _userService: UserService
    ) {
        let custID = this.ngRedux.select(state => state.appStatus);
        custID.subscribe(data => {
            if(data){
                this.customerID = data.get('payingCustomerID');
            }
        });
    }

    ngOnInit() {

    }

    submitAuthCode(form){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.sendAuthTwoFactor(form.authCode, this.customerID).subscribe(resp => {
            console.log(resp);
            this.ngRedux.dispatch({
                type: 'LOGIN_SUCCESS',
                payload: resp
            });
            if(resp && resp.data && resp.data.address && resp.data.address.home){
                this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.data.address.home
                });
            }
            this.ngRedux.dispatch({
                type: 'CHANGE_TO_LOGGED_IN'
            });
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this._userService.login();
            this.router.navigate(['/account/plan']);

        },
        error => {
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    }

}
