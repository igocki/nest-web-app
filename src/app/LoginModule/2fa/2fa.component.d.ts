import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { RootState } from '../../../store';
import { NgRedux } from 'ng2-redux';
export declare class TwoFAComponent implements OnInit {
    private route;
    private router;
    private _nc;
    private ngRedux;
    private _userService;
    customerID: any;
    constructor(route: ActivatedRoute, router: Router, _nc: NestcareService, ngRedux: NgRedux<RootState>, _userService: UserService);
    ngOnInit(): void;
    submitAuthCode(form: any): void;
}
