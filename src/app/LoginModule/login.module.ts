import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {IonicApp, IonicModule} from 'ionic-angular';
import { LoginComponent }    from './login.component';
import { LoginScreenComponent }    from './login/loginscreen.component';
import { TwoFAComponent }    from './2fa/2fa.component';
import { ForgetPasswordComponent }    from './forgetPassword/forgetPassword.component';
import { SignupComponent }    from './signup/signup.component';
import { LoginRoutingModule } from './login-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import {SsoLoginScreenComponent} from "./sso-login/sso-login.component";
import {SsoLoginSuccessComponent} from "./sso-success/sso-success.component";
import {Animations} from '../animations';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LoginRoutingModule,
        IonicModule,
        CommonComponentsModule
    ],
    declarations: [
        LoginComponent,
        ForgetPasswordComponent,
        LoginScreenComponent,
        TwoFAComponent,
        SsoLoginScreenComponent,
        SsoLoginSuccessComponent
    ],
    providers: [
        Animations
    ]
})
export class LoginModule {}
