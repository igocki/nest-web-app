var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { LoginComponent } from './login.component';
import { LoginScreenComponent } from './login/loginscreen.component';
import { TwoFAComponent } from './2fa/2fa.component';
import { ForgetPasswordComponent } from './forgetPassword/forgetPassword.component';
import { LoginRoutingModule } from './login-routing.module';
import { CommonComponentsModule } from '../CommonComponentsModule/commoncomponents.module';
import { SsoLoginScreenComponent } from "./sso-login/sso-login.component";
import { SsoLoginSuccessComponent } from "./sso-success/sso-success.component";
import { Animations } from '../animations';
export var LoginModule = (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                LoginRoutingModule,
                IonicModule,
                CommonComponentsModule
            ],
            declarations: [
                LoginComponent,
                ForgetPasswordComponent,
                LoginScreenComponent,
                TwoFAComponent,
                SsoLoginScreenComponent,
                SsoLoginSuccessComponent
            ],
            providers: [
                Animations
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], LoginModule);
    return LoginModule;
}());
