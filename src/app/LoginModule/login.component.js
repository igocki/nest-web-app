var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthActions } from '../../actions/auth-actions';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
import { RegisterService } from '../../services/registerService';
import { UserService } from '../../services/userService';
import { HttpClient } from '../../services/http-header-service';
import { ConfigEnvService } from '../../services/env/configEnvService';
import { NgRedux } from 'ng2-redux';
export var LoginComponent = (function () {
    function LoginComponent(ngRedux, router, _appStatusActions) {
        this.ngRedux = ngRedux;
        this.router = router;
        this._appStatusActions = _appStatusActions;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._appStatusActions.clearSignupProcess();
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var app_route = this.ngRedux.select(function (state) { return state.router; });
        var ctrl = this;
        app_route.subscribe(function (data) {
            _this.currentRoute = data;
        });
        var appSubscribe = app_status.subscribe(function (data) {
            if (data) {
                _this.status = data.get('loginStatus');
                if (_this.status === 'logged-in') {
                    console.log('logged in');
                    console.log(data);
                    setTimeout(function () {
                        if (ctrl.currentRoute === '/sso-login') {
                            ctrl.router.navigate(['/sso-login']);
                        }
                        else if (ctrl.currentRoute == '/login' || '/loginscreen') {
                            if (data.getIn(['admin'])) {
                                ctrl.router.navigate(['/admin']);
                            }
                            else {
                                ctrl.router.navigate(['/account']);
                            }
                        }
                    }, 100);
                }
            }
        });
        appSubscribe.unsubscribe();
    };
    LoginComponent = __decorate([
        Component({
            templateUrl: 'login.component.html',
            providers: [AuthActions, NestcareService, HttpClient, AppStatusActions, UserService, RegisterService, ConfigEnvService]
        }), 
        __metadata('design:paramtypes', [NgRedux, Router, AppStatusActions])
    ], LoginComponent);
    return LoginComponent;
}());
