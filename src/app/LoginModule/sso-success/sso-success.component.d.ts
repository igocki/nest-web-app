/**
 * Created by Tommy on 1/31/2017.
 */
import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Observable } from 'rxjs/Observable';
import { NestcareService } from '../../../services/nestcareService';
import { IAuth } from '../../../store';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store/index';
export declare class SsoLoginSuccessComponent implements OnInit {
    private _authActions;
    private router;
    private ngRedux;
    private _appStatusActions;
    private activatedRoute;
    private _nsc;
    auth$: Observable<IAuth>;
    private status;
    private loginStatus;
    private currentRoute;
    private incomingSso;
    private incomingSig;
    private userDetails;
    private loginInfo;
    private queryParamsRec;
    private authDataRec;
    private showLogin;
    constructor(_authActions: AuthActions, router: Router, ngRedux: NgRedux<RootState>, _appStatusActions: AppStatusActions, activatedRoute: ActivatedRoute, _nsc: NestcareService);
    finalRequest(): void;
    doTheStuff(): void;
    ngOnInit(): void;
    login(): void;
}
