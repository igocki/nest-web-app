/**
 * Created by Tommy on 1/31/2017.
 */

import { Component, OnInit } from '@angular/core';
import {routerTransition } from '../../router.animations';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import {Animations} from '../../animations';
import { Observable } from 'rxjs/Observable';
import { NestcareService } from '../../../services/nestcareService';
import { HttpClient } from '../../../services/http-header-service';
import { UserService } from '../../../services/userService';
import { IAuth } from '../../../store';
import { select } from 'ng2-redux';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store/index';
import {LoadingActions} from "../../../actions/loading-actions";
import { RegisterService } from '../../../services/registerService';

@Component({
    templateUrl: 'sso-success.component.html',
    providers: [AuthActions, NestcareService, HttpClient, AppStatusActions, UserService, RegisterService ],
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class SsoLoginSuccessComponent implements OnInit {
    @select() auth$: Observable<IAuth>;
    private status:any;
    private loginStatus: any;
    private currentRoute: any;
    private incomingSso: any;
    private incomingSig: any;
    private userDetails: any;
    private loginInfo: boolean;
    private queryParamsRec: boolean;
    private authDataRec: boolean;
    private showLogin: boolean;

    constructor(
        private _authActions: AuthActions,
        private router: Router,
        private ngRedux: NgRedux<RootState>,
        private _appStatusActions: AppStatusActions,
        private activatedRoute: ActivatedRoute,
        private _nsc: NestcareService
    ) {
        this.loginInfo = false;
        this.queryParamsRec = false;
        this.authDataRec = false;
        this.showLogin = false;
    }

    finalRequest() {
	console.log('Final called');
	console.log(this.userDetails);
    console.log(this.userDetails.get('id'));

        var userparams = {
            "nonce": this.incomingSso,
            "external_id": this.userDetails.get('id'),
            "email": this.userDetails.get('email'),
            "username": this.userDetails.get('email'),
            "name": this.userDetails.get('first_name') + ' ' + this.userDetails.get('last_name'),
            "avatar_url": this.userDetails.get('image'),
        /*    "external_id": this.userDetails.id,
            "email": this.userDetails.email,
            "username": this.userDetails.email,
            "name": this.userDetails.first_name + ' ' + this.userDetails.last_name,
            "avatar_url": this.userDetails.image*/
        };

        this._nsc.doSso(userparams).subscribe(data => {
            console.log('Made Final Request.');
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });

            window.location.href = 'http://community.mynest.care/session/sso_login?' + data.text().replace("\"", "").replace("\"", "");
        });
    }

    doTheStuff() {
        this.loginInfo = false;
        this.queryParamsRec = false;
        // -- Validate the incoming payload.
        this._nsc.ssoValidate(this.incomingSso, this.incomingSig).subscribe(data => {
            this.loginInfo = true;

            if (this.loginInfo && this.queryParamsRec) {
                this.finalRequest();
            }
        });

        this._nsc.getNonce(this.incomingSso).subscribe(data => {
            this.queryParamsRec = true;
            this.incomingSso = data.text().replace("\"", "").replace("\"", "");
            if (this.loginInfo && this.queryParamsRec) {
                this.finalRequest();
            }
        });


    }

    ngOnInit() {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let sso = params['sso'];
            let sig = params['sig'];
            this.incomingSig = sig;
            this.incomingSso = sso;
            this.queryParamsRec = true;
            if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
                this.doTheStuff();
            }
        });

        this._appStatusActions.clearSignupProcess();
        let app_status = this.ngRedux.select(state => state.appStatus);
        let app_route = this.ngRedux.select(state => state.router);
        let app_auth = this.ngRedux.select(state => state.auth);

        app_auth.subscribe(data => {
            if (data) {
                this.userDetails = data.get('profile');
                this.authDataRec = true;
                console.log('authData----------');
                console.log(this.userDetails);
                console.log(this.queryParamsRec);
                console.log(this.loginInfo);
                console.log(this.authDataRec);
                console.log(data);
                console.log('authEND-----------');
                if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
                    this.doTheStuff();
                }
                // this.pushBack();
            }
        });

        let ctrl = this;
        app_route.subscribe(data => {
            this.currentRoute = data;
        });
        app_status.subscribe(data => {
            if(data){
                this.status = data.get('appStatus');
                this.loginStatus = data.get('loginStatus');

                if(this.loginStatus === 'logged-in'){
                    console.log('logged in on success');
                    //    setTimeout(function(){
                    this.loginInfo = true;
                    if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
                        this.doTheStuff();
                    }
                    //   }, 100)
                } else {
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                }
            }
        });

    }


    login() {
    }


}
