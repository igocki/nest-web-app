import {Animations} from '../../animations';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthActions } from '../../../actions/auth-actions';
import { Observable } from 'rxjs/Observable';
import {routerTransition } from '../../router.animations';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { HttpClient } from '../../../services/http-header-service';
import { UserService } from '../../../services/userService';
import { IAuth } from '../../../store';
import { select } from 'ng2-redux';

@Component({
    templateUrl: 'loginscreen.component.html',
    providers: [AuthActions, NestcareService, HttpClient, UserService, RegisterService]
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class LoginScreenComponent implements OnInit {
    @select() auth$: Observable<IAuth>;

    constructor(
        private _authActions: AuthActions
    ) {

    }


    ngOnInit() {

    }

    login() {
    }


}
