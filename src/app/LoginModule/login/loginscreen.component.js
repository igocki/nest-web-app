var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { Observable } from 'rxjs/Observable';
import { NestcareService } from '../../../services/nestcareService';
import { RegisterService } from '../../../services/registerService';
import { HttpClient } from '../../../services/http-header-service';
import { UserService } from '../../../services/userService';
import { select } from 'ng2-redux';
export var LoginScreenComponent = (function () {
    function LoginScreenComponent(_authActions) {
        this._authActions = _authActions;
    }
    LoginScreenComponent.prototype.ngOnInit = function () {
    };
    LoginScreenComponent.prototype.login = function () {
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], LoginScreenComponent.prototype, "auth$", void 0);
    LoginScreenComponent = __decorate([
        Component({
            templateUrl: 'loginscreen.component.html',
            providers: [AuthActions, NestcareService, HttpClient, UserService, RegisterService]
        }), 
        __metadata('design:paramtypes', [AuthActions])
    ], LoginScreenComponent);
    return LoginScreenComponent;
}());
