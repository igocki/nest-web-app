import { OnInit } from '@angular/core';
import { AuthActions } from '../../../actions/auth-actions';
import { Observable } from 'rxjs/Observable';
import { IAuth } from '../../../store';
export declare class LoginScreenComponent implements OnInit {
    private _authActions;
    auth$: Observable<IAuth>;
    constructor(_authActions: AuthActions);
    ngOnInit(): void;
    login(): void;
}
