/**
 * Created by Tommy on 1/31/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import { Observable } from 'rxjs/Observable';
import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { select } from 'ng2-redux';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from "../../../actions/loading-actions";
import { RegisterService } from '../../../services/registerService';
export var SsoLoginScreenComponent = (function () {
    function SsoLoginScreenComponent(_authActions, router, ngRedux, _appStatusActions, activatedRoute, _nsc) {
        this._authActions = _authActions;
        this.router = router;
        this.ngRedux = ngRedux;
        this._appStatusActions = _appStatusActions;
        this.activatedRoute = activatedRoute;
        this._nsc = _nsc;
        this.loginInfo = false;
        this.queryParamsRec = false;
        this.authDataRec = false;
        this.showLogin = false;
    }
    SsoLoginScreenComponent.prototype.finalRequest = function () {
        var _this = this;
        var userparams = {
            "nonce": this.incomingSso,
            "external_id": this.userDetails.get('id'),
            "email": this.userDetails.get('email'),
            "username": this.userDetails.get('email'),
            "name": this.userDetails.get('first_name') + ' ' + this.userDetails.get('last_name'),
            "avatar_url": this.userDetails.get('image')
        };
        this._nsc.doSso(userparams).subscribe(function (data) {
            console.log('Made Request!!');
            console.log(data.text());
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            window.location.href = 'http://community.mynest.care/session/sso_login?' + data.text().replace("\"", "").replace("\"", "");
        });
    };
    SsoLoginScreenComponent.prototype.doTheStuff = function () {
        var _this = this;
        console.log('I got called.');
        this.loginInfo = false;
        this.queryParamsRec = false;
        // -- Validate the incoming payload.
        this._nsc.ssoValidate(this.incomingSso, this.incomingSig).subscribe(function (data) {
            console.log('Validate Response: ' + data.text());
            _this.loginInfo = true;
            if (_this.loginInfo && _this.queryParamsRec) {
                _this.finalRequest();
            }
        });
        this._nsc.getNonce(this.incomingSso).subscribe(function (data) {
            console.log('Nonce Response: ' + data.text().replace("\"", "").replace("\"", ""));
            _this.queryParamsRec = true;
            _this.incomingSso = data.text().replace("\"", "").replace("\"", "");
            if (_this.loginInfo && _this.queryParamsRec) {
                _this.finalRequest();
            }
        });
    };
    SsoLoginScreenComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.activatedRoute.queryParams.subscribe(function (params) {
            var sso = params['sso'];
            var sig = params['sig'];
            /*
             bm9uY2U9OTZiNzIxMzk2Y2ZiMTkzOWI3NTU3MjFkZTQ3YTFjMTImcmV0dXJu%0AX3Nzb191cmw9aHR0cCUzQSUyRiUyRmNvbW11bml0eS5teW5lc3QuY2FyZSUy%0ARnNlc3Npb24lMkZzc29fbG9naW4%3D%0A
 
             48145faf80313a86a334f3c22440c98083f5a3c593fc8a04800fa0ad972feb55
 
             */
            _this.incomingSig = sig;
            _this.incomingSso = sso;
            _this.queryParamsRec = true;
            console.log('queryParams');
            console.log(_this.queryParamsRec);
            console.log(_this.loginInfo);
            console.log(_this.authDataRec);
            if (_this.queryParamsRec && _this.loginInfo && _this.authDataRec) {
                _this.doTheStuff();
            }
        });
        this._appStatusActions.clearSignupProcess();
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var app_route = this.ngRedux.select(function (state) { return state.router; });
        var app_auth = this.ngRedux.select(function (state) { return state.auth; });
        app_auth.subscribe(function (data) {
            if (data) {
                _this.userDetails = data.get('profile');
                _this.authDataRec = true;
                console.log('authData');
                console.log(_this.queryParamsRec);
                console.log(_this.loginInfo);
                console.log(_this.authDataRec);
                if (_this.queryParamsRec && _this.loginInfo && _this.authDataRec) {
                    _this.doTheStuff();
                }
            }
        });
        var ctrl = this;
        app_route.subscribe(function (data) {
            _this.currentRoute = data;
        });
        app_status.subscribe(function (data) {
            if (data) {
                _this.status = data.get('appStatus');
                _this.loginStatus = data.get('loginStatus');
                if (_this.loginStatus === 'logged-in') {
                    console.log('logged in');
                    //    setTimeout(function(){
                    console.log(ctrl.currentRoute);
                    _this.loginInfo = true;
                    console.log('loginInfo');
                    console.log(_this.queryParamsRec);
                    console.log(_this.loginInfo);
                    console.log(_this.authDataRec);
                    if (_this.queryParamsRec && _this.loginInfo && _this.authDataRec) {
                        _this.doTheStuff();
                    }
                }
                else {
                    _this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    _this.showLogin = true;
                    _this.queryParamsRec = false;
                    _this.authDataRec = false;
                    _this.loginInfo = false;
                }
            }
        });
    };
    SsoLoginScreenComponent.prototype.pushBack = function () {
        var userparams = {
            "nonce": 'asdf',
            "external_id": this.userDetails.get('id'),
            "email": this.userDetails.get('email'),
            "username": 'asdf',
            "name": this.userDetails.get('first_name') + ' ' + this.userDetails.get('last_name'),
            "avatar_url": this.userDetails.get('image')
        };
        this._nsc.doSso(userparams).subscribe(function (data) {
            console.log('Made Request');
            console.log(data);
        });
    };
    SsoLoginScreenComponent.prototype.login = function () {
    };
    __decorate([
        select(), 
        __metadata('design:type', Observable)
    ], SsoLoginScreenComponent.prototype, "auth$", void 0);
    SsoLoginScreenComponent = __decorate([
        Component({
            templateUrl: 'sso-login.component.html',
            providers: [AuthActions, NestcareService, HttpClient, AppStatusActions, UserService, RegisterService],
        }), 
        __metadata('design:paramtypes', [AuthActions, Router, NgRedux, AppStatusActions, ActivatedRoute, NestcareService])
    ], SsoLoginScreenComponent);
    return SsoLoginScreenComponent;
}());
