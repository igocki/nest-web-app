/**
 * Created by Tommy on 1/31/2017.
 */


import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthActions } from '../../../actions/auth-actions';
import { AppStatusActions } from '../../../actions/app-status-actions';
import {Animations} from '../../animations';
import { Observable } from 'rxjs/Observable';
import { NestcareService } from '../../../services/nestcareService';
import { UserService } from '../../../services/userService';
import { HttpClient } from '../../../services/http-header-service';
import { IAuth } from '../../../store';
import { select } from 'ng2-redux';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../../store/index';
import {LoadingActions} from "../../../actions/loading-actions";
import { RegisterService } from '../../../services/registerService';
import {routerTransition } from '../../router.animations';

@Component({
    templateUrl: 'sso-login.component.html',
    providers: [AuthActions, NestcareService, HttpClient, AppStatusActions, UserService, RegisterService ],
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class SsoLoginScreenComponent implements OnInit {
    @select() auth$: Observable<IAuth>;
    private status:any;
    private loginStatus: any;
    private currentRoute: any;
    private incomingSso: any;
    private incomingSig: any;
    private userDetails: any;
    private loginInfo: boolean;
    private queryParamsRec: boolean;
    private authDataRec: boolean;
    private showLogin: boolean;

    constructor(
        private _authActions: AuthActions,
        private router: Router,
        private ngRedux: NgRedux<RootState>,
        private _appStatusActions: AppStatusActions,
        private activatedRoute: ActivatedRoute,
        private _nsc: NestcareService
    ) {
        this.loginInfo = false;
        this.queryParamsRec = false;
        this.authDataRec = false;
        this.showLogin = false;
    }

    finalRequest() {
        var userparams = {
            "nonce": this.incomingSso,
            "external_id": this.userDetails.get('id'),
            "email": this.userDetails.get('email'),
            "username": this.userDetails.get('email'),
            "name": this.userDetails.get('first_name') + ' ' + this.userDetails.get('last_name'),
            "avatar_url": this.userDetails.get('image')
        };

        this._nsc.doSso(userparams).subscribe(data => {
            console.log('Made Request!!');
            console.log(data.text());
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });

            window.location.href = 'http://community.mynest.care/session/sso_login?' + data.text().replace("\"", "").replace("\"", "");
        });
    }

    doTheStuff() {
        console.log('I got called.');
        this.loginInfo = false;
        this.queryParamsRec = false;
        // -- Validate the incoming payload.
        this._nsc.ssoValidate(this.incomingSso, this.incomingSig).subscribe(data => {
            console.log('Validate Response: ' + data.text());
            this.loginInfo = true;

            if (this.loginInfo && this.queryParamsRec) {
                this.finalRequest();
            }
        });

        this._nsc.getNonce(this.incomingSso).subscribe(data => {
            console.log('Nonce Response: ' + data.text().replace("\"", "").replace("\"", ""));
            this.queryParamsRec = true;
            this.incomingSso = data.text().replace("\"", "").replace("\"", "");
            if (this.loginInfo && this.queryParamsRec) {
                this.finalRequest();
            }
        });


    }

    ngOnInit() {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this.activatedRoute.queryParams.subscribe((params: Params) => {
           let sso = params['sso'];
           let sig = params['sig'];
           /*
            bm9uY2U9OTZiNzIxMzk2Y2ZiMTkzOWI3NTU3MjFkZTQ3YTFjMTImcmV0dXJu%0AX3Nzb191cmw9aHR0cCUzQSUyRiUyRmNvbW11bml0eS5teW5lc3QuY2FyZSUy%0ARnNlc3Npb24lMkZzc29fbG9naW4%3D%0A

            48145faf80313a86a334f3c22440c98083f5a3c593fc8a04800fa0ad972feb55

            */
           this.incomingSig = sig;
           this.incomingSso = sso;
           this.queryParamsRec = true;
            console.log('queryParams');
            console.log(this.queryParamsRec);
            console.log(this.loginInfo);
            console.log(this.authDataRec);
           if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
               this.doTheStuff();
           }
        });

        this._appStatusActions.clearSignupProcess();
        let app_status = this.ngRedux.select(state => state.appStatus);
        let app_route = this.ngRedux.select(state => state.router);
        let app_auth = this.ngRedux.select(state => state.auth);

        app_auth.subscribe(data => {
           if (data) {
               this.userDetails = data.get('profile');
               this.authDataRec = true;
               console.log('authData');
               console.log(this.queryParamsRec);
               console.log(this.loginInfo);
               console.log(this.authDataRec);
               if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
                   this.doTheStuff();
               }
              // this.pushBack();
           }
        });

        let ctrl = this;
        app_route.subscribe(data => {
            this.currentRoute = data;
        });
        app_status.subscribe(data => {
            if(data){
                this.status = data.get('appStatus');
                this.loginStatus = data.get('loginStatus');

                if(this.loginStatus === 'logged-in'){
                    console.log('logged in');
                //    setTimeout(function(){
                        console.log(ctrl.currentRoute);
                        this.loginInfo = true;
                        console.log('loginInfo');
                        console.log(this.queryParamsRec);
                        console.log(this.loginInfo);
                        console.log(this.authDataRec);
                        if (this.queryParamsRec && this.loginInfo && this.authDataRec) {
                            this.doTheStuff();
                        }
                 //   }, 100)
                } else {
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    this.showLogin = true;
                    this.queryParamsRec = false;
                    this.authDataRec = false;
                    this.loginInfo = false;
                }
            }
        });

    }

    pushBack() {
        var userparams = {
            "nonce": 'asdf',
            "external_id": this.userDetails.get('id'),
            "email": this.userDetails.get('email'),
            "username": 'asdf',
            "name": this.userDetails.get('first_name') + ' ' + this.userDetails.get('last_name'),
            "avatar_url": this.userDetails.get('image')
        };

        this._nsc.doSso(userparams).subscribe(data => {
            console.log('Made Request');
            console.log(data);
        });
    }

    login() {
    }


}
