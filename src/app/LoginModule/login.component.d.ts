import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../../store/index';
export declare class LoginComponent implements OnInit {
    private ngRedux;
    private router;
    private _appStatusActions;
    private status;
    private currentRoute;
    constructor(ngRedux: NgRedux<RootState>, router: Router, _appStatusActions: AppStatusActions);
    ngOnInit(): void;
}
