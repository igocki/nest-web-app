import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {routerTransition } from '../../router.animations';

@Component({
    templateUrl: 'forgetPassword.component.html',
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class ForgetPasswordComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {

    }

}
