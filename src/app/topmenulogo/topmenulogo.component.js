var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
export var TopMenuLogoComponent = (function () {
    function TopMenuLogoComponent(ngRedux, _appStatusActions, activatedRoute, router) {
        this.ngRedux = ngRedux;
        this._appStatusActions = _appStatusActions;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this._opened = false;
        this.isAdmin = false;
    }
    TopMenuLogoComponent.prototype.ngOnInit = function () {
        var _this = this;
        var ctrl = this;
        var app_route = this.ngRedux.select(function (state) { return state.router; });
        app_route.subscribe(function (data) {
            _this.currentRoute = data;
        });
        this.router.events
            .filter(function (event) { return event instanceof NavigationEnd; })
            .map(function () { return _this.activatedRoute; })
            .subscribe(function (event) {
            ctrl._opened = false;
        });
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        app_status.subscribe(function (data) {
            console.log('ya');
            console.log(data);
            if (data) {
                if (data.getIn(['admin'])) {
                    console.log('this worked');
                    _this.isAdmin = true;
                }
                else {
                    _this.isAdmin = false;
                }
            }
        });
    };
    TopMenuLogoComponent.prototype.goToAddCustomer = function () {
        this.ngRedux.dispatch({
            type: "CHANGE_TO_ENROLL_PLAN"
        });
        this.router.navigate(['/signupInfo']);
    };
    TopMenuLogoComponent.prototype.goToMessageBoard = function () {
        window.location.href = 'http://community.mynest.care';
    };
    TopMenuLogoComponent.prototype.logOff = function () {
        this._appStatusActions.logoff();
    };
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], TopMenuLogoComponent.prototype, "menuAppear", void 0);
    __decorate([
        Input(), 
        __metadata('design:type', Boolean)
    ], TopMenuLogoComponent.prototype, "logoAppear", void 0);
    TopMenuLogoComponent = __decorate([
        Component({
            templateUrl: 'topmenulogo.component.html',
            selector: 'top-menu-logo',
            providers: [AppStatusActions, NestcareService],
            changeDetection: ChangeDetectionStrategy.OnPush
        }), 
        __metadata('design:paramtypes', [NgRedux, AppStatusActions, ActivatedRoute, Router])
    ], TopMenuLogoComponent);
    return TopMenuLogoComponent;
}());
