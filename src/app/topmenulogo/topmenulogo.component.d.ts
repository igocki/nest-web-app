import { OnInit } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { Router, ActivatedRoute } from '@angular/router';
import { RootState } from '../../store';
import { AppStatusActions } from '../../actions/app-status-actions';
export declare class TopMenuLogoComponent implements OnInit {
    private ngRedux;
    private _appStatusActions;
    private activatedRoute;
    private router;
    private _opened;
    private currentRoute;
    private isAdmin;
    menuAppear: boolean;
    logoAppear: boolean;
    constructor(ngRedux: NgRedux<RootState>, _appStatusActions: AppStatusActions, activatedRoute: ActivatedRoute, router: Router);
    ngOnInit(): void;
    goToAddCustomer(): void;
    goToMessageBoard(): void;
    logOff(): void;
}
