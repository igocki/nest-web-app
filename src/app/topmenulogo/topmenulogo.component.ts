import { Component,
    Input,
    OnInit,
    ChangeDetectionStrategy
} from '@angular/core';
import {Animations} from '../../animations';
import { Observable } from 'rxjs/Observable';
import { select, NgRedux } from 'ng2-redux';
import { Map, List } from 'immutable';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute  } from '@angular/router'
import { RootState } from '../../store'
import { AppStatusActions } from '../../actions/app-status-actions';
import { NestcareService } from '../../services/nestcareService';
@Component({
    templateUrl: 'topmenulogo.component.html',
    selector: 'top-menu-logo',
    providers: [AppStatusActions, NestcareService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopMenuLogoComponent implements OnInit {
    private _opened: boolean = false;
    private currentRoute: any;
    private isAdmin: boolean = false;
    @Input() menuAppear: boolean;
    @Input() logoAppear: boolean;

    constructor(private ngRedux: NgRedux<RootState>,
                private _appStatusActions: AppStatusActions,
                private activatedRoute: ActivatedRoute,
                private router: Router) {

    }

    ngOnInit() {
        var ctrl = this;

        let app_route = this.ngRedux.select(state => state.router);

        app_route.subscribe(data => {
            this.currentRoute = data;
        });

        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .subscribe((event) => {
                ctrl._opened = false;
            });

        let app_status = this.ngRedux.select(state => state.appStatus);

        app_status.subscribe(data => {
            console.log('ya');
            console.log(data);
            if(data){
                if(data.getIn(['admin'])){
                    console.log('this worked');
                    this.isAdmin = true;
                } else {
                    this.isAdmin = false;
                }
            }
        });
    }

    goToAddCustomer(){
        this.ngRedux.dispatch({
            type: "CHANGE_TO_ENROLL_PLAN"
        });
        this.router.navigate(['/signupInfo']);
    }

    goToMessageBoard(){
        window.location.href = 'http://community.mynest.care'
    }

    logOff(){
        this._appStatusActions.logoff();
    }
}
