import moment from "moment";

export const NestCareConstants = {
    shippingFromAddress: {
        address_from: {
            object_purpose: "PURCHASE",
            name: "NestCare LLC",
            street1: "4595 Hidden Creek Trl",
            city: "Brookfield",
            state: "WI",
            country: "US",
            zip: "53005",
            phone: "+1 210 555 5555",
            email: "andrew@igocki.com"
        }
    },
    bundleInformation: {
        parcel: {
            length: "5",
            width: "5",
            height: "5",
            distance_unit: "in",
            weight: "5",
            mass_unit: "lb"
        }
    },
    account_type_hash: {
        1: "Subscriber",
        4: "Advocate",
        9: "Explorer"
    },
    secure: {
        payment_gateway_id: "6161"
    },
    recurlyPublicKey: 'ewr1-rDmPBC4Cq90umrqah44JJn',
    reoccuringPlans: [8201],
    defaultUserEmail: 'testnestuser41@mailinator.com',
    defaultCustomerPaywhirlId: '130',
    defaultNCJSONToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzMCwiaXNzIjoiaHR0cDpcL1wvNTQuODIuNzcuMzg6ODAwMVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTQ4NTkwNjg2MywiZXhwIjoxNTE3NDQyODYzLCJuYmYiOjE0ODU5MDY4NjMsImp0aSI6ImVkZDY0MGVmNzQ2ZmMyYmU4NzMxY2YyNTI1Y2M4ODc3In0.v3x4mhUomldl6At-CVe_BddrAV9UEZfP6zxnup33OuQ',
    defaultJSONToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMzAifQ.2z95YIlG17Lhw6oGlIw-aGckrKkf5Z85H_0ba3xtjGA',
    securityDepositMultiplier: 25,
    paymentPlans: {
        everyMonth: {
            id: 'main_monthly',
            amt: 9.99,
            title: 'Pay Once a Month',
            price: '$9.99 every month',
            totalprice: 139.99,
            lineItemTitle: 'Monthly Subscription',
            securityDeposit: 75,
            setupFee: 55
        },
        everyYear: {
            id: 'main_oneyear',
            amt: 107.88,
            title: 'Pay Once a Year',
            price: '$8.99 per month (upfront)',
            totalprice: 162.89,
            savings: '$22.89',
            lineItemTitle: '1 Year Subscription',
            securityDeposit: 0,
            setupFee: 55
        },
        every2Years:{
            id: 'main_twoyear',
            amt: 191.76,
            title: 'Pay Every 2 Years',
            totalprice: 246.81,
            price: '$7.99 per month (upfront)',
            savings: '$106.77',
            lineItemTitle: '2 Year Subscription',
            securityDeposit: 0,
            setupFee: 55
        },
        lifetime: {
            id: 'life',
            amt: 500,
            title: 'Pay once for Life',
            totalprice: 500,
            price: 'one time for life',
            savings: '$360.01',
            lineItemTitle: 'Lifetime Subscription',
            securityDeposit: 0,
            setupFee: 0
        }
    },
    householdPaymentPlans: {
        everyMonth: {
            id: 'main_monthly',
            amt: 19.98,
            title: 'Pay Once a Month',
            totalprice: 229.98,
            price: '$19.98 every month',
            lineItemTitle: 'Monthly Subscription x 2',
            securityDeposit: 100,
            setupFee: 110
        },
        everyYear: {
            id: 'main_oneyear',
            amt: 215.76,
            title: 'Pay Once a Year',
            totalprice: 325.76,
            price: '$17.98 per month (upfront)',
            savings: '$95.78',
            lineItemTitle: '1 Year Subscription x 2',
            securityDeposit: 0,
            setupFee: 110
        },
        every2Years:{
            id: 'main_twoyear',
            amt: 383.52,
            title: 'Pay Every 2 Years',
            totalprice: 493.62,
            price: '$15.98 per month (upfront)',
            savings: '$263.54',
            lineItemTitle: '2 Year Subscription x 2',
            securityDeposit: 0,
            setupFee: 110
        }
    },
    convertDate: function(date: Date): any{
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var day = date.getDate()

        var hours = date.getHours()
        var minutes = date.getMinutes()
        var seconds = date.getSeconds()
        var dateInt = moment(`${date.getFullYear()}-${date.getMonth()}-${date.getDate()} 00:00:00`, "YYYY-MM-DD HH:mm:ss").toDate().getTime()

        return {
            'Y': year,
            'm': month,
            'd': day,
            'H': hours,
            'i': minutes,
            's': seconds,
            'date': year + '-' + month + '-' + day,
            'time': hours + ':' + minutes + ':' + seconds,
            'full': year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds,
            'seconds': parseInt(((dateInt) / 1000).toString())
        }
    }
}
