export declare const NestCareConstants: {
    shippingFromAddress: {
        address_from: {
            object_purpose: string;
            name: string;
            street1: string;
            city: string;
            state: string;
            country: string;
            zip: string;
            phone: string;
            email: string;
        };
    };
    bundleInformation: {
        parcel: {
            length: string;
            width: string;
            height: string;
            distance_unit: string;
            weight: string;
            mass_unit: string;
        };
    };
    account_type_hash: {
        1: string;
        4: string;
        9: string;
    };
    secure: {
        payment_gateway_id: string;
    };
    recurlyPublicKey: string;
    reoccuringPlans: number[];
    defaultUserEmail: string;
    defaultCustomerPaywhirlId: string;
    defaultNCJSONToken: string;
    defaultJSONToken: string;
    securityDepositMultiplier: number;
    paymentPlans: {
        everyMonth: {
            id: string;
            amt: number;
            title: string;
            price: string;
            totalprice: number;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
        everyYear: {
            id: string;
            amt: number;
            title: string;
            price: string;
            totalprice: number;
            savings: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
        every2Years: {
            id: string;
            amt: number;
            title: string;
            totalprice: number;
            price: string;
            savings: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
        lifetime: {
            id: string;
            amt: number;
            title: string;
            totalprice: number;
            price: string;
            savings: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
    };
    householdPaymentPlans: {
        everyMonth: {
            id: string;
            amt: number;
            title: string;
            totalprice: number;
            price: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
        everyYear: {
            id: string;
            amt: number;
            title: string;
            totalprice: number;
            price: string;
            savings: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
        every2Years: {
            id: string;
            amt: number;
            title: string;
            totalprice: number;
            price: string;
            savings: string;
            lineItemTitle: string;
            securityDeposit: number;
            setupFee: number;
        };
    };
    convertDate: (date: Date) => any;
};
