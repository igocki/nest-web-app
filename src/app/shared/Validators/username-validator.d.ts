import { FormControl } from '@angular/forms';
export declare class EmailValidator {
    constructor();
    static validateEmail(c: FormControl): {
        validateEmail: {
            valid: boolean;
        };
    };
}
