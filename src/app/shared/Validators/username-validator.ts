import { FormControl } from '@angular/forms';


export class EmailValidator {
    constructor(){}

    static validateEmail(c: FormControl){
        let EMAIL_REGEXP: RegExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

        return EMAIL_REGEXP.test(c.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    }
}
