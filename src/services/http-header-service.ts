import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { NgRedux } from 'ng2-redux';
import { RootState, IAppStatus } from '../store';
import {Observable} from "rxjs";
import { Map } from 'immutable';

@Injectable()
export class HttpClient {

    secure: Observable<Map<string, any>>;
    constructor(private http: Http,
                private ngRedux: NgRedux<RootState>) {
        this.secure = this.ngRedux.select(state => (state.secure) ? state.secure : null);
    }

    createAuthorizationHeader(headers: Headers) {
        let unsubscribethis = this.secure.subscribe(tok => {
            if(tok && tok.get('igockiToken')){
                headers.append('Authorizationig', 'Bearer ' + tok.get('igockiToken'));
            }
            if(tok && tok.get('nestcareToken')){
                headers.append('Authorization', 'Bearer ' + tok.get('nestcareToken'));
            }
            if(tok && tok.get('tempigockiToken')){
                headers.append('Authorizationtempig', 'Bearer ' + tok.get('tempigockiToken'));
            }
        });
        unsubscribethis.unsubscribe();
    }

    get(url) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        });
    }

    post(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        });
    }

    put(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        });
    }
}
