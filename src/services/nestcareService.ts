import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {Observable} from "rxjs";
import { HttpClient } from './http-header-service'
import 'rxjs/Rx';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';

@Injectable()
export class NestcareService {
    private _server: string = '';
    private _apiUrl: string = "nc/";
    private _baseUrl = '';
    private userPhone;
    private userEmail;
    private isAdmin;
    private workflow;
    private currentNCToken;
    private ncBackupToken;

    constructor(private _http: HttpClient,
                private _config: ConfigEnvService,
                private ngRedux: NgRedux<RootState>) {
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        let appStatus = this.ngRedux.select(state => state.appStatus);
        let secure = this.ngRedux.select(state => state.secure);

        secure.subscribe(data => {
            if(data){
                this.currentNCToken = data.get('nestcareToken');
                this.ncBackupToken = data.get('nestcareBackupToken');
            }
        });

        appStatus.subscribe(data => {
            if(data){
                this.workflow = data.get('workflow');
                this.isAdmin = data.get('admin');
            }
            if(data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])){
                this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        userViewSub.subscribe((data) =>{
            if(data && !this.userEmail){
                this.userEmail = data.get('email') ;
            }
            if(data && !this.userPhone){
                this.userPhone = data.get('userPhone') ;
            }
        });

    }

    ssoLogout(id: any) {
        let bUrl = this._server + 'dc/logout?userId=' + id;
        let lhttp = this._http;

        return lhttp.post(bUrl, {});
    }

    addPaymentCustomer(params: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "paid_user": params.paid_for_customer,
            "id": params.paying_customer
        };

        return lhttp.post(bUrl + "insert_paid_user", postData);
    }

    changeAccountType(fromAccountType: any, toAccountType: any, cust: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var oldAcct = (fromAccountType === "explorer") ? 9 : undefined;
        var newAcct = (toAccountType === "support network") ? 1 : undefined;

        let postData = {
            "old_account_type_id": oldAcct,
            "new_account_type_id": newAcct,
            "id": cust
        };

        return lhttp.post(bUrl + "change_account_type", postData);
    }


    changeAccountTypeAdmin(toAccountType: any, cust: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var newAcct = (toAccountType === "support network") ? 1 : undefined;

        let postData = {
            "new_account_type_id": newAcct,
            "user_id": cust
        };

        return lhttp.post(bUrl + "changeAccountTypeAdmin", postData);
    }


    doSso(params: any) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        return lhttp.post(bUrl+"doSso", params);
    }


    ssoValidate(payload: string, sig: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        var params = {
            'payload': payload,
            'sig': sig
        };
        return lhttp.post(bUrl+"ssoValidate", params);
    }

    getNonce(nonce: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var params = {
            'nonce': nonce
        };

        return lhttp.post(bUrl+"getNonce", params);
    }

    getPeopleYouSupport(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "people_you_support")
            .map((res: Response) =>{
                return res.json();
            });
    }

    getPeoplePaidFor(custID){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var postData = {
            paying_user: custID
        };

        return lhttp.post(bUrl + "get_paid_for", postData)
            .map((res: Response) =>{
                return res.json();
            });
    }

    getPayingUser(custID){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var postData = {
            paid_user: custID
        };

        return lhttp.post(bUrl + "get_paying_user", postData)
            .map((res: Response) =>{
                return res.json();
            });
    }


    getMedicationActivities(date: string){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "get_medication_activities?date=" + date)
            .map((res: Response) =>{
                return res.json().data;
            });
    }

    medicationSchedules(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

    return lhttp.post(bUrl + "medications", postData)
        .map((res: Response) =>{
            return res.json().data;
        });;
    }

    getVitalMeasurements(date: string){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "vital_measurements?date=" + date)
            .map((res: Response) =>{
                return res.json().data;
            });
    }

    getVitalSchedules(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

        return lhttp.get(bUrl + "vital_schedules")
            .map((res: Response) =>{
                return res.json().data;
            });;
    }

    getWellnessActivities(date: string){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "wellness_activities?date=" + date)
            .map((res: Response) =>{
                return res.json().data;
            });
    }

    getWellnessSchedules(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

        return lhttp.get(bUrl + "wellness_schedules")
            .map((res: Response) =>{
                return res.json().data;
            });;
    }

    GetCustomer(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

        return lhttp.get(bUrl + "get_customer")
            .map((res: Response) =>{
                return res.json();
            });;
    }

    adminGetCustomer(custID){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

        return lhttp.get(bUrl + "admin_get_customer?id=" + custID)
            .map((res: Response) =>{
                return res.json();
            });;
    }

    adminGetAllCustomers(){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
        };

        return lhttp.get(bUrl + "admin_get_all_customers")
            .map((res: Response) =>{
                return res.json();
            });;
    }


    adminUpdateAccountVerified(params: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "status_id": params.status_id,
            "id": params.id
        };

        return lhttp.post(bUrl + "admin_change_verified", postData)
            .map((secondRes:Response) => {
                return secondRes.json();
            });
    }

    adminUpdateUserDetails(params: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "dob": params.dob.getFullYear() + '-' + ('0' + (params.dob.getMonth()+1)).slice(-2) + '-' + ('0' + (params.dob.getDate())).slice(-2),
            "gender": params.gender,
            "mobile": params.mobile,
            "id": params.id
        };

        return lhttp.post(bUrl + "admin_update_user_details", postData)
            .map((secondRes:Response) => {
                return secondRes.json();
            });
    }

    register(params: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "date_of_birth": params.date_of_birth,
            "gender": params.gender,
            "mobile": params.mobile,
            "password": params.password
        };

        return lhttp.post(bUrl + "register", postData);
            // .map((secondRes:Response) => {
            //     return secondRes.json();
            // })
            // .catch((error:any) => {
            //     let innerError = {
            //         nestCareError: error.json().message
            //     };
            //     return Observable.throw(innerError);
            // });
    }

    sendAuthTwoFactor(authCode, nestcareID){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "auth_code": authCode,
            "id": nestcareID
        };

        return lhttp.post(bUrl + "2fa", postData)
            .map((res:Response) => {
                if(res.json().token && res.json().meta){
                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                }

                return res.json()
            })
            .catch((error:any) => {
                let errMsg: string;
                return Observable.throw(error.json());
            });
    }

    adminLogin(email: string, password: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "email": email,
            "password": password
        };

        return lhttp.post(bUrl + "adminLogin", postData)
            .map((res:Response) => {
                if(res.json().meta){
                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                    this.ngRedux.dispatch({
                        type: 'ADMIN_LOGGED_IN',
                        payload: res.json().meta.token
                    });
                }

                return res.json()
            })
            .catch((error:any) => {
                let errMsg: string;
                return Observable.throw(error.json());
            });
    }

    login(email: string, password: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "email": email,
            "password": password
        };

        return lhttp.post(bUrl + "login", postData)
            .map((res:Response) => {
                if(res.json().meta){
                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                }

                return res.json()
            })
            .catch((error:any) => {
                console.log("HEYYYYY");
                console.log(error);
                let errMsg: string;
                return Observable.throw(error.json());
            });
    }


    getCompletedOrders(fromDate = undefined, toDate = undefined){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let urlString = 'view_completed_orders';

        if(fromDate && !toDate){
            urlString += '?fromDate=' + fromDate;
        }

        if(toDate && !fromDate){
            urlString += '?toDate=' + fromDate;
        }
        if(fromDate && toDate){
            urlString += '?fromDate=' + fromDate + '&toDate=' + toDate;
        }

        return lhttp.get(bUrl + urlString)
            .map((res: Response) =>{
                return res.json();
            });
    }


    addCompletedOrderTracker(params: any, cust: any){
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        var postData;

        if(this.workflow === 'household'){
            postData = {
                "paying_cust": cust,
                "shippo_id": params.shippo_id,
                "tracking_number": params.tracking_number,
                "status": params.status,
                "id": cust
            };
        } else {
            postData = {
                "paying_cust": params.paying_cust,
                "shippo_id": params.shippo_id,
                "tracking_number": params.tracking_number,
                "status": params.status,
                "id": cust
            };
        }

        let adminToken = undefined;
        if(this.isAdmin){
            adminToken = this.currentNCToken;
            if(this.ncBackupToken){
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.ncBackupToken
                });
            }
        }


        return lhttp.post(bUrl + "add_completed_order_tracker", postData)
            .map((res: Response) => {
                if(adminToken){
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: adminToken
                    });
                }
                return res.json();
            })
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
    }

    updateProfile(params: any) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "gender": params.gender,
            "date_of_birth": params.date_of_birth,
            "home_line1": params.street1,
            "home_line2": params.street2,
            "home_city_id": params.city,
            "home_state_id": params.state,
            "home_zip_code": params.zip,
            "home_country_id": 'US',
            "home_phone": this.userPhone
        };

        return lhttp.post(bUrl + "updateProfile", postData)
            .map((res:Response) => {
                console.log('RETURNED A RESPONSE');
                return res.json()
            })
            .catch((error:any) => {
                let errMsg: string;
                return Observable.throw(error.json());

            });
    }

    resetPassword(email: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "email": email
        };

        return lhttp.post(bUrl + "resetPassword", postData);
    }

    checkIfAuthorized(id){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var postData = {
            id: id
        }

        return lhttp.post(bUrl + "check_if_authorized", postData);
    }


    updateShipping(params: any) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": this.userPhone
        };

        return lhttp.post(bUrl + "updateShipping", postData);
    }

    updateShippingAdmin(params: any, userID: string, isAdmin = false) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": this.userPhone,
            "id": userID,
            "billing": (isAdmin) ? 1 : 0
        };
        console.log('hereerrr');
        console.log(postData);

        return lhttp.post(bUrl + "updateShippingAdmin", postData);
    }


    updateBillingAdminAsAuthorizedUser(params: any, userID: string, isAdmin = false, phone = '+15555555555') {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": phone,
            "id": userID,
            "billing": 1
        };
        console.log('hereerrr');
        console.log(postData);

        return lhttp.post(bUrl + "update_shipping_admin_as_auth_user", postData);
    }

    updateBilling(params: any){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": '+15555555555'
        };

        return lhttp.post(bUrl + "updateBilling", postData);
    }

}
