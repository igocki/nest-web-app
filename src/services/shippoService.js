var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgRedux } from 'ng2-redux';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs";
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';
import { List } from 'immutable';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';
export var ShippoService = (function () {
    function ShippoService(_http, _config, ngRedux) {
        var _this = this;
        this._http = _http;
        this._config = _config;
        this.ngRedux = ngRedux;
        this._server = '';
        this._apiUrl = "ship/";
        this._baseUrl = '';
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        this.shippingRates = this.ngRedux.select(function (state) { return (state.shippingRates) ? state.shippingRates.get('rates') : List([]); });
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        appStatus.subscribe(function (data) {
            if (data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])) {
                _this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                _this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data && !_this.userEmail) {
                console.log('RANSSSSSS2323');
                _this.userEmail = data.get('email');
            }
            if (data && !_this.userPhone) {
                _this.userPhone = data.get('userPhone');
            }
        });
    }
    ShippoService.prototype.validateAddress = function (params) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        console.log('heyyy');
        console.log(params.email);
        var postData = {
            object_purpose: 'PURCHASE',
            name: params.name,
            street1: params.street1,
            street2: params.street2,
            city: params.city,
            state: params.state,
            zip: params.zip,
            country: 'US',
            email: this.userEmail,
            validate: true
        };
        return lhttp.post(bUrl + "validate", postData)
            .map(function (res) {
            console.log('RETURNED A RESPONSE');
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    ShippoService.prototype.loadShippingRates = function (shipment, user) {
        var _this = this;
        var shippingRatesItems = [];
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var addressLine1Array = shipment.addressLine1.split(" ");
        var street1 = shipment.addressLine1.replace(addressLine1Array[0] + " ", "");
        var postData = {
            "object_purpose": "PURCHASE",
            "address_from": NestCareConstants.shippingFromAddress.address_from,
            "parcel": NestCareConstants.bundleInformation.parcel,
            "address_to": {
                "object_purpose": "PURCHASE",
                "name": shipment.name,
                "street_no": addressLine1Array[0],
                "street1": street1,
                "street2": shipment.addressLine2,
                "city": shipment.city,
                "state": shipment.state,
                "zip": shipment.zipCode,
                "country": "US",
                "phone": this.userPhone,
                "email": this.userEmail
            },
            "async": false
        };
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        lhttp.post(bUrl + "shipment", postData)
            .map(function (res) {
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            return Observable.throw(error.json());
        })
            .subscribe(function (resp) {
            resp.rates_list.sort(function (a, b) {
                return parseFloat(a.amount) - parseFloat(b.amount);
            });
            shippingRatesItems = resp.rates_list;
            _this.ngRedux.dispatch({
                type: 'ADD_RATES',
                payload: shippingRatesItems
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            _this.ngRedux.dispatch({
                type: 'FETCH_RATES_FAILURE',
                payload: { err: error }
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    ShippoService.prototype.createShipment = function (shipment, user) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var addressLine1Array = shipment.addressLine1.split(" ");
        var street1 = shipment.addressLine1.replace(addressLine1Array[0] + " ", "");
        var postData = {
            "object_purpose": "PURCHASE",
            "address_from": NestCareConstants.shippingFromAddress.address_from,
            "parcel": NestCareConstants.bundleInformation.parcel,
            "address_to": {
                "object_purpose": "PURCHASE",
                "name": shipment.name,
                "street_no": addressLine1Array[0],
                "street1": street1,
                "street2": shipment.addressLine2,
                "city": shipment.city,
                "state": shipment.state,
                "zip": shipment.zipCode,
                "country": "US",
                "phone": this.userPhone,
                "email": this.userEmail
            },
            "async": false
        };
        return lhttp.post(bUrl + "shipment", postData)
            .map(function (res) {
            console.log('RETURNED A RESPONSE');
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            return Observable.throw(error.json());
        });
    };
    ShippoService.prototype.createLabel = function (params) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            rate: params.shippingRateId,
            async: false
        };
        return lhttp.post(bUrl + "label", postData)
            .map(function (res) {
            console.log('RETURNED A RESPONSE');
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            _this.ngRedux.dispatch({
                type: 'GENERAL_SUBSCRIPTION_FAILURE',
                payload: {
                    message: 'Something went wrong with the Order. Contact Customer Service.'
                }
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    ShippoService.prototype.TrackPackage = function (carrier, tracking) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "track?carrier=" + carrier + "&tracking=" + tracking)
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    ShippoService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http, ConfigEnvService, NgRedux])
    ], ShippoService);
    return ShippoService;
}());
