import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
export declare class HttpClient {
    private http;
    constructor(http: Http);
    get(url: string): Observable<Response>;
    post(url: string, body: any): Observable<Response>;
    put(url: string, body: any): Observable<Response>;
    private request(url, method, body?);
    private createPaywhirlHeaders(headers);
}
