var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { NgRedux } from 'ng2-redux';
export var UserService = (function () {
    function UserService(ngRedux) {
        this.ngRedux = ngRedux;
        this.email = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile', 'email']);
        });
        this.fullname = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile', 'first_name']) + ' ' + state.auth.getIn(['profile', 'last_name']);
        });
        this.shipAddress = this.ngRedux.select(function (state) {
            return state.userView.getIn(['shippingAddress']);
        });
        this.userStatus = this.ngRedux.select(function (state) {
            return state.userView.getIn(['status']);
        });
        this.mobile = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile', 'mobile']);
        });
        this.birthdate = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile', 'date_of_birth']);
        });
        this.gender = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile', 'gender']);
        });
    }
    UserService.prototype.login = function () {
        this.refresh();
    };
    UserService.prototype.refresh = function () {
        var _this = this;
        var authSub = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile']);
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        this.subscriptionUserView = userViewSub.subscribe(function (data) {
            if (data) {
                _this.planName = data.getIn(['plan', 'planName']);
                _this.planAmt = data.getIn(['plan', 'planCost']);
                _this.lastPmt = data.getIn(['plan', 'last_payment']);
                _this.nextPmt = data.getIn(['plan', 'next_payment']);
                _this.orders = data.getIn(['orders']);
                console.log('heree now');
                console.log(data.getIn(['billingAddress']));
                _this.billingInfo = data.get('billingAddress');
                console.log('hit this');
            }
        });
        this.subscriptionUserView.unsubscribe();
        this.subscriptionAuth = authSub.subscribe(function (data) {
            console.log('LOOK AT THIS');
            console.log(data);
            if (data) {
                var userObj = {
                    email: data.get('email'),
                    fullname: data.get('first_name') + ' ' + data.get('last_name'),
                    first_name: data.get('first_name'),
                    last_name: data.get('last_name'),
                    userPhone: data.get('mobile'),
                    userBirth: data.get('date_of_birth'),
                    userGender: data.get('gender'),
                    accountType: (data.getIn(['account_type'])) ? data.getIn(['account_type', 'text']) || data.getIn(['account_type']).text : '',
                    nc_id: data.get('id')
                };
                _this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: userObj
                });
                _this.nestcareID = data.get('id');
                _this.name = data.get('first_name') + ' ' + data.get('last_name');
                // this.shipAddress = data.getIn(['address', 'home']);
                _this.paywhirlAccount = data.getIn(['paywhirl']);
                console.log('end here 1:');
            }
            console.log('end here 2:');
        });
        this.subscriptionAuth.unsubscribe();
    };
    UserService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux])
    ], UserService);
    return UserService;
}());
