/**
 * Created by Tommy on 11/17/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { HttpClient } from './http-header-service';
import { Observable } from "rxjs";
import { List } from 'immutable';
import { ConfigEnvService } from '../services/env/configEnvService';
export declare class PaywhirlService {
    private _http;
    private _config;
    private ngRedux;
    private _server;
    private _apiUrl;
    private _baseUrl;
    paymentCards: Observable<List<any>>;
    userPlans: Observable<List<any>>;
    customerInfo: any;
    customerID: any;
    private loginStatus;
    private backuptoken;
    constructor(_http: HttpClient, _config: ConfigEnvService, ngRedux: NgRedux<RootState>);
    customerState: any;
    subscribe(cust: any, {if: }: {
        if?: (cust: any) => void;
    }): any;
    custID: any;
    subscribe(data: any, {if: }: {
        if?: (data: any) => void;
    }): any;
}
