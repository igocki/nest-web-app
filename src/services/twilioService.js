var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgRedux } from 'ng2-redux';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs";
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';
export var TwilioService = (function () {
    function TwilioService(_http, _config, ngRedux) {
        this._http = _http;
        this._config = _config;
        this.ngRedux = ngRedux;
        this._server = '';
        this._apiUrl = "twilio/";
        this._baseUrl = '';
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
    }
    TwilioService.prototype.sendRandomNumberSMS = function (phone) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            phone: phone
        };
        return lhttp.post(bUrl + "send_random", postData)
            .map(function (res) {
            console.log('RETURNED A RESPONSE');
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    TwilioService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http, ConfigEnvService, NgRedux])
    ], TwilioService);
    return TwilioService;
}());
