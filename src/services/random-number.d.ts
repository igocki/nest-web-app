/**
 * Simple service designed to demonstrate using a DI-injected
 * service in your action creators.
 */
export declare class RandomNumberService {
    pick(): number;
}
