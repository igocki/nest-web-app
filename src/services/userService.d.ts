import 'rxjs/Rx';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
export declare class UserService {
    private ngRedux;
    email: any;
    fullname: any;
    birthdate: any;
    mobile: any;
    gender: any;
    nestcareID: any;
    planName: any;
    planAmt: any;
    lastPmt: any;
    nextPmt: any;
    orders: any;
    name: any;
    shipAddress: any;
    billingInfo: any;
    userStatus: any;
    subscriptionAuth: any;
    subscriptionUserView: any;
    paywhirlAccount: any;
    constructor(ngRedux: NgRedux<RootState>);
    login(): void;
    refresh(): void;
}
