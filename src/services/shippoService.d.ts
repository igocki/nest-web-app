/**
 * Created by Tommy on 11/22/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Http } from '@angular/http';
import { Observable } from "rxjs";
import { List } from 'immutable';
import { ConfigEnvService } from '../services/env/configEnvService';
export declare class ShippoService {
    private _http;
    private _config;
    private ngRedux;
    private _server;
    private _apiUrl;
    private _baseUrl;
    private userPhone;
    private userEmail;
    shippingRates: Observable<List<any>>;
    constructor(_http: Http, _config: ConfigEnvService, ngRedux: NgRedux<RootState>);
    validateAddress(params: any): Observable<any>;
    loadShippingRates(shipment: any, user: any): void;
    createShipment(shipment: any, user: any): Observable<any>;
    createLabel(params: any): Observable<any>;
    TrackPackage(carrier: string, tracking: string): Promise<{}>;
}
