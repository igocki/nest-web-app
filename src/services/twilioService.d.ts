/**
 * Created by Tommy on 11/22/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Http } from '@angular/http';
import { Observable } from "rxjs";
import { ConfigEnvService } from '../services/env/configEnvService';
export declare class TwilioService {
    private _http;
    private _config;
    private ngRedux;
    private _server;
    private _apiUrl;
    private _baseUrl;
    constructor(_http: Http, _config: ConfigEnvService, ngRedux: NgRedux<RootState>);
    sendRandomNumberSMS(phone: any): Observable<any>;
}
