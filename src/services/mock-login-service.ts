import { Injectable } from '@angular/core';
import { HttpClient } from './http-header-service';
/**
 * Simple service designed to demonstrate using a DI-injected
 * service in your action creators.
 */
@Injectable()
export class MockLoginService {

    constructor(public http: HttpClient) {

    }

    g
    login(customer: string, subscriptionID: string, mockFail?: string) {
       // this.http.post('http://localhost:1337/crossorigin?https://api.paywhirl.com/subscribe/customer',
        this.http.get('http://localhost:1337/pay/customers').subscribe(p => console.log('asdasd ' + p));
          //  {
          //      customer_id: customer,
          //      plan_id: subscriptionID,
          //      quantity: 1
          //  }


        console.log('did it');
        return new Promise( function (resolve, reject) {
            if(!mockFail) {
                resolve({
                    error: null,
                    token: 'tempToken',
                    profile: {
                        name: 'andrew alanis'
                    }
                });
            } else {
                reject({
                    error: null,
                    token: 'tempToken',
                    profile: {
                        name: 'andrew alanis'
                    }
                });
            }
        });
    }
}
