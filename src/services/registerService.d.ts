import { Observable } from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
export declare class RegisterService {
    private _nestcare;
    private _paywhirl;
    private ngRedux;
    private userFirstName;
    private userLastName;
    private userEmail;
    private loginStatus;
    private workflow;
    private nc_id;
    private isAdmin;
    private paidUser;
    private payingUser;
    private nestcareToken;
    constructor(_nestcare: NestcareService, _paywhirl: PaywhirlService, ngRedux: NgRedux<RootState>);
    getCardsAfterRecurlyRegister(userID: any): Observable<any>;
    adminGetUserInformation(nestcareID: any): Observable<any>;
    getUserInformation(nestcareID: any): Observable<any>;
    checkRecurlyAndCreateAndNoToken(nestcareID: any, userParams: any): Observable<any>;
    checkRecurlyAndCreate(nestcareID: any, userParams: any): Observable<any>;
    registerAndLogin(email: string, password: string): Observable<any>;
    registerUser(params: any, paidForOther?: boolean, isAdmin?: boolean): Observable<any>;
}
