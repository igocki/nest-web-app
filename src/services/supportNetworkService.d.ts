import { Observable } from "rxjs";
import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Router } from '@angular/router';
export declare class SupportNetworkService {
    private _nestcare;
    private ngRedux;
    private router;
    private isAdmin;
    constructor(_nestcare: NestcareService, ngRedux: NgRedux<RootState>, router: Router);
    getPeopleYouSupport(custID: any): Observable<any>;
}
