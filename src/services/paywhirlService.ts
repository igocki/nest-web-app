/**
 * Created by Tommy on 11/17/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from './http-header-service'
import {Observable} from "rxjs";
import { Map, List } from 'immutable';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants'
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';

@Injectable()
export class PaywhirlService {
    private _server: string = '';
    private _apiUrl: string = "pay/";
    private _baseUrl = '';
    paymentCards: Observable<List<any>>;
    userPlans: Observable<List<any>>;
    customerInfo: any;
    customerID: any;
    private loginStatus;
    private payingCustomer;
    private backuptoken;
    private accountType;
    private isAdmin;
    constructor(private _http: HttpClient,
                private _config: ConfigEnvService,
                private ngRedux: NgRedux<RootState>) {
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        this.paymentCards = this.ngRedux.select(state => (state.billing) ? state.billing.get('cards') : List<any>([]));
        this.userPlans = this.ngRedux.select(state => (state.appStatus) ? List<any>(state.appStatus.get('plans')) : List<any>([]));
        let customerState = this.ngRedux.select(state => (state.appStatus) ? state.appStatus.getIn(['signupInfo', 'userInfo']) : null);
        let custID = this.ngRedux.select(state => state.appStatus);
        let userView = this.ngRedux.select(state => state.userView);
        let app_status = this.ngRedux.select(state => state.appStatus);
        let secure = this.ngRedux.select(state => state.secure);
        app_status.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                this.payingCustomer = data.get('payingCustomerID');
                this.isAdmin = data.get('admin');
            }
        });
        secure.subscribe(data =>{
            if(data){
                this.backuptoken = data.get('igockiToken');
            }
        });
        userView.subscribe(data =>{
            if(data){
                this.accountType = data.get('accountType');
            }

        });
        customerState.subscribe(cust => {
            if(cust){
                this.customerInfo = cust;
            }
        });
        custID.subscribe(data => {
            if(data){
                this.customerID = data.get('payingCustomerID');
            }
        });
    }


    replaceCard(params: any, customerID: string){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.createCard(params, customerID)
            .subscribe(resp2 => {
                // this.updateCustomer({
                //     first_name: this.customerInfo.get('first_name'),
                //     last_name: this.customerInfo.get('last_name'),
                //     email: this.customerInfo.get('email'),
                //     address: params.addressLine1 + '_' + params.addressLine2,
                //     city: params.city,
                //     state: params.state,
                //     zip: params.zipCode,
                //     "currency": "USD",
                //     "gateway_id": NestCareConstants.secure.payment_gateway_id
                // }, customerID).subscribe(resp3 => {
                //     let updateCust = resp3.json();
                //     this.ngRedux.dispatch({
                //         type: 'ADD_BILLING_ADDRESS',
                //         payload: {
                //             address: updateCust.address,
                //             city: updateCust.city,
                //             state: updateCust.state,
                //             zip: updateCust.zip
                //         }
                //     });
                // },
                //  error => {
                //      this.ngRedux.dispatch({
                //          type: 'ADDING_CARD_FAILURE',
                //          payload: {
                //              error: error,
                //              defaultMessage: 'Something went wrong.. Try again'
                //          }
                //      });
                //  });
                this.loadPaymentCardsWithoutSub(customerID).subscribe((res)=>{
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    let pmtCards: any[] =[];
                    console.log('GOT PAYMENT CARDS');
                    console.log(res);
                    pmtCards = res;
                    this.ngRedux.dispatch({
                        type: 'GET_CARDS',
                        payload: pmtCards
                    });

                }, error =>{
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    this.ngRedux.dispatch({
                        type: 'FETCH_CARDS_FAILURE',
                        payload: {err: error}
                    });
                    console.log(error);
                });
                this.ngRedux.dispatch({
                    type: 'SELECT_CARD',
                    payload: resp2
                });
            });
    }


    deleteCard(params: any){
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        let cardToDelete = {
            id: ''
        };
        if(params){
            cardToDelete.id = params.id;
            this.ngRedux.dispatch({
                type: 'DELETING_OTHER_CARD'
            });
            return lhttp.post(bUrl + "deleteCard", cardToDelete)
                .map((res: Response) => {
                    return res.json();
                })
                .catch((error: any) => {
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    this.ngRedux.dispatch({
                        type: 'DELETING_OTHER_CARD_FAILURE',
                        payload: {
                            errorObj: error,
                            defaultMessage: 'Something went wrong. Try again'
                        }
                    });
                    return Observable.throw(error.json());
                })
        } else {
            return Observable.create(observer => {
                observer.next('done');
            })
        }


    }

    createCard(params: any, customerID: string){
        let cardToCreate = {
            id: customerID,
            token_id: params.token_id
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        this.ngRedux.dispatch({
            type: 'ADDING_CARD'
        });
        return lhttp.post(bUrl + "createCard", cardToCreate)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log('errored');
                console.log(error);
                this.ngRedux.dispatch({
                    type: 'ADDING_CARD_FAILURE',
                    payload: {
                        error: error,
                        defaultMessage: 'Something went wrong.. You card may have declined. Contact Support or try again.'
                    }
                });
                return Observable.throw(error.json());
            })
    }


    loadLoginPaymentCards(customerId: string){
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map((res: Response) =>{
            return res.json();
        });
    }


    loadPaymentCards(customerId: string){
        let pmtCards: any[] =[];
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        console.log('loadingPaymentCards');
        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map((res: Response) =>{
                console.log('GOT PAYMENT CARDS');
                console.log(res);
                pmtCards = res.json();
                this.ngRedux.dispatch({
                    type: 'GET_CARDS',
                    payload: pmtCards
                });

              return res.json();
            })
            .catch((error:any) =>{
                this.ngRedux.dispatch({
                    type: 'FETCH_CARDS_FAILURE',
                    payload: {err: error}
                });

                return Observable.throw(error.json());
            })
    }

    loadPaymentCardsWithoutSub(customerId: string){
        let pmtCards: any[] =[];
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        console.log('loadingPaymentCardsWithoutSub');
        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map((res: Response) =>{
                return res.json();
            })
            .catch((error:any) =>{
                return Observable.throw(error.json());
            })
    }

    GetCustomers() {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

         return new Promise(function(resolve, reject) {
             lhttp.get(bUrl + "customers/")
                 .catch((error:any) => Observable.throw(error || 'Server Error'))
                 .subscribe(d => resolve(d.json()), e => reject(e));
         });
    }

    GetCustomer(id: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "customer?id=" + id).map((resp: Response) => {
            return resp.json()
        }).catch((error) =>{
            return Observable.throw(error.json());
        });
    }

    GetPdfInvoiceUrl(id: string, custID: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        var ctrl = this;
        return lhttp.get(bUrl + "getInvoiceSecCheck?id=" + custID).flatMap((resp: Response) => {
            var data = resp.json();
            this.ngRedux.dispatch({
                type: 'ADD_IG_TEMP_TOKEN',
                payload: data.token
            });
            return Observable.create(observer => {
                observer.next(ctrl._baseUrl + 'getInvoicePdf?id=' + custID + '&invoice_id=' + id + '&temp=' + data.token);
            });
        })

    }

    LookupCollectedInvoices(id: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var getUrl = bUrl + "get_collected_invoices?id=" + id;
        console.log("asdasdasasdasdasdYASDASDSDASD");
        console.log(this.accountType);
        if(this.accountType && this.accountType === 'Subscriber'){
            getUrl = bUrl + "get_collected_invoices?id=" + id + "&household=true";
        }


        return lhttp.get(getUrl).map((resp: Response) => {
            return resp.json()
        });
    }

    LookupActiveSubscriptions(id: string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "get_active_subscriptions?id=" + id).map((resp: Response) => {
            return resp.json()
        });
    }

    CreateCustomer(params: any, paidForOther: boolean, noToken = false) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        let postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "nc_id": params.nc_id,
            "email": params.email,
            "payment_source_id": undefined
        };

        if(paidForOther){
            postData.payment_source_id = this.payingCustomer
            console.log('PAID FOR OTHER');
            console.log(postData.payment_source_id);
        }
        console.log('NNNNNNNOOOOOOOOOOPPPPPPEEE');

        return lhttp.post(bUrl + "customer", postData)
        .map((secondRes:Response) => {
            if(!noToken){
                if(this.loginStatus === 'logged-in' && paidForOther){
                    this.ngRedux.dispatch({
                        type: 'ADD_BACKUP_TOKEN',
                        payload: this.backuptoken
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: secondRes.json().token
                    });
                } else {
                    if(!this.isAdmin){
                        this.ngRedux.dispatch({
                            type: 'ADD_IG_TOKEN',
                            payload: secondRes.json().token
                        });
                        console.log('TOKEN USED:');
                        console.log(secondRes.json().token)
                    }
                }
            }

            return secondRes.json();
        })
        .catch((error:any) => {
            let innerError = {
                nestCareError: error
            };
            return Observable.throw(innerError);
        });
    }

    updateCustomer(params: any, customerID: string) {// -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = params;
        postData.id = customerID;

        return lhttp.put(bUrl + "customer", postData);
    }

    UpdateAnswer() {// -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.put(bUrl + "answer/", {})
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    GetQuestions() {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.get(bUrl + "questions/")
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    GetAllStatusCustomers() {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "all_subscribed_customers").map((resp: Response) => {
            return resp.json()
        });
    }

    GetPlans() {
        let mainPlans: any[] =[];
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.get(bUrl + "plans")
            .map((res: Response) =>{
                return res.json();
            })
            .catch((error:any) =>{
                return Observable.throw(error.json());
            })
            .subscribe(resp => {
                    console.log('GOT PAYMENT PLANS');
                    console.log(resp);
                    mainPlans = resp;
                    this.ngRedux.dispatch({
                        type: 'GET_PLANS',
                        payload: mainPlans
                    });
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                },
                error => {
                    // this.ngRedux.dispatch({
                    //     type: 'GET_PLANS_FAILURE',
                    //     payload: {err: error}
                    // });

                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                });
    }

    GetPlan(id: number) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.get(bUrl + "plan?id=" + id)
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    CreatePlan() {// -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.post(bUrl + "plan/", {})
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    UpdatePlan() { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.put(bUrl + "plan/", {})
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    GetSubscriptions() {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.get(bUrl + "subscriptions/")
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }

    GetSubscription(id: number) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.get(bUrl + "subscription?id=" + id)
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }


    createCharge(params: any, customerID: string) {
        let chargeParams = {
            id: customerID,
            amount: params.amount,
            description: params.description
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.post(bUrl + "create/charge", chargeParams)
            .map((res: Response) => {

                return res.json();
            })
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            })
    }

    refundCharge(uuid: any) {

        let bUrl = this._baseUrl;
        let lhttp = this._http;

        var params = {
            uuid: uuid
        }

        return lhttp.post(bUrl + "refund_charge", params)
            .map((res: Response) => {

                return res;
            })
    }

    getCouponDetails(params: any){
        let postData = {
            coupon_code: params.coupon_code
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.post(bUrl + "coupon", postData)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            })
    }

    getPromoCodeAmount(customerID: string){
        let params = {
            id: customerID
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.post(bUrl + "get_promo_amt", params)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            })
    }


    applyPromoCode(promo: string, customerID: string) {
        let params = {
            id: customerID,
            coupon: promo
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.post(bUrl + "redeem_coupon", params)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            })
    }

    //Subscribes only New Customers for the first time
    subscribeCustomer(params: any, customerID: string, quantity = 1) {
        let subscribeParams = {
            id: customerID,
            plan_id: params.plan_id,
            sec_dep: (params.sec_dep) ? params.sec_dep : 0,
            setup_fee: (params.setup_fee) ? params.setup_fee : 0,
            quantity: quantity
        };
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return lhttp.post(bUrl + "subscribeNew", subscribeParams)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                this.ngRedux.dispatch({
                    type: 'GENERAL_SUBSCRIPTION_FAILURE',
                    payload: {
                        message: 'Something went wrong with the Subscription. Contact Customer Service.'
                    }
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            })
    }
}
