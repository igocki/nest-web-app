/**
 * Created by Tommy on 11/22/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import { Map, List } from 'immutable';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';

@Injectable()
export class TwilioService {
    private _server: string = '';
    private _apiUrl: string = "twilio/";
    private _baseUrl = '';

    constructor(private _http: Http,
                private _config: ConfigEnvService,
                private ngRedux: NgRedux<RootState>) {
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
    }



    sendRandomNumberSMS(phone) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        let postData = {
            phone: phone
        };


        return lhttp.post(bUrl + "send_random", postData)
            .map((res:Response) => {
                console.log('RETURNED A RESPONSE');
                console.log(res.json());
                return res.json();
            })
            .catch((error:any) => {
                let errMsg: string;
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                return Observable.throw(error.json());
            });
    }
}
