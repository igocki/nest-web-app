var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
export var RegisterService = (function () {
    function RegisterService(_nestcare, _paywhirl, ngRedux) {
        var _this = this;
        this._nestcare = _nestcare;
        this._paywhirl = _paywhirl;
        this.ngRedux = ngRedux;
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        var secure = this.ngRedux.select(function (state) { return state.secure; });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
                _this.workflow = data.get('workflow');
                _this.paidUser = data.get('paidForCustomerID');
                _this.payingUser = data.get('payingCustomerID');
                _this.isAdmin = data.get('admin');
            }
            if (data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])) {
                _this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                _this.userFirstName = data.getIn(['signupInfo', 'userInfo', 'first_name']);
                _this.userLastName = data.getIn(['signupInfo', 'userInfo', 'last_name']);
            }
        });
        secure.subscribe(function (data) {
            if (data) {
                _this.nestcareToken = data.get('nestcareToken');
            }
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data && !_this.userEmail) {
                _this.userEmail = data.get('email');
            }
            if (data && !_this.userFirstName) {
                _this.userFirstName = data.get('first_name');
            }
            if (data && !_this.userLastName) {
                _this.userFirstName = data.get('last_name');
            }
            if (data) {
                _this.nc_id = data.get('nc_id');
            }
        });
    }
    RegisterService.prototype.getCardsAfterRecurlyRegister = function (userID) {
        var _this = this;
        if (this.workflow === 'other' || this.workflow === 'household') {
            var params = {
                "paid_for_customer": this.paidUser,
                "paying_customer": this.payingUser
            };
            return this._nestcare.addPaymentCustomer(params).flatMap(function (resp) {
                console.log('successful added paying customer');
                return _this._paywhirl.loadPaymentCards(userID);
            });
        }
        else {
            return this._paywhirl.loadPaymentCards(userID);
        }
    };
    RegisterService.prototype.adminGetUserInformation = function (nestcareID) {
        var _this = this;
        return this._paywhirl.GetCustomer(nestcareID).flatMap(function (custResp) {
            return Observable.forkJoin([
                _this._paywhirl.LookupActiveSubscriptions(nestcareID),
                _this._paywhirl.loadLoginPaymentCards(nestcareID),
                _this._paywhirl.LookupCollectedInvoices(nestcareID)
            ]);
        }).catch(function (error) {
            console.log('ERRORED OUT');
            if (error && error.message && error.message === 'Recurly Customer Not Found') {
                error.recurlyNotFound = true;
                return Observable.throw(error);
            }
            else {
                console.log('threw this');
                console.log(error);
                return Observable.throw(error);
            }
        });
    };
    RegisterService.prototype.getUserInformation = function (nestcareID) {
        var _this = this;
        console.log('GETTING USER INFORMATION');
        return Observable.forkJoin([
            this._nestcare.GetCustomer(),
            this._paywhirl.GetCustomer(nestcareID)]).flatMap(function (custResp) {
            if (!_this.isAdmin) {
                if (custResp && custResp[0]) {
                    var cust = {
                        first_name: custResp[0].first_name,
                        last_name: custResp[0].last_name,
                        fullname: custResp[0].first_name + ' ' + custResp[0].last_name,
                        email: custResp[0].email,
                        accountType: custResp[0].account_type.text,
                        userBirth: custResp[0].date_of_birth,
                        userGender: custResp[0].gender,
                        userPhone: custResp[0].mobile,
                        nc_id: custResp[0].id
                    };
                    _this.ngRedux.dispatch({
                        type: 'STORE_USER_INFO_VIEW1',
                        payload: cust
                    });
                }
            }
            return Observable.forkJoin([
                _this._paywhirl.LookupActiveSubscriptions(nestcareID),
                _this._paywhirl.loadLoginPaymentCards(nestcareID),
                _this._paywhirl.LookupCollectedInvoices(nestcareID)]);
        }).catch(function (error) {
            console.log('ERRORED OUT');
            if (error && error.message && error.message === 'Recurly Customer Not Found') {
                _this._nestcare.GetCustomer().subscribe(function (custGet) {
                    if (custGet) {
                        var cust = {
                            first_name: custGet.first_name,
                            last_name: custGet.last_name,
                            fullname: custGet.first_name + ' ' + custGet.last_name,
                            email: custGet.email,
                            accountType: custGet.account_type.text,
                            userBirth: custGet.date_of_birth,
                            userGender: custGet.gender,
                            userPhone: custGet.mobile,
                            nc_id: custGet.id
                        };
                        console.log('RASDASDAW THIS');
                        _this.ngRedux.dispatch({
                            type: 'STORE_USER_INFO_VIEW1',
                            payload: cust
                        });
                    }
                });
                error.recurlyNotFound = true;
                return Observable.throw(error);
            }
            else {
                console.log('threw this');
                console.log(error);
                return Observable.throw(error);
            }
        });
        // return this._paywhirl.GetCustomer(nestcareID);
    };
    RegisterService.prototype.checkRecurlyAndCreateAndNoToken = function (nestcareID, userParams) {
        var _this = this;
        return this._paywhirl.GetCustomer(nestcareID).map(function (resp) {
            return resp;
        }).catch(function (error) {
            var payingForOther = (_this.workflow === 'other') ? true : false;
            console.log('this and');
            console.log(error);
            if (error && error.status === 400 || error.status === 404) {
                return _this._paywhirl.CreateCustomer(userParams, payingForOther);
            }
            if (error && error.message === 'Recurly Customer Not Found') {
                return _this._paywhirl.CreateCustomer(userParams, payingForOther);
            }
            console.log(error);
            return Observable.throw(error);
        });
    };
    RegisterService.prototype.checkRecurlyAndCreate = function (nestcareID, userParams) {
        var _this = this;
        return this._paywhirl.GetCustomer(nestcareID).map(function (resp) {
            return resp;
        }).catch(function (error) {
            var payingForOther = (_this.workflow === 'other') ? true : false;
            console.log('this and');
            console.log(error);
            if (error && error.status === 400 || error.status === 404) {
                return _this._paywhirl.CreateCustomer(userParams, payingForOther, true);
            }
            if (error && error.message === 'Recurly Customer Not Found') {
                return _this._paywhirl.CreateCustomer(userParams, payingForOther, true);
            }
            console.log(error);
            return Observable.throw(error);
        });
    };
    RegisterService.prototype.registerAndLogin = function (email, password) {
        var postData = {
            "email": email,
            "password": password
        };
        var that = this;
        var objectResp;
        return this._nestcare.login(email, password)
            .flatMap(function (res) {
            objectResp = res;
            console.log('OYooooooo');
            console.log(res);
            if (objectResp.token) {
                that.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: objectResp.token
                });
                if (objectResp.meta) {
                    that.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: objectResp.meta.token
                    });
                }
                return Observable.create(function (observer) {
                    observer.next(objectResp);
                });
            }
            else {
                if (objectResp.meta) {
                    that.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: objectResp.meta.token
                    });
                }
                return Observable.create(function (observer) {
                    observer.next(objectResp);
                });
            }
        })
            .catch(function (error) {
            console.log("HEYYYYY");
            console.log(error);
            var errMsg;
            return Observable.throw(error);
        });
    };
    RegisterService.prototype.registerUser = function (params, paidForOther, isAdmin) {
        var _this = this;
        if (paidForOther === void 0) { paidForOther = false; }
        if (isAdmin === void 0) { isAdmin = false; }
        var originalhash = params.password;
        return this._nestcare.register(params)
            .flatMap(function (res) {
            console.log('NAHHSHSD');
            console.log(res.json());
            console.log('got 1st response');
            if (isAdmin) {
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TEMP_TOKEN',
                    payload: res.json().meta.token
                });
            }
            if (paidForOther) {
                _this.ngRedux.dispatch({
                    type: 'ADD_PAID_FOR_CUSTOMER_ID',
                    payload: res.json().data.user_id
                });
                if (_this.loginStatus === 'logged-in') {
                    _this.ngRedux.dispatch({
                        type: 'ADD_CUSTOMER_ID',
                        payload: _this.nc_id
                    });
                }
                if (_this.workflow === 'household') {
                    _this.ngRedux.dispatch({
                        type: 'ADD_NC_TEMP_TOKEN',
                        payload: res.json().meta.token
                    });
                }
                else {
                    _this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                }
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: res.json().token
                });
            }
            else {
                // ADD_CUSTOMER_ID IS ADDING_PAYING_CUSTOMER
                _this.ngRedux.dispatch({
                    type: 'ADD_CUSTOMER_ID',
                    payload: res.json().data.user_id
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_PAID_FOR_CUSTOMER_ID',
                    payload: res.json().data.user_id
                });
            }
            if (!isAdmin && _this.workflow !== 'other' && _this.workflow !== 'household') {
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: res.json().meta.token
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: res.json().token
                });
            }
            if (_this.workflow === 'household') {
                if (!_this.nestcareToken) {
                    _this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                    _this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                }
                else {
                    _this.ngRedux.dispatch({
                        type: 'ADD_NC_TEMP_TOKEN',
                        payload: res.json().meta.token
                    });
                }
            }
            if (!isAdmin && _this.workflow === 'other') {
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TEMP_TOKEN',
                    payload: _this.nestcareToken
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: res.json().meta.token
                });
            }
            params.nc_id = res.json().data.user_id;
            return Observable.create(function (observer) {
                observer.next(res.json().data);
            });
            // return this._paywhirl.CreateCustomer(params, paidForOther);
        })
            .catch(function (error) {
            if (error.nestCareError) {
                return Observable.throw(error.nestCareError);
            }
            else if (error.status === 422) {
                return Observable.throw(error.json().message);
            }
            else {
                return Observable.throw({
                    message: 'We could not complete the request. (PWR)'
                });
            }
        });
        // return this._paywhirl.CreateCustomer(params)
        //     .flatMap((res:Response) => {
        //         console.log('Got 1st response');
        //         console.log(res.json());
        //         console.log(res.json().account_code);
        //         this.ngRedux.dispatch({
        //             type: 'ADD_IG_TOKEN',
        //             payload: res.json().token
        //         });
        //         this.ngRedux.dispatch({
        //             type: 'ADD_CUSTOMER_ID',
        //             payload: res.json().account.account_code
        //         });
        //         params.password = originalhash;
        //         return this._nestcare.register(params);
        //     })
    };
    RegisterService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NestcareService, PaywhirlService, NgRedux])
    ], RegisterService);
    return RegisterService;
}());
