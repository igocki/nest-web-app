var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from './http-header-service';
/**
 * Simple service designed to demonstrate using a DI-injected
 * service in your action creators.
 */
export var MockLoginService = (function () {
    function MockLoginService(http) {
        this.http = http;
    }
    MockLoginService.prototype.login = function (customer, subscriptionID, mockFail) {
        // this.http.post('http://localhost:1337/crossorigin?https://api.paywhirl.com/subscribe/customer',
        this.http.get('http://localhost:1337/pay/customers').subscribe(function (p) { return console.log('asdasd ' + p); });
        //  {
        //      customer_id: customer,
        //      plan_id: subscriptionID,
        //      quantity: 1
        //  }
        console.log('did it');
        return new Promise(function (resolve, reject) {
            if (!mockFail) {
                resolve({
                    error: null,
                    token: 'tempToken',
                    profile: {
                        name: 'andrew alanis'
                    }
                });
            }
            else {
                reject({
                    error: null,
                    token: 'tempToken',
                    profile: {
                        name: 'andrew alanis'
                    }
                });
            }
        });
    };
    MockLoginService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [HttpClient])
    ], MockLoginService);
    return MockLoginService;
}());
