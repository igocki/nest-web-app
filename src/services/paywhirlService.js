var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgRedux } from 'ng2-redux';
import { Injectable } from '@angular/core';
import { HttpClient } from './http-header-service';
import { Observable } from "rxjs";
import { List } from 'immutable';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';
export var PaywhirlService = (function () {
    function PaywhirlService(_http, _config, ngRedux) {
        var _this = this;
        this._http = _http;
        this._config = _config;
        this.ngRedux = ngRedux;
        this._server = '';
        this._apiUrl = "pay/";
        this._baseUrl = '';
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        this.paymentCards = this.ngRedux.select(function (state) { return (state.billing) ? state.billing.get('cards') : List([]); });
        this.userPlans = this.ngRedux.select(function (state) { return (state.appStatus) ? List(state.appStatus.get('plans')) : List([]); });
        var customerState = this.ngRedux.select(function (state) { return (state.appStatus) ? state.appStatus.getIn(['signupInfo', 'userInfo']) : null; });
        var custID = this.ngRedux.select(function (state) { return state.appStatus; });
        var userView = this.ngRedux.select(function (state) { return state.userView; });
        var app_status = this.ngRedux.select(function (state) { return state.appStatus; });
        var secure = this.ngRedux.select(function (state) { return state.secure; });
        app_status.subscribe(function (data) {
            if (data) {
                _this.loginStatus = data.get('loginStatus');
                _this.payingCustomer = data.get('payingCustomerID');
                _this.isAdmin = data.get('admin');
            }
        });
        secure.subscribe(function (data) {
            if (data) {
                _this.backuptoken = data.get('igockiToken');
            }
        });
        userView.subscribe(function (data) {
            if (data) {
                _this.accountType = data.get('accountType');
            }
        });
        customerState.subscribe(function (cust) {
            if (cust) {
                _this.customerInfo = cust;
            }
        });
        custID.subscribe(function (data) {
            if (data) {
                _this.customerID = data.get('payingCustomerID');
            }
        });
    }
    PaywhirlService.prototype.replaceCard = function (params, customerID) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.createCard(params, customerID)
            .subscribe(function (resp2) {
            // this.updateCustomer({
            //     first_name: this.customerInfo.get('first_name'),
            //     last_name: this.customerInfo.get('last_name'),
            //     email: this.customerInfo.get('email'),
            //     address: params.addressLine1 + '_' + params.addressLine2,
            //     city: params.city,
            //     state: params.state,
            //     zip: params.zipCode,
            //     "currency": "USD",
            //     "gateway_id": NestCareConstants.secure.payment_gateway_id
            // }, customerID).subscribe(resp3 => {
            //     let updateCust = resp3.json();
            //     this.ngRedux.dispatch({
            //         type: 'ADD_BILLING_ADDRESS',
            //         payload: {
            //             address: updateCust.address,
            //             city: updateCust.city,
            //             state: updateCust.state,
            //             zip: updateCust.zip
            //         }
            //     });
            // },
            //  error => {
            //      this.ngRedux.dispatch({
            //          type: 'ADDING_CARD_FAILURE',
            //          payload: {
            //              error: error,
            //              defaultMessage: 'Something went wrong.. Try again'
            //          }
            //      });
            //  });
            _this.loadPaymentCardsWithoutSub(customerID).subscribe(function (res) {
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                var pmtCards = [];
                console.log('GOT PAYMENT CARDS');
                console.log(res);
                pmtCards = res;
                _this.ngRedux.dispatch({
                    type: 'GET_CARDS',
                    payload: pmtCards
                });
            }, function (error) {
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                _this.ngRedux.dispatch({
                    type: 'FETCH_CARDS_FAILURE',
                    payload: { err: error }
                });
                console.log(error);
            });
            _this.ngRedux.dispatch({
                type: 'SELECT_CARD',
                payload: resp2
            });
        });
    };
    PaywhirlService.prototype.deleteCard = function (params) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var cardToDelete = {
            id: ''
        };
        if (params) {
            cardToDelete.id = params.id;
            this.ngRedux.dispatch({
                type: 'DELETING_OTHER_CARD'
            });
            return lhttp.post(bUrl + "deleteCard", cardToDelete)
                .map(function (res) {
                return res.json();
            })
                .catch(function (error) {
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                _this.ngRedux.dispatch({
                    type: 'DELETING_OTHER_CARD_FAILURE',
                    payload: {
                        errorObj: error,
                        defaultMessage: 'Something went wrong. Try again'
                    }
                });
                return Observable.throw(error.json());
            });
        }
        else {
            return Observable.create(function (observer) {
                observer.next('done');
            });
        }
    };
    PaywhirlService.prototype.createCard = function (params, customerID) {
        var _this = this;
        var cardToCreate = {
            id: customerID,
            token_id: params.token_id
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        this.ngRedux.dispatch({
            type: 'ADDING_CARD'
        });
        return lhttp.post(bUrl + "createCard", cardToCreate)
            .map(function (res) {
            return res.json();
        })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('errored');
            console.log(error);
            _this.ngRedux.dispatch({
                type: 'ADDING_CARD_FAILURE',
                payload: {
                    error: error,
                    defaultMessage: 'Something went wrong.. You card may have declined. Contact Support or try again.'
                }
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.loadLoginPaymentCards = function (customerId) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map(function (res) {
            return res.json();
        });
    };
    PaywhirlService.prototype.loadPaymentCards = function (customerId) {
        var _this = this;
        var pmtCards = [];
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        console.log('loadingPaymentCards');
        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map(function (res) {
            console.log('GOT PAYMENT CARDS');
            console.log(res);
            pmtCards = res.json();
            _this.ngRedux.dispatch({
                type: 'GET_CARDS',
                payload: pmtCards
            });
            return res.json();
        })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: 'FETCH_CARDS_FAILURE',
                payload: { err: error }
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.loadPaymentCardsWithoutSub = function (customerId) {
        var pmtCards = [];
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        console.log('loadingPaymentCardsWithoutSub');
        return lhttp.get(bUrl + "cards?id=" + customerId)
            .map(function (res) {
            return res.json();
        })
            .catch(function (error) {
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.GetCustomers = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "customers/")
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.GetCustomer = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "customer?id=" + id).map(function (resp) {
            return resp.json();
        }).catch(function (error) {
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.GetPdfInvoiceUrl = function (id, custID) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var ctrl = this;
        return lhttp.get(bUrl + "getInvoiceSecCheck?id=" + custID).flatMap(function (resp) {
            var data = resp.json();
            _this.ngRedux.dispatch({
                type: 'ADD_IG_TEMP_TOKEN',
                payload: data.token
            });
            return Observable.create(function (observer) {
                observer.next(ctrl._baseUrl + 'getInvoicePdf?id=' + custID + '&invoice_id=' + id + '&temp=' + data.token);
            });
        });
    };
    PaywhirlService.prototype.LookupCollectedInvoices = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var getUrl = bUrl + "get_collected_invoices?id=" + id;
        console.log("asdasdasasdasdasdYASDASDSDASD");
        console.log(this.accountType);
        if (this.accountType && this.accountType === 'Subscriber') {
            getUrl = bUrl + "get_collected_invoices?id=" + id + "&household=true";
        }
        return lhttp.get(getUrl).map(function (resp) {
            return resp.json();
        });
    };
    PaywhirlService.prototype.LookupActiveSubscriptions = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "get_active_subscriptions?id=" + id).map(function (resp) {
            return resp.json();
        });
    };
    PaywhirlService.prototype.CreateCustomer = function (params, paidForOther, noToken) {
        var _this = this;
        if (noToken === void 0) { noToken = false; }
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "nc_id": params.nc_id,
            "email": params.email,
            "payment_source_id": undefined
        };
        if (paidForOther) {
            postData.payment_source_id = this.payingCustomer;
            console.log('PAID FOR OTHER');
            console.log(postData.payment_source_id);
        }
        console.log('NNNNNNNOOOOOOOOOOPPPPPPEEE');
        return lhttp.post(bUrl + "customer", postData)
            .map(function (secondRes) {
            if (!noToken) {
                if (_this.loginStatus === 'logged-in' && paidForOther) {
                    _this.ngRedux.dispatch({
                        type: 'ADD_BACKUP_TOKEN',
                        payload: _this.backuptoken
                    });
                    _this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: secondRes.json().token
                    });
                }
                else {
                    if (!_this.isAdmin) {
                        _this.ngRedux.dispatch({
                            type: 'ADD_IG_TOKEN',
                            payload: secondRes.json().token
                        });
                        console.log('TOKEN USED:');
                        console.log(secondRes.json().token);
                    }
                }
            }
            return secondRes.json();
        })
            .catch(function (error) {
            var innerError = {
                nestCareError: error
            };
            return Observable.throw(innerError);
        });
    };
    PaywhirlService.prototype.updateCustomer = function (params, customerID) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = params;
        postData.id = customerID;
        return lhttp.put(bUrl + "customer", postData);
    };
    PaywhirlService.prototype.UpdateAnswer = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.put(bUrl + "answer/", {})
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.GetQuestions = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "questions/")
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.GetAllStatusCustomers = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "all_subscribed_customers").map(function (resp) {
            return resp.json();
        });
    };
    PaywhirlService.prototype.GetPlans = function () {
        var _this = this;
        var mainPlans = [];
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "plans")
            .map(function (res) {
            return res.json();
        })
            .catch(function (error) {
            return Observable.throw(error.json());
        })
            .subscribe(function (resp) {
            console.log('GOT PAYMENT PLANS');
            console.log(resp);
            mainPlans = resp;
            _this.ngRedux.dispatch({
                type: 'GET_PLANS',
                payload: mainPlans
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            // this.ngRedux.dispatch({
            //     type: 'GET_PLANS_FAILURE',
            //     payload: {err: error}
            // });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    PaywhirlService.prototype.GetPlan = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "plan?id=" + id)
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.CreatePlan = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.post(bUrl + "plan/", {})
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.UpdatePlan = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.put(bUrl + "plan/", {})
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.GetSubscriptions = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "subscriptions/")
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.GetSubscription = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return new Promise(function (resolve, reject) {
            lhttp.get(bUrl + "subscription?id=" + id)
                .catch(function (error) { return Observable.throw(error || 'Server Error'); })
                .subscribe(function (d) { return resolve(d.json()); }, function (e) { return reject(e); });
        });
    };
    PaywhirlService.prototype.createCharge = function (params, customerID) {
        var _this = this;
        var chargeParams = {
            id: customerID,
            amount: params.amount,
            description: params.description
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "create/charge", chargeParams)
            .map(function (res) {
            return res.json();
        })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.refundCharge = function (uuid) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var params = {
            uuid: uuid
        };
        return lhttp.post(bUrl + "refund_charge", params)
            .map(function (res) {
            return res;
        });
    };
    PaywhirlService.prototype.getCouponDetails = function (params) {
        var _this = this;
        var postData = {
            coupon_code: params.coupon_code
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "coupon", postData)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.getPromoCodeAmount = function (customerID) {
        var _this = this;
        var params = {
            id: customerID
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "get_promo_amt", params)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService.prototype.applyPromoCode = function (promo, customerID) {
        var _this = this;
        var params = {
            id: customerID,
            coupon: promo
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "redeem_coupon", params)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    //Subscribes only New Customers for the first time
    PaywhirlService.prototype.subscribeCustomer = function (params, customerID, quantity) {
        var _this = this;
        if (quantity === void 0) { quantity = 1; }
        var subscribeParams = {
            id: customerID,
            plan_id: params.plan_id,
            sec_dep: (params.sec_dep) ? params.sec_dep : 0,
            setup_fee: (params.setup_fee) ? params.setup_fee : 0,
            quantity: quantity
        };
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "subscribeNew", subscribeParams)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: 'GENERAL_SUBSCRIPTION_FAILURE',
                payload: {
                    message: 'Something went wrong with the Subscription. Contact Customer Service.'
                }
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    PaywhirlService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [HttpClient, ConfigEnvService, NgRedux])
    ], PaywhirlService);
    return PaywhirlService;
}());
