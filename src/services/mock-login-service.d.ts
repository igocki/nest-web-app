import { HttpClient } from './http-header-service';
/**
 * Simple service designed to demonstrate using a DI-injected
 * service in your action creators.
 */
export declare class MockLoginService {
    http: HttpClient;
    constructor(http: HttpClient);
    g: any;
    login(customer: string, subscriptionID: string, mockFail?: string): Promise<{}>;
}
