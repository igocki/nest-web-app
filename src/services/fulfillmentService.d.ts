import { Observable } from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Router } from '@angular/router';
export declare class FulfillmentService {
    private _nestcare;
    private _paywhirl;
    private _shippo;
    private ngRedux;
    private router;
    private workflow;
    private isAdmin;
    private ncBackupToken;
    private nestcareToken;
    private userViewShipping;
    private signupShipping;
    private paidFor;
    private payingFor;
    private planSelected;
    constructor(_nestcare: NestcareService, _paywhirl: PaywhirlService, _shippo: ShippoService, ngRedux: NgRedux<RootState>, router: Router);
    completeOrderAndNavigate(): void;
    changeAccountTypeIfAdminOrNot(orderPostData: any, custID: any): Observable<any[]>;
    fulfillOrder(params: any, shippedItems: boolean, custID: any, shippingObj?: any): Observable<any>;
}
