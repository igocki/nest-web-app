var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from './http-header-service';
import 'rxjs/Rx';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';
export var NestcareService = (function () {
    function NestcareService(_http, _config, ngRedux) {
        var _this = this;
        this._http = _http;
        this._config = _config;
        this.ngRedux = ngRedux;
        this._server = '';
        this._apiUrl = "nc/";
        this._baseUrl = '';
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        var secure = this.ngRedux.select(function (state) { return state.secure; });
        secure.subscribe(function (data) {
            if (data) {
                _this.currentNCToken = data.get('nestcareToken');
                _this.ncBackupToken = data.get('nestcareBackupToken');
            }
        });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.workflow = data.get('workflow');
                _this.isAdmin = data.get('admin');
            }
            if (data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])) {
                _this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                _this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data && !_this.userEmail) {
                _this.userEmail = data.get('email');
            }
            if (data && !_this.userPhone) {
                _this.userPhone = data.get('userPhone');
            }
        });
    }
    NestcareService.prototype.ssoLogout = function (id) {
        var bUrl = this._server + 'dc/logout?userId=' + id;
        var lhttp = this._http;
        return lhttp.post(bUrl, {});
    };
    NestcareService.prototype.addPaymentCustomer = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "paid_user": params.paid_for_customer,
            "id": params.paying_customer
        };
        return lhttp.post(bUrl + "insert_paid_user", postData);
    };
    NestcareService.prototype.changeAccountType = function (fromAccountType, toAccountType, cust) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var oldAcct = (fromAccountType === "explorer") ? 9 : undefined;
        var newAcct = (toAccountType === "support network") ? 1 : undefined;
        var postData = {
            "old_account_type_id": oldAcct,
            "new_account_type_id": newAcct,
            "id": cust
        };
        return lhttp.post(bUrl + "change_account_type", postData);
    };
    NestcareService.prototype.changeAccountTypeAdmin = function (toAccountType, cust) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var newAcct = (toAccountType === "support network") ? 1 : undefined;
        var postData = {
            "new_account_type_id": newAcct,
            "user_id": cust
        };
        return lhttp.post(bUrl + "changeAccountTypeAdmin", postData);
    };
    NestcareService.prototype.doSso = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.post(bUrl + "doSso", params);
    };
    NestcareService.prototype.ssoValidate = function (payload, sig) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var params = {
            'payload': payload,
            'sig': sig
        };
        return lhttp.post(bUrl + "ssoValidate", params);
    };
    NestcareService.prototype.getNonce = function (nonce) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var params = {
            'nonce': nonce
        };
        return lhttp.post(bUrl + "getNonce", params);
    };
    NestcareService.prototype.getPeopleYouSupport = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "people_you_support")
            .map(function (res) {
            return res.json();
        });
    };
    NestcareService.prototype.getPeoplePaidFor = function (custID) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            paying_user: custID
        };
        return lhttp.post(bUrl + "get_paid_for", postData)
            .map(function (res) {
            return res.json();
        });
    };
    NestcareService.prototype.getPayingUser = function (custID) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            paid_user: custID
        };
        return lhttp.post(bUrl + "get_paying_user", postData)
            .map(function (res) {
            return res.json();
        });
    };
    NestcareService.prototype.getMedicationActivities = function (date) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "get_medication_activities?date=" + date)
            .map(function (res) {
            return res.json().data;
        });
    };
    NestcareService.prototype.medicationSchedules = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.post(bUrl + "medications", postData)
            .map(function (res) {
            return res.json().data;
        });
        ;
    };
    NestcareService.prototype.getVitalMeasurements = function (date) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "vital_measurements?date=" + date)
            .map(function (res) {
            return res.json().data;
        });
    };
    NestcareService.prototype.getVitalSchedules = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.get(bUrl + "vital_schedules")
            .map(function (res) {
            return res.json().data;
        });
        ;
    };
    NestcareService.prototype.getWellnessActivities = function (date) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        return lhttp.get(bUrl + "wellness_activities?date=" + date)
            .map(function (res) {
            return res.json().data;
        });
    };
    NestcareService.prototype.getWellnessSchedules = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.get(bUrl + "wellness_schedules")
            .map(function (res) {
            return res.json().data;
        });
        ;
    };
    NestcareService.prototype.GetCustomer = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.get(bUrl + "get_customer")
            .map(function (res) {
            return res.json();
        });
        ;
    };
    NestcareService.prototype.adminGetCustomer = function (custID) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.get(bUrl + "admin_get_customer?id=" + custID)
            .map(function (res) {
            return res.json();
        });
        ;
    };
    NestcareService.prototype.adminGetAllCustomers = function () {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {};
        return lhttp.get(bUrl + "admin_get_all_customers")
            .map(function (res) {
            return res.json();
        });
        ;
    };
    NestcareService.prototype.adminUpdateAccountVerified = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "status_id": params.status_id,
            "id": params.id
        };
        return lhttp.post(bUrl + "admin_change_verified", postData)
            .map(function (secondRes) {
            return secondRes.json();
        });
    };
    NestcareService.prototype.adminUpdateUserDetails = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "dob": params.dob.getFullYear() + '-' + ('0' + (params.dob.getMonth() + 1)).slice(-2) + '-' + ('0' + (params.dob.getDate())).slice(-2),
            "gender": params.gender,
            "mobile": params.mobile,
            "id": params.id
        };
        return lhttp.post(bUrl + "admin_update_user_details", postData)
            .map(function (secondRes) {
            return secondRes.json();
        });
    };
    NestcareService.prototype.register = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "date_of_birth": params.date_of_birth,
            "gender": params.gender,
            "mobile": params.mobile,
            "password": params.password
        };
        return lhttp.post(bUrl + "register", postData);
        // .map((secondRes:Response) => {
        //     return secondRes.json();
        // })
        // .catch((error:any) => {
        //     let innerError = {
        //         nestCareError: error.json().message
        //     };
        //     return Observable.throw(innerError);
        // });
    };
    NestcareService.prototype.sendAuthTwoFactor = function (authCode, nestcareID) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "auth_code": authCode,
            "id": nestcareID
        };
        return lhttp.post(bUrl + "2fa", postData)
            .map(function (res) {
            if (res.json().token && res.json().meta) {
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: res.json().token
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: res.json().meta.token
                });
            }
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            return Observable.throw(error.json());
        });
    };
    NestcareService.prototype.adminLogin = function (email, password) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "email": email,
            "password": password
        };
        return lhttp.post(bUrl + "adminLogin", postData)
            .map(function (res) {
            if (res.json().meta) {
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: res.json().token
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: res.json().meta.token
                });
                _this.ngRedux.dispatch({
                    type: 'ADMIN_LOGGED_IN',
                    payload: res.json().meta.token
                });
            }
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            return Observable.throw(error.json());
        });
    };
    NestcareService.prototype.login = function (email, password) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "email": email,
            "password": password
        };
        return lhttp.post(bUrl + "login", postData)
            .map(function (res) {
            if (res.json().meta) {
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: res.json().token
                });
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: res.json().meta.token
                });
            }
            return res.json();
        })
            .catch(function (error) {
            console.log("HEYYYYY");
            console.log(error);
            var errMsg;
            return Observable.throw(error.json());
        });
    };
    NestcareService.prototype.getCompletedOrders = function (fromDate, toDate) {
        if (fromDate === void 0) { fromDate = undefined; }
        if (toDate === void 0) { toDate = undefined; }
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var urlString = 'view_completed_orders';
        if (fromDate && !toDate) {
            urlString += '?fromDate=' + fromDate;
        }
        if (toDate && !fromDate) {
            urlString += '?toDate=' + fromDate;
        }
        if (fromDate && toDate) {
            urlString += '?fromDate=' + fromDate + '&toDate=' + toDate;
        }
        return lhttp.get(bUrl + urlString)
            .map(function (res) {
            return res.json();
        });
    };
    NestcareService.prototype.addCompletedOrderTracker = function (params, cust) {
        var _this = this;
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData;
        if (this.workflow === 'household') {
            postData = {
                "paying_cust": cust,
                "shippo_id": params.shippo_id,
                "tracking_number": params.tracking_number,
                "status": params.status,
                "id": cust
            };
        }
        else {
            postData = {
                "paying_cust": params.paying_cust,
                "shippo_id": params.shippo_id,
                "tracking_number": params.tracking_number,
                "status": params.status,
                "id": cust
            };
        }
        var adminToken = undefined;
        if (this.isAdmin) {
            adminToken = this.currentNCToken;
            if (this.ncBackupToken) {
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: this.ncBackupToken
                });
            }
        }
        return lhttp.post(bUrl + "add_completed_order_tracker", postData)
            .map(function (res) {
            if (adminToken) {
                _this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: adminToken
                });
            }
            return res.json();
        })
            .catch(function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    NestcareService.prototype.updateProfile = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "first_name": params.first_name,
            "last_name": params.last_name,
            "email": params.email,
            "gender": params.gender,
            "date_of_birth": params.date_of_birth,
            "home_line1": params.street1,
            "home_line2": params.street2,
            "home_city_id": params.city,
            "home_state_id": params.state,
            "home_zip_code": params.zip,
            "home_country_id": 'US',
            "home_phone": this.userPhone
        };
        return lhttp.post(bUrl + "updateProfile", postData)
            .map(function (res) {
            console.log('RETURNED A RESPONSE');
            return res.json();
        })
            .catch(function (error) {
            var errMsg;
            return Observable.throw(error.json());
        });
    };
    NestcareService.prototype.resetPassword = function (email) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "email": email
        };
        return lhttp.post(bUrl + "resetPassword", postData);
    };
    NestcareService.prototype.checkIfAuthorized = function (id) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            id: id
        };
        return lhttp.post(bUrl + "check_if_authorized", postData);
    };
    NestcareService.prototype.updateShipping = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": this.userPhone
        };
        return lhttp.post(bUrl + "updateShipping", postData);
    };
    NestcareService.prototype.updateShippingAdmin = function (params, userID, isAdmin) {
        if (isAdmin === void 0) { isAdmin = false; }
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": this.userPhone,
            "id": userID,
            "billing": (isAdmin) ? 1 : 0
        };
        console.log('hereerrr');
        console.log(postData);
        return lhttp.post(bUrl + "updateShippingAdmin", postData);
    };
    NestcareService.prototype.updateBillingAdminAsAuthorizedUser = function (params, userID, isAdmin, phone) {
        if (isAdmin === void 0) { isAdmin = false; }
        if (phone === void 0) { phone = '+15555555555'; }
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": phone,
            "id": userID,
            "billing": 1
        };
        console.log('hereerrr');
        console.log(postData);
        return lhttp.post(bUrl + "update_shipping_admin_as_auth_user", postData);
    };
    NestcareService.prototype.updateBilling = function (params) {
        var bUrl = this._baseUrl;
        var lhttp = this._http;
        var postData = {
            "address_line1": params.addressLine1,
            "address_line2": (params.addressLine2) ? params.addressLine2 : '',
            "city": params.city,
            "state": params.state,
            "zip_code": params.zipCode,
            "phone": '+15555555555'
        };
        return lhttp.post(bUrl + "updateBilling", postData);
    };
    NestcareService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [HttpClient, ConfigEnvService, NgRedux])
    ], NestcareService);
    return NestcareService;
}());
