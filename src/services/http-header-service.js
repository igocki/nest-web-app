var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgRedux } from 'ng2-redux';
export var HttpClient = (function () {
    function HttpClient(http, ngRedux) {
        this.http = http;
        this.ngRedux = ngRedux;
        this.secure = this.ngRedux.select(function (state) { return (state.secure) ? state.secure : null; });
    }
    HttpClient.prototype.createAuthorizationHeader = function (headers) {
        var unsubscribethis = this.secure.subscribe(function (tok) {
            if (tok && tok.get('igockiToken')) {
                headers.append('Authorizationig', 'Bearer ' + tok.get('igockiToken'));
            }
            if (tok && tok.get('nestcareToken')) {
                headers.append('Authorization', 'Bearer ' + tok.get('nestcareToken'));
            }
            if (tok && tok.get('tempigockiToken')) {
                headers.append('Authorizationtempig', 'Bearer ' + tok.get('tempigockiToken'));
            }
        });
        unsubscribethis.unsubscribe();
    };
    HttpClient.prototype.get = function (url) {
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        });
    };
    HttpClient.prototype.post = function (url, data) {
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        });
    };
    HttpClient.prototype.put = function (url, data) {
        var headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        });
    };
    HttpClient = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http, NgRedux])
    ], HttpClient);
    return HttpClient;
}());
