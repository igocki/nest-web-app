var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { ShippingActions } from '../actions/shipping-actions';
import { AppStatusActions } from '../actions/app-status-actions';
export var ShippingAddressService = (function () {
    function ShippingAddressService(_nestcare, _shippo, ngRedux, _appStatusActions) {
        var _this = this;
        this._nestcare = _nestcare;
        this._shippo = _shippo;
        this.ngRedux = ngRedux;
        this._appStatusActions = _appStatusActions;
        var custID = this.ngRedux.select(function (state) { return state.appStatus; });
        var authSub = this.ngRedux.select(function (state) {
            return state.auth.getIn(['profile']);
        });
        authSub.subscribe(function (data) {
            if (data) {
                _this.loginEmail = data.get('email');
            }
        });
        custID.subscribe(function (data) {
            if (data) {
                _this.customerID = data.get('payingCustomerID');
                _this.emailAddress = data.getIn(['signupInfo', 'userInfo', 'email']);
            }
        });
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        appStatus.subscribe(function (data) {
            if (data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])) {
                _this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                _this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });
        var userViewSub = this.ngRedux.select(function (state) {
            return state.userView;
        });
        userViewSub.subscribe(function (data) {
            if (data && !_this.userEmail) {
                _this.userEmail = data.get('email');
            }
            if (data && !_this.userPhone) {
                _this.userPhone = data.get('userPhone');
            }
        });
    }
    ShippingAddressService.prototype.addNewShippingAddress = function (params, isAdmin, userToUpdate, billID) {
        var _this = this;
        if (isAdmin === void 0) { isAdmin = false; }
        if (userToUpdate === void 0) { userToUpdate = undefined; }
        if (billID === void 0) { billID = undefined; }
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.ngRedux.dispatch({
            type: 'VERIFY_AND_SUBMIT_NEW_SHIPPING'
        });
        var postData = {
            street1: params.addressLine1,
            street2: params.addressLine2,
            city: params.city,
            state: params.state,
            zip: params.zipCode,
            email: this.userEmail,
            name: params.name,
            mobile: this.userPhone
        };
        this._shippo.validateAddress(postData)
            .flatMap(function (firstResp) {
            _this.ngRedux.dispatch({
                type: 'VERIFY_ADDRESS_SUCCESS'
            });
            if (isAdmin) {
                return _this._nestcare.updateShippingAdmin(params, userToUpdate);
            }
            else {
                return _this._nestcare.updateShipping(params);
            }
        })
            .subscribe(function (resp) {
            // add shipping to either nestcare and/or recurly
            _this.ngRedux.dispatch({
                type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                payload: {
                    name: params.name,
                    addressLine1: params.addressLine1,
                    addressLine2: params.addressLine2,
                    city: params.city,
                    state: params.state,
                    zipCode: params.zipCode
                }
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.ngRedux.dispatch({
                type: 'VERIFY_ADDRESS_FAILURE',
                payload: {
                    message: "This address came back invalid, please try a valid address."
                }
            });
            console.log(error);
        });
    };
    ShippingAddressService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NestcareService, ShippoService, NgRedux, AppStatusActions])
    ], ShippingAddressService);
    return ShippingAddressService;
}());
