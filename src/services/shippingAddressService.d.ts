import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { AppStatusActions } from '../actions/app-status-actions';
export declare class ShippingAddressService {
    private _nestcare;
    private _shippo;
    private ngRedux;
    private _appStatusActions;
    customerID: any;
    emailAddress: any;
    loginEmail: string;
    fullname: string;
    private userPhone;
    private userEmail;
    constructor(_nestcare: NestcareService, _shippo: ShippoService, ngRedux: NgRedux<RootState>, _appStatusActions: AppStatusActions);
    addNewShippingAddress(params: any, isAdmin?: boolean, userToUpdate?: any, billID?: any): void;
}
