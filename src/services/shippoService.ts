/**
 * Created by Tommy on 11/22/2016.
 */
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import { NestCareConstants } from '../app/shared/constants/nestcare.constants'
import { Map, List } from 'immutable';
import { LoadingActions } from '../actions/loading-actions';
import { ConfigEnvService } from '../services/env/configEnvService';

@Injectable()
export class ShippoService {
    private _server: string = '';
    private _apiUrl: string = "ship/";
    private _baseUrl = '';
    private userPhone;
    private userEmail;
    shippingRates: Observable<List<any>>;

    constructor(private _http: Http,
                private _config: ConfigEnvService,
                private ngRedux: NgRedux<RootState>) {
        this._server = _config.getAPIURL();
        this._baseUrl = this._server + this._apiUrl;
        this.shippingRates = this.ngRedux.select(state => (state.shippingRates) ? state.shippingRates.get('rates') : List<any>([]));

        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])){
                this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        userViewSub.subscribe((data) =>{
            if(data && !this.userEmail){
                console.log('RANSSSSSS2323');
                this.userEmail = data.get('email') ;
            }
            if(data && !this.userPhone){
                this.userPhone = data.get('userPhone') ;
            }
        });
    }



     validateAddress(params: any) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;
         console.log('heyyy');
         console.log(params.email);
         let postData = {
             object_purpose: 'PURCHASE',
             name: params.name,
             street1: params.street1,
             street2: params.street2,
             city: params.city,
             state: params.state,
             zip: params.zip,
             country: 'US',
             email: this.userEmail,
             validate: true
         };

         return lhttp.post(bUrl + "validate", postData)
             .map((res:Response) => {
                 console.log('RETURNED A RESPONSE');
                 console.log(res.json());
                 return res.json()
             })
             .catch((error:any) => {
                 let errMsg: string;
                 this.ngRedux.dispatch({
                     type: LoadingActions.HIDE_LOADING
                 });
                 return Observable.throw(error.json());

             });
    }

    loadShippingRates(shipment: any, user: any){
        let shippingRatesItems: any[] = [];

        let bUrl = this._baseUrl;
        let lhttp = this._http;
        let addressLine1Array = shipment.addressLine1.split(" ");
        let street1 = shipment.addressLine1.replace(addressLine1Array[0] + " ", "");
        let postData = {
            "object_purpose": "PURCHASE",
            "address_from": NestCareConstants.shippingFromAddress.address_from,
            "parcel": NestCareConstants.bundleInformation.parcel,
            "address_to": {
                "object_purpose": "PURCHASE",
                "name": shipment.name,
                "street_no": addressLine1Array[0],
                "street1": street1,
                "street2": shipment.addressLine2,
                "city": shipment.city,
                "state": shipment.state,
                "zip": shipment.zipCode,
                "country": "US",
                "phone": this.userPhone,
                "email": this.userEmail
            },
            "async": false
        };
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        lhttp.post(bUrl + "shipment", postData)
            .map((res:Response) => {
                return res.json();
            })
            .catch((error:any) => {
                let errMsg: string;
                return Observable.throw(error.json());
            })
            .subscribe(resp => {
                resp.rates_list.sort(function(a, b) {
                    return parseFloat(a.amount) - parseFloat(b.amount);
                });
                shippingRatesItems = resp.rates_list;
                this.ngRedux.dispatch({
                    type: 'ADD_RATES',
                    payload: shippingRatesItems
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            },
            error => {

                this.ngRedux.dispatch({
                    type: 'FETCH_RATES_FAILURE',
                    payload: {err: error}
                });

                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
    }

    createShipment(shipment: any, user: any) { // -- TODO:
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        let addressLine1Array = shipment.addressLine1.split(" ");
        let street1 = shipment.addressLine1.replace(addressLine1Array[0] + " ", "");
        let postData = {
            "object_purpose": "PURCHASE",
            "address_from": NestCareConstants.shippingFromAddress.address_from,
            "parcel": NestCareConstants.bundleInformation.parcel,
            "address_to": {
                "object_purpose": "PURCHASE",
                "name": shipment.name,
                "street_no": addressLine1Array[0],
                "street1": street1,
                "street2": shipment.addressLine2,
                "city": shipment.city,
                "state": shipment.state,
                "zip": shipment.zipCode,
                "country": "US",
                "phone": this.userPhone,
                "email": this.userEmail
            },
            "async": false
        };

        return lhttp.post(bUrl + "shipment", postData)
            .map((res:Response) => {
                console.log('RETURNED A RESPONSE');
                return res.json()
            })
            .catch((error:any) => {
                let errMsg: string;
                return Observable.throw(error.json());

            });
    }

    createLabel(params: any) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;
        let postData = {
            rate: params.shippingRateId,
            async: false
        };

        return lhttp.post(bUrl + "label", postData)
            .map((res:Response) => {
                console.log('RETURNED A RESPONSE');
                console.log(res.json());
                return res.json()
            })
            .catch((error:any) => {
                let errMsg: string;
                this.ngRedux.dispatch({
                    type: 'GENERAL_SUBSCRIPTION_FAILURE',
                    payload: {
                        message: 'Something went wrong with the Order. Contact Customer Service.'
                    }
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });

                return Observable.throw(error.json());

            });
    }

    TrackPackage(carrier:string, tracking:string) {
        let bUrl = this._baseUrl;
        let lhttp = this._http;

        return new Promise(function(resolve, reject) {
            lhttp.get(bUrl + "track?carrier=" + carrier + "&tracking=" + tracking)
                .catch((error:any) => Observable.throw(error || 'Server Error'))
                .subscribe(d => resolve(d.json()), e => reject(e));
        });
    }
}
