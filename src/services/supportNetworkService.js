var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
import { Router } from '@angular/router';
export var SupportNetworkService = (function () {
    function SupportNetworkService(_nestcare, ngRedux, router) {
        var _this = this;
        this._nestcare = _nestcare;
        this.ngRedux = ngRedux;
        this.router = router;
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.isAdmin = data.get('admin');
            }
        });
    }
    SupportNetworkService.prototype.getPeopleYouSupport = function (custID) {
        return Observable.forkJoin([
            this._nestcare.getPeopleYouSupport(),
            this._nestcare.getPeoplePaidFor(custID)])
            .map(function (res) {
            console.log('made it');
            console.log(res[0]);
            var paidUserIdsList = res[1].map(function (item) {
                return item.paid_user_id;
            });
            res[0].forEach(function (item) {
                if (paidUserIdsList.indexOf(item.id) > -1) {
                    item.canChangeBilling = true;
                }
            });
            return res[0];
        });
    };
    SupportNetworkService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NestcareService, NgRedux, Router])
    ], SupportNetworkService);
    return SupportNetworkService;
}());
