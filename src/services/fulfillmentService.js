var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { Router } from '@angular/router';
export var FulfillmentService = (function () {
    function FulfillmentService(_nestcare, _paywhirl, _shippo, ngRedux, router) {
        var _this = this;
        this._nestcare = _nestcare;
        this._paywhirl = _paywhirl;
        this._shippo = _shippo;
        this.ngRedux = ngRedux;
        this.router = router;
        var secure = this.ngRedux.select(function (state) { return state.secure; });
        secure.subscribe(function (data) {
            if (data) {
                _this.ncBackupToken = data.get('nestcareBackupToken');
                _this.nestcareToken = data.get('nestcareToken');
            }
        });
        var appStatus = this.ngRedux.select(function (state) { return state.appStatus; });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.paidFor = data.get('paidForCustomerID');
                _this.payingFor = data.get('payingCustomerID');
                _this.workflow = data.get('workflow');
                _this.isAdmin = data.get('admin');
                _this.signupShipping = data.getIn(['signupInfo', 'shippingAddress']);
                if (data.get('signupInfo') && data.getIn(['signupInfo', 'selectPlan'])) {
                    _this.planSelected = data.getIn(['signupInfo', 'selectPlan']);
                }
            }
        });
        var userView = this.ngRedux.select(function (state) { return state.userView; });
        appStatus.subscribe(function (data) {
            if (data) {
                _this.userViewShipping = data.get('shippingAddress');
            }
        });
    }
    FulfillmentService.prototype.completeOrderAndNavigate = function () {
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        this.router.navigate(['/order-confirm']);
    };
    FulfillmentService.prototype.changeAccountTypeIfAdminOrNot = function (orderPostData, custID) {
        console.log('change accout for');
        console.log(custID);
        if (this.isAdmin) {
            return Observable.forkJoin([
                this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                this._nestcare.changeAccountTypeAdmin("support network", custID)
            ]);
        }
        else {
            if (this.workflow === 'household' && custID === this.payingFor) {
                return Observable.forkJoin([
                    this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                ]);
            }
            else if (this.workflow === 'household' && custID === this.paidFor) {
                var mainUserToken = this.nestcareToken;
                var secondUserToken = this.ncBackupToken;
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TEMP_TOKEN',
                    payload: mainUserToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: secondUserToken
                });
                return Observable.forkJoin([
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                ]);
            }
            else {
                return Observable.forkJoin([
                    this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                ]);
            }
        }
    };
    FulfillmentService.prototype.fulfillOrder = function (params, shippedItems, custID, shippingObj) {
        var _this = this;
        if (shippingObj === void 0) { shippingObj = undefined; }
        var tracking_number;
        var invoiceNum;
        var deviceOrdered;
        var quant;
        this.ngRedux.dispatch({
            type: 'COMPLETING_ORDER'
        });
        var subscribePost = {
            plan_id: params.plan_id,
            setup_fee: (this.planSelected.get('id') !== 'life') ? 1 : 0,
            sec_dep: 0
        };
        if (shippedItems) {
            var shipData = {
                amount: params.shipAmount,
                description: "Shipping Charges for: " + params.shippingType
            };
            var securityDepData;
            if (this.workflow === 'household') {
                securityDepData = {
                    amount: this.planSelected.get('securityDeposit'),
                    description: "Security Deposit Charges for Quantity: 2"
                };
            }
            else {
                securityDepData = {
                    amount: this.planSelected.get('securityDeposit'),
                    description: "Security Deposit Charges for Quantity: 1"
                };
            }
            //IF THEY WANT TO ADD QUANTITY AND HOUSEHOLD PLAN;
            return Observable.forkJoin([
                this._paywhirl.createCharge(shipData, custID),
                this._paywhirl.createCharge(securityDepData, custID)])
                .flatMap(function (createChargeResp) {
                if (_this.workflow === 'household') {
                    quant = 2;
                }
                else {
                    quant = 1;
                }
                if (_this.planSelected.get('id') !== 'life') {
                    subscribePost.sec_dep = 1;
                }
                else {
                    subscribePost.sec_dep = 0;
                }
                return _this._paywhirl.subscribeCustomer(subscribePost, custID, quant).flatMap(function (subRes) {
                    if (_this.workflow === 'household') {
                        deviceOrdered = 'household bundle kit';
                    }
                    else {
                        deviceOrdered = 'individual kit';
                    }
                    invoiceNum = subRes.invoiceNum;
                    var shipping_string;
                    if (_this.signupShipping) {
                        shipping_string = _this.signupShipping.get('addressLine1') + ' ' + _this.signupShipping.get('addressLine2') + ' ' +
                            _this.signupShipping.get('city') + ' ' + _this.signupShipping.get('state') + ' ' + _this.signupShipping.get('zipCode');
                    }
                    if (_this.userViewShipping && !shipping_string) {
                        shipping_string = _this.userViewShipping.get('line1') + ' ' + _this.userViewShipping.get('line2') + ' ' +
                            _this.userViewShipping.get('city_id') + ' ' + _this.userViewShipping.get('state_id') + ' ' + _this.userViewShipping.get('zip_code');
                    }
                    var shippo_object_string = deviceOrdered + ',' + invoiceNum + ',' + shipping_string + ',' + shippingObj.amount;
                    shippo_object_string = shippo_object_string.substring(0, 254);
                    var orderPostData = {
                        paying_cust: custID,
                        shippo_id: shippo_object_string,
                        tracking_number: (shippingObj) ? shippingObj.service : '',
                        status: true
                    };
                    if (_this.workflow === 'household') {
                        console.log('did this just happen');
                        return Observable.forkJoin([
                            _this.changeAccountTypeIfAdminOrNot(orderPostData, _this.payingFor)
                        ]).flatMap(function (testResp) {
                            return Observable.forkJoin([
                                _this.changeAccountTypeIfAdminOrNot(orderPostData, _this.paidFor)
                            ]);
                        });
                    }
                    else {
                        console.log('did this just happen2');
                        return Observable.forkJoin([
                            _this.changeAccountTypeIfAdminOrNot(orderPostData, custID)
                        ]);
                    }
                })
                    .catch(function (error) {
                    console.log('here');
                    if (createChargeResp && createChargeResp[0].adjustment) {
                        _this._paywhirl.refundCharge(createChargeResp[0].adjustment.uuid).subscribe(function (resRefund) {
                            console.log('refunded charge 1');
                        });
                    }
                    if (createChargeResp && createChargeResp[1].adjustment) {
                        _this._paywhirl.refundCharge(createChargeResp[1].adjustment.uuid).subscribe(function (resRefund) {
                            console.log('refunded charge 2');
                        });
                    }
                    return Observable.throw(error);
                });
            });
        }
        else {
            if (this.workflow === 'household') {
                quant = 2;
            }
            else {
                quant = 1;
            }
            return this._paywhirl.subscribeCustomer(subscribePost, custID, quant).flatMap(function () {
                var orderPostData = {
                    paying_cust: custID,
                    shippo_id: (shippingObj) ? shippingObj.amount : '',
                    tracking_number: (shippingObj) ? shippingObj.service : '',
                    status: true
                };
                return _this.changeAccountTypeIfAdminOrNot(orderPostData, custID);
            });
        }
    };
    FulfillmentService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NestcareService, PaywhirlService, ShippoService, NgRedux, Router])
    ], FulfillmentService);
    return FulfillmentService;
}());
