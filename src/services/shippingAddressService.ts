import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { RootState } from '../store';
import { ShippingActions } from '../actions/shipping-actions'
import { AppStatusActions } from '../actions/app-status-actions'
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';

@Injectable()
export class ShippingAddressService {
    customerID: any;
    emailAddress: any;
    loginEmail: string;
    fullname: string;
    private userPhone;
    private userEmail;

    constructor(private _nestcare: NestcareService,
                private _shippo: ShippoService,
                private ngRedux: NgRedux<RootState>,
                private _appStatusActions: AppStatusActions) {
        let custID = this.ngRedux.select(state => state.appStatus);
        let authSub = this.ngRedux.select(state => {
            return state.auth.getIn(['profile']);
        });
        authSub.subscribe(data => {
            if(data){
                this.loginEmail = data.get('email');
            }

        });
        custID.subscribe(data => {
            if(data){
                this.customerID = data.get('payingCustomerID');
                this.emailAddress = data.getIn(['signupInfo', 'userInfo', 'email'])
            }
        });

        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])){
                this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                this.userPhone = data.getIn(['signupInfo', 'userInfo', 'mobile']);
            }
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        userViewSub.subscribe((data) =>{
            if(data && !this.userEmail){
                this.userEmail = data.get('email') ;
            }
            if(data && !this.userPhone){
                this.userPhone = data.get('userPhone') ;
            }
        });
    }



    addNewShippingAddress(params: any, isAdmin = false, userToUpdate = undefined, billID = undefined){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this.ngRedux.dispatch({
            type: 'VERIFY_AND_SUBMIT_NEW_SHIPPING'
        });
        let postData = {
            street1: params.addressLine1,
            street2: params.addressLine2,
            city: params.city,
            state: params.state,
            zip: params.zipCode,
            email: this.userEmail,
            name: params.name,
            mobile: this.userPhone
        };

        this._shippo.validateAddress(postData)
            .flatMap(firstResp => {

                this.ngRedux.dispatch({
                    type: 'VERIFY_ADDRESS_SUCCESS'
                });
                if(isAdmin){
                    return this._nestcare.updateShippingAdmin(params, userToUpdate);
                } else {
                    return this._nestcare.updateShipping(params);
                }

            })
            .subscribe(resp => {

            // add shipping to either nestcare and/or recurly

            this.ngRedux.dispatch({
                type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                payload: {
                    name: params.name,
                    addressLine1: params.addressLine1,
                    addressLine2: params.addressLine2,
                    city: params.city,
                    state: params.state,
                    zipCode: params.zipCode
                }
            })
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        },
        error => {
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.ngRedux.dispatch({
               type: 'VERIFY_ADDRESS_FAILURE',
                payload: {
                    message: "This address came back invalid, please try a valid address."
                }
            });
            console.log(error);
        });
    }


}
