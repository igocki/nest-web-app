import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { ShippoService } from "./shippoService";
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { RootState } from '../store';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';
import { Router } from '@angular/router';

@Injectable()
export class FulfillmentService {

    private workflow;
    private isAdmin;
    private ncBackupToken;
    private nestcareToken;
    private userViewShipping;
    private signupShipping;
    private paidFor;
    private payingFor;
    private planSelected;
    constructor(private _nestcare: NestcareService,
                private _paywhirl: PaywhirlService,
                private _shippo: ShippoService,
                private ngRedux: NgRedux<RootState>,
                private router: Router) {

        let secure = this.ngRedux.select(state => state.secure);

        secure.subscribe(data =>{
           if(data){
               this.ncBackupToken = data.get('nestcareBackupToken');
               this.nestcareToken = data.get('nestcareToken');
           }
        });

        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data){
                this.paidFor = data.get('paidForCustomerID');
                this.payingFor = data.get('payingCustomerID');
                this.workflow = data.get('workflow');
                this.isAdmin = data.get('admin');
                this.signupShipping = data.getIn(['signupInfo', 'shippingAddress']);
                if(data.get('signupInfo') && data.getIn(['signupInfo', 'selectPlan'])){
                    this.planSelected = data.getIn(['signupInfo', 'selectPlan']);
                }
            }
        });

        let userView = this.ngRedux.select(state => state.userView);
        appStatus.subscribe(data => {
            if(data){
                this.userViewShipping = data.get('shippingAddress');
            }
        });
    }

    completeOrderAndNavigate(){
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
        this.router.navigate(['/order-confirm']);

    }

    changeAccountTypeIfAdminOrNot(orderPostData, custID){
        console.log('change accout for');
        console.log(custID);
        if(this.isAdmin){
            return Observable.forkJoin([
                this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                this._nestcare.changeAccountTypeAdmin("support network", custID)
            ]);
        } else {
            if(this.workflow === 'household' && custID === this.payingFor){
                return Observable.forkJoin([
                    this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                ]);
            } else if(this.workflow === 'household' && custID === this.paidFor){
                var mainUserToken = this.nestcareToken;
                var secondUserToken = this.ncBackupToken;
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TEMP_TOKEN',
                    payload: mainUserToken
                });
                this.ngRedux.dispatch({
                    type: 'ADD_NC_TOKEN',
                    payload: secondUserToken
                });
                return Observable.forkJoin([
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                    ]);
            } else {
                return Observable.forkJoin([
                    this._nestcare.addCompletedOrderTracker(orderPostData, custID),
                    this._nestcare.changeAccountType("explorer", "support network", custID)
                ]);
            }
        }
    }

    fulfillOrder(params: any, shippedItems: boolean, custID: any, shippingObj = undefined) { // -- TODO:
        var tracking_number;
        var invoiceNum;
        var deviceOrdered;
        var quant;
        this.ngRedux.dispatch({
            type: 'COMPLETING_ORDER'
        });
        let subscribePost = {
            plan_id: params.plan_id,
            setup_fee: (this.planSelected.get('id') !== 'life') ? 1 : 0,
            sec_dep: 0
        };

        if(shippedItems){
            var shipData = {
                amount: params.shipAmount,
                description: "Shipping Charges for: " + params.shippingType
            };
            var securityDepData;
            if(this.workflow === 'household'){
                securityDepData = {
                    amount: this.planSelected.get('securityDeposit'),
                    description: "Security Deposit Charges for Quantity: 2"
                }
            } else {
                securityDepData = {
                    amount: this.planSelected.get('securityDeposit'),
                    description: "Security Deposit Charges for Quantity: 1"
                }
            }
            //IF THEY WANT TO ADD QUANTITY AND HOUSEHOLD PLAN;
            return Observable.forkJoin([
                this._paywhirl.createCharge(shipData, custID),
                this._paywhirl.createCharge(securityDepData, custID)])
                .flatMap((createChargeResp)=>{
                    if(this.workflow === 'household'){
                        quant = 2;
                    } else {
                        quant = 1;
                    }
                    if(this.planSelected.get('id') !== 'life'){
                        subscribePost.sec_dep = 1;
                    } else {
                        subscribePost.sec_dep = 0;
                    }

                    return this._paywhirl.subscribeCustomer(subscribePost, custID, quant).flatMap((subRes) =>{
                        if(this.workflow === 'household'){
                            deviceOrdered = 'household bundle kit';
                        } else {
                            deviceOrdered = 'individual kit';
                        }
                        invoiceNum = subRes.invoiceNum;
                        var shipping_string;
                        if(this.signupShipping){
                            shipping_string = this.signupShipping.get('addressLine1') + ' ' + this.signupShipping.get('addressLine2') + ' ' +
                                this.signupShipping.get('city') + ' ' +  this.signupShipping.get('state') + ' ' + this.signupShipping.get('zipCode');
                        }

                        if(this.userViewShipping && !shipping_string){
                            shipping_string = this.userViewShipping.get('line1') + ' ' + this.userViewShipping.get('line2') + ' ' +
                                this.userViewShipping.get('city_id') + ' ' +  this.userViewShipping.get('state_id') + ' ' + this.userViewShipping.get('zip_code');
                        }

                        var shippo_object_string = deviceOrdered+ ',' + invoiceNum + ',' + shipping_string + ',' + shippingObj.amount;
                        shippo_object_string = shippo_object_string.substring(0, 254);
                        let orderPostData = {
                            paying_cust: custID,
                            shippo_id: shippo_object_string,
                            tracking_number: (shippingObj) ? shippingObj.service : '',
                            status: true
                        };
                        if(this.workflow === 'household'){
                            console.log('did this just happen');


                            return Observable.forkJoin([
                                this.changeAccountTypeIfAdminOrNot(orderPostData, this.payingFor)
                            ]).flatMap((testResp)=> {
                               return Observable.forkJoin([
                                   this.changeAccountTypeIfAdminOrNot(orderPostData, this.paidFor)
                               ])
                            });
                        } else {
                            console.log('did this just happen2');
                            return Observable.forkJoin([
                                this.changeAccountTypeIfAdminOrNot(orderPostData, custID)
                            ]);
                        }
                    })
                    .catch((error) =>{
                        console.log('here');
                        if(createChargeResp && createChargeResp[0].adjustment){
                            this._paywhirl.refundCharge(createChargeResp[0].adjustment.uuid).subscribe(resRefund =>{
                                console.log('refunded charge 1');
                            });
                        }

                        if(createChargeResp && createChargeResp[1].adjustment){
                            this._paywhirl.refundCharge(createChargeResp[1].adjustment.uuid).subscribe(resRefund =>{
                                console.log('refunded charge 2');
                            });
                        }


                        return Observable.throw(error);
                    })
                });
        } else {
            if(this.workflow === 'household'){
                quant = 2;
            } else {
                quant = 1;
            }
            return this._paywhirl.subscribeCustomer(subscribePost, custID, quant).flatMap(() => {
                let orderPostData = {
                    paying_cust: custID,
                    shippo_id: (shippingObj) ? shippingObj.amount : '',
                    tracking_number: (shippingObj) ? shippingObj.service : '',
                    status: true
                };
                return this.changeAccountTypeIfAdminOrNot(orderPostData, custID);
            })
        }
    }

}
