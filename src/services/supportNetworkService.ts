import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { RootState } from '../store';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';
import { Router } from '@angular/router';

@Injectable()
export class SupportNetworkService {


    private isAdmin;
    constructor(private _nestcare: NestcareService,
                private ngRedux: NgRedux<RootState>,
                private router: Router) {

        let appStatus = this.ngRedux.select(state => state.appStatus);
        appStatus.subscribe(data => {
            if(data){
                this.isAdmin = data.get('admin');
            }
        });
    }

    getPeopleYouSupport(custID){

        return Observable.forkJoin([
            this._nestcare.getPeopleYouSupport(),
            this._nestcare.getPeoplePaidFor(custID)])
            .map((res) => {
                console.log('made it');
                console.log(res[0]);
                var paidUserIdsList = res[1].map(function(item){
                    return item.paid_user_id;
                });

                res[0].forEach(function(item){
                    if(paidUserIdsList.indexOf(item.id) > -1){
                        item.canChangeBilling = true;
                    }
                });
                return res[0];
            });
    }


}
