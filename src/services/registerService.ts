import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import { PaywhirlService } from "./paywhirlService";
import { NestcareService } from "./nestcareService";
import { NgRedux } from 'ng2-redux';
import { Map, List } from 'immutable';
import { RootState } from '../store';
import { LoadingActions } from '../actions/loading-actions';

@Injectable()
export class RegisterService {

    private userFirstName;
    private userLastName;
    private userEmail;
    private loginStatus;
    private workflow;
    private nc_id;
    private isAdmin;
    private paidUser;
    private payingUser;
    private nestcareToken;
    constructor(private _nestcare: NestcareService,
                private _paywhirl: PaywhirlService,
                private ngRedux: NgRedux<RootState>) {

        let appStatus = this.ngRedux.select(state => state.appStatus);
        let secure = this.ngRedux.select(state => state.secure);
        appStatus.subscribe(data => {
            if(data){
                this.loginStatus = data.get('loginStatus');
                this.workflow = data.get('workflow');
                this.paidUser = data.get('paidForCustomerID');
                this.payingUser = data.get('payingCustomerID');
                this.isAdmin = data.get('admin');
            }

            if(data && data.get('signupInfo') && data.getIn(['signupInfo', 'userInfo'])){
                this.userEmail = data.getIn(['signupInfo', 'userInfo', 'email']);
                this.userFirstName = data.getIn(['signupInfo', 'userInfo', 'first_name']);
                this.userLastName = data.getIn(['signupInfo', 'userInfo', 'last_name']);
            }
        });

        secure.subscribe(data => {
            if(data){
                this.nestcareToken = data.get('nestcareToken');
            }
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });
        userViewSub.subscribe((data) =>{
            if(data && !this.userEmail){
                this.userEmail = data.get('email') ;
            }
            if(data && !this.userFirstName){
                this.userFirstName = data.get('first_name') ;
            }
            if(data && !this.userLastName){
                this.userFirstName = data.get('last_name') ;
            }
            if(data){
                this.nc_id = data.get('nc_id');
            }
        });
    }


    getCardsAfterRecurlyRegister(userID){
        if(this.workflow === 'other' || this.workflow === 'household'){
            var params = {
                "paid_for_customer": this.paidUser,
                "paying_customer": this.payingUser
            };
            return this._nestcare.addPaymentCustomer(params).flatMap((resp) =>{
                    console.log('successful added paying customer');
                    return this._paywhirl.loadPaymentCards(userID);
                });
        } else {
            return this._paywhirl.loadPaymentCards(userID);
        }
    }


    adminGetUserInformation(nestcareID: any){
        return this._paywhirl.GetCustomer(nestcareID).flatMap((custResp) => {
            return Observable.forkJoin([
                this._paywhirl.LookupActiveSubscriptions(nestcareID),
                this._paywhirl.loadLoginPaymentCards(nestcareID),
                this._paywhirl.LookupCollectedInvoices(nestcareID)
            ])
        }).catch(error => {
            console.log('ERRORED OUT');
            if(error && error.message && error.message === 'Recurly Customer Not Found'){
                error.recurlyNotFound = true;
                return Observable.throw(error);
            } else {
                console.log('threw this');
                console.log(error);
                return Observable.throw(error);
            }
        });
    }

    getUserInformation(nestcareID: any){
        console.log('GETTING USER INFORMATION');
        return Observable.forkJoin([
            this._nestcare.GetCustomer(),
            this._paywhirl.GetCustomer(nestcareID)]).flatMap((custResp)=>{
            if(!this.isAdmin){
                if(custResp && custResp[0]){
                    var cust = {
                        first_name: custResp[0].first_name,
                        last_name: custResp[0].last_name,
                        fullname: custResp[0].first_name + ' ' + custResp[0].last_name,
                        email: custResp[0].email,
                        accountType: custResp[0].account_type.text,
                        userBirth: custResp[0].date_of_birth,
                        userGender: custResp[0].gender,
                        userPhone: custResp[0].mobile,
                        nc_id: custResp[0].id
                    };
                    this.ngRedux.dispatch({
                        type: 'STORE_USER_INFO_VIEW1',
                        payload: cust
                    });
                }
            }

            return Observable.forkJoin([
                this._paywhirl.LookupActiveSubscriptions(nestcareID),
                this._paywhirl.loadLoginPaymentCards(nestcareID),
                this._paywhirl.LookupCollectedInvoices(nestcareID)]);
        }).catch(error => {
            console.log('ERRORED OUT');
            if(error && error.message && error.message === 'Recurly Customer Not Found'){
                this._nestcare.GetCustomer().subscribe(custGet =>{
                    if(custGet){
                        var cust = {
                            first_name: custGet.first_name,
                            last_name: custGet.last_name,
                            fullname: custGet.first_name + ' ' + custGet.last_name,
                            email: custGet.email,
                            accountType: custGet.account_type.text,
                            userBirth: custGet.date_of_birth,
                            userGender: custGet.gender,
                            userPhone: custGet.mobile,
                            nc_id: custGet.id
                        };
                        console.log('RASDASDAW THIS');
                        this.ngRedux.dispatch({
                            type: 'STORE_USER_INFO_VIEW1',
                            payload: cust
                        });
                    }
                });

                error.recurlyNotFound = true;
                return Observable.throw(error);
                // var userParams = {
                //     first_name: this.userFirstName,
                //     last_name: this.userLastName,
                //     email: this.userEmail,
                //     nc_id: nestcareID
                // };
                // return this._paywhirl.CreateCustomer(userParams, false, true).flatMap((resp) => {
                //     console.log('success');
                //     console.log(resp);
                //     return Observable.forkJoin([
                //         this._paywhirl.GetCustomer(nestcareID),
                //         this._paywhirl.LookupActiveSubscriptions(nestcareID),
                //         this._paywhirl.loadLoginPaymentCards(nestcareID),
                //         this._paywhirl.LookupCollectedInvoices(nestcareID)]);
                // }).catch(innerError => {
                //     return Observable.throw(innerError);
                // });
            } else {
                console.log('threw this');
                console.log(error);
                return Observable.throw(error);
            }
        });
        // return this._paywhirl.GetCustomer(nestcareID);
    }

    checkRecurlyAndCreateAndNoToken(nestcareID, userParams){
        return this._paywhirl.GetCustomer(nestcareID).map((resp: any) => {

            return resp;
        }).catch((error:any) => {
            var payingForOther = (this.workflow === 'other') ? true : false;
            console.log('this and');
            console.log(error);
            if(error && error.status === 400 || error.status === 404){
                return this._paywhirl.CreateCustomer(userParams, payingForOther);
            }
            if(error && error.message === 'Recurly Customer Not Found'){
                return this._paywhirl.CreateCustomer(userParams, payingForOther);
            }
            console.log(error);
            return Observable.throw(error);
        });
    }

    checkRecurlyAndCreate(nestcareID, userParams){
        return this._paywhirl.GetCustomer(nestcareID).map((resp: any) => {

            return resp;
        }).catch((error:any) => {
            var payingForOther = (this.workflow === 'other') ? true : false;
            console.log('this and');
            console.log(error);
            if(error && error.status === 400 || error.status === 404){
                return this._paywhirl.CreateCustomer(userParams, payingForOther, true);
            }
            if(error && error.message === 'Recurly Customer Not Found'){
                return this._paywhirl.CreateCustomer(userParams, payingForOther, true);
            }
            console.log(error);
            return Observable.throw(error);
        });
    }

    registerAndLogin(email: string, password: string) {
        let postData = {
            "email": email,
            "password": password
        };
        let that = this;
        let objectResp;
        return this._nestcare.login(email, password)
            .flatMap((res) => {

                objectResp = res;
                console.log('OYooooooo');
                console.log(res);
                if(objectResp.token){
                    that.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: objectResp.token
                    });
                    if(objectResp.meta){

                        that.ngRedux.dispatch({
                            type: 'ADD_NC_TOKEN',
                            payload: objectResp.meta.token
                        });
                    }
                    return Observable.create(observer => {
                        observer.next(objectResp);
                    });
                } else {
                    if(objectResp.meta){

                        that.ngRedux.dispatch({
                            type: 'ADD_NC_TOKEN',
                            payload: objectResp.meta.token
                        });
                    }
                    return Observable.create(observer => {
                        observer.next(objectResp);
                    });
                }
            })
            .catch((error:any) => {
                console.log("HEYYYYY");
                console.log(error);
                let errMsg: string;
                return Observable.throw(error);
            });
    }

    registerUser(params: any, paidForOther = false, isAdmin = false) { // -- TODO:
        var originalhash = params.password;
        return this._nestcare.register(params)
            .flatMap((res:Response) => {
            console.log('NAHHSHSD');
                console.log(res.json());
                console.log('got 1st response');

                if(isAdmin){
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TEMP_TOKEN',
                        payload: res.json().meta.token
                    });
                }
                if(paidForOther){
                    this.ngRedux.dispatch({
                        type: 'ADD_PAID_FOR_CUSTOMER_ID',
                        payload: res.json().data.user_id
                    });
                    if(this.loginStatus === 'logged-in'){
                        this.ngRedux.dispatch({
                            type: 'ADD_CUSTOMER_ID',
                            payload: this.nc_id
                        });
                    }
                    if(this.workflow === 'household'){
                        this.ngRedux.dispatch({
                            type: 'ADD_NC_TEMP_TOKEN',
                            payload: res.json().meta.token
                        });
                    } else {
                        this.ngRedux.dispatch({
                            type: 'ADD_NC_TOKEN',
                            payload: res.json().meta.token
                        });
                    }

                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                } else {

                    // ADD_CUSTOMER_ID IS ADDING_PAYING_CUSTOMER
                    this.ngRedux.dispatch({
                        type: 'ADD_CUSTOMER_ID',
                        payload: res.json().data.user_id
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_PAID_FOR_CUSTOMER_ID',
                        payload: res.json().data.user_id
                    });
                }

                if(!isAdmin && this.workflow !== 'other' && this.workflow !== 'household'){
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_IG_TOKEN',
                        payload: res.json().token
                    });
                }

                if(this.workflow === 'household'){
                    if(!this.nestcareToken){
                        this.ngRedux.dispatch({
                            type: 'ADD_NC_TOKEN',
                            payload: res.json().meta.token
                        });
                        this.ngRedux.dispatch({
                            type: 'ADD_IG_TOKEN',
                            payload: res.json().token
                        });
                    } else {
                        this.ngRedux.dispatch({
                            type: 'ADD_NC_TEMP_TOKEN',
                            payload: res.json().meta.token
                        });
                    }
                }


                if(!isAdmin && this.workflow === 'other'){

                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TEMP_TOKEN',
                        payload: this.nestcareToken
                    });
                    this.ngRedux.dispatch({
                        type: 'ADD_NC_TOKEN',
                        payload: res.json().meta.token
                    });
                }

                params.nc_id = res.json().data.user_id;
                return Observable.create(observer => {
                    observer.next(res.json().data);
                });
                // return this._paywhirl.CreateCustomer(params, paidForOther);
            })
            .catch((error:any) => {
                if(error.nestCareError){
                    return Observable.throw(error.nestCareError);
                } else if(error.status === 422) {
                    return Observable.throw(error.json().message);
                }
                else {
                    return Observable.throw({
                        message: 'We could not complete the request. (PWR)'
                    });
                }
            });
        // return this._paywhirl.CreateCustomer(params)
        //     .flatMap((res:Response) => {
        //         console.log('Got 1st response');
        //         console.log(res.json());
        //         console.log(res.json().account_code);
        //         this.ngRedux.dispatch({
        //             type: 'ADD_IG_TOKEN',
        //             payload: res.json().token
        //         });
        //         this.ngRedux.dispatch({
        //             type: 'ADD_CUSTOMER_ID',
        //             payload: res.json().account.account_code
        //         });
        //         params.password = originalhash;
        //         return this._nestcare.register(params);
        //     })
    }
}
