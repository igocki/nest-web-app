import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import { Map, List } from 'immutable';
import { NgRedux } from 'ng2-redux';
import { LoadingActions } from '../actions/loading-actions';
import { RootState } from '../store';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';

@Injectable()
export class UserService {
    public email;
    public fullname;
    public birthdate;
    public mobile;
    public gender;
    public nestcareID;
    public planName;
    public planAmt;
    public lastPmt;
    public nextPmt;
    public orders;
    public name;
    public shipAddress;
    public billingInfo;
    public userStatus;
    public subscriptionAuth;
    public subscriptionUserView;
    public paywhirlAccount;

    constructor(private ngRedux: NgRedux<RootState>) {



        this.email = this.ngRedux.select(state => {
            return state.auth.getIn(['profile', 'email']);
        });

        this.fullname = this.ngRedux.select(state => {
            return state.auth.getIn(['profile', 'first_name']) + ' ' + state.auth.getIn(['profile', 'last_name']);
        });

        this.shipAddress = this.ngRedux.select(state => {
            return state.userView.getIn(['shippingAddress']);
        });

        this.userStatus = this.ngRedux.select(state => {
            return state.userView.getIn(['status']);
        });

        this.mobile = this.ngRedux.select(state => {
           return state.auth.getIn(['profile', 'mobile']);
        });

        this.birthdate = this.ngRedux.select(state => {
            return state.auth.getIn(['profile', 'date_of_birth']);
        });

        this.gender = this.ngRedux.select(state => {
            return state.auth.getIn(['profile', 'gender']);
        });


    }

    login(){
        this.refresh();
    }

    refresh(){
        let authSub = this.ngRedux.select(state => {
            return state.auth.getIn(['profile']);
        });

        let userViewSub = this.ngRedux.select(state => {
            return state.userView;
        });

        this.subscriptionUserView = userViewSub.subscribe(data => {
            if(data){
                this.planName = data.getIn(['plan', 'planName']);
                this.planAmt = data.getIn(['plan', 'planCost']);
                this.lastPmt = data.getIn(['plan', 'last_payment']);
                this.nextPmt = data.getIn(['plan', 'next_payment']);
                this.orders = data.getIn(['orders']);
                console.log('heree now');
                console.log( data.getIn(['billingAddress']));
                this.billingInfo = data.get('billingAddress');
                console.log('hit this');
                // let userObj = {
                //     userStatus: data.get('status')
                // };
                // this.ngRedux.dispatch({
                //     type: 'STORE_USER_INFO_VIEW2',
                //     payload: userObj
                // });
            }

        });
        this.subscriptionUserView.unsubscribe();
        this.subscriptionAuth = authSub.subscribe(data => {
            console.log('LOOK AT THIS');
            console.log(data);
            if(data){
                let userObj = {
                    email: data.get('email'),
                    fullname: data.get('first_name') + ' ' + data.get('last_name'),
                    first_name: data.get('first_name'),
                    last_name: data.get('last_name'),
                    userPhone: data.get('mobile'),
                    userBirth: data.get('date_of_birth'),
                    userGender: data.get('gender'),
                    accountType: (data.getIn(['account_type'])) ? data.getIn(['account_type', 'text']) || data.getIn(['account_type']).text : '',
                    nc_id: data.get('id')
                };


                this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: userObj
                });

                this.nestcareID = data.get('id');
                this.name = data.get('first_name') + ' ' + data.get('last_name');
                // this.shipAddress = data.getIn(['address', 'home']);
                this.paywhirlAccount = data.getIn(['paywhirl']);
                console.log('end here 1:');
            }
            console.log('end here 2:');

        });
        this.subscriptionAuth.unsubscribe();
    }


}
