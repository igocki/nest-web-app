import { AppStatusActions } from '../../actions/app-status-actions';
import { INITIAL_STATE } from './app-status.initial-state';
import { Map, List } from 'immutable';
export function appStatusReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case AppStatusActions.STORE_SIGNUP_INFO:
            return Map({
                appStatus: 'nestcare-signedup',
                signupInfo: Map({
                    userInfo: Map({
                        first_name: action.payload.first_name,
                        last_name: action.payload.last_name,
                        email: action.payload.email,
                        gender: action.payload.gender,
                        mobile: action.payload.mobile
                    }),
                    selectPlan: null,
                    deviceLineItems: List([]),
                    shippingAddress: state.getIn(['signupInfo', 'shippingAddress'])
                }),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case AppStatusActions.STORE_SELECT_PLAN:
            return Map({
                appStatus: 'selected-plan',
                signupInfo: Map({
                    userInfo: state.getIn(['signupInfo', 'userInfo']),
                    selectPlan: Map(action.payload),
                    deviceLineItems: state.getIn(['signupInfo', 'deviceLineItems']),
                    shippingAddress: state.getIn(['signupInfo', 'shippingAddress'])
                }),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case AppStatusActions.STORE_SHIPPING_ADDRESS:
            return Map({
                appStatus: 'selected-shipping-address',
                signupInfo: Map({
                    userInfo: state.getIn(['signupInfo', 'userInfo']),
                    selectPlan: state.getIn(['signupInfo', 'selectPlan']),
                    deviceLineItems: state.getIn(['signupInfo', 'deviceLineItems']),
                    shippingAddress: Map(action.payload)
                }),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case AppStatusActions.STORE_SELECT_DEVICES:
            return Map({
                appStatus: 'selected-devices',
                signupInfo: Map({
                    userInfo: state.getIn(['signupInfo', 'userInfo']),
                    selectPlan: state.getIn(['signupInfo', 'selectPlan']),
                    deviceLineItems: List(action.payload),
                    shippingAddress: state.getIn(['signupInfo', 'shippingAddress'])
                }),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'STORE_OTHER_SIGNUP_INFO':
            return Map({
                appStatus: 'nestcare-signedup',
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: Map({
                    userInfo: Map({
                        first_name: action.payload.first_name,
                        last_name: action.payload.last_name,
                        email: action.payload.email,
                        gender: action.payload.gender,
                        mobile: action.payload.mobile
                    }),
                    selectPlan: null,
                    deviceLineItems: state.getIn(['signupInfo', 'deviceLineItems']),
                    shippingAddress: state.getIn(['signupInfo', 'shippingAddress'])
                }),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'GET_PLANS':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: List(action.payload),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'ADD_CUSTOMER_ID':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: action.payload,
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'ADD_PAID_FOR_CUSTOMER_ID':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: action.payload,
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'REMOVE_CUSTOMER_ID':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: null,
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'ADD_IG_TOKEN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: action.payload,
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CLEAR_IG_TOKEN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: null,
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'ADD_NC_TOKEN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: action.payload,
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CLEAR_NC_TOKEN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: null,
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CLEAR_ONLY_SIGNUP_INFO':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: null,
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CLEAR_SIGNUP':
            return Map({
                appStatus: state.get('appStatus'),
                signUpInfo: null,
                otherSignUpInfo: null,
                payingCustomerID: null,
                igToken: null,
                workflow: null,
                plans: null,
                paidForCustomerID: null,
                ncToken: null,
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CHANGE_WORKFLOW':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: action.payload,
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CHANGE_TO_LOGGED_IN':
            return Map({
                appStatus: 'logged-in',
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: 'logged-in',
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CHANGE_TO_NOT_REGISTERED':
            return Map({
                appStatus: 'not-registered',
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: 'logged-out',
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'LOGOUT':
            return Map({
                appStatus: 'not-registered',
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: false,
                loginStatus: 'logged-out',
                enrollNewPlan: false
            });
        case 'ADMIN_LOGGED_IN':
            return Map({
                appStatus: 'logged-in',
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: action.payload,
                admin: true,
                loginStatus: 'logged-in',
                enrollNewPlan: state.get('enrollNewPlan')
            });
        case 'CHANGE_TO_ENROLL_PLAN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: true
            });
        case 'CLEAR_ENROLL_PLAN':
            return Map({
                appStatus: state.get('appStatus'),
                signupInfo: state.get('signupInfo'),
                otherSignUpInfo: state.get('otherSignUpInfo'),
                payingCustomerID: state.get('payingCustomerID'),
                igToken: state.get('igToken'),
                workflow: state.get('workflow'),
                plans: state.get('plans'),
                paidForCustomerID: state.get('paidForCustomerID'),
                ncToken: state.get('ncToken'),
                admin: state.get('admin'),
                loginStatus: state.get('loginStatus'),
                enrollNewPlan: false
            });
        default:
            return state;
    }
}
