import { Map, fromJS  } from 'immutable';
import { IAppStatus, IAppStatusType } from './app-status.types';

export function deimmutifyAppStatus(state: IAppStatusType): Object {
    return state.toJS();
}

export function reimmutifyAppStatus(plain): IAppStatusType {
    return Map<string, IAppStatus>(plain ? fromJS(plain) : {});
}
