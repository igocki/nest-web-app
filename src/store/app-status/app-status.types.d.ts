import { List, Map } from 'immutable';
export interface ISignupInfo {
    userInfo: any;
    selectPlan: any;
    deviceLineItems: List<IDeviceLineItem>;
    shippingAddress: any;
}
export interface IDeviceLineItem {
    amt: number;
    name: string;
    qty: number;
}
export interface IAppStatus {
    appStatus: string;
    signupInfo: Map<string, ISignupInfo>;
    otherSignUpInfo: Map<string, ISignupInfo>;
    payingCustomerID: string;
    igToken: string;
    workflow: string;
    plans: any;
    paidForCustomerID: string;
    ncToken: string;
    admin: boolean;
    loginStatus: string;
    enrollNewPlan: boolean;
}
export declare type IAppStatusType = Map<string, IAppStatus>;
