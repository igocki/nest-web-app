import { IAppStatusType } from './app-status.types';
export declare function deimmutifyAppStatus(state: IAppStatusType): Object;
export declare function reimmutifyAppStatus(plain: any): IAppStatusType;
