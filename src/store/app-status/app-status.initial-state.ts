import { Map, List } from 'immutable';
import { IAppStatus, IDeviceLineItem } from './app-status.types';

export const INITIAL_STATE = Map<string, IAppStatus>({
    appStatus: 'not-registered',
    signUpInfo: null,
    otherSignUpInfo: null,
    payingCustomerID: null,
    igToken: null,
    workflow: null,
    plans: null,
    paidForCustomerID: null,
    ncToken: null,
    admin: false,
    loginStatus: 'logged-out',
    enrollNewPlan: false
});
