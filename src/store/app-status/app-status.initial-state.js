import { Map } from 'immutable';
export var INITIAL_STATE = Map({
    appStatus: 'not-registered',
    signUpInfo: null,
    otherSignUpInfo: null,
    payingCustomerID: null,
    igToken: null,
    workflow: null,
    plans: null,
    paidForCustomerID: null,
    ncToken: null,
    admin: false,
    loginStatus: 'logged-out',
    enrollNewPlan: false
});
