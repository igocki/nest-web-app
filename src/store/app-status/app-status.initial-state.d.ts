import { Map } from 'immutable';
import { IAppStatus } from './app-status.types';
export declare const INITIAL_STATE: Map<string, IAppStatus>;
