import { Map, fromJS } from 'immutable';
export function deimmutifyAppStatus(state) {
    return state.toJS();
}
export function reimmutifyAppStatus(plain) {
    return Map(plain ? fromJS(plain) : {});
}
