import { Map, List } from 'immutable';
//import { IShippingRates } from './shipping-rates.types';

export const INITIAL_STATE = Map<string, any>({
    orderCart: List([]),
    status: null,
    error: null
});
