import { Map, fromJS } from 'immutable';
export function deimmutifyOrderSummary(state) {
    return state.toJS();
}
export function reimmutifyOrderSummary(plain) {
    return Map(plain ? fromJS(plain) : {});
}
