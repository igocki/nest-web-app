import { Map } from 'immutable';
export declare function deimmutifyOrderSummary(state: Map<string, any>): Object;
export declare function reimmutifyOrderSummary(plain: any): Map<string, any>;
