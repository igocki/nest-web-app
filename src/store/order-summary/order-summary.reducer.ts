
import { INITIAL_STATE } from './order-summary.initial-state';
import { Map, List, fromJS } from 'immutable';


export function orderSummaryReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any> {
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case 'COMPLETING_ORDER':
            return Map<string, any>({
                orderCart: state.get('orderCart'),
                status: 'submitted',
                error: null
            });
        case 'GENERAL_SUBSCRIPTION_FAILURE':
            return Map<string, any>({
                orderCart: state.get('orderCart'),
                status: 'failure',
                error: Map<string, any>(action.payload)
            });
        // case 'SUBSCRIPTION_FAILURE':
        //     return Map<string, any>({
        //         orderCart: state.get('orderCart'),
        //         status: 'try-again',
        //         error: Map<string, any>(action.payload)
        //     });
        case 'CLEAR_ORDER_SUMMARY':
            return Map<string, any>({
                orderCart: List<Map<string, any>>([]),
                status: null,
                error: null
            });
        case 'CLEAR_LISTED_DEVICES':
            let cart = state.get('orderCart').toJS();
            cart = cart.filter(function(item){
                if(item.type){
                    return item.type !== 'product';
                }
                return item;
            });
            console.log(cart);
            console.log('hello');
            return Map<string, any>({
                orderCart: List<Map<string, any>>(fromJS(cart)),
                status: null,
                error: null
            });
            //TO DO WRITE LOGIC OF CLEAR PRODUCTS
        case 'ADD_ORDER_LINE_ITEM':
            let newIndex = 0;
            if(state.get('orderCart').size > 0){
                newIndex = state.get('orderCart').last().get('id');
            }
            return Map<string, any>({
                orderCart: state.get('orderCart').push(Map<string,any>({
                    id: newIndex + 1,
                    lineDesc: action.payload.lineItemTitle,
                    amt: action.payload.amt,
                    type: action.payload.type
                })),
                status: state.get('status'),
                error: state.get('error')
            });
        case 'CLEAR_SIGNUP':
            return Map<string, any>({
                orderCart: List([]),
                status: null,
                error: null
            });
        // case 'REMOVE_LINE_ITEM':
        //     return state.filter(n => n.p);
        default:
            return state;
    }
}
