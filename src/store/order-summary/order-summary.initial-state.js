import { Map, List } from 'immutable';
//import { IShippingRates } from './shipping-rates.types';
export var INITIAL_STATE = Map({
    orderCart: List([]),
    status: null,
    error: null
});
