import { INITIAL_STATE } from './order-summary.initial-state';
import { Map, List, fromJS } from 'immutable';
export function orderSummaryReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case 'COMPLETING_ORDER':
            return Map({
                orderCart: state.get('orderCart'),
                status: 'submitted',
                error: null
            });
        case 'GENERAL_SUBSCRIPTION_FAILURE':
            return Map({
                orderCart: state.get('orderCart'),
                status: 'failure',
                error: Map(action.payload)
            });
        // case 'SUBSCRIPTION_FAILURE':
        //     return Map<string, any>({
        //         orderCart: state.get('orderCart'),
        //         status: 'try-again',
        //         error: Map<string, any>(action.payload)
        //     });
        case 'CLEAR_ORDER_SUMMARY':
            return Map({
                orderCart: List([]),
                status: null,
                error: null
            });
        case 'CLEAR_LISTED_DEVICES':
            var cart = state.get('orderCart').toJS();
            cart = cart.filter(function (item) {
                if (item.type) {
                    return item.type !== 'product';
                }
                return item;
            });
            console.log(cart);
            console.log('hello');
            return Map({
                orderCart: List(fromJS(cart)),
                status: null,
                error: null
            });
        //TO DO WRITE LOGIC OF CLEAR PRODUCTS
        case 'ADD_ORDER_LINE_ITEM':
            var newIndex = 0;
            if (state.get('orderCart').size > 0) {
                newIndex = state.get('orderCart').last().get('id');
            }
            return Map({
                orderCart: state.get('orderCart').push(Map({
                    id: newIndex + 1,
                    lineDesc: action.payload.lineItemTitle,
                    amt: action.payload.amt,
                    type: action.payload.type
                })),
                status: state.get('status'),
                error: state.get('error')
            });
        case 'CLEAR_SIGNUP':
            return Map({
                orderCart: List([]),
                status: null,
                error: null
            });
        // case 'REMOVE_LINE_ITEM':
        //     return state.filter(n => n.p);
        default:
            return state;
    }
}
