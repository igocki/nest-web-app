import { Map, List, fromJS  } from 'immutable';

export function deimmutifyOrderSummary(state: Map<string, any>): Object {
    return state.toJS();
}

export function reimmutifyOrderSummary(plain): Map<string, any> {
    return Map<string, any>(plain ? fromJS(plain) : {});
}
