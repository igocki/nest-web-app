import { Map } from 'immutable';
export interface ILoading {
    visible: boolean;
}
export declare type ILoadingType = Map<string, ILoading>;
