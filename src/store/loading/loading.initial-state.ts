import { Map } from 'immutable';
import { ILoading } from './loading.types';

export const INITIAL_STATE = Map<string, ILoading>({
    visible: false
});
