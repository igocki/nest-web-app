import { Map } from 'immutable';
import { ILoadingType, ILoading } from './loading.types';
import { INITIAL_STATE } from './loading.initial-state';
import { LoadingActions } from '../../actions/loading-actions';

export function loadingReducer(state: ILoadingType = INITIAL_STATE, action): ILoadingType {
    switch (action.type) {
        case LoadingActions.SHOW_LOADING:
            return Map<string, ILoading>({
                visible: true
            });
        case LoadingActions.HIDE_LOADING:
            return Map<string, ILoading>({
                visible: false
            });
        default:
            return state;
    }
}
