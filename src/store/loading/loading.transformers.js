import { Map, fromJS } from 'immutable';
export function deimmutifyLoading(state) {
    return state.toJS();
}
export function reimmutifyLoading(plain) {
    return Map(plain ? fromJS(plain) : {});
}
