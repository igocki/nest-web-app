import { Map, fromJS  } from 'immutable';
import { ILoadingType, ILoading } from './loading.types';

export function deimmutifyLoading(state: ILoadingType): Object {
    return state.toJS();
}

export function reimmutifyLoading(plain): ILoadingType {
    return Map<string, ILoading>(plain ? fromJS(plain) : {});
}
