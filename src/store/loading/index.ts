import { ILoading, ILoadingType } from './loading.types';
import { loadingReducer } from './loading.reducer';
import {deimmutifyLoading, reimmutifyLoading } from './loading.transformers';

export {
    ILoading,
    ILoadingType,
    loadingReducer,
    deimmutifyLoading,
    reimmutifyLoading
}
