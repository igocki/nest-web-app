import { Map } from 'immutable';

export interface ILoading {
    visible: boolean;
}

export type ILoadingType = Map<string, ILoading>;

