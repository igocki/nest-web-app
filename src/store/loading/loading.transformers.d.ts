import { ILoadingType } from './loading.types';
export declare function deimmutifyLoading(state: ILoadingType): Object;
export declare function reimmutifyLoading(plain: any): ILoadingType;
