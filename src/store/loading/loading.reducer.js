import { Map } from 'immutable';
import { INITIAL_STATE } from './loading.initial-state';
import { LoadingActions } from '../../actions/loading-actions';
export function loadingReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case LoadingActions.SHOW_LOADING:
            return Map({
                visible: true
            });
        case LoadingActions.HIDE_LOADING:
            return Map({
                visible: false
            });
        default:
            return state;
    }
}
