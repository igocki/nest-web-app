import { INITIAL_STATE } from './shipping-rates.initial-state';
import { Map, List } from 'immutable';
export function shippingRatesReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ADD_RATES':
            return Map({
                rates: List(action.payload),
                selectedRate: state.get('selectedRate')
            });
        case 'FETCH_RATES_FAILURE':
            return Map({
                rates: List([]),
                selectedRate: null
            });
        case 'SELECT_RATE':
            return Map({
                rates: state.get('rates'),
                selectedRate: Map(action.payload)
            });
        case 'CLEAR_SIGNUP':
            return Map({
                rates: null,
                selectedRate: null
            });
        default:
            return state;
    }
}
