//import { ShippingRatesActions } from '../../actions/shipping-rates-actions';
//import { IShipping, IShippingType } from './shipping.types';
import { INITIAL_STATE } from './shipping-rates.initial-state';
import { Map, List } from 'immutable';


export function shippingRatesReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any>{
    switch (action.type) {
        case 'ADD_RATES':
            return Map<string, any>({
                rates: List<any>(action.payload),
                selectedRate: state.get('selectedRate')
            });
        case 'FETCH_RATES_FAILURE':
            return Map<string, any>({
                rates: List<any>([]),
                selectedRate: null
            });
        case 'SELECT_RATE':
            return Map<string, any>({
                rates: state.get('rates'),
                selectedRate: Map<string, any>(action.payload)
            });
        case 'CLEAR_SIGNUP':
            return Map<string, any>({
                rates: null,
                selectedRate: null
            });
        default:
            return state;
    }
}
