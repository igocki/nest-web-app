import { Map } from 'immutable';
export declare function deimmutifyShippingRates(state: Map<string, any>): Object;
export declare function reimmutifyShippingRates(plain: any): Map<string, any>;
