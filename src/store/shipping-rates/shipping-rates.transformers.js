import { Map, fromJS } from 'immutable';
//import { IShipping, IShippingType } from './shipping.types';
export function deimmutifyShippingRates(state) {
    return state.toJS();
}
export function reimmutifyShippingRates(plain) {
    return Map(plain ? fromJS(plain) : {});
}
