import { Map, fromJS } from 'immutable';
export function deimmutifySecure(state) {
    return state.toJS();
}
export function reimmutifySecure(plain) {
    return Map(plain ? fromJS(plain) : {});
}
