import { Map, List, fromJS  } from 'immutable';

export function deimmutifySecure(state: Map<string, any>): Object {
    return state.toJS();
}

export function reimmutifySecure(plain): Map<string, any> {
    return Map<string, any>(plain ? fromJS(plain) : {});
}
