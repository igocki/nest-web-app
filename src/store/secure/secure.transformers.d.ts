import { Map } from 'immutable';
export declare function deimmutifySecure(state: Map<string, any>): Object;
export declare function reimmutifySecure(plain: any): Map<string, any>;
