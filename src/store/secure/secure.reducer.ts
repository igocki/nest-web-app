
import { INITIAL_STATE } from './secure.initial-state';
import { Map } from 'immutable';


export function secureReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any> {
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case 'ADD_IG_TOKEN':
            return Map<string, any>({
                igockiToken: action.payload,
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'ADD_NC_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: action.payload,
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'ADD_BACKUP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: action.payload,
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'ADD_IG_TEMP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: action.payload,
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'ADD_NC_TEMP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: action.payload
            });
        case 'CLEAR_NC_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: null,
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'CLEAR_IG_TOKEN':
            return Map<string, any>({
                igockiToken: null,
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'CLEAR_BACKUP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: null,
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'CLEAR_NC_BACKUP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: state.get('tempigockiToken'),
                backupToken: state.get('backupToken'),
                nestcareBackupToken: null
            });
        case 'CLEAR_IG_TEMP_TOKEN':
            return Map<string, any>({
                igockiToken: state.get('igockiToken'),
                nestcareToken: state.get('nestcareToken'),
                tempigockiToken: null,
                backupToken: state.get('backupToken'),
                nestcareBackupToken: state.get('nestcareBackupToken')
            });
        case 'CLEAR_ALL_TOKENS':
            return Map<string, any>({
                igockiToken: null,
                nestcareToken: null,
                tempigockiToken: null,
                backupToken: null,
                nestcareBackupToken: null
            });

        default:
            return state;
    }
}
