import { Map } from 'immutable';
export declare function deimmutifyBilling(state: Map<string, any>): Object;
export declare function reimmutifyBilling(plain: any): Map<string, any>;
