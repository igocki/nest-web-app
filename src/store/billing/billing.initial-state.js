import { Map } from 'immutable';
//import { IShippingRates } from './shipping-rates.types';
export var INITIAL_STATE = Map({
    cards: null,
    selectedCard: null,
    status: null,
    billingAddress: null,
    error: null
});
