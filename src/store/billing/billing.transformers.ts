import { Map, fromJS, List  } from 'immutable';
//import { IShipping, IShippingType } from './shipping.types';

export function deimmutifyBilling(state: Map<string, any>): Object {
    return state.toJS();
}

export function reimmutifyBilling(plain): Map<string, any> {
    return Map<string, any>(plain ? fromJS(plain) : {});
}
