import { INITIAL_STATE } from './billing.initial-state';
import { Map, List } from 'immutable';
export function billingReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'GET_CARDS':
            console.log('THIS WENT ONCE');
            // console.log(List<any>([action.payload.billing_info]));
            return Map({
                cards: [action.payload.billing_info],
                selectedCard: state.get('selectedCard'),
                status: state.get('status'),
                billingAddress: state.get('billingAddress'),
                error: state.get('error')
            });
        case 'FETCH_CARDS_FAILURE':
            return Map({
                cards: List([]),
                selectedCard: null,
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'CLEAR_BILLING_ERRORS':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'payment-billing',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'ADD_BILLING_ADDRESS':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: state.get('status'),
                billingAddress: Map(action.payload),
                error: state.get('error')
            });
        case 'SELECT_CARD':
            return Map({
                cards: state.get('cards'),
                selectedCard: Map(action.payload),
                status: state.get('status'),
                billingAddress: state.get('billingAddress'),
                error: state.get('error')
            });
        case 'DELETING_OTHER_CARD':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'deleting',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'DELETING_OTHER_CARD_FAILURE':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map(action.payload)
            });
        case 'ADDING_CARD':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'adding',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'ADDING_CARD_FAILURE':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map(action.payload)
            });
        case 'SAVING_BILLING_FAILURE':
            return Map({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map(action.payload)
            });
        case 'CLEAR_SIGNUP':
            return Map({
                cards: null,
                selectedCard: null,
                status: null,
                billingAddress: null,
                error: null
            });
        default:
            return state;
    }
}
