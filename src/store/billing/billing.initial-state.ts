import { Map, List } from 'immutable';
//import { IShippingRates } from './shipping-rates.types';

export const INITIAL_STATE = Map<string, any>({
    cards: null,
    selectedCard: null,
    status: null,
    billingAddress: null,
    error: null
});
