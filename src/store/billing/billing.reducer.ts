//import { ShippingRatesActions } from '../../actions/shipping-rates-actions';
//import { IShipping, IShippingType } from './shipping.types';
import { INITIAL_STATE } from './billing.initial-state';
import { Map, List } from 'immutable';


export function billingReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any>{
    switch (action.type) {
        case 'GET_CARDS':
            console.log('THIS WENT ONCE');
            // console.log(List<any>([action.payload.billing_info]));
            return Map<string, any>({
                cards: [action.payload.billing_info],
                selectedCard: state.get('selectedCard'),
                status: state.get('status'),
                billingAddress: state.get('billingAddress'),
                error: state.get('error')
            });
        case 'FETCH_CARDS_FAILURE':
            return Map<string, any>({
                cards: List<any>([]),
                selectedCard: null,
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'CLEAR_BILLING_ERRORS':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'payment-billing',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'ADD_BILLING_ADDRESS':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: state.get('status'),
                billingAddress: Map<string, any>(action.payload),
                error: state.get('error')
            });
        case 'SELECT_CARD':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: Map<string, any>(action.payload),
                status: state.get('status'),
                billingAddress: state.get('billingAddress'),
                error: state.get('error')
            });
        case 'DELETING_OTHER_CARD':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'deleting',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'DELETING_OTHER_CARD_FAILURE':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map<string, any>(action.payload)
            });
        case 'ADDING_CARD':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'adding',
                billingAddress: state.get('billingAddress'),
                error: null
            });
        case 'ADDING_CARD_FAILURE':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map<string, any>(action.payload)
            });
        case 'SAVING_BILLING_FAILURE':
            return Map<string, any>({
                cards: state.get('cards'),
                selectedCard: state.get('selectedCard'),
                status: 'submitted',
                billingAddress: state.get('billingAddress'),
                error: Map<string, any>(action.payload)
            });
        case 'CLEAR_SIGNUP':
            return Map<string, any>({
                cards: null,
                selectedCard: null,
                status: null,
                billingAddress: null,
                error: null
            });
        default:
            return state;
    }
}
