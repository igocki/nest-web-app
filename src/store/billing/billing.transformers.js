import { Map, fromJS } from 'immutable';
//import { IShipping, IShippingType } from './shipping.types';
export function deimmutifyBilling(state) {
    return state.toJS();
}
export function reimmutifyBilling(plain) {
    return Map(plain ? fromJS(plain) : {});
}
