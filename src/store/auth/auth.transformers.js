import { Map, fromJS } from 'immutable';
export function deimmutifyAuth(state) {
    return state.toJS();
}
export function reimmutifyAuth(plain) {
    return Map(plain ? fromJS(plain) : {});
}
