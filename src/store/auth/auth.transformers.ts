import { Map, fromJS  } from 'immutable';
import { IAuth, IAuthType } from './auth.types';

export function deimmutifyAuth(state: IAuthType): Object {
    return state.toJS();
}

export function reimmutifyAuth(plain): IAuthType {
    return Map<string, IAuth>(plain ? fromJS(plain) : {});
}
