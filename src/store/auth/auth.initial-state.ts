import { Map } from 'immutable';
import { IAuth } from './auth.types';

export const INITIAL_STATE = Map<string, IAuth>({
    profile: null,
    error: null,
    token: null,
    status: 'logged-out'
});
