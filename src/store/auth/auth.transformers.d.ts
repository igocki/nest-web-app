import { IAuthType } from './auth.types';
export declare function deimmutifyAuth(state: IAuthType): Object;
export declare function reimmutifyAuth(plain: any): IAuthType;
