import { AuthActions } from '../../actions/auth-actions';
import { IAuth, IAuthType } from './auth.types';
import { INITIAL_STATE } from './auth.initial-state';
import { Map } from 'immutable';


export function authReducer(state: IAuthType = INITIAL_STATE, action): IAuthType {
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            if(action.payload === '/dashboard' && state.toJS().status === 'submitted'){
                return Map<string, IAuth>({
                    profile: state.get('profile'),
                    error: state.get('error'),
                    token: state.get('token'),
                    status: 'authenticated'
                });
            }
            return state;
        case 'LOGOUT':
            return Map<string, IAuth>({
                profile: null,
                error: null,
                token: null,
                status: 'logged-out'
            });
        case AuthActions.ROUTER_STATE_CHANGE:
            return state;
        case AuthActions.LOGGING_IN:
            return Map<string, IAuth>({
                profile: state.get('profile'),
                error: state.get('error'),
                token: state.get('token'),
                status: 'pending'
            });
        case AuthActions.LOGIN_SUCCESS:
            return Map<string, IAuth>({
                profile: Map<string, any>(action.payload.data),
                error: null,
                token: action.payload.meta.token,
                status: 'submitted'
            });
        case AuthActions.AUTHENTICATED:
            return Map<string, IAuth>({
                profile: state.get('profile'),
                error: state.get('error'),
                token: state.get('token'),
                status: 'authenticated'
            });
        case AuthActions.CLEAR_ERRORS:
            return Map<string, IAuth>({
                profile: state.get('profile'),
                error: null,
                token: state.get('token'),
                status: 'logged-out'
            });
        case AuthActions.LOGIN_FAILURE:
            if(action.payload.isTrusted){
                return Map<string, IAuth>({
                    profile: state.get('profile'),
                    error: Map<string, any>({message: 'Could not connect to server. contact support'}),
                    token: state.get('token'),
                    status: 'submitted'
                });
            } else {
                return Map<string, IAuth>({
                    profile: state.get('profile'),
                    error: Map<string, any>(action.payload),
                    token: state.get('token'),
                    status: 'submitted'
                });
            }

        default:
            return state;
    }
}
