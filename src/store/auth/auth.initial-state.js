import { Map } from 'immutable';
export var INITIAL_STATE = Map({
    profile: null,
    error: null,
    token: null,
    status: 'logged-out'
});
