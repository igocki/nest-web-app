import { Map } from 'immutable';

export interface IAuth {
    profile?: any;
    error?: any;
    token?: string;
    status: string;
};

export type IAuthType = Map<string, IAuth>;

