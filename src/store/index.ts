import persistState from 'redux-localstorage';
import createLogger from 'redux-logger';
import * as auth from './auth';
import * as signup from './nc-sign-up';
import * as shipping from './shipping';
import * as billing from './billing';
import * as secure from './secure';
import * as orderSummary from './order-summary';
import * as shippingRates from './shipping-rates';
import * as loading from './loading';
import * as appStatus from './app-status';
import * as userView from './user-view';
import * as supportNetwork from './support-network';
import * as Redux from 'redux';
const { combineReducers } = Redux;
import { routerReducer } from 'ng2-redux-router';
import { IAuth } from './auth';
import { IShipping } from './shipping';
import { ISignUp } from './nc-sign-up';
import { ILoading } from './loading';
import { IAppStatus } from './app-status';
import { Map} from 'immutable';

export {
    IAuth,
    IAppStatus,
    ISignUp,
    IShipping,
    ILoading
}

export const enhancers = [
    persistState(
        '', {
            key: 'trendy-brunch',
            serialize: s => JSON.stringify(deimmutify(s)),
            deserialize: s => reimmutify(JSON.parse(s)),
        })
];

if (window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
}

export interface RootState {
    auth? : auth.IAuthType;
    signup?: signup.ISignUpType;
    shipping?: shipping.IShippingType;
    loading?: loading.ILoadingType;
    appStatus?: appStatus.IAppStatusType;
    router?: any;
    shippingRates?: Map<string,any>;
    billing?: Map<string, any>;
    orderSummary?: Map<string, any>;
    secure?: Map<string, any>;
    userView?: Map<string, any>;
    supportNetwork?: Map<string, any>;
}


const rootReducer = combineReducers<RootState>({
    auth: auth.authReducer,
    signup: signup.ncSignUpReducer,
    shipping: shipping.shippingReducer,
    loading: loading.loadingReducer,
    appStatus: appStatus.appStatusReducer,
    router: routerReducer,
    shippingRates: shippingRates.shippingRatesReducer,
    billing: billing.billingReducer,
    orderSummary: orderSummary.orderSummaryReducer,
    secure: secure.secureReducer,
    userView: userView.userViewReducer,
    supportNetwork: supportNetwork.supportNetworkReducer
});

export function deimmutify(state: RootState): Object {
    return {
        auth: auth.deimmutifyAuth(state.auth),
        signup: signup.deimmutifySignUp(state.signup),
        shipping: shipping.deimmutifyShipping(state.shipping),
        loading: loading.deimmutifyLoading(state.loading),
        appStatus: appStatus.deimmutifyAppStatus(state.appStatus),
        router: state.router,
        shippingRates: shippingRates.deimmutifyShippingRates(state.shippingRates),
        billing: billing.deimmutifyBilling(state.billing),
        orderSummary: orderSummary.deimmutifyOrderSummary(state.orderSummary),
        secure: secure.deimmutifySecure(state.secure),
        userView: userView.deimmutifyUserView(state.userView),
        supportNetwork: supportNetwork.deimmutifySupportNetwork(state.supportNetwork)
    };
}

export function reimmutify(plain): RootState {
    return plain ? {
        auth: auth.reimmutifyAuth(plain.auth),
        signup: signup.reimmutifySignUp(plain.signup),
        shipping: shipping.reimmutifyShipping(plain.shipping),
        loading: loading.reimmutifyLoading(plain.loading),
        appStatus: appStatus.reimmutifyAppStatus(plain.appStatus),
        router: plain.router,
        shippingRates: shippingRates.reimmutifyShippingRates(plain.shippingRates),
        billing: billing.reimmutifyBilling(plain.billing),
        orderSummary: orderSummary.reimmutifyOrderSummary(plain.orderSummary),
        secure: secure.reimmutifySecure(plain.secure),
        userView: userView.reimmutifyUserView(plain.userView),
        supportNetwork: supportNetwork.reimmutifySupportNetwork(plain.supportNetwork)
    } : {};
}

export const middleware = [
    createLogger({
        level: 'info',
        collapsed: true,
        stateTransformer: deimmutify
    })
];




export default rootReducer;
