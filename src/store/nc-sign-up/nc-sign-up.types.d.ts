import { Map } from 'immutable';
export interface ISignUp {
    resp?: Map<string, any>;
    error?: any;
    status: string;
}
export declare type ISignUpType = Map<string, ISignUp>;
