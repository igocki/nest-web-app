import { Map, fromJS } from 'immutable';
export function deimmutifySignUp(state) {
    return state.toJS();
}
export function reimmutifySignUp(plain) {
    return Map(plain ? fromJS(plain) : {});
}
