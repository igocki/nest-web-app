import { Map } from 'immutable';
export var INITIAL_STATE = Map({
    resp: null,
    error: null,
    status: 'empty'
});
