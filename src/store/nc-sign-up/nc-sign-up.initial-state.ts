import { Map } from 'immutable';
import { ISignUp } from './nc-sign-up.types';

export const INITIAL_STATE = Map<string, ISignUp>({
    resp: null,
    error: null,
    status: 'empty'
});
