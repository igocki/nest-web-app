import { Map } from 'immutable';

export interface ISignUp {
    resp?: Map<string, any>;
    error?: any;
    status: string;
};

export type ISignUpType = Map<string, ISignUp>;

