import { ISignUpType } from './nc-sign-up.types';
export declare function deimmutifySignUp(state: ISignUpType): Object;
export declare function reimmutifySignUp(plain: any): ISignUpType;
