import { Map, fromJS  } from 'immutable';
import { ISignUp, ISignUpType } from './nc-sign-up.types';

export function deimmutifySignUp(state: ISignUpType): Object {
    return state.toJS();
}

export function reimmutifySignUp(plain): ISignUpType {
    return Map<string, ISignUp>(plain ? fromJS(plain) : {});
}
