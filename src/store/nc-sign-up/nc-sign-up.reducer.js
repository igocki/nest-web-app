import { SignUpActions } from '../../actions/signup-actions';
import { INITIAL_STATE } from './nc-sign-up.initial-state';
import { Map } from 'immutable';
export function ncSignUpReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case SignUpActions.SIGNING_UP:
            return Map({
                resp: state.get('resp'),
                error: state.get('error'),
                status: 'pending'
            });
        case SignUpActions.SIGNUP_SUCCESS:
            console.log('in reducer');
            console.log(action.payload);
            return Map({
                resp: Map(action.payload),
                error: null,
                status: 'submitted'
            });
        case SignUpActions.CLEAR_ERRORS:
            return Map({
                resp: state.get('resp'),
                error: null,
                status: 'logged-out'
            });
        case SignUpActions.SIGNUP_FAILURE:
            return Map({
                resp: state.get('resp'),
                error: Map(action.payload),
                status: 'submitted'
            });
        case 'CLEAR_SIGNUP':
            return Map({
                resp: null,
                error: null,
                status: 'empty'
            });
        default:
            return state;
    }
}
