import { SignUpActions } from '../../actions/signup-actions';
import { ISignUp, ISignUpType } from './nc-sign-up.types';
import { INITIAL_STATE } from './nc-sign-up.initial-state';
import { Map } from 'immutable';


export function ncSignUpReducer(state: ISignUpType = INITIAL_STATE, action): ISignUpType {
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case SignUpActions.SIGNING_UP:
            return Map<string, ISignUp>({
                resp: state.get('resp'),
                error: state.get('error'),
                status: 'pending'
            });
        case SignUpActions.SIGNUP_SUCCESS:
            console.log('in reducer');
            console.log(action.payload);
            return Map<string, ISignUp>({
                resp: Map<string, any>(action.payload),
                error: null,
                status: 'submitted'
            });
        case SignUpActions.CLEAR_ERRORS:
            return Map<string, ISignUp>({
                resp: state.get('resp'),
                error: null,
                status: 'logged-out'
            });
        case SignUpActions.SIGNUP_FAILURE:
            return Map<string, ISignUp>({
                resp: state.get('resp'),
                error: Map<string, any>(action.payload),
                status: 'submitted'
            });
        case 'CLEAR_SIGNUP':
            return Map<string, any>({
                resp: null,
                error: null,
                status: 'empty'
            });
        default:
            return state;
    }
}
