
import { INITIAL_STATE } from './user-view.initial-state';
import { Map, List } from 'immutable';


export function userViewReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any>{
    switch (action.type) {
        case 'STORE_USER_INFO_VIEW1':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: action.payload.email,
                fullname: action.payload.fullname,
                first_name: action.payload.first_name,
                last_name: action.payload.last_name,
                userStatus: state.get('userStatus'),
                accountType: action.payload.accountType,
                userBirth: action.payload.userBirth,
                userGender: action.payload.userGender,
                userPhone: action.payload.userPhone,
                nc_id: action.payload.nc_id
            });

        case 'STORE_USER_INFO_VIEW2':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: action.payload.userStatus,
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });

        case 'STORE_USER_STATUS':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: action.payload,
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });

        case 'STORE_PLAN_LOGIN':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: Map<string, any>(action.payload),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'STORING_BILLING_INFO':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: Map<string, any>({
                    last_four: action.payload.billing_info.last_four,
                    first_name: action.payload.billing_info.first_name,
                    last_name: action.payload.billing_info.last_name,
                    address1: action.payload.billing_info.address1,
                    address2: action.payload.billing_info.address2,
                    city: action.payload.billing_info.city,
                    state: action.payload.billing_info.state,
                    zipcode: action.payload.billing_info.zip
                }),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'CLEAR_BILLING_INFO':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: null,
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'CLEAR_SHIPPING_INFO':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: null,
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'STORING_ORDERS':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: action.payload,
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'STORE_ACCOUNT_STATUS':
            return Map<string, any>({
                status: action.payload,
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'USE_EXPLORER_VIEW':
            return Map<string, any>({
                status: 'Inactive',
                type: 'Explorer',
                plan: {},
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: [],
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'STORE_SHIPPING_ADDRESS_USER_VIEW':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: state.get('plan'),
                billingAddress: state.get('billingAddress'),
                shippingAddress: Map<string, any>(action.payload),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        case 'CLEAR_USER_VIEW':
            return Map<string, any>({
                status: null,
                type: null,
                plan: null,
                billingAddress: null,
                shippingAddress: null,
                orders: null,
                email: null,
                fullname: null,
                first_name: null,
                last_name: null,
                userStatus: null,
                userBirth: null,
                userGender: null,
                userPhone: null,
                nc_id: null
            });
        case 'CLEAR_PLAN':
            return Map<string, any>({
                status: state.get('status'),
                type: state.get('type'),
                plan: null,
                billingAddress: state.get('billingAddress'),
                shippingAddress: state.get('shippingAddress'),
                orders: state.get('orders'),
                email: state.get('email'),
                fullname: state.get('fullname'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                userStatus: state.get('userStatus'),
                accountType: state.get('accountType'),
                userBirth: state.get('userBirth'),
                userGender: state.get('userGender'),
                userPhone: state.get('userPhone'),
                nc_id: state.get('nc_id')
            });
        default:
            return state;
    }
}
