import { Map } from 'immutable';
export var INITIAL_STATE = Map({
    status: null,
    type: null,
    plan: null,
    billingAddress: null,
    shippingAddress: null,
    orders: null,
    email: null,
    fullname: null,
    first_name: null,
    last_name: null,
    userStatus: null,
    userBirth: null,
    userGender: null,
    userPhone: null,
    nc_id: null
});
