import { Map, fromJS, List  } from 'immutable';

export function deimmutifyUserView(state: Map<string, any>): Object {
    return state.toJS();
}

export function reimmutifyUserView(plain): Map<string, any> {
    return Map<string, any>(plain ? fromJS(plain) : {});
}
