import { Map } from 'immutable';
export declare function deimmutifyUserView(state: Map<string, any>): Object;
export declare function reimmutifyUserView(plain: any): Map<string, any>;
