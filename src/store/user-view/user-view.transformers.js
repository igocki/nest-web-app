import { Map, fromJS } from 'immutable';
export function deimmutifyUserView(state) {
    return state.toJS();
}
export function reimmutifyUserView(plain) {
    return Map(plain ? fromJS(plain) : {});
}
