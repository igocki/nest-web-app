import { Map, fromJS  } from 'immutable';
import { IRouterType, IRouter } from './router.types';

export function deimmutifyRouter(state: IRouterType): Object {
    return state.toJS();
}

export function reimmutifyRouter(plain): IRouterType {
    return Map<string, IRouter>(plain ? fromJS(plain) : {});
}
