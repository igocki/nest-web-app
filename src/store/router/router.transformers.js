import { Map, fromJS } from 'immutable';
export function deimmutifyRouter(state) {
    return state.toJS();
}
export function reimmutifyRouter(plain) {
    return Map(plain ? fromJS(plain) : {});
}
