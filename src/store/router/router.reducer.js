import { Map } from 'immutable';
import { INITIAL_STATE } from './router.initial-state';
export function routerReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return Map({
                route: action.payload
            });
        default:
            return state;
    }
}
