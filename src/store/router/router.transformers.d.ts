import { IRouterType } from './router.types';
export declare function deimmutifyRouter(state: IRouterType): Object;
export declare function reimmutifyRouter(plain: any): IRouterType;
