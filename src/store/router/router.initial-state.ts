import { Map } from 'immutable';
import { IRouter } from './router.types';

export const INITIAL_STATE = Map<string, IRouter>({
    route: ''
});
