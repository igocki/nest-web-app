import { Map } from 'immutable';
import { IRouterType, IRouter } from './router.types';
import { INITIAL_STATE } from './router.initial-state';


export function routerReducer(state: IRouterType = INITIAL_STATE, action): IRouterType {
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return Map<string, IRouter>({
                route: action.payload
            });
        default:
            return state;
    }
}
