import { Map } from 'immutable';

export interface IRouter {
    route?: any;
};

export type IRouterType = Map<string, IRouter>;
