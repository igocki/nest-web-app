import { Map } from 'immutable';
export interface IRouter {
    route?: any;
}
export declare type IRouterType = Map<string, IRouter>;
