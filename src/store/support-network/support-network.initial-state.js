import { Map } from 'immutable';
export var INITIAL_STATE = Map({
    id: null,
    email: null,
    first_name: null,
    last_name: null,
    phone: null,
    last4OfChange: null,
    changed: null,
});
