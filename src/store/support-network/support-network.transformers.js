import { Map, fromJS } from 'immutable';
export function deimmutifySupportNetwork(state) {
    return state.toJS();
}
export function reimmutifySupportNetwork(plain) {
    return Map(plain ? fromJS(plain) : {});
}
