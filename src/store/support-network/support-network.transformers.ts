import { Map, fromJS, List  } from 'immutable';

export function deimmutifySupportNetwork(state: Map<string, any>): Object {
    return state.toJS();
}

export function reimmutifySupportNetwork(plain): Map<string, any> {
    return Map<string, any>(plain ? fromJS(plain) : {});
}
