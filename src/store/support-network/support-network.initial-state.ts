import { Map, List } from 'immutable';

export const INITIAL_STATE = Map<string, any>({
    id: null,
    email:  null,
    first_name: null,
    last_name: null,
    phone: null,
    last4OfChange: null,
    changed: null,
});
