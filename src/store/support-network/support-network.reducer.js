import { INITIAL_STATE } from './support-network.initial-state';
import { Map } from 'immutable';
export function supportNetworkReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'CLEAR_SUPPORT_NETWORK':
            return Map({
                id: null,
                email: null,
                first_name: null,
                last_name: null,
                phone: null,
                last4OfChange: null,
                changed: null,
            });
        case 'STORE_SUPPORT_NETWORK_USER':
            return Map({
                id: action.payload.id,
                email: action.payload.email,
                first_name: action.payload.first_name,
                last_name: action.payload.last_name,
                phone: action.payload.phone,
                last4OfChange: state.get('last4OfChange'),
                changed: state.get('changed'),
            });
        case 'SUPPORT_NETWORK_BILLING_SUCCESS':
            return Map({
                id: state.get('id'),
                email: state.get('email'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                phone: state.get('phone'),
                last4OfChange: action.payload.last4,
                changed: action.payload.changed,
            });
        default:
            return state;
    }
}
