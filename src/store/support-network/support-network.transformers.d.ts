import { Map } from 'immutable';
export declare function deimmutifySupportNetwork(state: Map<string, any>): Object;
export declare function reimmutifySupportNetwork(plain: any): Map<string, any>;
