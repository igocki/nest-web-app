
import { INITIAL_STATE } from './support-network.initial-state';
import { Map, List } from 'immutable';


export function supportNetworkReducer(state: Map<string, any> = INITIAL_STATE, action): Map<string, any>{
    switch (action.type) {
        case 'CLEAR_SUPPORT_NETWORK':
            return Map<string, any>({
                id: null,
                email:  null,
                first_name: null,
                last_name: null,
                phone: null,
                last4OfChange: null,
                changed: null,
            });
        case 'STORE_SUPPORT_NETWORK_USER':
            return Map<string, any>({
                id: action.payload.id,
                email:  action.payload.email,
                first_name: action.payload.first_name,
                last_name: action.payload.last_name,
                phone: action.payload.phone,
                last4OfChange: state.get('last4OfChange'),
                changed: state.get('changed'),
            });
        case 'SUPPORT_NETWORK_BILLING_SUCCESS':
            return Map<string, any>({
                id: state.get('id'),
                email:  state.get('email'),
                first_name: state.get('first_name'),
                last_name: state.get('last_name'),
                phone: state.get('phone'),
                last4OfChange: action.payload.last4,
                changed: action.payload.changed,
            });
        default:
            return state;
    }
}
