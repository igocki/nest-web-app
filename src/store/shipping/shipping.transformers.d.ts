import { IShippingType } from './shipping.types';
export declare function deimmutifyShipping(state: IShippingType): Object;
export declare function reimmutifyShipping(plain: any): IShippingType;
