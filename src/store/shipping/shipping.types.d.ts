import { Map } from 'immutable';
export interface IShipping {
    resp?: any;
    error?: any;
    status: string;
    shippingOptions?: Array<any>;
}
export declare type IShippingType = Map<string, IShipping>;
