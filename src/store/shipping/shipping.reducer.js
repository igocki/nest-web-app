import { ShippingActions } from '../../actions/shipping-actions';
import { INITIAL_STATE } from './shipping.initial-state';
import { Map, List } from 'immutable';
export function shippingReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case 'ng2-redux-router::UPDATE_LOCATION':
            return state;
        case 'VERIFY_AND_SUBMIT_NEW_SHIPPING':
            return Map({
                resp: state.get('resp'),
                error: null,
                status: 'submitted',
                shippingOptions: state.get('shippingOptions'),
                addressVerfied: state.get('addressVerfied')
            });
        case 'VERIFY_ADDRESS_FAILURE':
            return Map({
                resp: state.get('resp'),
                error: Map(action.payload),
                status: 'try-again',
                shippingOptions: state.get('shippingOptions'),
                addressVerfied: null
            });
        case 'VERIFY_ADDRESS_SUCCESS':
            return Map({
                resp: state.get('resp'),
                error: null,
                status: 'submitted',
                shippingOptions: state.get('shippingOptions'),
                addressVerfied: true
            });
        case ShippingActions.SHIPPING_UPDATE_SUCCESS:
            return Map({
                resp: Map({
                    name: action.payload.name,
                    addressLine1: action.payload.addressLine1,
                    addressLine2: action.payload.addressLine2,
                    city: action.payload.city,
                    state: action.payload.state,
                    zipCode: action.payload.zipCode
                }),
                error: null,
                status: 'submitted',
                shippingOptions: null,
                addressVerfied: state.get('addressVerfied')
            });
        case ShippingActions.GET_SHIPPING_RATES_SUCCESS:
            return Map({
                resp: state.get('resp'),
                error: null,
                status: 'submitted',
                shippingOptions: List(action.payload),
                addressVerfied: state.get('addressVerfied')
            });
        case ShippingActions.GET_SHIPPING_RATES_FAILURE:
            return Map({
                resp: state.get('resp'),
                error: Map(action.payload.err),
                status: 'submitted',
                shippingOptions: null,
                addressVerfied: state.get('addressVerfied')
            });
        case ShippingActions.CLEAR_ERRORS:
            return Map({
                resp: state.get('resp'),
                error: null,
                status: 'no-shipping',
                shippingOptions: null,
                addressVerfied: state.get('addressVerfied')
            });
        case ShippingActions.CLEAR_SHIPPING:
            return Map({
                resp: null,
                error: null,
                status: 'no-shipping',
                shippingOptions: null,
                addressVerfied: state.get('addressVerfied')
            });
        case ShippingActions.SHIPPING_UPDATE_FAILURE:
            return Map({
                resp: state.get('resp'),
                error: Map(action.payload.err),
                status: 'submitted',
                shippingOptions: null,
                addressVerfied: state.get('addressVerfied')
            });
        case 'CLEAR_SIGNUP':
            return Map({
                resp: null,
                error: null,
                status: 'empty',
                shippingOptions: null,
                addressVerfied: null
            });
        default:
            return state;
    }
}
