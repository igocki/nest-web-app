import { Map, fromJS  } from 'immutable';
import { IShipping, IShippingType } from './shipping.types';

export function deimmutifyShipping(state: IShippingType): Object {
    return state.toJS();
}

export function reimmutifyShipping(plain): IShippingType {
    return Map<string, IShipping>(plain ? fromJS(plain) : {});
}
