import { Map, fromJS } from 'immutable';
export function deimmutifyShipping(state) {
    return state.toJS();
}
export function reimmutifyShipping(plain) {
    return Map(plain ? fromJS(plain) : {});
}
