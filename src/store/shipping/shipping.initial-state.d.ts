import { Map } from 'immutable';
import { IShipping } from './shipping.types';
export declare const INITIAL_STATE: Map<string, IShipping>;
