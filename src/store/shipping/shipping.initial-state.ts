import { Map } from 'immutable';
import { IShipping } from './shipping.types';

export const INITIAL_STATE = Map<string, IShipping>({
    resp: null,
    error: null,
    status: 'empty',
    shippingOptions: null,
    addressVerfied: null
});
