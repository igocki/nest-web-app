import { Map } from 'immutable';

export interface IShipping {
    resp?: any;
    error?: any;
    status: string;
    shippingOptions?: Array<any>;
};

// export interface IShippingResponse {
//     name?: string;
//     addressLine1?: string;
//     addressLine2?: string;
//     city?: string;
//     state?: string;
//     zipCode?: string;
// }

export type IShippingType = Map<string, IShipping>;

