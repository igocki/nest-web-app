var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../services/nestcareService';
import { UserService } from '../services/userService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export var AuthActions = (function () {
    function AuthActions(ngRedux, _nc, loadingCtrl, _userService, _registerService, router) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.loadingCtrl = loadingCtrl;
        this._userService = _userService;
        this._registerService = _registerService;
        this.router = router;
    }
    AuthActions.prototype.clearErrors = function () {
        this.ngRedux.dispatch({
            type: AuthActions.CLEAR_ERRORS
        });
    };
    AuthActions.prototype.authenticationComplete = function () {
        this.ngRedux.dispatch({
            type: AuthActions.AUTHENTICATED
        });
    };
    AuthActions.prototype.adminLogin = function (email, password) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._nc.adminLogin(email, password).subscribe(function (resp) {
            _this.router.navigate(['/admin']);
            _this.ngRedux.dispatch({
                type: 'CHANGE_TO_LOGGED_IN'
            });
        }, function (error) {
            console.log(error);
            console.log('hit this error');
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.ngRedux.dispatch({
                type: AuthActions.LOGIN_FAILURE,
                payload: error
            });
        });
    };
    AuthActions.prototype.login = function (email, password, sso) {
        var _this = this;
        this.ngRedux.dispatch({
            type: AuthActions.LOGGING_IN
        });
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        // this.mockLoginService.login(customer, subscriptionID).then(resp => {
        // this._pw.GetCustomers().then(resp => {
        this._registerService.registerAndLogin(email, password)
            .subscribe(function (resp) {
            if (resp.data.first_name) {
                _this.ngRedux.dispatch({
                    type: AuthActions.LOGIN_SUCCESS,
                    payload: resp
                });
                _this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.data.address.home
                });
                _this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                _this.ngRedux.dispatch({
                    type: 'CHANGE_WORKFLOW',
                    payload: null
                });
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                _this._userService.login();
                if (sso === true) {
                    _this.router.navigate(['/sso-success'], { preserveQueryParams: true });
                }
                else {
                    console.log('end here 3:');
                    _this.router.navigate(['/account']);
                }
            }
            else {
                _this.ngRedux.dispatch({
                    type: 'ADD_CUSTOMER_ID',
                    payload: resp.data.id
                });
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                _this.router.navigate(['/twofactor']);
            }
        }, function (error) {
            console.log(error);
            console.log('hit this error');
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            _this.ngRedux.dispatch({
                type: AuthActions.LOGIN_FAILURE,
                payload: error
            });
        });
        // this._nc.login(customer, subscriptionID).then(resp => {
        //     this.ngRedux.dispatch({
        //         type: AuthActions.LOGIN_SUCCESS,
        //         payload: resp
        //     });
        //     this.ngRedux.dispatch({
        //         type: LoadingActions.HIDE_LOADING
        //     });
        // }).catch(err => {
        //     console.log('inside this catch....');
        //     console.log(err);
        //     this.ngRedux.dispatch({
        //         type: AuthActions.LOGIN_FAILURE,
        //         payload: {err: err}
        //     });
        //     this.ngRedux.dispatch({
        //         type: LoadingActions.HIDE_LOADING
        //     });
        // });
    };
    AuthActions.ROUTER_STATE_CHANGE = 'ROUTER_STATE_CHANGE';
    AuthActions.CLEAR_ERRORS = 'CLEAR_ERRORS';
    AuthActions.AUTHENTICATED = 'AUTHENTICATED';
    AuthActions.LOGGING_IN = 'LOGGING_IN';
    AuthActions.LOGIN_SUCCESS = 'LOGIN_SUCCESS';
    AuthActions.LOGIN_FAILURE = 'LOGIN_FAILURE';
    AuthActions = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, LoadingController, UserService, RegisterService, Router])
    ], AuthActions);
    return AuthActions;
}());
