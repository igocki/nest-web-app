var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export var LoadingActions = (function () {
    function LoadingActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    // increment(): void {
    //     this.ngRedux.dispatch({ type: CounterActions.INCREMENT_COUNTER });
    // }
    //
    // decrement(): void {
    //     this.ngRedux.dispatch({ type: CounterActions.DECREMENT_COUNTER });
    // }
    // incrementIfOdd(): void {
    //     const { counter } = this.ngRedux.getState();
    //     if (counter % 2 !== 0) {
    //         this.increment();
    //     }
    // }
    //
    // incrementAsync(delay: number = 1000): void {
    //     setTimeout(this.increment.bind(this), delay);
    // }
    LoadingActions.prototype.showLoader = function () {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
    };
    LoadingActions.prototype.hideLoader = function () {
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
    };
    LoadingActions.SHOW_LOADING = 'SHOW_LOADING';
    LoadingActions.HIDE_LOADING = 'HIDE_LOADING';
    LoadingActions = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux])
    ], LoadingActions);
    return LoadingActions;
}());
