import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { ShippoService } from '../services/shippoService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
import { AppStatusActions } from './app-status-actions'
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
@Injectable()
export class ShippingActions {
    constructor (
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _shippo: ShippoService,
        public loadingCtrl: LoadingController,
        private _appStatusActions: AppStatusActions) {}

    static CLEAR_ERRORS: string = 'CLEAR_ERRORS';
    static CLEAR_SHIPPING: string = 'CLEAR_SHIPPING';
    static GET_SHIPPING_RATES_SUCCESS: string = 'GET_SHIPPING_RATES_SUCCESS';
    static GET_SHIPPING_RATES_FAILURE: string = 'GET_SHIPPING_RATES_FAILURE';
    static SHIPPING_UPDATE_SUCCESS: string = 'SHIPPING_UPDATE_SUCCESS';
    static SHIPPING_UPDATE_FAILURE: string = 'SHIPPING_UPDATE_FAILURE';


    clearErrors(){
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_ERRORS
        });
    }

    getShippingRates(shippingTo, user){
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        console.log('HIT SHIPPING RATES');
        console.log(shippingTo);
        console.log(user);

        this._shippo.createShipment(shippingTo, user)
            .subscribe(resp => {

                    console.log("RESPONDED");
                    console.log(resp);
                    this.ngRedux.dispatch({
                        type: ShippingActions.GET_SHIPPING_RATES_SUCCESS,
                        payload: resp.rates_list
                    });

                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                },
                error => {

                    this.ngRedux.dispatch({
                        type: ShippingActions.GET_SHIPPING_RATES_FAILURE,
                        payload: {err: error}
                    });

                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                });
    }

    changeShipping(){
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_SHIPPING
        });
    }

    

    fakeUpdateShippingAddressSuccess(params): void {

        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        let shippingData = {
            "name": params.name,
            "addressLine1": params.addressLine1,
            "addressLine2": params.addressLine2,
            "city": params.city,
            "state": params.state,
            "zipCode": params.zipCode
        };

        setTimeout(() => {
            this.ngRedux.dispatch({
                type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                payload: shippingData
            });

            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, 3000);


    }

    // updateShippingAddress(params): void {
    //
    // }
}
