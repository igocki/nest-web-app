var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../services/nestcareService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { Observable } from "rxjs";
import { LoadingActions } from './loading-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export var AppStatusActions = (function () {
    function AppStatusActions(ngRedux, _nc, loadingCtrl, router) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
    }
    // static SIGNUP_SUCCESS: string = 'SIGNUP_SUCCESS';
    // static SIGNUP_FAILURE: string = 'SIGNUP_FAILURE';
    AppStatusActions.prototype.storeSignUpInfo = function (signupObj) {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SIGNUP_INFO,
            payload: signupObj
        });
    };
    AppStatusActions.prototype.storeOtherSignUpInfo = function (signupObj) {
        this.ngRedux.dispatch({
            type: 'STORE_OTHER_SIGNUP_INFO',
            payload: signupObj
        });
    };
    AppStatusActions.prototype.storeShippingAddress = function (shippingObj) {
        console.log('STORE SHIPPING');
        console.log(shippingObj);
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SHIPPING_ADDRESS,
            payload: shippingObj
        });
        this.router.navigate(['/shipping-details']);
    };
    AppStatusActions.prototype.storeSelectPlan = function (planObj) {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SELECT_PLAN,
            payload: planObj
        });
        this.router.navigate(['/select-devices']);
    };
    AppStatusActions.prototype.storeSelectHouseholdPlan = function (planObj) {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SELECT_PLAN,
            payload: planObj
        });
        this.router.navigate(['/select-devices-household']);
    };
    AppStatusActions.prototype.storeSelectDevices = function (deviceList) {
        if (deviceList.length > 0) {
            this.ngRedux.dispatch({
                type: AppStatusActions.STORE_SELECT_DEVICES,
                payload: deviceList
            });
            this.router.navigate(['/shipping']);
        }
        else {
            this.router.navigate(['/payment']);
        }
    };
    AppStatusActions.prototype.tokenLoginWithSubscription = function (token, enroll) {
        var _this = this;
        if (enroll === void 0) { enroll = false; }
        return this._nc.GetCustomer().map(function (custGet) {
            if (custGet) {
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: custGet.token
                });
                if (custGet.address.home) {
                    _this.ngRedux.dispatch({
                        type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                        payload: custGet.address.home
                    });
                }
                var cust = {
                    first_name: custGet.first_name,
                    last_name: custGet.last_name,
                    fullname: custGet.first_name + ' ' + custGet.last_name,
                    email: custGet.email,
                    accountType: custGet.account_type.text,
                    userBirth: custGet.date_of_birth,
                    userGender: custGet.gender,
                    userPhone: custGet.mobile,
                    nc_id: custGet.id
                };
                _this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: cust
                });
                _this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                if (!enroll) {
                    _this.ngRedux.dispatch({
                        type: 'CHANGE_TO_ENROLL_PLAN'
                    });
                }
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            }
        }).catch(function (error) {
            console.log('could not get Customer');
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    };
    AppStatusActions.prototype.tokenLogin = function (token, enroll) {
        var _this = this;
        if (enroll === void 0) { enroll = false; }
        this.ngRedux.dispatch({
            type: 'ADD_NC_TOKEN',
            payload: token
        });
        this._nc.GetCustomer().subscribe(function (custGet) {
            if (custGet) {
                _this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: custGet.token
                });
                if (custGet.address.home) {
                    _this.ngRedux.dispatch({
                        type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                        payload: custGet.address.home
                    });
                }
                var cust = {
                    first_name: custGet.first_name,
                    last_name: custGet.last_name,
                    fullname: custGet.first_name + ' ' + custGet.last_name,
                    email: custGet.email,
                    accountType: custGet.account_type.text,
                    userBirth: custGet.date_of_birth,
                    userGender: custGet.gender,
                    userPhone: custGet.mobile,
                    nc_id: custGet.id
                };
                _this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: cust
                });
                _this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                if (!enroll) {
                    _this.ngRedux.dispatch({
                        type: 'CHANGE_TO_ENROLL_PLAN'
                    });
                }
                _this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            }
        }, function (error) {
            console.log('could not get Customer');
            console.log(error);
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    AppStatusActions.prototype.storeFulfillmentOrder = function (fulfillObj) {
        this.ngRedux.dispatch({
            type: 'STORE_ORDER_FULFILLMENT',
            payload: fulfillObj
        });
        this.router.navigate(['/order-complete']);
    };
    AppStatusActions.prototype.clearSignupProcess = function () {
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
    };
    AppStatusActions.prototype.clearTokens = function () {
        this.ngRedux.dispatch({
            type: 'CLEAR_ALL_TOKENS'
        });
        this.ngRedux.dispatch({
            type: 'CHANGE_TO_NOT_REGISTERED'
        });
        this.ngRedux.dispatch({
            type: 'LOGOUT'
        });
    };
    AppStatusActions.prototype.logoff = function () {
        var _this = this;
        var app_auth = this.ngRedux.select(function (state) { return state.auth; });
        var userView = this.ngRedux.select(function (state) { return state.userView; });
        var userID;
        userView.subscribe(function (data) {
            if (data) {
                userID = data.get('nc_id');
            }
        });
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var sub = app_auth.subscribe(function (data) {
            if (data) {
                var prof = data.get('profile');
                console.log('asdasdsadsaddd');
                console.log(prof);
                var uId = (data.get('profile')) ? prof.get('id') : undefined;
                if (prof === undefined || prof === null) {
                    uId = userID;
                }
                console.log(uId);
                _this._nc.ssoLogout(uId).subscribe(function (data) {
                    _this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });
                    _this.ngRedux.dispatch({
                        type: 'CLEAR_ALL_TOKENS'
                    });
                    _this.ngRedux.dispatch({
                        type: 'CHANGE_TO_NOT_REGISTERED'
                    });
                    _this.ngRedux.dispatch({
                        type: 'LOGOUT'
                    });
                    _this.ngRedux.dispatch({
                        type: 'CLEAR_USER_VIEW'
                    });
                    _this.router.navigate(['/login']);
                });
            }
        });
        sub.unsubscribe();
    };
    // static CLEAR_ERRORS: string = 'CLEAR_ERRORS';
    AppStatusActions.STORE_SIGNUP_INFO = 'STORE_SIGNUP_INFO';
    AppStatusActions.STORE_SHIPPING_ADDRESS = 'STORE_SHIPPING_ADDRESS';
    AppStatusActions.STORE_SELECT_PLAN = 'STORE_SELECT_PLAN';
    AppStatusActions.STORE_SELECT_DEVICES = 'STORE_SELECT_DEVICES';
    AppStatusActions = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, LoadingController, Router])
    ], AppStatusActions);
    return AppStatusActions;
}());
