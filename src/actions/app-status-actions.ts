import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import {Observable} from "rxjs";
import { Map, List } from 'immutable';
import { LoadingActions } from './loading-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
@Injectable()
export class AppStatusActions {
    constructor (
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        public loadingCtrl: LoadingController,
        private router: Router) {}

    // static CLEAR_ERRORS: string = 'CLEAR_ERRORS';
    static STORE_SIGNUP_INFO: string = 'STORE_SIGNUP_INFO';
    static STORE_SHIPPING_ADDRESS: string = 'STORE_SHIPPING_ADDRESS';
    static STORE_SELECT_PLAN: string = 'STORE_SELECT_PLAN';
    static STORE_SELECT_DEVICES: string = 'STORE_SELECT_DEVICES';
    // static SIGNUP_SUCCESS: string = 'SIGNUP_SUCCESS';
    // static SIGNUP_FAILURE: string = 'SIGNUP_FAILURE';
    storeSignUpInfo(signupObj): void {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SIGNUP_INFO,
            payload: signupObj
        })
    }



    storeOtherSignUpInfo(signupObj): void {
        this.ngRedux.dispatch({
            type: 'STORE_OTHER_SIGNUP_INFO',
            payload: signupObj
        })
    }

    storeShippingAddress(shippingObj): void {
        console.log('STORE SHIPPING');
        console.log(shippingObj);

        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SHIPPING_ADDRESS,
            payload: shippingObj
        });
        this.router.navigate(['/shipping-details'])
    }

    storeSelectPlan(planObj): void {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SELECT_PLAN,
            payload: planObj
        });
        this.router.navigate(['/select-devices'])

    }

    storeSelectHouseholdPlan(planObj): void {
        this.ngRedux.dispatch({
            type: AppStatusActions.STORE_SELECT_PLAN,
            payload: planObj
        });
        this.router.navigate(['/select-devices-household'])

    }

    storeSelectDevices(deviceList): void {
        if(deviceList.length > 0){
            this.ngRedux.dispatch({
                type: AppStatusActions.STORE_SELECT_DEVICES,
                payload: deviceList
            });
            this.router.navigate(['/shipping'])
        } else {

            this.router.navigate(['/payment'])
        }

    }

    tokenLoginWithSubscription(token, enroll = false){
        return this._nc.GetCustomer().map(custGet => {
            if(custGet){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: custGet.token
                });
                if(custGet.address.home){
                    this.ngRedux.dispatch({
                        type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                        payload: custGet.address.home
                    });
                }
                var cust = {
                    first_name: custGet.first_name,
                    last_name: custGet.last_name,
                    fullname: custGet.first_name + ' ' + custGet.last_name,
                    email: custGet.email,
                    accountType: custGet.account_type.text,
                    userBirth: custGet.date_of_birth,
                    userGender: custGet.gender,
                    userPhone: custGet.mobile,
                    nc_id: custGet.id
                };
                this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: cust
                });
                this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                if(!enroll){
                    this.ngRedux.dispatch({
                        type: 'CHANGE_TO_ENROLL_PLAN'
                    });
                }

                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });

            }
        }).catch(error =>{
            console.log('could not get Customer');
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            return Observable.throw(error.json());
        });
    }

    tokenLogin(token, enroll = false): void{
        this.ngRedux.dispatch({
            type: 'ADD_NC_TOKEN',
            payload: token
        });
        this._nc.GetCustomer().subscribe(custGet => {
            if(custGet){
                this.ngRedux.dispatch({
                    type: 'ADD_IG_TOKEN',
                    payload: custGet.token
                });
                if(custGet.address.home){
                    this.ngRedux.dispatch({
                        type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                        payload: custGet.address.home
                    });
                }
                var cust = {
                    first_name: custGet.first_name,
                    last_name: custGet.last_name,
                    fullname: custGet.first_name + ' ' + custGet.last_name,
                    email: custGet.email,
                    accountType: custGet.account_type.text,
                    userBirth: custGet.date_of_birth,
                    userGender: custGet.gender,
                    userPhone: custGet.mobile,
                    nc_id: custGet.id
                };
                this.ngRedux.dispatch({
                    type: 'STORE_USER_INFO_VIEW1',
                    payload: cust
                });
                this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                if(!enroll){
                    this.ngRedux.dispatch({
                        type: 'CHANGE_TO_ENROLL_PLAN'
                    });
                }

                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });

            }
        }, error => {
            console.log('could not get Customer');
            console.log(error);
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    }

    storeFulfillmentOrder(fulfillObj): void {
        this.ngRedux.dispatch({
            type: 'STORE_ORDER_FULFILLMENT',
            payload: fulfillObj
        });
        this.router.navigate(['/order-complete'])
    }

    clearSignupProcess(): void {
        this.ngRedux.dispatch({
            type: 'CLEAR_SIGNUP'
        });
    }

    clearTokens(){
        this.ngRedux.dispatch({
            type: 'CLEAR_ALL_TOKENS'
        });
        this.ngRedux.dispatch({
            type: 'CHANGE_TO_NOT_REGISTERED'
        });
        this.ngRedux.dispatch({
            type: 'LOGOUT'
        });
    }

    logoff(): void {
        let app_auth = this.ngRedux.select(state => state.auth);
        let userView = this.ngRedux.select(state => state.userView);
        let userID;
        userView.subscribe(data => {
            if(data){
                userID = data.get('nc_id');
            }
        });
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        let sub = app_auth.subscribe(data => {
            if (data) {
                let prof = data.get('profile') as any;
                console.log('asdasdsadsaddd')
                console.log(prof);

                let uId = (data.get('profile')) ? prof.get('id') as any : undefined;
                if (prof === undefined || prof === null) {
                    uId = userID;
                }


                console.log(uId);

                this._nc.ssoLogout(uId).subscribe(data => {
                    this.ngRedux.dispatch({
                        type: LoadingActions.HIDE_LOADING
                    });

                    this.ngRedux.dispatch({
                        type: 'CLEAR_ALL_TOKENS'
                    });
                    this.ngRedux.dispatch({
                        type: 'CHANGE_TO_NOT_REGISTERED'
                    });
                    this.ngRedux.dispatch({
                        type: 'LOGOUT'
                    });
                    this.ngRedux.dispatch({
                        type: 'CLEAR_USER_VIEW'
                    });
                    this.router.navigate(['/login']);
                });
                // this.pushBack();
            }
        });
        sub.unsubscribe();
    }
}
