import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { Observable } from "rxjs";
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export declare class AppStatusActions {
    private ngRedux;
    private _nc;
    loadingCtrl: LoadingController;
    private router;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, loadingCtrl: LoadingController, router: Router);
    static STORE_SIGNUP_INFO: string;
    static STORE_SHIPPING_ADDRESS: string;
    static STORE_SELECT_PLAN: string;
    static STORE_SELECT_DEVICES: string;
    storeSignUpInfo(signupObj: any): void;
    storeOtherSignUpInfo(signupObj: any): void;
    storeShippingAddress(shippingObj: any): void;
    storeSelectPlan(planObj: any): void;
    storeSelectHouseholdPlan(planObj: any): void;
    storeSelectDevices(deviceList: any): void;
    tokenLoginWithSubscription(token: any, enroll?: boolean): Observable<any>;
    tokenLogin(token: any, enroll?: boolean): void;
    storeFulfillmentOrder(fulfillObj: any): void;
    clearSignupProcess(): void;
    clearTokens(): void;
    logoff(): void;
}
