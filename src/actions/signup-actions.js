var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../services/nestcareService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
import { AppStatusActions } from './app-status-actions';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export var SignUpActions = (function () {
    function SignUpActions(ngRedux, _nc, _register, loadingCtrl, _appStatusActions, router) {
        var _this = this;
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._register = _register;
        this.loadingCtrl = loadingCtrl;
        this._appStatusActions = _appStatusActions;
        this.router = router;
        var workflowSub = this.ngRedux.select(function (state) { return state.appStatus; });
        workflowSub.subscribe(function (data) {
            if (data) {
                _this.workflow = data.get('workflow');
                _this.payerCustId = data.get('payingCustomerID');
                _this.paidCustId = data.get('paidForCustomerID');
                _this.isAdmin = data.get('admin');
            }
        });
    }
    SignUpActions.prototype.clearErrors = function () {
        this.ngRedux.dispatch({
            type: SignUpActions.CLEAR_ERRORS
        });
    };
    SignUpActions.prototype.fakeRegister = function (params) {
        if (this.workflow === 'yourself') {
            this.router.navigate(['/select-plan']);
        }
        if (this.workflow === 'other') {
            this.router.navigate(['/other-form-signup']);
        }
        if (this.workflow === 'explore') {
            this.router.navigate(['/select-plan']);
        }
        this.ngRedux.dispatch({
            type: 'ADD_CUSTOMER_ID',
            payload: NestCareConstants.defaultCustomerPaywhirlId
        });
        this.ngRedux.dispatch({
            type: 'ADD_IG_TOKEN',
            payload: NestCareConstants.defaultJSONToken
        });
        this.ngRedux.dispatch({
            type: 'ADD_NC_TOKEN',
            payload: NestCareConstants.defaultNCJSONToken
        });
        this._appStatusActions.storeSignUpInfo({
            first_name: params.first_name,
            last_name: params.last_name,
            email: params.email,
            gender: params.gender,
            mobile: params.mobile
        });
    };
    SignUpActions.prototype.registerHousehold = function (params) {
        this.registerOther(params);
    };
    SignUpActions.prototype.registerOther = function (params) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var that = this;
        this._register.registerUser(params, true, that.isAdmin)
            .flatMap(function (res) {
            var registerResp = res;
            console.log('register sadsadasdasasd');
            console.log(registerResp);
            that.ngRedux.dispatch({
                type: SignUpActions.SIGNUP_SUCCESS,
                payload: registerResp
            });
            var paymentPost = {
                paying_customer: that.payerCustId,
                paid_for_customer: that.paidCustId
            };
            console.log('done hereeeee');
            return Observable.create(function (observer) {
                observer.next('done');
            });
            // return that._nc.addPaymentCustomer(paymentPost);
        }).flatMap(function (res2) {
            console.log('Added Payment Customer Success');
            var orderPostData = {
                paying_cust: that.payerCustId,
                shippo_id: null,
                tracking_number: null,
                status: false
            };
            if (_this.workflow === 'household') {
                return Observable.create(function (observer) {
                    observer.next('done');
                });
            }
            else {
                return _this._nc.addCompletedOrderTracker(orderPostData, that.payerCustId);
            }
        }).subscribe(function (resp3) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            console.log('herersadasdasd');
            console.log(params);
            that._appStatusActions.storeOtherSignUpInfo({
                first_name: params.first_name,
                last_name: params.last_name,
                email: params.email,
                gender: params.gender,
                mobile: params.mobile
            });
            if (that.workflow === 'other') {
                that.router.navigate(['/select-plan']);
            }
            if (that.workflow === 'household') {
                that.router.navigate(['/select-plan-household']);
            }
        }, function (error) {
            console.log('in register1: ');
            console.log(error);
            _this.ngRedux.dispatch({
                type: SignUpActions.SIGNUP_FAILURE,
                payload: error
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    SignUpActions.prototype.register = function (params) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        this._register.registerUser(params, false, this.isAdmin)
            .flatMap(function (res) {
            var registerResp = res;
            _this.ngRedux.dispatch({
                type: SignUpActions.SIGNUP_SUCCESS,
                payload: {}
            });
            return Observable.create(function (observer) {
                observer.next('done');
            });
        }).flatMap(function (res2) {
            console.log('Added Payment Customer Success');
            var orderPostData = {
                paying_cust: _this.paidCustId,
                shippo_id: null,
                tracking_number: null,
                status: false
            };
            return _this._nc.addCompletedOrderTracker(orderPostData, _this.payerCustId);
        }).subscribe(function (resp3) {
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            if (_this.workflow === 'yourself') {
                _this.router.navigate(['/select-plan']);
            }
            if (_this.workflow === 'other') {
                _this.router.navigate(['/other-form-signup']);
            }
            if (_this.workflow === 'household') {
                _this.router.navigate(['/household-form-signup']);
            }
            if (_this.workflow === 'explore') {
                _this.router.navigate(['/order-confirm']);
            }
            _this._appStatusActions.storeSignUpInfo({
                first_name: params.first_name,
                last_name: params.last_name,
                email: params.email,
                gender: params.gender,
                mobile: params.mobile
            });
        }, function (error) {
            console.log('in register1: ');
            console.log(error);
            _this.ngRedux.dispatch({
                type: SignUpActions.SIGNUP_FAILURE,
                payload: error
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    SignUpActions.CLEAR_ERRORS = 'CLEAR_ERRORS';
    SignUpActions.SIGNING_UP = 'SIGNING_UP';
    SignUpActions.SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
    SignUpActions.SIGNUP_FAILURE = 'SIGNUP_FAILURE';
    SignUpActions = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, RegisterService, LoadingController, AppStatusActions, Router])
    ], SignUpActions);
    return SignUpActions;
}());
