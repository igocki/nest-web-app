var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { NestcareService } from '../services/nestcareService';
import { ShippoService } from '../services/shippoService';
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
import { AppStatusActions } from './app-status-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export var ShippingActions = (function () {
    function ShippingActions(ngRedux, _nc, _shippo, loadingCtrl, _appStatusActions) {
        this.ngRedux = ngRedux;
        this._nc = _nc;
        this._shippo = _shippo;
        this.loadingCtrl = loadingCtrl;
        this._appStatusActions = _appStatusActions;
    }
    ShippingActions.prototype.clearErrors = function () {
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_ERRORS
        });
    };
    ShippingActions.prototype.getShippingRates = function (shippingTo, user) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        console.log('HIT SHIPPING RATES');
        console.log(shippingTo);
        console.log(user);
        this._shippo.createShipment(shippingTo, user)
            .subscribe(function (resp) {
            console.log("RESPONDED");
            console.log(resp);
            _this.ngRedux.dispatch({
                type: ShippingActions.GET_SHIPPING_RATES_SUCCESS,
                payload: resp.rates_list
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, function (error) {
            _this.ngRedux.dispatch({
                type: ShippingActions.GET_SHIPPING_RATES_FAILURE,
                payload: { err: error }
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        });
    };
    ShippingActions.prototype.changeShipping = function () {
        this.ngRedux.dispatch({
            type: ShippingActions.CLEAR_SHIPPING
        });
    };
    ShippingActions.prototype.fakeUpdateShippingAddressSuccess = function (params) {
        var _this = this;
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var shippingData = {
            "name": params.name,
            "addressLine1": params.addressLine1,
            "addressLine2": params.addressLine2,
            "city": params.city,
            "state": params.state,
            "zipCode": params.zipCode
        };
        setTimeout(function () {
            _this.ngRedux.dispatch({
                type: ShippingActions.SHIPPING_UPDATE_SUCCESS,
                payload: shippingData
            });
            _this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
        }, 3000);
    };
    ShippingActions.CLEAR_ERRORS = 'CLEAR_ERRORS';
    ShippingActions.CLEAR_SHIPPING = 'CLEAR_SHIPPING';
    ShippingActions.GET_SHIPPING_RATES_SUCCESS = 'GET_SHIPPING_RATES_SUCCESS';
    ShippingActions.GET_SHIPPING_RATES_FAILURE = 'GET_SHIPPING_RATES_FAILURE';
    ShippingActions.SHIPPING_UPDATE_SUCCESS = 'SHIPPING_UPDATE_SUCCESS';
    ShippingActions.SHIPPING_UPDATE_FAILURE = 'SHIPPING_UPDATE_FAILURE';
    ShippingActions = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [NgRedux, NestcareService, ShippoService, LoadingController, AppStatusActions])
    ], ShippingActions);
    return ShippingActions;
}());
