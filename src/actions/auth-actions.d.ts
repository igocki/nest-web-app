import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { UserService } from '../services/userService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export declare class AuthActions {
    private ngRedux;
    private _nc;
    loadingCtrl: LoadingController;
    private _userService;
    private _registerService;
    private router;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, loadingCtrl: LoadingController, _userService: UserService, _registerService: RegisterService, router: Router);
    static ROUTER_STATE_CHANGE: string;
    static CLEAR_ERRORS: string;
    static AUTHENTICATED: string;
    static LOGGING_IN: string;
    static LOGIN_SUCCESS: string;
    static LOGIN_FAILURE: string;
    clearErrors(): void;
    authenticationComplete(): void;
    adminLogin(email: any, password: any): void;
    login(email: any, password: any, sso: any): void;
}
