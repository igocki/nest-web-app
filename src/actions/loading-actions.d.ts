import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export declare class LoadingActions {
    private ngRedux;
    constructor(ngRedux: NgRedux<RootState>);
    static SHOW_LOADING: string;
    static HIDE_LOADING: string;
    showLoader(): void;
    hideLoader(): void;
}
