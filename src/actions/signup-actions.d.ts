import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { AppStatusActions } from './app-status-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export declare class SignUpActions {
    private ngRedux;
    private _nc;
    private _register;
    loadingCtrl: LoadingController;
    private _appStatusActions;
    private router;
    private workflow;
    private payerCustId;
    isAdmin: any;
    private paidCustId;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _register: RegisterService, loadingCtrl: LoadingController, _appStatusActions: AppStatusActions, router: Router);
    static CLEAR_ERRORS: string;
    static SIGNING_UP: string;
    static SIGNUP_SUCCESS: string;
    static SIGNUP_FAILURE: string;
    clearErrors(): void;
    fakeRegister(params: any): void;
    registerHousehold(params: any): void;
    registerOther(params: any): void;
    register(params: any): void;
}
