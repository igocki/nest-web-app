import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { ShippoService } from '../services/shippoService';
import { LoadingController } from 'ionic-angular';
import { AppStatusActions } from './app-status-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
export declare class ShippingActions {
    private ngRedux;
    private _nc;
    private _shippo;
    loadingCtrl: LoadingController;
    private _appStatusActions;
    constructor(ngRedux: NgRedux<RootState>, _nc: NestcareService, _shippo: ShippoService, loadingCtrl: LoadingController, _appStatusActions: AppStatusActions);
    static CLEAR_ERRORS: string;
    static CLEAR_SHIPPING: string;
    static GET_SHIPPING_RATES_SUCCESS: string;
    static GET_SHIPPING_RATES_FAILURE: string;
    static SHIPPING_UPDATE_SUCCESS: string;
    static SHIPPING_UPDATE_FAILURE: string;
    clearErrors(): void;
    getShippingRates(shippingTo: any, user: any): void;
    changeShipping(): void;
    fakeUpdateShippingAddressSuccess(params: any): void;
}
