import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { UserService } from '../services/userService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
@Injectable()
export class AuthActions {
    constructor (
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        public loadingCtrl: LoadingController,
        private _userService: UserService,
        private _registerService: RegisterService,
        private router: Router) {}

    static ROUTER_STATE_CHANGE: string = 'ROUTER_STATE_CHANGE';
    static CLEAR_ERRORS: string = 'CLEAR_ERRORS';
    static AUTHENTICATED: string = 'AUTHENTICATED';
    static LOGGING_IN: string = 'LOGGING_IN';
    static LOGIN_SUCCESS: string = 'LOGIN_SUCCESS';
    static LOGIN_FAILURE: string = 'LOGIN_FAILURE';

    clearErrors(){
        this.ngRedux.dispatch({
            type: AuthActions.CLEAR_ERRORS
        });
    }

    authenticationComplete(){
        this.ngRedux.dispatch({
            type: AuthActions.AUTHENTICATED
        });
    }

    adminLogin(email, password): void {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this._nc.adminLogin(email, password).subscribe(resp => {
                this.router.navigate(['/admin']);
                this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
            },
            error => {
                console.log(error);
                console.log('hit this error');
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                this.ngRedux.dispatch({
                    type: AuthActions.LOGIN_FAILURE,
                    payload: error
                });

            });
    }

    login(email, password, sso): void {
        this.ngRedux.dispatch({
            type: AuthActions.LOGGING_IN
        });
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
       // this.mockLoginService.login(customer, subscriptionID).then(resp => {
         // this._pw.GetCustomers().then(resp => {

        this._registerService.registerAndLogin(email, password)
            .subscribe(resp => {

            if(resp.data.first_name){
                this.ngRedux.dispatch({
                    type: AuthActions.LOGIN_SUCCESS,
                    payload: resp
                });
                this.ngRedux.dispatch({
                    type: 'STORE_SHIPPING_ADDRESS_USER_VIEW',
                    payload: resp.data.address.home
                });
                this.ngRedux.dispatch({
                    type: 'CHANGE_TO_LOGGED_IN'
                });
                this.ngRedux.dispatch({
                    type: 'CHANGE_WORKFLOW',
                    payload: null
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                this._userService.login();
                if (sso === true) {
                    this.router.navigate(['/sso-success'], {preserveQueryParams: true});
                } else {
                    console.log('end here 3:');
                    this.router.navigate(['/account']);
                }
            } else {
                this.ngRedux.dispatch({
                    type: 'ADD_CUSTOMER_ID',
                    payload: resp.data.id
                });
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                this.router.navigate(['/twofactor']);
            }


        },
        error => {
            console.log(error);
            console.log('hit this error');
            this.ngRedux.dispatch({
                type: LoadingActions.HIDE_LOADING
            });
            this.ngRedux.dispatch({
                type: AuthActions.LOGIN_FAILURE,
                payload: error
            });

        });

        // this._nc.login(customer, subscriptionID).then(resp => {
        //     this.ngRedux.dispatch({
        //         type: AuthActions.LOGIN_SUCCESS,
        //         payload: resp
        //     });
        //     this.ngRedux.dispatch({
        //         type: LoadingActions.HIDE_LOADING
        //     });
        // }).catch(err => {
        //     console.log('inside this catch....');
        //     console.log(err);
        //     this.ngRedux.dispatch({
        //         type: AuthActions.LOGIN_FAILURE,
        //         payload: {err: err}
        //     });
        //     this.ngRedux.dispatch({
        //         type: LoadingActions.HIDE_LOADING
        //     });
        // });
    }
}
