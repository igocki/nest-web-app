import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import { RootState } from '../store';
import { NestcareService } from '../services/nestcareService';
import { RegisterService } from '../services/registerService';
import { Router } from '@angular/router';
import {Observable} from "rxjs";
import { LoadingController } from 'ionic-angular';
import { LoadingActions } from './loading-actions';
import { AppStatusActions } from './app-status-actions';
import { Response } from '@angular/http';
import { NestCareConstants } from '../app/shared/constants/nestcare.constants';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
@Injectable()
export class SignUpActions {

    private workflow: any;
    private payerCustId: any;
    public isAdmin;
    private paidCustId: any;
    constructor (
        private ngRedux: NgRedux<RootState>,
        private _nc: NestcareService,
        private _register: RegisterService,
        public loadingCtrl: LoadingController,
        private _appStatusActions: AppStatusActions,
        private router: Router) {

        let workflowSub = this.ngRedux.select(state => state.appStatus);
        workflowSub.subscribe(data => {
            if(data){
                this.workflow = data.get('workflow');
                this.payerCustId = data.get('payingCustomerID');
                this.paidCustId = data.get('paidForCustomerID');
                this.isAdmin = data.get('admin');
            }
        });
    }

    static CLEAR_ERRORS: string = 'CLEAR_ERRORS';
    static SIGNING_UP: string = 'SIGNING_UP';
    static SIGNUP_SUCCESS: string = 'SIGNUP_SUCCESS';
    static SIGNUP_FAILURE: string = 'SIGNUP_FAILURE';


    clearErrors(){
        this.ngRedux.dispatch({
            type: SignUpActions.CLEAR_ERRORS
        });
    }

    fakeRegister(params): void {
        if(this.workflow === 'yourself'){
            this.router.navigate(['/select-plan']);
        }

        if(this.workflow === 'other'){
            this.router.navigate(['/other-form-signup']);
        }

        if(this.workflow === 'explore'){
            this.router.navigate(['/select-plan']);
        }

        this.ngRedux.dispatch({
            type: 'ADD_CUSTOMER_ID',
            payload: NestCareConstants.defaultCustomerPaywhirlId
        });

        this.ngRedux.dispatch({
            type: 'ADD_IG_TOKEN',
            payload: NestCareConstants.defaultJSONToken
        });

        this.ngRedux.dispatch({
            type: 'ADD_NC_TOKEN',
            payload: NestCareConstants.defaultNCJSONToken
        });
        this._appStatusActions.storeSignUpInfo(
            {
                first_name: params.first_name,
                last_name: params.last_name,
                email: params.email,
                gender: params.gender,
                mobile: params.mobile
            });
    }

    registerHousehold(params): void {
        this.registerOther(params);
    }

    registerOther(params): void {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
        var that = this;
        this._register.registerUser(params, true, that.isAdmin)
            .flatMap((res:Response) => {
                var registerResp = res;
                console.log('register sadsadasdasasd');
                console.log(registerResp);
                that.ngRedux.dispatch({
                    type: SignUpActions.SIGNUP_SUCCESS,
                    payload: registerResp
                });
                let paymentPost ={
                    paying_customer: that.payerCustId,
                    paid_for_customer: that.paidCustId
                };
                console.log('done hereeeee');

                return Observable.create(observer => {
                    observer.next('done');
                })
                // return that._nc.addPaymentCustomer(paymentPost);
            }).flatMap((res2:Response) =>{
                console.log('Added Payment Customer Success');
                let orderPostData = {
                    paying_cust: that.payerCustId,
                    shippo_id: null,
                    tracking_number: null,
                    status: false
                };
                if(this.workflow === 'household'){
                    return Observable.create(observer => {
                        observer.next('done');
                    });
                } else {
                    return this._nc.addCompletedOrderTracker(orderPostData, that.payerCustId);
                }
            }).subscribe(resp3 => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
                console.log('herersadasdasd');
                console.log(params);
                that._appStatusActions.storeOtherSignUpInfo(
                    {
                        first_name: params.first_name,
                        last_name: params.last_name,
                        email: params.email,
                        gender: params.gender,
                        mobile: params.mobile
                    });
                if(that.workflow === 'other'){
                    that.router.navigate(['/select-plan']);
                }
                if(that.workflow === 'household'){
                    that.router.navigate(['/select-plan-household']);
                }
            },
            error => {
                console.log('in register1: ');
                console.log(error);
                this.ngRedux.dispatch({
                    type: SignUpActions.SIGNUP_FAILURE,
                    payload: error
                });

                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
    }

    register(params): void {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });

        this._register.registerUser(params, false, this.isAdmin)
            .flatMap((res:any) => {
                var registerResp = res;
                this.ngRedux.dispatch({
                    type: SignUpActions.SIGNUP_SUCCESS,
                    payload: {}
                });
                return Observable.create(observer => {
                    observer.next('done');
                });
            }).flatMap((res2) =>{
                console.log('Added Payment Customer Success');
                let orderPostData = {
                    paying_cust: this.paidCustId,
                    shippo_id: null,
                    tracking_number: null,
                    status: false
                };
                return this._nc.addCompletedOrderTracker(orderPostData, this.payerCustId);
            }).subscribe(resp3 => {
                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });

                if(this.workflow === 'yourself'){
                    this.router.navigate(['/select-plan']);
                }

                if(this.workflow === 'other'){
                    this.router.navigate(['/other-form-signup']);
                }

                if(this.workflow === 'household'){
                    this.router.navigate(['/household-form-signup']);
                }

                if(this.workflow === 'explore'){
                    this.router.navigate(['/order-confirm']);
                }

                this._appStatusActions.storeSignUpInfo(
                    {
                        first_name: params.first_name,
                        last_name: params.last_name,
                        email: params.email,
                        gender: params.gender,
                        mobile: params.mobile
                    });


            },
            error => {
                console.log('in register1: ');
                console.log(error);
                this.ngRedux.dispatch({
                    type: SignUpActions.SIGNUP_FAILURE,
                    payload: error
                });

                this.ngRedux.dispatch({
                    type: LoadingActions.HIDE_LOADING
                });
            });
    }
}
