import { Injectable } from '@angular/core';
import { NgRedux } from 'ng2-redux';
import * as Redux from 'redux';
import { RootState } from '../store';
import { LoadingController } from 'ionic-angular';
/**
 * Action creators in Angular 2. We may as well adopt a more
 * class-based approach to satisfy Angular 2's OOP idiom. It
 * has the advantage of letting us use the dependency injector
 * as a replacement for redux-thunk.
 */
@Injectable()
export class LoadingActions {
    constructor (
        private ngRedux: NgRedux<RootState>) {}

    static SHOW_LOADING: string = 'SHOW_LOADING';
    static HIDE_LOADING: string = 'HIDE_LOADING';

    // increment(): void {
    //     this.ngRedux.dispatch({ type: CounterActions.INCREMENT_COUNTER });
    // }
    //
    // decrement(): void {
    //     this.ngRedux.dispatch({ type: CounterActions.DECREMENT_COUNTER });
    // }

    // incrementIfOdd(): void {
    //     const { counter } = this.ngRedux.getState();
    //     if (counter % 2 !== 0) {
    //         this.increment();
    //     }
    // }
    //
    // incrementAsync(delay: number = 1000): void {
    //     setTimeout(this.increment.bind(this), delay);
    // }

    showLoader() : void {
        this.ngRedux.dispatch({
            type: LoadingActions.SHOW_LOADING
        });
    }
    
    hideLoader() : void {
        this.ngRedux.dispatch({
            type: LoadingActions.HIDE_LOADING
        });
    }
}
