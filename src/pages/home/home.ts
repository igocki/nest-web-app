import {Component} from '@angular/core';
import { Router } from '@angular/router';
import {NavController} from 'ionic-angular';
import { AboutPage } from '../about/about';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(private navCtrl:NavController, private router: Router) {

    }

    sayHello() {
        console.log('Hello');
        this.router.navigate(['/second']);
    }
}
