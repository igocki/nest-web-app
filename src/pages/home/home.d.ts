import { Router } from '@angular/router';
import { NavController } from 'ionic-angular';
export declare class HomePage {
    private navCtrl;
    private router;
    constructor(navCtrl: NavController, router: Router);
    sayHello(): void;
}
