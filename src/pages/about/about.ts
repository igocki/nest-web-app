import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, private router: Router) {

  }

    ngOnInit(){
        //called after the constructor and called  after the first ngOnChanges()
        this.router.navigate(['/about']);
    }

}
