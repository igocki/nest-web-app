import { NavController } from 'ionic-angular';
import { Router } from '@angular/router';
export declare class AboutPage {
    navCtrl: NavController;
    private router;
    constructor(navCtrl: NavController, router: Router);
    ngOnInit(): void;
}
